
let cart = [];
let cartTotal = 0;


const cartDom = document.querySelector(".cart");
const addtocartbtnDom = document.querySelectorAll('[data-action="add-to-carts"]');



addtocartbtnDom.forEach(addtocartbtnDom => {
    addtocartbtnDom.addEventListener("click", () => {


      var sell_type = $("#sell_type").val();
      var sell_bt = document.getElementById('sub_pay').innerHTML;

            sell_bt = parseFloat(sell_bt);
           

        const productDom = addtocartbtnDom.parentNode.parentNode;
        const product = {
            name: productDom.querySelector(".product-name").innerText,
            price: productDom.querySelector(".product-price").innerText,
            prices: productDom.querySelector(".products-prices").innerText,
            product_code: productDom.querySelector(".product-code").innerText,
            quantity: 1
        };

        if(sell_type == "Retail"){
          product.price = product.price;
        }
        if(sell_type == "Whole"){
          product.price = product.prices;
        }

         var sub_bt = parseFloat(product.price);

          if(sell_bt){
            document.querySelector('.payt').innerText = sell_bt + sub_bt;
          }
          if(!sell_bt){
            document.querySelector('.payt').innerText = sub_bt;
          }


          var divstp = document.getElementById("stop_sell");
          divstp.style.display = "block";



        const IsinCart = cart.filter(cartItem => cartItem.name === product.name).length > 0;
        if (IsinCart === false) {


            cartDom.insertAdjacentHTML("beforeend", `
            <div class="inner_row flex-row shadow-sm card cart-items mt-2 mb-3 animated flipInX" style="padding: 10px">
            <div class="p-2 mt-3">
                <p class="text-info cart_item_name" name="product_name[]" style="margin-bottom: 0px; width: 94px; height: 20px; overflow: hidden">${product.name}</p>
                <input type="hidden" name="product_code[]" value="${product.product_code}"/>
                <input type="hidden" name="product_name[]" value="${product.name}"/>
            </div>
            <div class="p-2 mt-3 ml-auto">
                <button type="button" data-action="increase-item" name="pro">&plus;
            </div>
            <div class="p-2 mt-3" style="padding: 0px 5px">
              <input readonly class="text-success cart_item_quantity text-center product_quantity input" name="product_quantity[]" id="product_key" style="width: 30px; border-radius: 0" value="${product.quantity}">
            </div>

            <div class="p-2 mt-3" style="margin-right: 15px">
              <button type="button" data-action="decrease-item">&minus;
            </div>
            <div class="p-2 mt-3" style="margin: 0px">
                <input id="price" readonly class="text-success cart_item_price sub_price input" onchange="on_sub();" name="sub_price[]" style="width: 50px; margin: 0px; padding-left:5px; border-radius: 0" value="${product.price}">
                <input type="hidden" class="text-success cart_item_price" name="product_price[]" value="${product.price}"/>
            </div>
            <div class="p-2 mt-3" style="margin-left: 20px">
              <button onclick="on_discount()" type="button" data-action="remove-item">&times;
            </div>
          </div> `);



            addtocartbtnDom.innerText = "In cart";
            addtocartbtnDom.disabled = true;
            cart.push(product);

            const cartItemsDom = cartDom.querySelectorAll(".cart-items");
            cartItemsDom.forEach(cartItemDom => {

                if (cartItemDom.querySelector(".cart_item_name").innerText === product
                    .name) {
                  if(sell_bt > 0){
                    cartTotal = sell_bt;
                  }
                    cartTotal += parseFloat(cartItemDom.querySelector(".cart_item_quantity")
                            .value) *
                        parseFloat(cartItemDom.querySelector(".cart_item_price").value);
                    document.querySelector('.pay').innerText = cartTotal;
                    // "$ " +
                    document.getElementById('vanco').value = cartTotal;

                    // keyup item in cart

                    var upInputs = document.querySelector('#product_key');

                    upInputs.addEventListener('keyup', function() {
                      cart.forEach(cartItem => {
                          if (cartItem.name === product.name) {
                              console.log(cartItem.quantity);
                              console.log(cartItem.price);

                                  cartItem.quantity = document.getElementById('product_key').value;
                              cartItemDom.querySelector(
                                      ".cart_item_price").value =
                                  parseInt(cartItem.quantity) *
                                  parseInt(cartItem.price);
                              cartTotal = parseInt(cartItem.quantity) *  parseInt(cartItem.price);
                              console.log(cartTotal);
                              document.querySelector('.pay').innerText =
                                  cartTotal;
                              // "$ " +
                          }
                      });
                    });

                        // increase item in cart
                        cartItemDom.querySelector('[data-action="increase-item"]')
                            .addEventListener("click", () => {
                                cart.forEach(cartItem => {
                                    if (cartItem.name === product.name) {
                                   
                                        cartItemDom.querySelector(
                                                ".cart_item_quantity")
                                            .value = ++cartItem.quantity;
                                        cartItemDom.querySelector(
                                                ".cart_item_price").value =
                                            parseInt(cartItem.quantity) *
                                            parseFloat(cartItem.price);
                                        cartTotal += parseFloat(cartItem.price);                                       

                                            var real_total = $("#vanco").val();
                                            var product_pric = parseFloat(cartItem.price);
                                            var T = parseFloat(real_total) + product_pric;
                                            document.getElementById('vanco').value = T;

                                            document.querySelector('.pay').innerText =
                                            T;

                                            on_discount();

                                       
                                    }
                                });
                            });

                    // decrease item in cart
                    cartItemDom.querySelector('[data-action="decrease-item"]')
                        .addEventListener("click", () => {
                            cart.forEach(cartItem => {
                                if (cartItem.name === product.name) {
                                    if (cartItem.quantity > 1) {
                                        console.log(cartItem.quantity);
                                        cartItemDom.querySelector(
                                                ".cart_item_quantity")
                                            .value = --cartItem.quantity;
                                        cartItemDom.querySelector(
                                                ".cart_item_price").value =
                                            parseInt(cartItem.quantity) *
                                            parseFloat(cartItem.price);
                                        cartTotal -= parseFloat(cartItem.price)
                               

                                            var real_total = $("#vanco").val();
                                            var product_pric = parseFloat(cartItem.price);
                                            var T = parseFloat(real_total) - product_pric;
                                            document.getElementById('vanco').value = T;

                                            document.querySelector('.pay')
                                            .innerText = T;

                                            on_discount();
                                        // "$ " +
                                    }
                                }
                            });
                        });

                    //remove item from cart
                    cartItemDom.querySelector('[data-action="remove-item"]')
                        .addEventListener("click", () => {
                            cart.forEach(cartItem => {
                                if (cartItem.name === product.name) {
                                    cartTotal -= parseFloat(cartItemDom
                                        .querySelector(".cart_item_price")
                                        .value);
                                        cartItemDom.remove();
                                        document.querySelector('.payt').innerText =
                                            document.getElementById('sub_pay').innerHTML - parseFloat(cartItem.price) * parseInt(cartItem.quantity);
                                            document.querySelector('.pay').innerText =
                                                document.getElementById('sub_pay').innerHTML - parseFloat(cartItem.price) * parseInt(cartItem.quantity);
                                        sell_bt = document.querySelector('.pay').value - parseFloat(cartItem.price) * parseInt(cartItem.quantity);
                                    // "$ " +

                                            var real_total = $("#vanco").val();
                                            var product_pric = parseFloat(cartItem.price);
                                            var T = parseFloat(real_total) - product_pric * parseInt(cartItem.quantity);
                                            document.getElementById('vanco').value = T;

                                            document.querySelector('.sub_pay').innerText =  T;
                                            document.getElementById('total_pay').value = T;

                                    cart = cart.filter(cartItem => cartItem
                                        .name !== product.name);
                                    addtocartbtnDom.innerText = "Add to cart";
                                    addtocartbtnDom.disabled = false;
                                    if (T < 1) {
                                      
                                      cartTotal = T;
                                      document.querySelector('.payt').innerText =T;
                                      document.querySelector('.pay').innerText = T;
                                      sell_bt = T;
                                      document.querySelector('.sub_pay').innerText =  T;
                                      document.getElementById('sub_total').value = T;
                                      document.getElementById('total').value = T;
                                      var divstp = document.getElementById("stop_sell");
                                      divstp.style.display = "none";
                                    }
                                    on_discount();
                                }
                            });
                        });



                    //clear cart
                    document.querySelector('[data-action="clear-cart"]').addEventListener(
                        "click", () => {
                            cartItemDom.remove();
                            cart = [];
                            cartTotal = 0;

                            addtocartbtnDom.innerText = "Add to cart";
                            addtocartbtnDom.disabled = false;

                            document.querySelector('.payt').innerText = 0;
                            document.querySelector('.pay').innerText = 0;
                            sell_bt = 0;
                            var divstpc = document.getElementById("stop_sell");
                            divstpc.style.display = "none";
                            on_discount();
                        });
                }
            });
        }
    });
});

function animateImg(img) {
    img.classList.add("animated", "shake");
}

function normalImg(img) {
    img.classList.remove("animated", "shake");
}
