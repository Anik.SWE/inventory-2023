<?php

namespace App\Traits;

use App\Models\SMSConfig;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

trait CommonTraits
{
    public function sendSMS($mobile_number, $message, $users_type = '')
    {

        $sms_config =  SMSConfig::where('business_id', session()->get('business_id'))->first();

        if (isset($sms_config->api_key)) {

            $response = Http::get("https://smpp.ajuratech.com:7790/sendtext?apikey=" . $sms_config->api_key . "&secretkey=" . $sms_config->secretkey . "&callerID=$sms_config->caller_id&toUser=88" . $mobile_number . "&messageContent=" . $message);
            Log::channel('smslog')->info("https://smpp.ajuratech.com:7790/sendtext?apikey=" . $sms_config->api_key . "&secretkey=" . $sms_config->secretkey . "&callerID=$sms_config->caller_id&toUser=88" . $mobile_number . "&messageContent=" . $message);


            return  1;
        } else {
            Log::channel('smslog')->info('Failed ' . Auth::user()->institute_id);
            return  0;
        }
    }

}
