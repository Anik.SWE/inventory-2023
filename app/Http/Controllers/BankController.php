<?php

namespace App\Http\Controllers;

use App\Helpers\Order;
use App\Http\Requests\AddBankForm;
use App\Http\Requests\EditBankFormPost;
use App\Models\Bank;
use App\Models\Customar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\TransactionDetail;
use App\Models\Transaction;
use Session;
class BankController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Bank Form:
    public function AddBank()
    {
        return view('Business.Bank.add_bank_form',[
            'Total_Bank' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get()
        ]);
    }

    // Insert Brands:
    public function BankFormPost(AddBankForm $request)
    {
        Bank::insert([
            'bank_name' => $request->bank_name,
            'account_holder' => $request->account_holder,
            'account_number' => $request->account_number,
            'account_group' => $request->account_group,
            'account_type' => $request->account_type,
            'opening_amount' => $request->opening_amount,
            'business' => Session::get('business_id'),
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        $bank =  Bank::all();
        $bank_id=0;
        foreach ($bank->reverse() as $bank_value){ 
            if($bank_value->business == Session::get('business_id')){
                $bank_id = $bank_value->id;
                break;
            }                   
        } 
        if($bank_id){
            $post_data['tran_id'] = uniqid();
        }
        Transaction::insert([
            'bank_name' => $request->bank_name,
            'account_number' => $request->account_number,
            'operation_type' => "Deposit",
            'media' => "Account Opening",
            'transaction_id' => $post_data['tran_id'],
            'transaction_date' => Carbon::now(),
            'balance' => $request->opening_amount,
            'bank' => $bank_id,
            'business' => Session::get('business_id'),
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        // TransactionDetail::insert([
        //     'bank_name' => $request->bank_name,
        //     'balance' => $request->opening_amount,
        //     'user_id' => Auth::id(),
        // ]);
        return back();
    }

    // Edit Bank:
    public function EditBank($bank_id)
    {
        return view('Business.Bank.edit_bank',[
            'Total_Bank' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Bank_info' => Bank::find($bank_id)
        ]);
    }

    // Update Bank Information
    public function EditBankFormPost(EditBankFormPost $request)
    {
        Bank::find($request->id)->update([
            'bank_name' => $request->bank_name,
            'account_holder' => $request->account_holder,
            'account_number' => $request->account_number,
            'account_group' => $request->account_group,
            'account_type' => $request->account_type,
            'opening_amount' => $request->opening_amount
        ]);
        return back();
    }

    // Delete Bank Information
    public function DeleteBank($bank_id)
    {
        Bank::find($bank_id)->forceDelete();
        return back()->with('delete_status', 'Your Category Permanently Deleted Successfully!');
    }


    public function index(Request $request){
        $account_number = Bank::orderBy('id')->where('id', $request->id)->get();

        $stringToSend="";
        foreach($account_number as $number){
            $stringToSend .= $number->account_number;

        }
        return $stringToSend;
     }
}
