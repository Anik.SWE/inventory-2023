<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAdjustment;
use App\Models\ProductPurchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddProductForm;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\Supplier;
use App\Models\Customar;
use App\Models\Order;
use App\Models\Bank;
use App\Models\Transaction;
use Picqer;
use Illuminate\Support\Facades\DB;
use App\Models\CustomarLedger;
use Session;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }





    // Add Product Form:
    public function AddProduct()
    {
        return view('Business.Product.add_product_form', [
            'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Supplier_info' => Supplier::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get()
        ]);
    }

    public function AdjustProduct($ids)
    {
        return view('Business.Product.adjust_product', [
            'Total_Adjust' => ProductAdjustment::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'product' => Product::find($ids)
        ]);
    }

    public function AdjustProductList()
    {
        return view('Business.Product.adjust_product', [
            'Total_Adjust' => ProductAdjustment::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'product' => Product::find(0)
        ]);
    }

    public function AdjustProductFormPost(Request $request)
    {

        $product = Product::find($request->pro_id);

        if($product){
            if($request->operation_type == "Increase"){
                $product->product_quantity = $product->product_quantity + $request->quantity;
                $product->save();
                ProductAdjustment::insert([
                    'reason' => $request->reason,
                    'type' => $request->operation_type,
                    'quantity' => $request->quantity,
                    'product' => $request->pro_id,
                    'business' => $request->business_ids,
                    'user_id' => Auth::id(),
                    'created_at' => Carbon::now()
                ]);
            }
            if($request->operation_type == "Decrease"){
                if($product->product_quantity >= $request->quantity){
                    $product->product_quantity = $product->product_quantity - $request->quantity;
                    $product->save();
                    ProductAdjustment::insert([
                        'reason' => $request->reason,
                        'type' => $request->operation_type,
                        'quantity' => $request->quantity,
                        'product' => $request->pro_id,
                        'business' => $request->business_ids,
                        'user_id' => Auth::id(),
                        'created_at' => Carbon::now()
                    ]);
                }
                else{
                    return redirect()->back()->withErrors([' Decrease Quantity is more than stock ! ']);
                }
            }
        }

        return back();
    }

    public function AdjustProductDelete($adjust_id)
    {
        $adj = ProductAdjustment::find($adjust_id);
        $product = Product::find($adj->product);

        if($adj->type == "Increase"){
            if($product->product_quantity >= $adj->quantity){
                $product->product_quantity = $product->product_quantity - $adj->quantity;
                $product->save();
            }
            else{
                return redirect()->back()->withErrors([' Delete Quantity is more than stock ! ']);
            }

        }
        if($adj->type == "Decrease"){
            $product->product_quantity = $product->product_quantity + $adj->quantity;
            $product->save();
        }


        ProductAdjustment::find($adjust_id)->forceDelete();
        return back()->with('delete_status', 'Your Product Permanently Deleted Successfully!');
    }

    // All Product List:
    public function AllProduct()
    {
        return view('Business.Product.all_product', [
            'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->latest()->get()
        ]);
    }
        // All Purchase List:
public function AllPurchase()
    {
            return view('Business.Product.purchase_product_form', [
                'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
                'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
                'bank_info' => Bank::orderBy('bank_name')->where('business', Session::get('business_id'))->get(),
                'Supplier_info' => Supplier::all(),
                'Total_ProductPurchase' => ProductPurchase::orderBy('id')->where('business', Session::get('business_id'))->latest()->get(),
                'Total_Product' => Product::all()
            ]);
    }
    // Search Product List:
    public function SearchPro(Request $request)
    {
        $select_product = Product::where('category_id', $request->cat_id)->where('user_id', Auth::id())->latest()->get();
        return view('Business.Product.select_product', [
            'Category_info' => Category::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Total_Product' => $select_product
        ]);
    }
    // Insert Product:
    public function ProductFormPost(Request $request)
    {

        $Product_code = Helper::IDGenerator(new Product, 'product_code', 5, 'P');
        /** Generate Product Code */
        // $select_product = Product::find($request->select_product_name)->product_buying_price;
        // dd($select_product);

            Product::insert([
                'product_name' => $request->product_name,
                'product_description' => $request->product_description,
                'category_id' => $request->category_id,
                'brand_id' => $request->brand_id,
                'product_code' => $Product_code,
                'whole_selling_price' => $request->whole_selling_price,
                'retail_selling_price' => $request->retail_selling_price,
                'business' => $request->business_ids,
                'user_id' => Auth::id(),
                'created_at' => Carbon::now()
            ]);



        return back();
    }

        // Insert Purchase:
        public function PurchaseFormPost(Request $request)
        {

            $pre_due = 0;
            $cur_due = 0;
            $pre_advnc = 0;
            $cur_advnc = 0;

            $sup_predue = Supplier::find($request->supplier_id);
            $pre_due = $sup_predue->due;
            $pre_advnc = $sup_predue->advance;

            if($request->change > 0){
                if($pre_due > 0){
                    $cur_due = $pre_due + $request->change;
                }
                if(!$pre_due && !$pre_advnc){
                    $cur_due = $request->change;
                }
                if($pre_advnc > 0){
                    if($pre_advnc < $request->change){
                        $lower_advance = $request->change - $pre_advnc;
                        $cur_advnc = 0;
                        $cur_due = $lower_advance;
                    }
                    if($pre_advnc >= $request->change){
                        $cur_advnc = $pre_advnc - $request->change;
                    }
                }
            }

            if($request->change < 0){
                $get_advance = $request->payment - $request->total_amount;

                if($pre_advnc > 0){
                   $cur_advnc = $pre_advnc + $get_advance;
                }
                if(!$pre_due && !$pre_advnc){
                    $cur_advnc = $get_advance;
                }

                if($pre_due > 0){
                    if($pre_due < $get_advance){
                        $lower_due = $get_advance - $pre_due;
                        $cur_due = 0;
                        $cur_advnc = $lower_due;
                    }
                    if($pre_due >= $get_advance){
                        $cur_due = $pre_due - $get_advance;
                    }
                }
            }


            /** Generate Product Code */
            // $select_product = Product::find($request->select_product_name)->product_buying_price;
            // dd($select_product);
            $Banks = Bank::find($request->bankB_id);
            if($Banks){
                if($request->payment <= $Banks->opening_amount){

                    ProductPurchase::insert([
                            'product' => $request->select_product_name,
                            'supplier_id' => $request->supplier_id,
                            'quantity' => $request->product_quantity,
                            'price' => $request->product_buying_price,
                            'discount_amount' => $request->discount,
                            'total_amount' => $request->total_amount,

                            'shipping_cost' => $request->shipp_cost,
                            'previous_due' => $pre_due,
                            'current_due' => $cur_due,
                            'previous_advance' => $pre_advnc,
                            'current_advance' => $cur_advnc,

                            'product_buying_date' => $request->product_buying_date,
                            'product_expire_date' => $request->product_expire_date,
                            'payment_type' => $Banks->bank_name,
                            'payment' => $request->payment,
                            'change' => $request->change,
                            'business' => $request->business_ids,
                            'user_id' => Auth::id(),
                            'created_at' => Carbon::now()
                        ]);

                        $Stock = Product::find($request->select_product_name);
                        $Stock->product_quantity = $Stock->product_quantity + $request->product_quantity;
                        $Stock->save();

                        if($request->bankB_id){
                            $post_data['tran_id'] = uniqid();

                            $ProductPurchase =  ProductPurchase::all();
                            $ProductPurchase_id = 0;
                            foreach ($ProductPurchase->reverse() as $purchase_value){
                                if($purchase_value->business == Session::get('business_id')){
                                    $ProductPurchase_id = $purchase_value->id;
                                    break;
                                }
                            }



                            if($request->change > 0){
                                $sup = Supplier::find($request->supplier_id);

                                if($sup->due > 0){
                                   $sup->due = $sup->due + $request->change;
                                }
                                if(!$sup->due && !$sup->advance){
                                    $sup->due = $request->change;
                                }

                                if($sup->advance > 0){
                                    if($sup->advance < $request->change){
                                        $lower_advance = $request->change - $sup->advance;
                                        $sup->advance = 0;
                                        $sup->due = $lower_advance;
                                    }
                                    if($sup->advance >= $request->change){
                                        $sup->advance = $sup->advance - $request->change;
                                    }
                                }
                                $sup->save();
                            }

                            if($request->change < 0){
                                $sup = Supplier::find($request->supplier_id);
                                $get_advance = $request->payment - $request->total_amount;

                                if($sup->advance > 0){
                                   $sup->advance = $sup->advance + $get_advance;
                                }
                                if(!$sup->due && !$sup->advance){
                                    $sup->advance = $get_advance;
                                }

                                if($sup->due > 0){
                                    if($sup->due < $get_advance){
                                        $lower_due = $get_advance - $sup->due;
                                        $sup->due = 0;
                                        $sup->advance = $lower_due;
                                    }
                                    if($sup->due >= $get_advance){
                                        $sup->due = $sup->due - $get_advance;
                                    }
                                }
                                $sup->save();
                            }
                            if($request->payment){
                            Transaction::insert([
                                'bank_name' => $Banks->bank_name,
                                'bank' => $Banks->id,
                                'operation_type' => "Auto",
                                'account_number' => $Banks->account_number,
                                'balance' => $request->payment,
                                'media' => "Product Purchase",
                                'purchase' => $ProductPurchase_id,
                                'transaction_id' => $post_data['tran_id'],
                                'transaction_date' => date('y-m-d'),
                                'business' => Session::get('business_id'),
                                'user_id' => Auth::id(),
                                'created_at' => Carbon::now()
                            ]);
                            $Banks->opening_amount = $Banks->opening_amount - $request->payment;
                            $Banks->save();
                           }
                        }

                }
                else{
                    return redirect()->back()->withErrors([' insufficient balance ! ']);
                }
            }

            else{
                if(!$request->bankB_id){

                    ProductPurchase::insert([
                            'product' => $request->select_product_name,
                            'supplier_id' => $request->supplier_id,
                            'quantity' => $request->product_quantity,
                            'price' => $request->product_buying_price,

                            'discount_amount' => $request->discount,
                            'total_amount' => $request->total_amount,

                            'shipping_cost' => $request->shipp_cost,
                            'previous_due' => $pre_due,
                            'current_due' => $cur_due,
                            'previous_advance' => $pre_advnc,
                            'current_advance' => $cur_advnc,

                            'product_buying_date' => $request->product_buying_date,
                            'product_expire_date' => $request->product_expire_date,
                            'payment_type' => "Due Purchase",
                            'payment' => $request->payment,
                            'change' => $request->change,
                            'business' => $request->business_ids,
                            'user_id' => Auth::id(),
                            'created_at' => Carbon::now()
                        ]);

                        $Stock = Product::find($request->select_product_name);
                        $Stock->product_quantity = $Stock->product_quantity + $request->product_quantity;
                        $Stock->save();


                            if($request->change > 0){
                                $sup = Supplier::find($request->supplier_id);

                                if($sup->due > 0){
                                   $sup->due = $sup->due + $request->change;
                                }
                                if(!$sup->due && !$sup->advance){
                                    $sup->due = $request->change;
                                }

                                if($sup->advance > 0){
                                    if($sup->advance < $request->change){
                                        $lower_advance = $request->change - $sup->advance;
                                        $sup->advance = 0;
                                        $sup->due = $lower_advance;
                                    }
                                    if($sup->advance >= $request->change){
                                        $sup->advance = $sup->advance - $request->change;
                                    }
                                }
                                $sup->save();
                            }

                            if($request->change < 0){
                                $sup = Supplier::find($request->supplier_id);
                                $get_advance = $request->payment - $request->total_amount;

                                if($sup->advance > 0){
                                   $sup->advance = $sup->advance + $get_advance;
                                }
                                if(!$sup->due && !$sup->advance){
                                    $sup->advance = $get_advance;
                                }

                                if($sup->due > 0){
                                    if($sup->due < $get_advance){
                                        $lower_due = $get_advance - $sup->due;
                                        $sup->due = 0;
                                        $sup->advance = $lower_due;
                                    }
                                    if($sup->due >= $get_advance){
                                        $sup->due = $sup->due - $get_advance;
                                    }
                                }
                                $sup->save();
                            }



                }

            }


            return back();
        }

    // Edit Product
    public function EditProduct($product_id)
    {
        return view('Business.Product.edit_product', [
            'Product_info' => Product::find($product_id),
            'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Supplier_info' => Supplier::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get()
        ]);
    }

    // Edit Purchase
    public function EditPurchaseProduct($purchase_id)
    {
            $Purchase_info = ProductPurchase::all();
            $Purchase_info = ProductPurchase::where('business', Session::get('business_id'))->latest()->paginate(5000, ['*'], 'Purchase');
            $Purchase_infoS = ProductPurchase::find($purchase_id);
            $Total_Product = Product::all();

            return view('Business/Product/edit_purchaseproduct',compact('Purchase_info','Total_Product','Purchase_infoS'));
    }

    // Update Product Information
    public function EditProductFormPost(Request $request)
    {
        $All_Product = Product::find($request->id);

        $All_Product->product_name = $request->product_name;
        $All_Product->product_description = $request->product_description;
        $All_Product->category_id = $request->category_id;
        $All_Product->brand_id = $request->brand_id;

        $All_Product->whole_selling_price = $request->whole_selling_price;
        $All_Product->retail_selling_price = $request->retail_selling_price;
        $All_Product->save();

        return back();
    }
    public function EditPurchaseFormPost(Request $request)
    {
        $Purchase_Product = ProductPurchase::find($request->purchase_ids);

        $Purchase_Product->product_buying_date = $request->product_buying_date;
        $Purchase_Product->product_expire_date = $request->product_expire_date;
        $Purchase_Product->save();

        return back();
    }



    // Delete Product Information
    public function DeleteProduct($product_id)
    {
        Product::find($product_id)->forceDelete();
        return back()->with('delete_status', 'Your Product Permanently Deleted Successfully!');
    }

    // Delete Product Purchase
    public function DeleteProductPurchase($purchase_id)
    {
        $purchase = ProductPurchase::find($purchase_id);
        $transaction = DB::table('transactions')->where('purchase', $purchase_id)->first();

        if($purchase->payment > 0){

            $bank = Bank::find($transaction->bank);
            $bank->opening_amount = $bank->opening_amount + $transaction->balance;
            $bank->save();

            $product = Product::find($purchase->product);
            $product->product_quantity = $product->product_quantity - $purchase->quantity;
            $product->save();

            if($purchase->change > 0){
                $sup = Supplier::find($purchase->supplier_id);
                if($sup->advance > 0){
                    $sup->advance = $sup->advance + $purchase->change;
                }
                if($sup->due < $purchase->change && $sup->due > 0){
                    $lower_due = $purchase->change - $sup->due;
                    $sup->due = 0;
                    $sup->advance = $lower_due;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->advance = $purchase->change;
                }
                if($sup->due >= $purchase->change){
                   $sup->due = $sup->due - $purchase->change;
                }

                $sup->save();
            }

            if($purchase->change < 0){
                $sup = Supplier::find($purchase->supplier_id);
                $get_advance = $purchase->payment - $purchase->total_amount;
                if($sup->due > 0){
                    $sup->due = $sup->due + $get_advance;
                }
                if($sup->advance < $get_advance && $sup->advance > 0){
                    $lower_advance = $get_advance - $sup->advance;
                    $sup->advance = 0;
                    $sup->due = $lower_advance;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->due = $get_advance;
                }
                if($sup->advance >= $get_advance){
                   $sup->advance = $sup->advance - $get_advance;
                }

                $sup->save();
            }

            Transaction::find($transaction->id)->forceDelete();
            ProductPurchase::find($purchase_id)->forceDelete();

        }
        else{

            if($purchase->change > 0){
                $sup = Supplier::find($purchase->supplier_id);
                if($sup->advance > 0){
                    $sup->advance = $sup->advance + $purchase->change;
                }
                if($sup->due < $purchase->change && $sup->due > 0){
                    $lower_due = $purchase->change - $sup->due;
                    $sup->due = 0;
                    $sup->advance = $lower_due;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->advance = $purchase->change;
                }
                if($sup->due >= $purchase->change){
                   $sup->due = $sup->due - $purchase->change;
                }

                $sup->save();
            }

            if($purchase->change < 0){
                $sup = Supplier::find($purchase->supplier_id);
                $get_advance = $purchase->payment - $purchase->total_amount;
                if($sup->due > 0){
                    $sup->due = $sup->due + $get_advance;
                }
                if($sup->advance < $get_advance && $sup->advance > 0){
                    $lower_advance = $get_advance - $sup->advance;
                    $sup->advance = 0;
                    $sup->due = $lower_advance;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->due = $get_advance;
                }
                if($sup->advance >= $get_advance){
                   $sup->advance = $sup->advance - $get_advance;
                }

                $sup->save();
            }

            $product = Product::find($purchase->product);
            $product->product_quantity = $product->product_quantity - $purchase->quantity;
            $product->save();
            ProductPurchase::find($purchase_id)->forceDelete();
        }


        return back()->with('delete_status', 'Your Product Permanently Deleted Successfully!');
    }

        // Delete Product Purchase
        public function DeleteProductSell($sell_id)
        {

            $order = Order::find($sell_id);
            $product_cod = json_decode($order->product_code);
            $order_quantity = json_decode($order->product_quantity);


            $transaction = DB::table('transactions')->where('sell', $order->id)->first();

            if($order->payment > 0){

                $bank = Bank::find($transaction->bank);

                if( $transaction->balance <= $bank->opening_amount ){

                    $bank->opening_amount = $bank->opening_amount - $transaction->balance;
                    $bank->save();


                    if($order->change > 0){
                        $cus = Customar::find($order->customar_id);
                        if($cus->advance > 0){
                            $cus->advance = $cus->advance + $order->change;
                        }
                        if($cus->due < $order->change && $cus->due > 0){
                            $lower_due = $order->change - $cus->due;
                            $cus->due = 0;
                            $cus->advance = $lower_due;
                        }
                        if(!$cus->due && !$cus->advance){
                            $cus->advance = $order->change;
                        }
                        if($cus->due >= $order->change){
                           $cus->due = $cus->due - $order->change;
                        }


                        $cus->save();
                    }

                    if($order->change < 0){
                        $cus = Customar::find($order->customar_id );
                        $get_advance = $order->payment - $order->total;
                        if($cus->due > 0){
                            $cus->due = $cus->due + $get_advance;
                        }
                        if($cus->advance < $get_advance && $cus->advance > 0){
                            $lower_advance = $get_advance - $cus->advance;
                            $cus->advance = 0;
                            $cus->due = $lower_advance;
                        }
                        if(!$cus->due && !$cus->advance){
                            $cus->due = $get_advance;
                        }
                        if($cus->advance >= $get_advance){
                           $cus->advance = $cus->advance - $get_advance;
                        }


                        $cus->save();
                    }

                    $i = 0;

                    foreach ($product_cod as $code) {
                      $i++;
                    }

                    for ($row = 0; $row < $i; $row++){
                      $find_product = DB::table('products')->where('product_code', $product_cod[$row])->first();
                      DB::table('products')->where('product_code', $product_cod[$row])->update(['product_quantity' => ($find_product->product_quantity + $order_quantity[$row])]);
                    }

                    Transaction::find($transaction->id)->forceDelete();
                    Order::find($sell_id)->forceDelete();
                }
                else{
                    return redirect()->back()->withErrors([' Sell Amount is more than current Bank balance! ']);
                }

            }
            else{

                if($order->change > 0){
                    $cus = Customar::find($order->customar_id);
                    if($cus->advance > 0){
                        $cus->advance = $cus->advance + $order->change;
                    }
                    if($cus->due < $order->change && $cus->due > 0){
                        $lower_due = $order->change - $cus->due;
                        $cus->due = 0;
                        $cus->advance = $lower_due;
                    }
                    if(!$cus->due && !$cus->advance){
                        $cus->advance = $order->change;
                    }
                    if($cus->due >= $order->change){
                       $cus->due = $cus->due - $order->change;
                    }


                    $cus->save();
                }

                if($order->change < 0){
                    $cus = Customar::find($order->customar_id );
                    $get_advance = $order->payment - $order->total;
                    if($cus->due > 0){
                        $cus->due = $cus->due + $get_advance;
                    }
                    if($cus->advance < $get_advance && $cus->advance > 0){
                        $lower_advance = $get_advance - $cus->advance;
                        $cus->advance = 0;
                        $cus->due = $lower_advance;
                    }
                    if(!$cus->due && !$cus->advance){
                        $cus->due = $get_advance;
                    }
                    if($cus->advance >= $get_advance){
                       $cus->advance = $cus->advance - $get_advance;
                    }


                    $cus->save();
                }

                $i = 0;

                foreach ($product_cod as $code) {
                  $i++;
                }

                for ($row = 0; $row < $i; $row++){
                  $find_product = DB::table('products')->where('product_code', $product_cod[$row])->first();
                  DB::table('products')->where('product_code', $product_cod[$row])->update(['product_quantity' => ($find_product->product_quantity + $order_quantity[$row])]);
                }

                Order::find($sell_id)->forceDelete();
            }


            return back()->with('delete_status', 'Your Sell Permanently Deleted Successfully!');
        }

    // View Single Product
    public function ViewSingleProduct($product_id)
    {
        $find_product = DB::table('orders')->where('business', Session::get('business_id'))->get();
        $fixed_product = Product::find($product_id);
          
        $p_sell = 0;

       foreach ($find_product as $pdr) {
         $j = 0;
         $product_cods = json_decode($pdr->product_code);
         $order_quantitys = json_decode($pdr->product_quantity);
         
         foreach ($product_cods as $code) {             
                 $j++;  
           }

         for ($col = 0; $col < $j; $col++){
             if($product_cods[$col] == $fixed_product->product_code){            
               $p_sell =  $p_sell + $order_quantitys[$col];
             }

          }
       } 
        return view('Business.Product.single_product_view', [
            'Total_Product' => Product::all(),
            'Total_ProductPurchase' => ProductPurchase::orderBy('id')->where('product', $product_id)->latest()->get(),
            'single_product_view' => Product::find($product_id),
            'total_sell_pr' => $p_sell
        ]);
    }
}
