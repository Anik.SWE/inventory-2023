<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Helpers\Order as HelpersOrder;
use App\Models\Product;
use App\Models\Category;
use App\Models\Customar;
use App\Models\Employee;
use App\Models\Order;
use App\Models\Bank;
use App\Models\Due;
use Illuminate\Support\Facades\Auth;
use Cart;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // View Pos:
    public function ViewPos()
    {
        return view('Business.POS.view_pos', [
            'Total_Stock_Item' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->count(),
            'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Customar_info' => Customar::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'banks' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'invoice_list' => Order::orderBy('user_id')->where('business', Session::get('business_id'))->latest()->paginate(20),
            'Total_Stock' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->latest()->get()
        ]);
    }
    // Category Search to Find Product:
    public function SearchProduct(Request $request)
    {

        $select_product = Product::where('category_id', $request->cat_id)->where('user_id', Auth::id())->latest()->get();
        $stringToSend = "";
        $stringToSend .= $select_product;
        return $stringToSend;
    }

    // View Bank Page:
    public function BankPayment()
    {
        return view('Business.POS.view_bank_page', [
            'Total_Stock_Item' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->count(),
            'Category_info' => Category::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Customar_info' => Customar::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Brand_info' => Brand::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'order_info' => Order::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Stock' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
        ]);
    }


    public function index(Request $request)
    {
        // $total_due = Order::orderBy('id')->where('customar_id', $request->due_id)->get();
        // $payment = Due::orderBy('id')->where('customar_id', $request->due_id)->sum('payment');
        // $balance = DB::table('banks')->where('user_id', Auth::id())->where('account_number',$request->account_number)->sum('opening_amount');
        $payment = Due::orderBy('id')->where('customar_id', $request->due_id)->sum('payment');
        $total_due = DB::table('orders')->select('customar_id')->where('customar_id', $request->due_id)->groupBy('customar_id')->where('user_id', Auth::id())->sum('change');
        $due = $total_due - $payment;
        $stringToSend = "";
        // foreach($total_due as $due){
        $stringToSend .= $due;
        "<span>" . $due . "</span>";
        // break;
        // }
        return $stringToSend;
    }

    public function findCustomers(Request $request)
    {


    $customers = DB::table('customars')->where('id', $request->search)->first();

    return view('Business.POS.customerdrop', compact('customers'));
    }

    public function findProductPos(Request $request)
    {

      $Total_Stock = Product::where("product_name","LIKE","%".$request->search."%")->where('business', Session::get('business_id'))
                         ->orWhere('product_code',"LIKE","%".$request->search."%")->where('business', Session::get('business_id'))
                         ->get();
      return view('Business.POS.product_div', compact('Total_Stock'));

    }


    // Add Cart:
    // public function AddCart(Request $request)
    // {
    //     $data = array();
    //     $data['id'] = $request->id;
    //     $data['name'] = $request->name;
    //     $data['qty'] = $request->qty;
    //     $data['price'] = $request->price;
    //     $add = Cart::add($data);
    //     return back();
    // }
    // Update Cart:
    // public function UpdateCart(Request $request,$rowId)
    // {
    //     $Uqty = $request->qty;
    //     $update = Cart::update($rowId, $Uqty);
    //     return back();
    // }
    // Remove Cart:
    // public function RemoveCart($rowId)
    // {
    //     $remove = Cart::remove($rowId);
    //     return back();
    // }
}
