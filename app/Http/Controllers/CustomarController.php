<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCustomarForm;
use App\Models\Customar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Session;
class CustomarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Add Customar Form:
    public function AddCustomar()
    {
        return view('Business.Customar.add_customar_form', [
            'Customar' => Customar::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
        ]);
    }

    // Customar Data Store Form:
    public function CustomarDataStore(AddCustomarForm $request)
    {
        // if ($request->hasFile('customar_photo')) {
        //     $file = $request->file('customar_photo');
        //     $fileName = time() . '.' . $file->getClientOriginalExtension();
        //     $location = 'public/uploads/customar_photos/' . $fileName;
        //     Image::make($file)->save(base_path($location));
        // }

        $dues = 0;
        $advances = 0;
        if($request->amount_type == "Due"){
            $dues = $request->opening_amount;
        }
        if($request->amount_type == "Advance"){
            $advances = $request->opening_amount;
        }


        Customar::insert([
            'customar_name' => $request->customar_name,

            'due' => $dues,
            'advance' => $advances,

            'customar_email' => $request->customar_email,
            'phone_number' => $request->phone_number,

            'address' => $request->address,

            'business' => Session::get('business_id'),
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back()->with('add_status', 'hello');
    }

    // Edit Customar
    public function EditCustomar($customar_id)
    {
        return view('Business.Customar.edit_customar', [
            'Customar' => Customar::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'customar_info' => Customar::find($customar_id)
        ]);
    }



    // Update Customar Information
    public function EditCustomarFormPost(Request $request, $customar_id)
    {

        // $delete = Customar::find($customar_id)->first();
        // $old_photo = 'public/uploads/customar_photos/' . $delete->customar_photo;
        // if ($request->hasFile('customar_photo')) {
        //     if ($request->customar_photo != $delete->customar_photo) {
        //         unlink(base_path($old_photo));
        //     }

        //     $file = $request->file('customar_photo');
        //     $fileName = time() . '.' . $file->getClientOriginalExtension();
        //     $location = 'public/uploads/customar_photos/' . $fileName;
        //     Image::make($file)->save(base_path($location));
            Customar::find($customar_id)->update([
                'customar_name' => $request->customar_name,
                'customar_email' => $request->customar_email,
                'phone_number' => $request->phone_number,

                'address' => $request->address,

            ]);
            return back()->with('add_status', 'hello');
        // } else {
        //     Customar::find($customar_id)->update([
        //         'customar_name' => $request->customar_name,
        //         'customar_email' => $request->customar_email,
        //         'phone_number' => $request->phone_number,
        //         'location' => $request->location,
        //         'city' => $request->city,
        //         'address' => $request->address,
        //         'zip' => $request->zip,
        //         'bank_name' => $request->bank_name,
        //         'bank_account' => $request->bank_account,
        //         'customar_photo' => $delete->customar_photo
        //     ]);
        //     return back()->with('add_status', 'hello');
        // }
    }


    // Delete Customar Information
    public function DeleteCustomar($customar_id)
    {
      
        Customar::find($customar_id)->forceDelete();
        
        return redirect()->route('customar.addform');
    }

    // Edit Customar
    public function ViewSingleCustomar($customar_id)
    {
        return view('Business.Customar.single_customar_view', [
            'Customar' => Customar::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'customar_info' => Customar::find($customar_id)
        ]);
    }
}
