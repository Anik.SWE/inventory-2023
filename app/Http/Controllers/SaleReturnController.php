<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Product;
use App\Models\SaleReturn;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Bank;
use App\Models\Transaction;
use App\Models\Order;
use Session;
use App\Models\Customar;

class SaleReturnController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Category Form:
    public function SaleReturnForm($ids)
    {
        return view('Business.Return.view_salereturn', [
            'Customer_info' => Customar::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Return_Product' => SaleReturn::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'banks' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'sell' => Order::find($ids)
        ]);
    }

    public function SaleReturn()
    {      
        return view('Business.Return.view_salereturn', [
            'Customer_info' => Customar::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Return_Product' => SaleReturn::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'banks' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'sell' => Order::find(0)
        ]);
    }




    public function SaleReturnFormPost(Request $request)
    {
        $order = Order::find($request->sell_id);
        $fnd_product = Product::find($request->pro_id);

        $product_cod = json_decode($order->product_code);
        $order_quantity = json_decode($order->product_quantity);
        $order_price = json_decode($order->product_price);

        $i = 0;
        $quantity = 0;
        $price = 0;
        $p_code = 0;
        
        foreach ($product_cod as $code) {
          $i++;
        } 
        for ($row = 0; $row < $i; $row++){
          if($product_cod[$row] == $fnd_product->product_code){
                $quantity = $order_quantity[$row]; 
                $price = $order_price[$row];
                $p_code = $product_cod[$row];
          }
        }

        $valid = DB::table('sale_returns')->where('sell' , $request->sell_id)->where('product' , $request->pro_id)->sum('product_quantity') + $request->product_quantity;
        $sum = $price * $request->product_quantity;
        if ($quantity) {
        
            if($valid <= $quantity){
                if($order->payment){
    
                    $transaction = DB::table('transactions')->where('sell', $request->sell_id)->first();
                    $Banks = Bank::find( $transaction->bank);
    
                    if ($Banks->opening_amount >= $sum) {
    
                        $products = DB::table('products')->where('product_code', $p_code)->first();               
                        if($request->bankr == "Due"){
                            SaleReturn::insert([
                                'sell' => $request->sell_id,
                                'product' => $products->id,
                                'product_quantity' => $request->product_quantity,
                                'product_price' => $price,
                                'total_amount' => $sum,
                                'bank' => $transaction->bank,
                                'payment' => 0,
                                'change' => $sum,
                                'business' => Session::get('business_id'),
                                'user_id' => Auth::id(),
                                'created_at' => Carbon::now()
                            ]); 
                            
                                $cus = Customar::find($request->customer_id);   
                                if($cus->advance > 0){
                                    $cus->advance = $cus->advance + $sum;
                                } 
                                if($cus->due < $sum && $cus->due > 0){
                                    $lower_due = $sum - $cus->due;
                                    $cus->due = 0;
                                    $cus->advance = $lower_due;
                                }
                                if(!$cus->due && !$cus->advance){
                                    $cus->advance = $sum;
                                }
                                if($cus->due >= $sum){
                                   $cus->due = $cus->due - $sum;
                                }
                           
                                $cus->save();                   
    
                        }
                        if($request->bankr == "Cash"){
                            SaleReturn::insert([
                                'sell' => $request->sell_id,
                                'product' => $products->id,
                                'product_quantity' => $request->product_quantity,
                                'product_price' => $price,
                                'total_amount' => $sum,
                                'bank' => $transaction->bank,
                                'payment' => $sum,
                                'change' => 0,
                                'business' => Session::get('business_id'),
                                'user_id' => Auth::id(),
                                'created_at' => Carbon::now()
                            ]); 
    
                            $SellReturn = SaleReturn::all();
                            $SellReturn_id = 0;
                            foreach ($SellReturn->reverse() as $return_value) {
                                if ($return_value->business == Session::get('business_id')) {
                                    $SellReturn_id = $return_value->id;
                                    break;
                                }
                            }
    
                            $post_data['tran_id'] = uniqid();
                            Transaction::insert([
                                'bank_name' => $Banks->bank_name,
                                'bank' => $Banks->id,
                                'operation_type' => "Auto",
                                'account_number' => $Banks->account_number,
                                'balance' => $sum,
                                'media' => "Sell Return",
                                'sell_return' => $SellReturn_id,
                                'transaction_id' => $post_data['tran_id'],
                                'transaction_date' => date('y-m-d'),
                                'business' => Session::get('business_id'),
                                'user_id' => Auth::id(),
                                'created_at' => Carbon::now()
                            ]);
                            $Banks->opening_amount = $Banks->opening_amount - $sum;
                            $Banks->save();
    
                        }                 
                        $fnd_product->product_quantity = $fnd_product->product_quantity + $request->product_quantity;
                        $fnd_product->save();
    
    
                    }
                    else{
                        return redirect()->back()->withErrors([' Sell Amount is more than current Bank balance! ']);
                    }
    
                }
                else{

                    $products = DB::table('products')->where('product_code', $p_code)->first();               
                    if($request->bankr == "Due"){
                        SaleReturn::insert([
                            'sell' => $request->sell_id,
                            'product' => $products->id,
                            'product_quantity' => $request->product_quantity,
                            'product_price' => $price,
                            'total_amount' => $sum,                      
                            'payment' => 0,
                            'change' => $sum,
                            'business' => Session::get('business_id'),
                            'user_id' => Auth::id(),
                            'created_at' => Carbon::now()
                        ]); 
                        
                            $cus = Customar::find($request->customer_id);   
                            if($cus->advance > 0){
                                $cus->advance = $cus->advance + $sum;
                            } 
                            if($cus->due < $sum && $cus->due > 0){
                                $lower_due = $sum - $cus->due;
                                $cus->due = 0;
                                $cus->advance = $lower_due;
                            }
                            if(!$cus->due && !$cus->advance){
                                $cus->advance = $sum;
                            }
                            if($cus->due >= $sum){
                               $cus->due = $cus->due - $sum;
                            }
                       
                            $cus->save();                   
    
                    }               
                    $fnd_product->product_quantity = $fnd_product->product_quantity + $request->product_quantity;
                    $fnd_product->save();
                }
    
            }
            else{
                return redirect()->back()->withErrors([' Quantity is more than sell ! ']);
            }

        }
        else{
            return redirect()->back()->withErrors([' Product you selected is not in sell list! ']);
        }


    
        return back();
       
    }


    // Edit Product
    public function EditReturnProduct($product_id)
    {
        return view('Business.Return.edit_salereturn', [
            'info' => SaleReturn::find($product_id),
            'Supplier_info' => Supplier::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Total_Product' => Product::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Return_Product' => SaleReturn::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'banks' => Bank::orderBy('user_id')->where('user_id', Auth::id())->get(),
        ]);
    }



    // Update Product Information
    public function EditReturnFormPost(Request $request)
    {
        SaleReturn::find($request->id)->update([
            'product_name' => $request->product_name,
            'supplier_id' => $request->supplier_id,
            'product_quantity' => $request->product_quantity,
            'product_price' => $request->product_price,
            'total_amount' => $request->total_amount,
            'payment_type' => $request->payment_type,
            'payment' => $request->payment,
            'change' => $request->change,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }





    // Delete Product Information
    public function DeleteReturnProduct($sell_return_id)
    {
        
        $fnd_sell_return = SaleReturn::find($sell_return_id);
        $fnd_product = Product::find($fnd_sell_return->product);
        $order = Order::find($fnd_sell_return->sell);

        if($fnd_sell_return->payment){

            $transaction = DB::table('transactions')->where('sell_return', $sell_return_id)->first();
            $Banks = Bank::find( $transaction->bank);
            Transaction::find($transaction->id)->forceDelete();
            $Banks->opening_amount = $Banks->opening_amount + $fnd_sell_return->payment;
            $Banks->save();

        }
        if($fnd_sell_return->change){

            $cus = Customar::find($order->customar_id);   
            if($cus->due > 0){
                $cus->due = $cus->due + $fnd_sell_return->change;
            } 
            if($cus->advance < $fnd_sell_return->change && $cus->advance > 0){
                $lower_advance = $fnd_sell_return->change - $cus->advance;
                $cus->advance = 0;
                $cus->due = $lower_advance;
            }
            if(!$cus->due && !$cus->advance){
                $cus->due = $fnd_sell_return->change;
            }
            if($cus->advance >= $fnd_sell_return->change){
               $cus->advance = $cus->advance - $fnd_sell_return->change;
            }
       
            $cus->save(); 

        }

        $fnd_product->product_quantity = $fnd_product->product_quantity - $fnd_sell_return->product_quantity;
        $fnd_product->save();

        SaleReturn::find($sell_return_id)->forceDelete();
        return back()->with('delete_status', 'Your Product Permanently Deleted Successfully!');
    }
}
