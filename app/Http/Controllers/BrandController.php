<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Http\Requests\AddBrandForm;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class BrandController extends Controller
{
    public $user;


    // public function __construct()
    // {
    //     $this->middleware(function ($request, $next) {
    //         $this->user = Auth::guard('admin')->user();
    //         return $next($request);
    //     });
    // }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    // Brands Form:
    public function AddBrand()
    {
        return view('Business.Brand.add_brand_form',[
            'Total_Brand' => Brand::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }
    // Insert Brands:
    public function BrandFormPost(AddBrandForm $request)
    {
        Brand::insert([
            'brand_name' => $request->brand_name,
            'brand_description' => $request->brand_description,
            'business' => $request->business_ids,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }

    // Edit Brands:
    public function EditBrand($brand_id)
    {
        return view('Business.Brand.edit_brand',[
            'Total_Brand' => Brand::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Brand_info' => Brand::find($brand_id)
        ]);
    }

    // Update Brands Information
    public function EditBrandFormPost(Request $request)
    {
        Brand::find($request->id)->update([
            'brand_name' => $request->brand_name,
            'brand_description' => $request->brand_description
        ]);
        return back();
    }

    // Delete Brands Information
    public function DeleteBrand($brand_id)
    {
        if (is_null($this->user) || !$this->user->can('brand.delete')) {
            abort(403, 'Sorry !! You are Unauthorized to delete any brand !');
        }
        Brand::find($brand_id)->forceDelete();
        return back()->with('delete_status', 'Your Category Permanently Deleted Successfully!');
    }
}
