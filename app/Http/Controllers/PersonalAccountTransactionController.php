<?php

namespace App\Http\Controllers;

use App\Models\AccountHead;
use App\Models\Bank;
use App\Models\PersonalAccountTransaction;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PersonalAccountTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banks = Bank::orderBy('user_id')->where('business', session()->get('business_id'))->latest('id')->get();
        $accountHead = AccountHead::where('user_id', Auth::user()->id)->where('business_id', session()->get('business_id'))->get();
        $personalAccountTransaction = PersonalAccountTransaction::where('user_id', Auth::user()->id)
            ->where('business_id', session()->get('business_id'))
            ->orderBy('id', 'desc')
            ->get();
        return view("Business.Personal_Account.account_transection", compact('accountHead', 'banks', 'personalAccountTransaction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $personalAccountTransaction =  new PersonalAccountTransaction();
        $personalAccountTransaction->transaction_date =  $request->transaction_date;
        $personalAccountTransaction->transaction_type =  $request->transaction_type;
        $personalAccountTransaction->note =  $request->note;
        $personalAccountTransaction->amount =  $request->amount;
        $personalAccountTransaction->main_head_id =  $request->main_head_id;
        $personalAccountTransaction->sub_head_id =  $request->sub_head_id;
        $personalAccountTransaction->bank_id =  $request->bank_id;
        $personalAccountTransaction->business_id =  Auth::user()->id;
        $personalAccountTransaction->user_id =  session()->get('business_id');
        $personalAccountTransaction->save();


        Transaction::insert([
            'bank_name' => "",
            'bank' => $request->bank_id,
            'operation_type' => $request->transaction_type,
            'account_number' => $request->account_number,
            'balance' => $request->amount,
            'media' => '',
            'transaction_id' => uniqid(),
            'transaction_date' => date('y-m-d'),
            'business' =>  session()->get('business_id'),
            'user_id' => Auth::id(),
        ]);
        $balance = DB::table('banks')->where('user_id', Auth::id())->where('id', $request->bank_id)->sum('opening_amount');
        if ($request->transaction_type == "Income") {
            Bank::where('id', $request->bank_id)->update([
                'opening_amount' => ($balance + $request->amount)
            ]);
        } else {
            Bank::where('id', $request->bank_id)->update([
                'opening_amount' => ($balance - $request->amount)
            ]);
        }


        return back()->with("status", "Head Added");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PersonalAccountTransaction  $personalAccountTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalAccountTransaction $personalAccountTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PersonalAccountTransaction  $personalAccountTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalAccountTransaction $personalAccountTransaction)
    {
        //

        Transaction::insert([
            'bank_name' => "",
            'bank' => $personalAccountTransaction->bank_id,
            'operation_type' => $personalAccountTransaction->transaction_type,
            'account_number' => $personalAccountTransaction->account_number,
            'balance' => -$personalAccountTransaction->amount,
            'media' => '',
            'transaction_id' => uniqid(),
            'transaction_date' => date('y-m-d'),
            'business' =>  session()->get('business_id'),
            'user_id' => Auth::id(),
        ]);
        $balance = DB::table('banks')->where('user_id', Auth::id())->where('id', $personalAccountTransaction->bank_id)->sum('opening_amount');
        if ($personalAccountTransaction->transaction_type == "Income") {
            Bank::where('id', $personalAccountTransaction->bank_id)->update([
                'opening_amount' => ($balance - $personalAccountTransaction->amount)
            ]);
        } else {
            Bank::where('id', $personalAccountTransaction->bank_id)->update([
                'opening_amount' => ($balance + $personalAccountTransaction->amount)
            ]);
        }

        $personalAccountTransaction->delete();

        return back()->with("status", "Head Added");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PersonalAccountTransaction  $personalAccountTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalAccountTransaction $personalAccountTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PersonalAccountTransaction  $personalAccountTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalAccountTransaction $personalAccountTransaction)
    {
        //
    }

    public function searchPersonalAccount(Request $request)
    {
        $banks = Bank::orderBy('user_id')->where('business', session()->get('business_id'))->latest('id')->get();
        $accountHead = AccountHead::where('user_id', Auth::user()->id)->where('business_id', session()->get('business_id'))->get();
        $personalAccountTransaction = [];

        if ($request->getMethod() == 'POST') {

            $personalAccountTransaction = PersonalAccountTransaction::where('user_id', Auth::user()->id)
                ->where('business_id', session()->get('business_id'))
                ->when($request->transaction_date != "", function ($query) use ($request) {
                    $query->where('transaction_date', '>=', date('Y-m-d', strtotime($request->transaction_date)))
                        ->where('transaction_date', '<=', date('Y-m-d', strtotime($request->to_transaction_date)));
                })
                ->when($request->main_head_id != "", function ($query) use ($request) {
                    $query->where('main_head_id',  $request->main_head_id);
                })
                ->when($request->sub_head_id != "", function ($query) use ($request) {
                    $query->where('sub_head_id',  $request->sub_head_id);
                })
                ->when($request->transaction_type != "", function ($query) use ($request) {
                    $query->where('transaction_type',  $request->transaction_type);
                })
                ->orderBy('id', 'desc')
                ->get();

        }
        return view("Business.Personal_Account.search_report", compact('accountHead', 'banks', 'personalAccountTransaction'));
    }
}
