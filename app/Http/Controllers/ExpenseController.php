<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddExpenseForm;
use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ExpenseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Expense Form:
    public function AddExpense()
    {
        return view('Business.Expense.add_expense_form',[
            'Total_Expense' => Expense::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }

    // Insert Expense:
    public function ExpenseFormPost(AddExpenseForm $request)
    {
        Expense::insert([
            'expense_details' => $request->expense_details,
            'expense_amount' => $request->expense_amount,
            'expense_date' => Carbon::now(),
            'expense_month' => Carbon::now()->monthName,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }
    // Edit Expense:
    public function EditExpense($expense_id)
    {
        return view('Business.Expense.edit_expense',[
            'Total_Expense' => Expense::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Expense_info' => Expense::find($expense_id)
        ]);
    }

    // Update Expense Information
    public function EditExpenseFormPost(Request $request)
    {
        Expense::find($request->id)->update([
            'expense_details' => $request->expense_details,
            'expense_amount' => $request->expense_amount,
        ]);
        return back();
    }

    // Delete Expense Information
    public function DeleteExpense($expense_id)
    {
        Expense::find($expense_id)->forceDelete();
        return back()->with('delete_status', 'Your Category Permanently Deleted Successfully!');
    }
}
