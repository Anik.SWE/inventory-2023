<?php

namespace App\Http\Controllers;

use App\Http\Requests\DueCollection;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Customar;
use Illuminate\Support\Facades\Auth;
use App\Models\Due;
use App\Models\Bank;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\SearchProduct;
use App\Models\Supplier;
use App\Models\CustomarLedger;
use App\Models\Product;
use App\Traits\CommonTraits;
use Session;

class DueController extends Controller
{
    use CommonTraits;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('Business.Due.due_collection', [
            'Customar_info' => Customar::orderBy('id')->where('business', Session::get('business_id'))->get(),
            'Supplier_info' => Supplier::orderBy('id')->where('business', Session::get('business_id'))->get(),
            'bank_info' => Bank::orderBy('id')->where('business', Session::get('business_id'))->get(),
            'Due_info' => Due::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
        ]);
    }

    // Due Post Form:
    public function AddDue(Request $request)
    {
        $medias = "";
        if ($request->customer_id) {
            $medias = "Due Collection";
            $cus = Customar::find($request->customer_id);
            $cur_due = 0;
            if($cus->due >= $request->payment){
                $cur_due = $cus->due - $request->payment;
            }

            Due::insert([
                'customar_id' => $request->customer_id,
                'payment' => $request->payment,
                'previous_due' => $cus->due,
                'current_due' => $cur_due,
                'note' => $request->note,
                'operation_type' => "Customer due",
                'business' => Session::get('business_id'),
                'user_id' => Auth::id(),
                'collection_date' => $request->collection_date,
                'created_at' => Carbon::now()
            ]);
            if($cus->due < $request->payment && $cus->due > 0){
               $extra = $request->payment - $cus->due;
               $cus->due = 0;
               $cus->advance = $extra;
            }
            if($cus->due >= $request->payment){
                $cus->due = $cus->due - $request->payment;
            }
            $cus->save();

            $DUE = Due::all();
            $due_user_id = 0;
            foreach ($DUE->reverse() as $d_value){
                if($d_value->business == Session::get('business_id')){
                    $due_user_id = $d_value->id;
                    break;
                }
            }

            if($request->operation_type){

                    if ($request->payment > 0) {

                        $Banks = Bank::find($request->operation_type);
                        $post_data['tran_id'] = uniqid();
                        Transaction::insert([
                            'bank_name' => $Banks->bank_name,
                            'bank' => $Banks->id,
                            'operation_type' => "Auto",
                            'account_number' => $Banks->account_number,
                            'balance' => $request->payment,
                            'media' => $medias,
                            'due' => $due_user_id,
                            'transaction_id' => $post_data['tran_id'],
                            'transaction_date' => $request->collection_date,
                            'business' => Session::get('business_id'),
                            'user_id' => Auth::id(),
                            'created_at' => Carbon::now()
                        ]);

                        if($medias == "Due Collection"){
                            $Banks->opening_amount = $Banks->opening_amount + $request->payment;
                        }

                        $Banks->save();
                    }
               }

        }

        elseif ($request->supplier_id) {

            $Bank_balance = Bank::find($request->operation_type);
            if($Bank_balance->opening_amount >= $request->payment){
                $medias = "Due Payment";
                $sup = Supplier::find($request->supplier_id);
                $cus = Supplier::find($request->supplier_id);
                $cur_due_s = 0;
                $cur_due = 0;
                if($sup->due >= $request->payment){
                    $cur_due_s = $sup->due - $request->payment;
                    $cur_due = $sup->due - $request->payment;
                }

                Due::insert([
                    'supplier_id' => $request->supplier_id,
                    'payment' => $request->payment,
                    'previous_due' => $sup->due,
                    'current_due' => $cur_due_s,
                    'note' => $request->note,
                    'operation_type' => "Supplier due",
                    'business' => Session::get('business_id'),
                    'user_id' => Auth::id(),
                    'collection_date' => $request->collection_date,
                    'created_at' => Carbon::now()
                ]);
                if($sup->due < $request->payment && $sup->due > 0){
                    $extra = $request->payment - $sup->due;
                    $sup->due = 0;
                    $sup->advance = $extra;
                }
                if($sup->due >= $request->payment){
                    $sup->due = $sup->due - $request->payment;
                }
                $sup->save();

                $DUE = Due::all();
                $due_user_id = 0;
                foreach ($DUE->reverse() as $d_value){
                    if($d_value->business == Session::get('business_id')){
                        $due_user_id = $d_value->id;
                        break;
                    }
                }

                if($request->operation_type){

                        if ($request->payment > 0) {
                            $Banks = Bank::find($request->operation_type);
                            $post_data['tran_id'] = uniqid();
                            Transaction::insert([
                                'bank_name' => $Banks->bank_name,
                                'bank' => $Banks->id,
                                'operation_type' => "Auto",
                                'account_number' => $Banks->account_number,
                                'balance' => $request->payment,
                                'media' => $medias,
                                'due' => $due_user_id,
                                'transaction_id' => $post_data['tran_id'],
                                'transaction_date' => $request->collection_date,
                                'business' => Session::get('business_id'),
                                'user_id' => Auth::id(),
                                'created_at' => Carbon::now()
                            ]);

                            if($medias == "Due Payment"){
                                $Banks->opening_amount = $Banks->opening_amount - $request->payment;
                            }
                            $Banks->save();
                        }

                   }
            }
            else{
                return redirect()->back()->withErrors([' insufficient balance ! ']);
            }


        }


        //SM    S
        $message = "আসসালামু আলাইকুম, আপনি বকেয়া পরিশোধ করেছেন: ". $request->payment ."/- টাকা, এবং বর্তমান বকেয়া আছে: ".($cur_due)."/- টাকা";

        $this->sendSMS( $cus->phone_number,$message);

        return back();
    }

    // Customar Dur Form
    public function CustomarDueList()
    {
        return view('Business.Due.customar_due_list', [
            'customar_invoice' => DB::table('orders')->select('customar_id')->groupBy('customar_id')->where('user_id', Auth::id())->get(),
        ]);
    }

    // Supplier Dur Form
    public function SupplierDueList()
    {
        return view('Business.Due.supplier_due_list', [
            'suppliers' => DB::table('products')->select('supplier_id')->groupBy('supplier_id')->where('user_id', Auth::id())->get()
        ]);
    }

    // Delete Due
    public function DeleteDue($due_id)
    {
        $dues = Due::find($due_id);
        $tran = DB::table('transactions')->where('due', $due_id)->first();
        $banks = Bank::find($tran->bank);

            if($dues->customar_id){
                if($banks->opening_amount >= $dues->payment){
                    $cus = Customar::find($dues->customar_id);
                    if($cus->due > 0){
                       $cus->due = $cus->due + $dues->payment;
                    }
                    if(!$cus->due && !$cus->advance){
                        $cus->due = $dues->payment;
                    }
                    if($cus->advance < $dues->payment && $cus->advance > 0){
                        $lower_advance = $dues->payment - $cus->advance;
                        $cus->advance = 0;
                        $cus->due = $lower_advance;
                    }
                    if($cus->advance >= $dues->payment){
                        $cus->advance = $cus->advance - $dues->payment;
                    }
                    $cus->save();
                    $bank = Bank::find($tran->bank);
                    $bank->opening_amount = $bank->opening_amount - $dues->payment;
                    $bank->save();
                }
                else{
                    return redirect()->back()->withErrors([' insufficient balance delete failed right now! ']);
                }
            }

            if($dues->supplier_id){
                $sup = Supplier::find($dues->supplier_id);
                if($sup->due > 0){
                    $sup->due = $sup->due + $dues->payment;
                 }
                 if(!$sup->due && !$sup->advance){
                     $sup->due = $dues->payment;
                 }
                 if($sup->advance < $dues->payment && $sup->advance > 0){
                     $lower_advance = $dues->payment - $sup->advance;
                     $sup->advance = 0;
                     $sup->due = $lower_advance;
                 }
                 if($sup->advance >= $dues->payment){
                     $sup->advance = $sup->advance - $dues->payment;
                 }
                 $sup->save();
                 $bank = Bank::find($tran->bank);
                 $bank->opening_amount = $bank->opening_amount + $dues->payment;
                 $bank->save();
            }

            Transaction::find($tran->id)->forceDelete();
            Due::find($due_id)->forceDelete();

            return back()->with('delete_status', 'Your Due Permanently Deleted Successfully!');

    }






    public function Selected(Request $request)
    {
        if ($request->id == 1) {
            $selected = Customar::orderBy('id')->where('business', Session::get('business_id'))->get();
            $stringToSend = "";
            foreach ($selected as $select) {

                $stringToSend .= "<option value='" . $select->id . "'>" . $select->customar_name . "</option>";
            }
        } else if ($request->id == 2) {
            $selected = Supplier::orderBy('id')->where('business', Session::get('business_id'))->get();
            $stringToSend = "";
            foreach ($selected as $select) {

                $stringToSend .= "<option value='" . $select->id . "'>" . $select->supplier_name . "</option>";
            }
        }
        return $stringToSend;
    }
    public function SupplierDue(Request $request)
    {
        // echo $request->select;
        if ($request->select == 1) {
            $cus_due = Customar::find($request->id);
            $due = $cus_due->due;
            $stringToSend = "";
            $stringToSend .= $due;
        } else if ($request->select == 2) {
            $sup_due = Supplier::find($request->id);
            $due = $sup_due->due;
            $stringToSend = "";
            $stringToSend .= $due;
        }
        return $stringToSend;
    }
}
