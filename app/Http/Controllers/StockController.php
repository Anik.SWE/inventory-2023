<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Category;

class StockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Category Form:
    public function ViewStock()
    {
        return view('Business.Stock.view_stock', [
            'Total_Stock_Item' => Product::orderBy('user_id')->where('user_id', Auth::id())->count(),
            'Category_info' => Category::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Total_Stock' => Product::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }
}
// if (Auth::guard('admin'))
// {{ Auth::guard('admin')->user()->name }}
// else
// {{ Auth::user()->name }}
// endif
// if () {
//     # code...
// } else {
//     # code...
// }

