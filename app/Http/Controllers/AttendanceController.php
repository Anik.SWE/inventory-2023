<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Attendance Form:
    public function Attendance_Form()
    {
        return view('Business.Attendance.add_attendance_form',[
            'Total_Employee' => Employee::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Attendance_Date' => DB::table('attendances')->select('edit_date')->groupBy('edit_date')->where('user_id', Auth::id())->get()
        ]);
    }
    // Attendance Data Store:
    public function AttendanceDataStore(Request $request)
    {
        $date = $request->att_date;
        $att_date = DB::table('attendances')->where('att_date', $date)->first();
        if ($att_date) {
            return back();
        } else {
            foreach ($request->emp_id as $id) {
                $data[] = [
                    'emp_id' => $id,
                    'attendance' => $request->attendance[$id],
                    'att_date' => $request->att_date,
                    'att_year' => $request->att_year,
                    'edit_date' => date("d_m_y"),
                    'user_id' => Auth::id(),
                    'created_at' => Carbon::now()
                ];
            }
        }

        $att = DB::table('attendances')->insert($data);
        return back();
    }

    // Attendance Edit:
    public function EditAttendance($edit_date)
    {
        return view('Business.Attendance.edit_attendance',[
            'Attendance_Date_info' => DB::table('attendances')->where('edit_date', $edit_date)->where('user_id', Auth::id())->get()
        ]);
    }
    // Attendance Update:
    public function DeleteAttendance($edit_date)
    {
        // dd($request->attendance);
        $delete = DB::table('attendances')->where('edit_date', $edit_date)->where('user_id', Auth::id())->get();
        $delete->forceDelete();
        return back();
    }
}
