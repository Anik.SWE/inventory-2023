<?php

namespace App\Http\Controllers;

use App\Models\HouseRange;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HouseRangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Show House Range:
    public function index()
    {
        return view('Business.HouseRange.add_houserange_form',[
            'Range_info' => HouseRange::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }

    // Insert House Range Data:
    public function DataStore(Request $request)
    {
        HouseRange::insert([
            'shop_name' => $request->shop_name,
            'owner_name' => $request->owner_name,
            'start_month' => $request->start_month,
            'advance' => $request->advance,
            'salary_range' => $request->salary_range,
            'status' => $request->status,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }
    // Edit House Range:
    public function EditHouseRange($id)
    {
        return view('Business.HouseRange.edit_houserange_form',[
            'Range_info' => HouseRange::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'House_info' => HouseRange::find($id)
        ]);
    }
    // Update House Range:
    public function UpdateHouseRange(Request $request)
    {
        HouseRange::find($request->id)->update([
            'shop_name' => $request->shop_name,
            'owner_name' => $request->owner_name,
            'start_month' => $request->start_month,
            'advance' => $request->advance,
            'salary_range' => $request->salary_range,
            'status' => $request->status,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }
    // Delete House Range:
    public function DeleteHouseRange($id)
    {
        HouseRange::find($id)->forceDelete();
        return back()->with('delete_status', 'Your House Range Permanently Deleted Successfully!');
    }
    // Single House Range:
    public function HouseRangeView($id)
    {
        return view('Business.HouseRange.single_houserange',[
            'house_range' => HouseRange::where('id', $id)->get(),
            'range' => HouseRange::find($id)
        ]);
    }
}
