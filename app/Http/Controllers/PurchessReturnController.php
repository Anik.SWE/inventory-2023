<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Product;
use App\Models\SaleReturn;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Bank;
use App\Models\Transaction;
use App\Models\PurchessReturn;
use App\Models\ProductPurchase;
use Session;
class PurchessReturnController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Category Form:
    public function PurchessReturnForm($ids)
    {    
        return view('Business.Return.view_purchessreturn', [
            'Supplier_info' => Supplier::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Return_Product' => PurchessReturn::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'banks' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(), 
            'purchase' => ProductPurchase::find($ids)
           
        ]);
    }
    public function PurchessReturn()
    {      
        return view('Business.Return.view_purchessreturn', [
            'Supplier_info' => Supplier::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Total_Product' => Product::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'Return_Product' => PurchessReturn::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'banks' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->get(),
            'purchase' => ProductPurchase::find(0)
        ]);
    }

    public function PurchessReturnFormPost(Request $request)
    {
        $Product_Quantity = $request->product_quantity;
        $find_product = Product::find($request->pro_id)->product_quantity;
        $valid = DB::table('purchess_returns')->where('purchase' , $request->pur_id)->sum('product_quantity') + $request->product_quantity;
        $pr_quantity = ProductPurchase::find($request->pur_id);
        if ( $valid <=  $pr_quantity->quantity )
        {
          
            if($find_product >= $Product_Quantity){

                PurchessReturn::insert([
                    'purchase' => $request->pur_id,
                    'product_quantity' => $request->product_quantity,
                    'product_price' => $request->product_price,
                    'total_amount' => $request->total_amount,
                    'bank' => $request->bankr,
                    'payment' => $request->payment,
                    'change' => $request->change,
                    'business' => Session::get('business_id'),
                    'user_id' => Auth::id(),
                    'created_at' => Carbon::now()
                ]);
                DB::table('products')->where('id', $request->pro_id)->update([
                    'product_quantity' => ($find_product - $Product_Quantity)
                ]);


                $Banks = Bank::find($request->bankr);
                if ($request->bankr) {

                    $PurchessReturn = PurchessReturn::all();
                    $PurchessReturn_id = 0;
                    foreach ($PurchessReturn->reverse() as $return_value) {
                        if ($return_value->business == Session::get('business_id')) {
                            $PurchessReturn_id = $return_value->id;
                            break;
                        }
                    }

                    if ($request->change > 0) {
                        $sup = Supplier::find($request->supplier_id);

                        if ($sup->advance > 0) {
                            $sup->advance = $sup->advance + $request->change;
                        }
                        if (!$sup->due && !$sup->advance) {
                            $sup->advance = $request->change;
                        }

                        if ($sup->due > 0) {
                            if ($sup->due < $request->change) {
                                $lower_due = $request->change - $sup->due;
                                $sup->due = 0;
                                $sup->advance = $lower_due;
                            }
                            if ($sup->due >= $request->change) {
                                $sup->due = $sup->due - $request->change;
                            }
                        }
                        $sup->save();
                    }

                    if ($request->change < 0) {
                        $sup = Supplier::find($request->supplier_id);
                        $get_advance = $request->payment - $request->total_amount;

                        if ($sup->due > 0) {
                            $sup->due = $sup->due + $get_advance;
                        }
                        if (!$sup->due && !$sup->advance) {
                            $sup->due = $get_advance;
                        }

                        if ($sup->advance > 0) {
                            if ($sup->advance < $get_advance) {
                                $lower_advance = $get_advance - $sup->advance;
                                $sup->advance = 0;
                                $sup->due = $lower_advance;
                            }
                            if ($sup->advance >= $get_advance) {
                                $sup->advance = $sup->advance - $get_advance;
                            }
                        }
                        $sup->save();
                    }

                    if ($request->payment) {
                        $post_data['tran_id'] = uniqid();
                        Transaction::insert([
                            'bank_name' => $Banks->bank_name,
                            'bank' => $Banks->id,
                            'operation_type' => "Auto",
                            'account_number' => $Banks->account_number,
                            'balance' => $request->payment,
                            'media' => "Purchase Return",
                            'purchase_return' => $PurchessReturn_id,
                            'transaction_id' => $post_data['tran_id'],
                            'transaction_date' => date('y-m-d'),
                            'business' => Session::get('business_id'),
                            'user_id' => Auth::id(),
                            'created_at' => Carbon::now()
                        ]);
                        $Banks->opening_amount = $Banks->opening_amount + $request->payment;
                        $Banks->save();
                    }

                }

                if (!$request->bankr) {

                    if ($request->change > 0) {
                        $sup = Supplier::find($request->supplier_id);

                        if ($sup->advance > 0) {
                            $sup->advance = $sup->advance + $request->change;
                        }
                        if (!$sup->due && !$sup->advance) {
                            $sup->advance = $request->change;
                        }

                        if ($sup->due > 0) {
                            if ($sup->due < $request->change) {
                                $lower_due = $request->change - $sup->due;
                                $sup->due = 0;
                                $sup->advance = $lower_due;
                            }
                            if ($sup->due >= $request->change) {
                                $sup->due = $sup->due - $request->change;
                            }
                        }
                        $sup->save();
                    }

                    if ($request->change < 0) {
                        $sup = Supplier::find($request->supplier_id);
                        $get_advance = $request->payment - $request->total_amount;

                        if ($sup->due > 0) {
                            $sup->due = $sup->due + $get_advance;
                        }
                        if (!$sup->due && !$sup->advance) {
                            $sup->due = $get_advance;
                        }

                        if ($sup->advance > 0) {
                            if ($sup->advance < $get_advance) {
                                $lower_advance = $get_advance - $sup->advance;
                                $sup->advance = 0;
                                $sup->due = $lower_advance;
                            }
                            if ($sup->advance >= $get_advance) {
                                $sup->advance = $sup->advance - $get_advance;
                            }
                        }
                        $sup->save();
                    }

                }
            }
            else{
                return redirect()->back()->withErrors([' Quantity is more than stock ! ']);
            }

        }
        else{
            return redirect()->back()->withErrors([' Quantity is more than purchase ! ']);    
        }


        return back();
    }


    // Edit Product
    public function EditReturnProduct($product_id)
    {
        return view('Business.Return.edit_purchessreturn', [
            'info' => PurchessReturn::find($product_id),
            'Supplier_info' => Supplier::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Total_Product' => Product::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Return_Product' => SaleReturn::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'banks' => Bank::orderBy('user_id')->where('user_id', Auth::id())->get(),
        ]);
    }



    // Update Product Information
    public function EditReturnFormPost(Request $request)
    {
        PurchessReturn::find($request->id)->update([
            'product_name' => $request->product_name,
            'supplier_id' => $request->supplier_id,
            'product_quantity' => $request->product_quantity,
            'product_price' => $request->product_price,
            'total_amount' => $request->total_amount,
            'payment_type' => $request->payment_type,
            'payment' => $request->payment,
            'change' => $request->change,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }


    // Delete Product Information
    public function DeleteReturnProduct($purchase_return_id)
    {
        
        $purchase_return = PurchessReturn::find($purchase_return_id);
        $purchases = ProductPurchase::find($purchase_return->purchase);



        if($purchase_return->payment > 0){ 
            $transaction = DB::table('transactions')->where('purchase_return', $purchase_return_id)->first();

                $bank = Bank::find($transaction->bank);
        
            if($bank->opening_amount >= $transaction->balance){
                $bank->opening_amount = $bank->opening_amount - $transaction->balance;
                $bank->save();
    
                $product = Product::find($purchases->product);
                $product->product_quantity = $product->product_quantity + $purchase_return->product_quantity;
                $product->save();
    
                if($purchase_return->change > 0){
                    $sup = Supplier::find($purchases->supplier_id);   
                    if($sup->due > 0){
                        $sup->due = $sup->due + $purchase_return->change;
                    } 
                    if($sup->advance < $purchase_return->change && $sup->advance > 0){
                        $lower_advance = $purchase_return->change - $sup->advance;
                        $sup->advance = 0;
                        $sup->due = $lower_advance;
                    }
                    if(!$sup->due && !$sup->advance){
                        $sup->due = $purchase_return->change;
                    }
                    if($sup->advance >= $purchase_return->change){
                       $sup->advance = $sup->advance - $purchase_return->change;
                    }
               
                    $sup->save();
                }
    
                if($purchase_return->change < 0){
                    $sup = Supplier::find($purchases->supplier_id); 
                    $get_advance = $purchase_return->payment - $purchase_return->total_amount; 
                    if($sup->advance > 0){
                        $sup->advance = $sup->advance + $get_advance;
                    } 
                    if($sup->due < $get_advance && $sup->due > 0){
                        $lower_due = $get_advance - $sup->due;
                        $sup->due = 0;
                        $sup->advance = $lower_due;
                    }
                    if(!$sup->due && !$sup->advance){
                        $sup->advance = $get_advance;
                    }
                    if($sup->due >= $get_advance){
                       $sup->due = $sup->due - $get_advance;
                    }
                
                    $sup->save();
                }
        
                Transaction::find($transaction->id)->forceDelete();
                PurchessReturn::find($purchase_return_id)->forceDelete();
                return back()->with('delete_status', 'Your Return Permanently Deleted Successfully!');
            }
            else{
                return redirect()->back()->withErrors([' insufficient balance delete failed right now! ']);
            }   
        }

        else{ 
            
            if($purchase_return->change > 0){
                $sup = Supplier::find($purchases->supplier_id);   
                if($sup->due > 0){
                    $sup->due = $sup->due + $purchase_return->change;
                } 
                if($sup->advance < $purchase_return->change && $sup->advance > 0){
                    $lower_advance = $purchase_return->change - $sup->advance;
                    $sup->advance = 0;
                    $sup->due = $lower_advance;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->due = $purchase_return->change;
                }
                if($sup->advance >= $purchase_return->change){
                   $sup->advance = $sup->advance - $purchase_return->change;
                }
           
                $sup->save();
            }

            if($purchase_return->change < 0){
                $sup = Supplier::find($purchases->supplier_id); 
                $get_advance = $purchase_return->payment - $purchase_return->total_amount; 
                if($sup->advance > 0){
                    $sup->advance = $sup->advance + $get_advance;
                } 
                if($sup->due < $get_advance && $sup->due > 0){
                    $lower_due = $get_advance - $sup->due;
                    $sup->due = 0;
                    $sup->advance = $lower_due;
                }
                if(!$sup->due && !$sup->advance){
                    $sup->advance = $get_advance;
                }
                if($sup->due >= $get_advance){
                   $sup->due = $sup->due - $get_advance;
                }
            
                $sup->save();
            }

            $product = Product::find($purchases->product);
            $product->product_quantity = $product->product_quantity + $purchase_return->product_quantity;
            $product->save();
            PurchessReturn::find($purchase_return_id)->forceDelete();
            return back()->with('delete_status', 'Your Return Permanently Deleted Successfully!');
        }
        

    }

}
