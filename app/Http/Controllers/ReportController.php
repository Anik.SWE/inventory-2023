<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Order;
use App\Models\SearchProduct;
use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('Business.Report.sale_report', [
            'sale_report' => Product::orderBy('user_id')->where('user_id', Auth::id())->get(),
        ]);
    }

    public function SingleSaleReport($product_id)
    {
        $find_product = Product::where('product_code', $product_id)->get();
        $saledetails_report = SearchProduct::where('product_code', $product_id)->get();

        return view('Business.Report.single_sale_report', [
            'single_sale_report' => $find_product,
            'saledetails_report' => $saledetails_report,
        ]);
    }

    public function ProductFilterFormPost(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $product_code = $request->product_code;

        // dd($request->all());
        $find_data = DB::table('search_products')->whereBetween('created_at', [$start_date, $end_date])->where('product_code', $product_code)->get();
        // dd($find_data);
        return view('Business.Report.single_product_report', [
            'search_details' => $find_data,
        ]);
    }


    // Sale And Purchase Report
    public function SaleandPurchase()
    {
        return view('Business.Report.sale_and_purchase_report', [
            'sale_report' => Product::orderBy('user_id')->where('user_id', Auth::id())->get(),
        ]);
    }
}
