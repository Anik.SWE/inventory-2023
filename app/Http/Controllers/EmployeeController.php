<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddEmployeeForm;
use App\Models\Business;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Add Employee Form:
    public function AddEmployee()
    {
        return view('Business.Employee.add_employee', [
            'business_id' => Business::orderBy('user_id')->where('user_id', Auth::id())->get(),
        ]);
    }

    // Employee Data Store Form:
    public function DataStore(AddEmployeeForm $request)
    {
        if ($request->hasFile('employee_photo')) {
            $file = $request->file('employee_photo');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $location = 'public/uploads/employee_photos/' . $fileName;
            Image::make($file)->save(base_path($location));
        }
        Employee::insert([
            'employee_name' => $request->employee_name,
            'employee_email' => $request->employee_email,
            'phone_number' => $request->phone_number,
            'location' => $request->location,
            'city' => $request->city,
            'address' => $request->address,
            'zip' => $request->zip,
            'experience' => $request->experience,
            'nid' => $request->nid,
            'salary' => $request->salary,
            'vacation' => $request->vacation,
            'employee_photo' => $fileName,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back()->with('add_status', 'hello');
    }

    // All Employee:
    public function AllEmployee()
    {
        return view('Business.Employee.all_employee', [
            'Total_Employee' => Employee::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Count' => Employee::orderBy('user_id')->where('user_id', Auth::id())->count()
        ]);
    }

    // Edit Employee
    public function EditEmployee($employee_id)
    {
        return view('Business.Employee.edit_employee', [
            'employee_info' => Employee::find($employee_id)
        ]);
    }

    // Update Employee Information
    public function EditEmployeeFormPost(Request $request, $employee_id)
    {

        $delete = Employee::find($employee_id)->first();
        $old_photo = 'public/uploads/employee_photos/' . $delete->employee_photo;
        if ($request->hasFile('employee_photo')) {
            if ($request->employee_photo != $delete->employee_photo) {
                unlink(base_path($old_photo));
            }

            $file = $request->file('employee_photo');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $location = 'public/uploads/employee_photos/' . $fileName;
            Image::make($file)->save(base_path($location));
            Employee::find($employee_id)->update([
                'employee_name' => $request->employee_name,
                'employee_email' => $request->employee_email,
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'city' => $request->city,
                'address' => $request->address,
                'zip' => $request->zip,
                'experience' => $request->experience,
                'nid' => $request->nid,
                'salary' => $request->salary,
                'vacation' => $request->vacation,
                'employee_photo' => $fileName
            ]);
            return back()->with('add_status', 'hello');
        } else {
            Employee::find($employee_id)->update([
                'employee_name' => $request->employee_name,
                'employee_email' => $request->employee_email,
                'phone_number' => $request->phone_number,
                'location' => $request->location,
                'city' => $request->city,
                'address' => $request->address,
                'zip' => $request->zip,
                'experience' => $request->experience,
                'nid' => $request->nid,
                'salary' => $request->salary,
                'vacation' => $request->vacation,
                'employee_photo' => $delete->employee_photo
            ]);
            return back()->with('add_status', 'hello');
        }
    }

    // Delete Employee Information
    public function DeleteEmployee($employee_id)
    {
        $delete = Employee::find($employee_id)->first();
        $old_photo = 'public/uploads/employee_photos/' . $delete->employee_photo;
        unlink(base_path($old_photo));
        $delete->forceDelete();
        return back()->with('delete_status', 'Your Business Permanently Deleted Successfully!');
    }

    // View Single Employee
    public function ViewSingleEmployee($employee_id)
    {
        return view('Business.Employee.single_employee_view', [
            'single_employee_view' => Employee::find($employee_id)
        ]);
    }
}
