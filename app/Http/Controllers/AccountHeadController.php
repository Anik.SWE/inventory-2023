<?php

namespace App\Http\Controllers;

use App\Models\AccountHead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\FinancialConnections\Account;

class AccountHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $accountHead = AccountHead::where('user_id', Auth::user()->id)->where('business_id', session()->get('business_id'))->get();
        return view("Business.Personal_Account.accounts_head", compact('accountHead'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (isset($request->id)) {
            $acc =  AccountHead::find($request->id);
        } else {
            $acc = new AccountHead();
        }

        $acc->head_name = $request->head_name;
        $acc->main_head_id = $request->main_head_id;
        $acc->user_id = Auth::user()->id;
        $acc->business_id = session()->get('business_id');
        $acc->save();
        return back()->with("status", "Head Added");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AccountHead  $accountHead
     * @return \Illuminate\Http\Response
     */
    public function show(AccountHead $accountHead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AccountHead  $accountHead
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountHead $accountHead)
    {
        //
        $accountHead->delete();
        return back()->with("delete_status", "Head Added");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AccountHead  $accountHead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountHead $accountHead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AccountHead  $accountHead
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountHead $accountHead)
    {
        //
        dd($accountHead);
    }
    public function getSubHead(Request $request)
    {
        return AccountHead::where("main_head_id", $request->head_id)->get();
    }
}
