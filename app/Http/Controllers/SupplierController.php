<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddSupplierForm;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class SupplierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Add Suppliers Form:
    public function AddSupplier()
    {
        return view('Business.Supplier.add_supplier_form');
    }

    // Supplier Data Store Form:
    public function SupplierDataStore(AddSupplierForm $request)
    {
        // if ($request->hasFile('supplier_photo')) {
        //     $file = $request->file('supplier_photo');
        //     $fileName = time() . '.' . $file->getClientOriginalExtension();
        //     $location = 'public/uploads/supplier_photos/' . $fileName;
        //     Image::make($file)->save(base_path($location));
        // }

        $dues = 0;
        $advances = 0;
        if($request->amount_type == "Due"){
            $dues = $request->opening_amount;
        }
        if($request->amount_type == "Advance"){
            $advances = $request->opening_amount;
        }

        Supplier::insert([
            'supplier_name' => $request->supplier_name,

            'due' => $dues,
            'advance' => $advances,

            'supplier_email' => $request->supplier_email,
            'phone_number' => $request->phone_number,
            'supplier_type' => $request->supplier_type,
            'supplier_shop_name' => $request->supplier_shop_name,
            'supplier_address' => $request->supplier_address,
            'account_holder' => $request->account_holder,
            'account_number' => $request->account_number,
            'bank_name' => $request->bank_name,
            'branch_name' => $request->branch_name,
            'business' => $request->business_ids,
            'city' => $request->city,
            // 'supplier_photo' => $fileName,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back()->with('add_status', 'hello');
    }

    // All Supplier:
    public function AllSupplier()
    {
        return view('Business.Supplier.all_supplier', [
            'Total_Supplier' => Supplier::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Count' => Supplier::orderBy('user_id')->where('user_id', Auth::id())->count()
        ]);
    }

    // Edit Supplier
    public function EditSupplier($supplier_id)
    {
        return view('Business.Supplier.edit_supplier', [
            'supplier_info' => Supplier::find($supplier_id)
        ]);
    }

    // Update Supplier Information
    public function EditSupplierFormPost(Request $request, $supplier_id)
    {

        // $delete = Supplier::find($supplier_id)->first();
        // $old_photo = 'public/uploads/supplier_photos/' . $delete->supplier_photo;
        // if ($request->hasFile('supplier_photo')) {
        //     if ($request->supplier_photo != $delete->supplier_photo) {
        //         unlink(base_path($old_photo));
        //     }

        //     $file = $request->file('supplier_photo');
        //     $fileName = time() . '.' . $file->getClientOriginalExtension();
        //     $location = 'public/uploads/supplier_photos/' . $fileName;
        //     Image::make($file)->save(base_path($location));
            Supplier::find($supplier_id)->update([
                'supplier_name' => $request->supplier_name,
                'supplier_email' => $request->supplier_email,
                'phone_number' => $request->phone_number,
                'supplier_type' => $request->supplier_type,
                'supplier_shop_name' => $request->supplier_shop_name,
                'supplier_address' => $request->supplier_address,
                'account_holder' => $request->account_holder,
                'account_number' => $request->account_number,
                'bank_name' => $request->bank_name,
                'branch_name' => $request->branch_name,
                'city' => $request->city,
                // 'supplier_photo' => $fileName,
            ]);
            return back()->with('add_status', 'hello');
        // } else {
        //     Supplier::find($supplier_id)->update([
        //         'supplier_name' => $request->supplier_name,
        //         'supplier_email' => $request->supplier_email,
        //         'phone_number' => $request->phone_number,
        //         'supplier_type' => $request->supplier_type,
        //         'supplier_shop_name' => $request->supplier_shop_name,
        //         'supplier_address' => $request->supplier_address,
        //         'account_holder' => $request->account_holder,
        //         'account_number' => $request->account_number,
        //         'bank_name' => $request->bank_name,
        //         'branch_name' => $request->branch_name,
        //         'city' => $request->city,
        //         'supplier_photo' => $delete->supplier_photo
        //     ]);
        //     return back()->with('add_status', 'hello');
        // }
    }

    // Delete Supplier Information
    public function DeleteSupplier($supplier_id)
    {

        Supplier::find($supplier_id)->forceDelete();

        return back()->with('delete_status', 'Your Supplier Permanently Deleted Successfully!');
    }

    // View Single Supplier
    public function ViewSingleSupplier($supplier_id)
    {
        return view('Business.Supplier.single_supplier_view', [
            'single_supplier_view' => Supplier::find($supplier_id)
        ]);
    }
}
