<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\Bank;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;
use function PHPUnit\Framework\at;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Transaction Form:
    public function AddTransaction()
    {
        return view('Business.Transaction.add_transaction_form', [
            'order_info' => Order::orderBy('user_id')->where('business', Session::get('business_id'))->latest('id')->get(),
            'bank_info' => Bank::orderBy('user_id')->where('business', Session::get('business_id'))->latest('id')->get(),
            'transaction_info' => Transaction::orderBy('user_id')->where('business', Session::get('business_id'))->get()
        ]);
    }


    // Transaction Post Form:
    public function TransactionFormPost(TransactionForm $request)
    {
        $bank =  Bank::all();
        $bank_nm="";
        $bank_id="";
        foreach ($bank->reverse() as $bank_value){ 
            if($bank_value->business == Session::get('business_id') && $bank_value->id == $request->bank_name){
                $bank_nm = $bank_value->bank_name;
                $bank_id = $bank_value->id;
                break;
            }                   
        } 
        if($bank_nm){
            $post_data['tran_id'] = uniqid();
        }
        Transaction::insert([
            'bank_name' => $bank_nm,
            'bank' => $bank_id,
            'operation_type' => $request->operation_type,
            'account_number' => $request->account_number,
            'balance' => $request->balance,
            'media' => $request->media,
            'transaction_id' => $post_data['tran_id'],
            'transaction_date' => date('y-m-d'),
            'business' => Session::get('business_id'),
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        $balance = DB::table('banks')->where('user_id', Auth::id())->where('account_number',$request->account_number)->sum('opening_amount');
        if ($request->operation_type == "Withdraw") {
            Bank::where('account_number',$request->account_number)->update([
                'opening_amount' => ($balance - $request->balance)
            ]);


        } else if($request->operation_type == "Deposit"){
            Bank::where('account_number',$request->account_number)->update([
                'opening_amount' => ($balance + $request->balance)
            ]);
        }

        return back();
    }

    // Transaction Form:
    public function ViewSingleBankTransaction($bank_id)
    {
        return view('Business.Transaction.bank_transaction_table', [
            'bank_transaction_view' => DB::table('transactions')->where('bank', $bank_id)->get(),
            'bank_tid' => $bank_id
        ]);
    }

    // Transaction Form:
    public function FilterFormPost(Request $request)
    {
        $this->validate($request,[
            'start_date'=>'required',
            'end_date'=>'required'
         ]);
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $bank_id = $request->bank_id;

        // dd($request->all());
        $find_data = DB::table('transactions')->whereBetween('created_at', [$start_date, $end_date])->where('bank_name', $bank_id)->get();
        return view('Business.Transaction.filter_transaction_table', [
            'find_data' => $find_data
        ]);
    }

    public function transactionDelete($tr_id)
    {
        
        $tran = Transaction::find($tr_id);
        $bank = bank::find($tran->bank);

        if($tran->operation_type == "Deposit"){
            if($bank->opening_amount >= $tran->balance){
                $bank->opening_amount = $bank->opening_amount - $tran->balance;
                $bank->save();
            }
            else{
                return redirect()->back()->withErrors([' Deposit is more than balance ! ']);
            }

        }
        if($tran->operation_type == "Withdraw"){ 
            $bank->opening_amount = $bank->opening_amount + $tran->balance;
            $bank->save();     
        }


        Transaction::find($tr_id)->forceDelete();
        return back()->with('delete_status', 'Deleted Successfully!');
    }

}
