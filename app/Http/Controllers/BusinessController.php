<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddBusinessForm;
use App\Models\Business;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Session;

class BusinessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    // Business Dashboard:
    public function Dashboard()
    {
        return view('Business.view_business');
    }
    // Add Business Form:
    public function AddBusiness()
    {
        return view('Business.business_form');
    }

    // Verificaton Account:
    // public function VerifyAccount(Request $request)
    // {
        // echo Auth::user()->password;
    //     if ($request->email == Auth::user()->email) {
    //         return view('Business.view_business');
    //     } else {
    //         return back()->with('error_status', 'Your Email ID is not Match!');
    //     }
    // }

    // Insert Business:
    public function BusinessFormPost(AddBusinessForm $request)
    {
        Business::insert([
            'business_name' => $request->business_name,
            'business_email' => $request->business_email,
            'business_location' => $request->business_location,
            'city' => $request->city,
            'zip' => $request->zip,
            'state' => $request->state,
            'user_id' => Auth::id(),
            // 'business_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back()->with('add_status', $request->business_name);
    }

    // Edit Business
    public function EditBusiness($business_id)
    {
        return view('Business.edit_business', [
            'business_info' => Business::find($business_id)
        ]);
    }

    // Update Business Information
    public function EditBusinessFormPost(Request $request)
    {
        Business::find($request->id)->update([
            'business_name' => $request->business_name,
            'business_email' => $request->business_email,
            'business_location' => $request->business_location,
            'city' => $request->city,
            'zip' => $request->zip,
            'state' => $request->state
        ]);
        return redirect('/home');
    }

    // Delete Business Information
    public function DeleteBusiness($business_id)
    {
        Business::find($business_id)->forceDelete();
        return back()->with('delete_status', 'Your Business Permanently Deleted Successfully!');
    }

    // View All Business
    public function VerifyBusiness($business_id)
    {
        Session::put('business_id', $business_id); 
        return view('Business.view_business', [
            'view_business' => Business::find($business_id)
        ]);
    }
}
