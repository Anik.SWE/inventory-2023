<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Requests\AddCategoryForm;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Category Form:
    public function AddCategory()
    {
        return view('Business.Category.add_category_form',[
            'Total_Category' => Category::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }
    // Insert Category:
    public function CategoryFormPost(AddCategoryForm $request)
    {
        Category::insert([
            'category_name' => $request->category_name,
            'category_description' => $request->category_description,
            'business' => $request->business_ids,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }

    // Edit Category:
    public function EditCategory($category_id)
    {
        return view('Business.Category.edit_category',[
            'Total_Category' => Category::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'Category_info' => Category::find($category_id)
        ]);
    }

    // Update Category Information
    public function EditCategoryFormPost(Request $request)
    {
        Category::find($request->id)->update([
            'category_name' => $request->category_name,
            'category_description' => $request->category_description
        ]);
        return back();
    }

    // Delete Category Information
    public function DeleteCategory($category_id)
    {
        Category::find($category_id)->forceDelete();
        return back()->with('delete_status', 'Your Category Permanently Deleted Successfully!');
    }
}
