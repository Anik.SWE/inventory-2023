<?php

namespace App\Http\Controllers;

use App\Models\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->admin_ind) {
            return view('admin.index');
        } else {
            return view('home', [
                'Total_Business' => Business::orderBy('user_id')->where('user_id', Auth::id())->paginate(5),
                'Count' => Business::orderBy('user_id')->where('user_id', Auth::id())->count()
            ]);
        }
    }
}
