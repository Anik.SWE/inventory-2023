<?php

namespace App\Http\Controllers;

use App\Models\Customar;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Due;
use App\Models\Product;
use App\Models\Supplier;
use Session;
use App\Models\ProductPurchase;

class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Customar Invoice List
    public function CustomarInvoice()
    {
        return view('Business.Invoice.customar_invoice', [
            // 'customar_invoice' =>Order::orderBy('customar_id')->where('user_id', Auth::id())->get(),
            'customar_invoice' => DB::table('orders')->select('customar_id')->groupBy('customar_id')->where('business', Session::get('business_id'))->get(),
        ]);
    }
    // Customar Invoice List
    public function SupplierInvoice()
    {
        return view('Business.Invoice.supplier_invoice', [
            // 'customar_invoice' =>Order::orderBy('customar_id')->where('user_id', Auth::id())->get(),
            'supplier_invoice' => DB::table('product_purchases')->select('supplier_id')->groupBy('supplier_id')->where('business', Session::get('business_id'))->get(),
        ]);
    }
    // View Invoice
    public function Invoice($invoice_id)
    {

        return view('Business.Invoice.invoice', [
            'order_details' => Order::orderBy('order_id', 'asc')->find($invoice_id),
        ]);
    }
    public function Supplier_Invoice($invoice_id)
    {
        return view('Business.Invoice.sup_invoice', [
            'supplier_invoice' => DB::table('product_purchases')->where('id', $invoice_id)->where('business', Session::get('business_id'))->get(),
        ]);
    }

    // SingleCustomarInvoice
    public function SingleCustomarInvoice($customar_id)
    {
        return view('Business.Invoice.single_customar_invoice', [
            'customar_info' => Customar::find($customar_id),
            'fixed_customar_order' => DB::table('orders')->where('customar_id', $customar_id)->where('business', Session::get('business_id'))->get(),
            'fixed_customar_ledger' => DB::table('orders')->where('customar_id', $customar_id)->where('business', Session::get('business_id'))->get(),
            'due_ledgers' => DB::table('dues')->where('customar_id', $customar_id)->where('business', Session::get('business_id'))->get(),
        ]);
    }
    // SingleCustomarInvoice
    public function SingleSupplierInvoice($supplier_id)
    {
        return view('Business.Invoice.single_supplier_invoice', [
            'supplier_info' => Supplier::find($supplier_id),
            'fixed_supplier_order' => DB::table('product_purchases')->where('supplier_id', $supplier_id)->where('business', Session::get('business_id'))->get(),
            'due_ledgers' => DB::table('dues')->where('supplier_id', $supplier_id)->where('business', Session::get('business_id'))->get(),

        ]);
    }
}
