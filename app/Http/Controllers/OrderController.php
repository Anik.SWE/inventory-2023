<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Helpers\Order as HelpersOrder;
use App\Http\Requests\AddOrderForm;
use App\Models\Customar;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Bank;
use App\Models\CustomarLedger;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\SearchProduct;
use App\Models\Transaction;
use App\Traits\CommonTraits;
use Illuminate\Support\Arr;
use Session;

class OrderController extends Controller
{
    use CommonTraits;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Order Data Store Form:
    public function OrderDataStore(Request $request)
    {

        $this->validate($request, [
            'order_date' => 'required'
        ]);
        $Order_code = HelpersOrder::IDGenerator(new Order, 'order_id', 8, 'Order');



            $pre_due = 0;
            $cur_due = 0;
            $pre_advnc = 0;
            $cur_advnc = 0;

            $cus_predue = Customar::find($request->select_customer);
            $pre_due = $cus_predue->due;
            $pre_advnc = $cus_predue->advance;



            if($request->change > 0){
                if($pre_due > 0){
                    $cur_due = $pre_due + $request->change;
                }
                if(!$pre_due && !$pre_advnc){
                    $cur_due = $request->change;
                }
                if($pre_advnc > 0){
                    if($pre_advnc < $request->change){
                        $lower_advance = $request->change - $pre_advnc;
                        $cur_advnc = 0;
                        $cur_due = $lower_advance;
                    }
                    if($pre_advnc >= $request->change){
                        $cur_advnc = $pre_advnc - $request->change;
                    }
                }
            }

            if($request->change < 0){
                $get_advance = $request->payment - $request->total;

                if($pre_advnc > 0){
                   $cur_advnc = $pre_advnc + $get_advance;
                }
                if(!$pre_due && !$pre_advnc){
                    $cur_advnc = $get_advance;
                }

                if($pre_due > 0){
                    if($pre_due < $get_advance){
                        $lower_due = $get_advance - $pre_due;
                        $cur_due = 0;
                        $cur_advnc = $lower_due;
                    }
                    if($pre_due >= $get_advance){
                        $cur_due = $pre_due - $get_advance;
                    }
                }
            }





        $product_cod = $request->product_code;
        $order_quantity = $request->product_quantity;

          $i = 0;

          foreach ($product_cod as $code) {
            $i++;
          }

          for ($row = 0; $row < $i; $row++){
            $find_product = DB::table('products')->where('product_code', $product_cod[$row])->first();
            DB::table('products')->where('product_code', $product_cod[$row])->update(['product_quantity' => $find_product->product_quantity - $order_quantity[$row]]);
          }


        $order = new Order;
        $order->order_id = $Order_code;
        $order->order_date = $request->order_date;
        $order->customar_id = $request->select_customer;

        $order->product_code = json_encode($request->product_code);
        $order->product_quantity = json_encode($request->product_quantity);
        $order->product_price = json_encode($request->product_price);
        $order->sub_price = json_encode($request->sub_price);
        $order->tax = $request->tax;
        if($pre_due > 0){
            $order->previous_due = $pre_due;
        }
        if($cur_due > 0){
            $order->current_due = $cur_due;
        }
        if($request->total == $request->payment && $pre_due){
            $order->current_due = $pre_due;
        }
        if($pre_advnc > 0){
            $order->previous_advance = $pre_advnc;
        }
        if($cur_advnc > 0){
            $order->current_advance = $cur_advnc;
        }
        if($request->total == $request->payment && $pre_advnc){
            $order->current_advance = $pre_advnc;
        }
        $order->shipping_cost = $request->ship_cost;
        $order->discount = $request->discount;
        $order->total = $request->total;
        $order->payment_type = json_encode($request->payment_type);
        $order->payment = $request->payment;
        $order->sub_total = $request->sub_total;
        $order->change = $request->change;
        // $order->transaction_id = $request->transaction_id;
        $order->business = Session::get('business_id');
        $order->user_id = Auth::id();
        $order->created_at = Carbon::now();
        $order->save();

        $ProductSell =  Order::all();
        $ProductSell_id = 0;
        foreach ($ProductSell->reverse() as $sell_value){
            if($sell_value->business == Session::get('business_id')){
                $ProductSell_id = $sell_value->id;
                break;
            }
        }

        $Banks = Bank::find($request->payment_type);

        if($request->change > 0){
            $cusdue = Customar::find($request->select_customer);

            if($cusdue->due > 0){
               $cusdue->due = $cusdue->due + $request->change;
            }
            if(!$cusdue->due && !$cusdue->advance){
                $cusdue->due = $request->change;
            }

            if($cusdue->advance > 0){
                if($cusdue->advance >= $request->change){
                    $cusdue->advance = $cusdue->advance - $request->change;
                }
                if($cusdue->advance < $request->change){
                    $lower_advance = $request->change - $cusdue->advance;
                    $cusdue->advance = 0;
                    $cusdue->due = $lower_advance;
                }
            }
            $cusdue->save();
        }

        if($request->change < 0){
            $cusdue = Customar::find($request->select_customer);
            $get_advance = $request->payment - $request->total;

            if($cusdue->advance > 0){
               $cusdue->advance = $cusdue->advance + $get_advance;
            }
            if(!$cusdue->due && !$cusdue->advance){
                $cusdue->advance = $get_advance;
            }

            if($cusdue->due > 0){
                if($cusdue->due < $get_advance){
                    $lower_due = $get_advance - $cusdue->due;
                    $cusdue->due = 0;
                    $cusdue->advance = $lower_due;
                }
                if($cusdue->due >= $get_advance){
                    $cusdue->due = $cusdue->due - $get_advance;
                }
            }
            $cusdue->save();
        }

        if($request->payment_type > 0){
            if ($request->payment > 0) {
                Transaction::insert([
                    'bank_name' => $Banks->bank_name,
                    'bank' => $Banks->id,
                    'operation_type' => "Auto",
                    'account_number' => $Banks->account_number,
                    'balance' => $request->payment,
                    'media' => "Product Sell",
                    'sell' => $ProductSell_id,
                    'transaction_id' => $request->transaction_id,
                    'transaction_date' => date('y-m-d'),
                    'business' => Session::get('business_id'),
                    'user_id' => Auth::id(),
                    'created_at' => Carbon::now()
                ]);
                $balance = DB::table('banks')->where('user_id', Auth::id())->where('id',$Banks->id)->sum('opening_amount');
                if ($request->operation_type == "Withdraw") {
                    Bank::where('account_number', $Banks->account_number)->update([
                        'opening_amount' => ($balance - $request->payment)
                    ]);


                }
                else if($request->operation_type == "Deposit"){
                    Bank::where('id',$Banks->id)->update([
                        'opening_amount' => ($balance + $request->payment)
                    ]);
                }
            }
        }

        //SMS
        $message = "আসসালামু আলাইকুম, আপনার বিল: " .$request->total."/- টাকা, পরিশোধিত: ". $request->payment.
        // " এবং বকেয়া আছে ".($request->total-$request->payment).
        "/- টাকা";

        $this->sendSMS( $cus_predue->phone_number,$message);


        return back();
    }
}
