<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddDocumentForm;
use App\Models\Personal;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PersonalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Personal Document Form:
    public function AddDocument()
    {
        return view('Business.Personal_Document.add_document_form', [
            'Total_Document' => Personal::orderBy('user_id')->where('user_id', Auth::id())->get()
        ]);
    }

    // Insert Personal Document:
    public function StoreDocument(AddDocumentForm $request)
    {
        // $document_photo = array();
        // if ($request->hasFile('image')) {
        //     $file = $request->file('image');
        //     $fileName = time() . '.' . $file->getClientOriginalExtension();
        //     $location = 'public/uploads/document_photos/' . $fileName;
        //     Image::make($file)->save(base_path($location));
        // }
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $location = 'public/uploads/document_photos/' . $fileName;
            Image::make($file)->save(base_path($location));
        }

        // $image = array();
        // if($files = $request->file('image')){
        //     foreach ($files as $file) {
        //         $image_name = md5(rand(1000, 10000));
        //         $ext = strtolower($file->getClientOriginalExtension());
        //         $image_full_name = $image_name.'.'.$ext;
        //         $upload_path = 'public/multiple_imager/';
        //         $image_url = $upload_path.$image_full_name;
        //         $file->move($upload_path, $image_full_name);
        //         $image[] = $image_url;
        //     }
        // }




        Personal::insert([
            'document_title' => $request->document_title,
            'document_des' => $request->document_des,
            'image' => $fileName,
            'business' => $request->business_ids,
            'user_id' => Auth::id(),
            'created_at' => Carbon::now()
        ]);
        return back();
    }

    // Edit Document
    public function EditDocument($document_id)
    {
        return view('Business.Personal_Document.edit_document', [
            'Total_Document' => Personal::orderBy('user_id')->where('user_id', Auth::id())->get(),
            'document_info' => Personal::find($document_id)
        ]);
    }

    // Update Employee Information
    public function EditDocumentFormPost(Request $request, $document_id)
    {

        $delete = Personal::find($document_id);
        $old_photo = 'public/uploads/document_photos/' . $delete->image;
        if ($request->hasFile('image')) {
            if ($request->image != $delete->image) {
                unlink(base_path($old_photo));
            }

            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $location = 'public/uploads/document_photos/' . $fileName;
            Image::make($file)->save(base_path($location));
            Personal::find($document_id)->update([
                'document_title' => $request->document_title,
                'document_des' => $request->document_des,
                'image' => $fileName,
            ]);
            return back()->with('add_status', 'hello');
        } else {
            Personal::find($document_id)->update([
                'document_title' => $request->document_title,
                'document_des' => $request->document_des,
                'image' => $delete->image
            ]);
            return back()->with('add_status', 'hello');
        }
    }

    // Delete Personal Information
    public function DeleteDocument($document_id)
    {
        $delete = Personal::find($document_id)->first();
        $old_photo = 'public/uploads/document_photos/' . $delete->image;
        unlink(base_path($old_photo));
        $delete->forceDelete();
        return back()->with('delete_status', 'Your Business Permanently Deleted Successfully!');
    }
    // View Personal Information
    public function ViewDocument($document_id)
    {
        return view('Business.Personal_Document.view_document',[
            "document_info" => Personal::find($document_id)
        ]);
    }
}
