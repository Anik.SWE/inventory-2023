<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\SMSConfig;
use App\Models\User;
use Illuminate\Http\Request;

class SMSConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sms_config = SMSConfig::all();
        return view("admin.sms_config_list",compact('sms_config'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::where("admin_ind", "!=", 1)->get();
        $business = Business::all();
        return view("admin.sms_config", compact('business', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (isset($request->id))
            $sms =  SMSConfig::find($request->id);
        else
            $sms = new SMSConfig();

        $sms->user_id  =  $request->user_id;
        $sms->business_id  =  $request->business_id;
        $sms->api_key  =  $request->api_key;
        $sms->caller_id  =  $request->caller_id;
        $sms->secretkey  =  $request->secretkey;
        $sms->url  =  $request->url;
        $sms->sms_rate  =  $request->sms_rate;
        $sms->save();
        $notification = array(
            'message' => 'Successfully Done',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SMSConfig  $sMSConfig
     * @return \Illuminate\Http\Response
     */
    public function show(SMSConfig $sMSConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SMSConfig  $sMSConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(SMSConfig $sMSConfig, $id)
    {
        //

        $sMSConfig = SMSConfig::find($id);
        $users = User::where("admin_ind", "!=", 1)->get();
        $business = Business::all();
        return view("admin.manage_config", compact('business', 'users','sMSConfig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SMSConfig  $sMSConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SMSConfig $sMSConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SMSConfig  $sMSConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(SMSConfig $sMSConfig)
    {
        //
    }
}
