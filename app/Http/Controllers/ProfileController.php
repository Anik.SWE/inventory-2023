<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        return view('Business.Profile.profile');
    }

    public function ProfileEdit(Request $request){
        $request->validate([
            'name' => 'required'
         ]);
         if(Auth::user()->updated_at->addDays(30) < Carbon::now()){
            User::find(Auth::id())->update([
                'name' => $request->name
            ]);
            return back()->with('name_change_status', 'Your name changed successfully!');
        }else{
            $left_days = Carbon::now()->diffInDays(Auth::user()->updated_at->addDays(30)) + 1;
            return back()->withErrors('You can change your name after '. $left_days .' days');
        }
    }

    public function PasswordEdit(Request $request){
        // dd($request->all());
        $request->validate([
            'old_password' => 'required',
            'password' => 'confirmed|min:8'
        ]);
        if (Hash::check($request->old_password, Auth::user()->password)){
            if($request->old_password == $request->password){
                return back()->with('password_error','This Password Alrady has been exist.');
            } else {
                User::find(Auth::id())->update([
                    'password' => Hash::make($request->password)
                ]);
                return back();
        }
        } else{
            return back()->withErrors('Your old password does not match');
        }
    }

}
