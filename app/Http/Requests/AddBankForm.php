<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBankForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_name' => 'required|min:2',
            'account_holder' => 'required',
            'account_number' => 'required',
            'account_group' => 'required',
            'account_type' => 'required',
            'opening_amount' => 'required',
        ];
    }
}
