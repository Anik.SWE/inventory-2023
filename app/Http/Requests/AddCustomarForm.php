<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCustomarForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customar_name' => 'required|min:3',
            'customar_email' => 'required',
            'phone_number' => 'required',
            //'location' => 'required',
           // 'city' => 'required',
            'address' => 'required',
            //'zip' => 'required',
           // 'bank_name' => 'required',
           // 'bank_account' => 'required',
            // 'customar_photo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
