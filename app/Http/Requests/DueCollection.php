<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DueCollection extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customar_id' => 'required',
            'operation_type' => 'required',
            'payment' => 'required',
            'media' => 'required',
            'due_date' => 'required',
        ];
    }
}
