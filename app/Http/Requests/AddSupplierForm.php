<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSupplierForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier_name' => 'required|min:3',
            'supplier_email' => '',
            'phone_number' => 'required',
            'supplier_type' => '',
            'supplier_shop_name' => '',
            'supplier_address' => '',
            'account_holder' => '',
            'account_number' => '',
            'bank_name' => '',
            'branch_name' => '',
            'city' => '',
            // 'supplier_photo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
