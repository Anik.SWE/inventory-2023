<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddEmployeeForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_name' => 'required|min:3',
            'employee_email' => 'required|unique:employees,employee_email',
            'phone_number' => 'required',
            'location' => 'required',
            'city' => 'required',
            'address' => 'required',
            'zip' => 'required',
            'experience' => 'required',
            'nid' => 'required|unique:employees,nid',
            'salary' => 'required',
            'vacation' => 'required',
            'employee_photo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ];
    }
}
