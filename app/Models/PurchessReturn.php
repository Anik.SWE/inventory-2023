<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchessReturn extends Model
{
    use HasFactory;
    protected $table = "purchess_returns";
}
