<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SMSConfig extends Model
{
    use HasFactory;

    public function user_rel()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function business_rel()
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }
}
