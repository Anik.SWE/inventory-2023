<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customar extends Model
{
    use HasFactory;
    protected $fillable = ['customar_name', 'customar_email', 'phone_number', 'location', 'city', 'address', 'zip', 'bank_name', 'bank_account', 'customar_photo'];
}
