<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HouseRange extends Model
{
    use HasFactory;
    protected $fillable = ['shop_name', 'owner_name', 'start_month', 'advance', 'salary_range', 'status'];
}
