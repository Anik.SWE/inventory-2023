<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PDO;

class PersonalAccountTransaction extends Model
{
    use HasFactory;

    public function account_head()
    {
        return $this->belongsTo(AccountHead::class, 'main_head_id', 'id');
    }
    public function sub_account_head()
    {
        return $this->belongsTo(AccountHead::class, 'sub_head_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }
}
