<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    protected $fillable = ['supplier_name', 'due', 'advance', 'supplier_email', 'phone_number', 'supplier_type', 'supplier_shop_name', 'supplier_address', 'account_holder', 'account_number', 'bank_name', 'branch_name', 'city', 'supplier_photo'];
}
