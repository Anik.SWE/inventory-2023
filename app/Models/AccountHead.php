<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountHead extends Model
{
    use HasFactory;

    public function account_head()
    {
        return $this->belongsTo(AccountHead::class, 'main_head_id', 'id');
    }
}
