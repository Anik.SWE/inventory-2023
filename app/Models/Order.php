<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = ['order_id','order_date', 'customar_id', 'product_name', 'product_code', 'product_quantity', 'product_price','sub_price', 'tax','discount', 'total', 'payment_type','payment','sub_total', 'change','status', 'currency'];
}
