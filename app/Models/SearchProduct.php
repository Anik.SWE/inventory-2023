<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchProduct extends Model
{
    use HasFactory;
    protected $fillable = ['product_code', 'product_quantity', 'product_price', 'customar_id', 'discount', 'payment', 'change', 'total'];
}
