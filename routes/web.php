<?php

use App\Http\Controllers\AccountHeadController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PosController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\Backend\UserController;
use Illuminate\Database\Seeder;
use App\Http\Controllers\DueController;
use App\Http\Controllers\CustomarController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PersonalAccountTransactionController;
use App\Http\Controllers\SMSConfigController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Frontend web links
Route::get('/', function () {
    return view('welcome');
});

// Login Web Links
Route::get('login', function () {
    return view('login');
});
Route::get('admin/login', function () {
    return view('adminlogin');
});
Route::get('register', function () {
    return view('register');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\UserController::class, 'index'])->name('user');
Route::get('/Dashboard', [App\Http\Controllers\BusinessController::class, 'Dashboard'])->name('dashboard');

// Business Route Start:
Route::get('/add/businessform', [App\Http\Controllers\BusinessController::class, 'AddBusiness'])->name('add');
Route::post('/business/formpost', [App\Http\Controllers\BusinessController::class, 'BusinessFormPost'])->name('formpost');
Route::get('/edit/business/{business_id}', [App\Http\Controllers\BusinessController::class, 'EditBusiness'])->name('Bedit');
Route::post('/edit/business/formpost', [App\Http\Controllers\BusinessController::class, 'EditBusinessFormPost'])->name('Beditformpost');
Route::get('/delete/business/{business_id}', [App\Http\Controllers\BusinessController::class, 'DeleteBusiness'])->name('Bdelete');
Route::get('/verify/business/{business_id}', [App\Http\Controllers\BusinessController::class, 'VerifyBusiness'])->name('Bview');

// Employee Route Start:
Route::get('/add/employeeform', [App\Http\Controllers\EmployeeController::class, 'AddEmployee'])->name('Eadd');
Route::post('/employee/data/store', [App\Http\Controllers\EmployeeController::class, 'DataStore'])->name('store');
Route::get('/all/employee', [App\Http\Controllers\EmployeeController::class, 'AllEmployee'])->name('AllE');
Route::get('/edit/employee/{employee_id}', [App\Http\Controllers\EmployeeController::class, 'EditEmployee'])->name('Eedit');
Route::post('/edit/employee/formpost/{employee_id}', [App\Http\Controllers\EmployeeController::class, 'EditEmployeeFormPost'])->name('Eeditformpost');
Route::get('/delete/employee/{employee_id}', [App\Http\Controllers\EmployeeController::class, 'DeleteEmployee'])->name('Edelete');
Route::get('/single/employee/view/{employee_id}', [App\Http\Controllers\EmployeeController::class, 'ViewSingleEmployee'])->name('SEview');

// Suppliers Route Start:
Route::get('/add/supplierform', [App\Http\Controllers\SupplierController::class, 'AddSupplier'])->name('Sadd');
Route::post('/supplier/data/store', [App\Http\Controllers\SupplierController::class, 'SupplierDataStore'])->name('s.store');
Route::get('/all/supplier', [App\Http\Controllers\SupplierController::class, 'AllSupplier'])->name('AllS');
Route::get('/edit/supplier/{supplier_id}', [App\Http\Controllers\SupplierController::class, 'EditSupplier'])->name('Sedit');
Route::post('/edit/supplier/formpost/{supplier_id}', [App\Http\Controllers\SupplierController::class, 'EditSupplierFormPost'])->name('Seditformpost');
Route::get('/delete/supplier/{supplier_id}', [App\Http\Controllers\SupplierController::class, 'DeleteSupplier'])->name('Sdelete');
Route::get('/single/supplier/view/{employee_id}', [App\Http\Controllers\SupplierController::class, 'ViewSingleSupplier'])->name('SSview');

// Category Route Start:
Route::get('/add/categoryform', [App\Http\Controllers\CategoryController::class, 'AddCategory'])->name('Cadd');
Route::post('/category/formpost', [App\Http\Controllers\CategoryController::class, 'CategoryFormPost'])->name('CFadd');
Route::get('/edit/category/{category_id}', [App\Http\Controllers\CategoryController::class, 'EditCategory'])->name('Cedit');
Route::post('/edit/category/formpost', [App\Http\Controllers\CategoryController::class, 'EditCategoryFormPost'])->name('Ceditformpost');
Route::get('/delete/category/{category_id}', [App\Http\Controllers\CategoryController::class, 'DeleteCategory'])->name('Cdelete');

// Brands Route Start:
Route::get('/add/brandform', [App\Http\Controllers\BrandController::class, 'AddBrand'])->name('Bradd');
Route::post('/brand/formpost', [App\Http\Controllers\BrandController::class, 'BrandFormPost'])->name('BFadd');
Route::get('/edit/brand/{brand_id}', [App\Http\Controllers\BrandController::class, 'EditBrand'])->name('Bredit');
Route::post('/edit/brand/formpost', [App\Http\Controllers\BrandController::class, 'EditBrandFormPost'])->name('Breditformpost');
Route::get('/delete/brand/{category_id}', [App\Http\Controllers\BrandController::class, 'DeleteBrand'])->name('Brdelete');

// Products Route Start:
Route::get('/add/productform', [App\Http\Controllers\ProductController::class, 'AddProduct'])->name('Padd');
Route::post('/product/formpost', [App\Http\Controllers\ProductController::class, 'ProductFormPost'])->name('PFadd');
Route::post('/product/purchaseformpost', [App\Http\Controllers\ProductController::class, 'PurchaseFormPost'])->name('PHadd');
Route::get('/all/products', [App\Http\Controllers\ProductController::class, 'AllProduct'])->name('AllP');
Route::get('/all/product-purchase', [App\Http\Controllers\ProductController::class, 'AllPurchase'])->name('AllPurchase');
Route::get('/edit/product/{product_id}', [App\Http\Controllers\ProductController::class, 'EditProduct'])->name('Pedit');
Route::get('/edit/purchase/product/{purchase_id}', [App\Http\Controllers\ProductController::class, 'EditPurchaseProduct'])->name('PHedit');
Route::post('/edit/product/formpost', [App\Http\Controllers\ProductController::class, 'EditProductFormPost'])->name('product.edit.formpost');
Route::post('/edit/purchase/formpost', [App\Http\Controllers\ProductController::class, 'EditPurchaseFormPost'])->name('purchase.edit.formpost');
Route::get('/delete/product/{product_id}', [App\Http\Controllers\ProductController::class, 'DeleteProduct'])->name('Pdelete');
Route::get('/delete/product/purchase/{purchase_id}', [App\Http\Controllers\ProductController::class, 'DeleteProductPurchase'])->name('PurchaseDelete');
Route::get('/delete/product/sell/{sell_id}', [App\Http\Controllers\ProductController::class, 'DeleteProductSell'])->name('SellsDelete');
Route::get('/single/product/view/{product_id}', [App\Http\Controllers\ProductController::class, 'ViewSingleProduct'])->name('SPview');
Route::get('/product/adjust/form/{ids}', [App\Http\Controllers\ProductController::class, 'AdjustProduct'])->name('get.AdjustProduct');
Route::get('/product/adjust', [App\Http\Controllers\ProductController::class, 'AdjustProductList'])->name('AdjustProduct.List');
Route::post('/product/adjust/formpost', [App\Http\Controllers\ProductController::class, 'AdjustProductFormPost'])->name('product.adjust');
Route::get('/product/adjust/delete/{adjust_id}', [App\Http\Controllers\ProductController::class, 'AdjustProductDelete'])->name('product.adjust.delete');

// Products SaleReturn Route Start:
Route::get('/sale/return/form/{ids}', [App\Http\Controllers\SaleReturnController::class, 'SaleReturnForm'])->name('sale_id.return');
Route::get('/sale/return/', [App\Http\Controllers\SaleReturnController::class, 'SaleReturn'])->name('sale.return');
Route::post('/add/sale-return/formpost', [App\Http\Controllers\SaleReturnController::class, 'SaleReturnFormPost'])->name('sale.return.add');
Route::get('/edit/sale-return/product/{product_id}', [App\Http\Controllers\SaleReturnController::class, 'EditReturnProduct']);
Route::post('/edit/product/formpost1', [App\Http\Controllers\SaleReturnController::class, 'EditReturnFormPost'])->name('edit.return.add');
Route::get('/delete/sale-return/product/{product_id}', [App\Http\Controllers\SaleReturnController::class, 'DeleteReturnProduct']);

// Products PurchessReturn Route Start:
Route::get('/purchess/return/form/{ids}', [App\Http\Controllers\PurchessReturnController::class, 'PurchessReturnForm'])->name('purches_id.return');
Route::get('/purchess/return/', [App\Http\Controllers\PurchessReturnController::class, 'PurchessReturn'])->name('purchess.return');
Route::post('/add/purchess-return/formpost', [App\Http\Controllers\PurchessReturnController::class, 'PurchessReturnFormPost'])->name('purchess.return.add');
Route::get('/edit/purchess-return/product/{product_id}', [App\Http\Controllers\PurchessReturnController::class, 'EditReturnProduct']);
Route::post('/edit/purchessproduct/formpost', [App\Http\Controllers\PurchessReturnController::class, 'EditReturnFormPost'])->name('edit.purchessreturn.add');
Route::get('/delete/purchess-return/product/{product_id}', [App\Http\Controllers\PurchessReturnController::class, 'DeleteReturnProduct']);

// Expense Route Start:
Route::get('/add/expenseform', [App\Http\Controllers\ExpenseController::class, 'AddExpense'])->name('Exadd');
Route::post('/expense/formpost', [App\Http\Controllers\ExpenseController::class, 'ExpenseFormPost'])->name('ExFadd');
Route::get('/edit/expense/{expense_id}', [App\Http\Controllers\ExpenseController::class, 'EditExpense'])->name('Exedit');
Route::post('/edit/expense/formpost', [App\Http\Controllers\ExpenseController::class, 'EditExpenseFormPost'])->name('Exeditformpost');
Route::get('/delete/expense/{expense_id}', [App\Http\Controllers\ExpenseController::class, 'DeleteExpense'])->name('Exdelete');

// Stock Route Start
Route::get('/view/stock', [App\Http\Controllers\StockController::class, 'ViewStock'])->name('view.stock');
// Pos Route Start---------------------------------------------------------------------------------------------------------------- //
Route::get('/view/pos-inventory', [App\Http\Controllers\PosController::class, 'ViewPos'])->name('view.pos');
// Customar Route Start:
Route::get('/add/customarform', [App\Http\Controllers\CustomarController::class, 'AddCustomar'])->name('customar.addform');
Route::post('/customar/data/store', [App\Http\Controllers\CustomarController::class, 'CustomarDataStore'])->name('customar.store');
Route::get('/edit/customar/{customar_id}', [App\Http\Controllers\CustomarController::class, 'EditCustomar'])->name('customar.edit');
Route::post('/edit/customar/formpost/{customar_id}', [App\Http\Controllers\CustomarController::class, 'EditCustomarFormPost'])->name('customar.editformpost');
Route::get('/delete/customar/{customar_id}', [App\Http\Controllers\CustomarController::class, 'DeleteCustomar'])->name('customar.delete');
Route::get('/single/customar/view/{customar_id}', [App\Http\Controllers\CustomarController::class, 'ViewSingleCustomar'])->name('single.customar.view');

// Order Route Start:
Route::post('/customar/order/data/store', [App\Http\Controllers\OrderController::class, 'OrderDataStore'])->name('order.store');
Route::get('/view/bank-payment', [App\Http\Controllers\PosController::class, 'BankPayment'])->name('view.bank');

// Invoice Route Start:
Route::get('customar/invoice/list', [App\Http\Controllers\InvoiceController::class, 'CustomarInvoice'])->name('customar.invoice');
Route::get('supplier/invoice/list', [App\Http\Controllers\InvoiceController::class, 'SupplierInvoice'])->name('supplier.invoice');
Route::get('/single/customar/invoice/view/{customar_id}', [App\Http\Controllers\InvoiceController::class, 'SingleCustomarInvoice'])->name('single.customar.invoice');
Route::get('/single/supplier/invoice/view/{supplier_id}', [App\Http\Controllers\InvoiceController::class, 'SingleSupplierInvoice'])->name('single.supplier.invoice');
Route::get('/invoice/view/{invoice_id}', [App\Http\Controllers\InvoiceController::class, 'Invoice'])->name('invoice.view');
Route::get('supplier/invoice/view/{invoice_id}', [App\Http\Controllers\InvoiceController::class, 'Supplier_Invoice'])->name('sup.invoice.view');

// Personal Route Start:
Route::get('/personal/data/form', [App\Http\Controllers\PersonalController::class, 'AddDocument'])->name('personal.form');
Route::post('/personal/data/store', [App\Http\Controllers\PersonalController::class, 'StoreDocument'])->name('personal.data.store');
Route::get('/edit/document/{document_id}', [App\Http\Controllers\PersonalController::class, 'EditDocument'])->name('document.edit');
Route::post('/edit/document/formpost/{document_id}', [App\Http\Controllers\PersonalController::class, 'EditDocumentFormPost'])->name('update.document');
Route::get('/delete/document/{document_id}', [App\Http\Controllers\PersonalController::class, 'DeleteDocument'])->name('delete.document.data');
Route::get('/view/document/{document_id}', [App\Http\Controllers\PersonalController::class, 'ViewDocument'])->name('view.document');

// Attendance Route Start:
Route::get('/employee/attendance', [App\Http\Controllers\AttendanceController::class, 'Attendance_Form'])->name('emp.attendance');
Route::post('/attendance/data/store', [App\Http\Controllers\AttendanceController::class, 'AttendanceDataStore'])->name('attendance.store');
Route::get('/edit/attendance/{edit_date}', [App\Http\Controllers\AttendanceController::class, 'EditAttendance'])->name('edit.attendance');

// Bank Route Start:
Route::get('/add/bankform', [App\Http\Controllers\BankController::class, 'AddBank'])->name('bank.add');
Route::post('/bank/formpost', [App\Http\Controllers\BankController::class, 'BankFormPost'])->name('bankform.add');
Route::get('/edit/bank/{bank_id}', [App\Http\Controllers\BankController::class, 'EditBank'])->name('edit.bank');
Route::post('/edit/bank/formpost', [App\Http\Controllers\BankController::class, 'EditBankFormPost'])->name('bank.editformpost');
Route::get('/delete/bank/{bank_id}', [App\Http\Controllers\BankController::class, 'DeleteBank'])->name('bank.delete');

// Transaction Route Start:
Route::get('/add/transactionform', [App\Http\Controllers\TransactionController::class, 'AddTransaction'])->name('transaction.add');
Route::post('/transaction/formpost', [App\Http\Controllers\TransactionController::class, 'TransactionFormPost'])->name('transactionform.add');
Route::post('/transaction/filters/date/formpost', [App\Http\Controllers\TransactionController::class, 'FilterFormPost'])->name('filter.formpost');
// Delete Transaction
Route::get('/delete/transaction/{tr_id}', [App\Http\Controllers\TransactionController::class, 'transactionDelete'])->name('transaction.delete');
Route::get('/single/bank/transaction/view/{transaction_id}', [App\Http\Controllers\TransactionController::class, 'ViewSingleBankTransaction'])->name('single.banktransaction.view');

// Due Route Start:
Route::get('/due/collection', [App\Http\Controllers\DueController::class, 'index'])->name('due.collection');
Route::post('payment/due', [App\Http\Controllers\DueController::class, 'AddDue'])->name('payment.add');
Route::get('customar/due/list', [App\Http\Controllers\DueController::class, 'CustomarDueList'])->name('customar.due.list');
Route::get('supplier/due/list', [App\Http\Controllers\DueController::class, 'SupplierDueList'])->name('supplier.due.list');
Route::get('/delete/due/{due_id}', [App\Http\Controllers\DueController::class, 'DeleteDue'])->name('due.delete');

// Profile Route Start
Route::get('/profile/form', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.form');
Route::post('/profile/edit/formpost', [App\Http\Controllers\ProfileController::class, 'ProfileEdit'])->name('profile.edit.formpost');
Route::post('/password/edit/formpost', [App\Http\Controllers\ProfileController::class, 'PasswordEdit'])->name('password.edit.formpost');
Route::post('/profile/edit/formpost', [App\Http\Controllers\ProfileController::class, 'ProfileEdit'])->name('profile.edit.formpost');
Route::post('/password/edit/formpost', [App\Http\Controllers\ProfileController::class, 'PasswordEdit'])->name('password.edit.formpost');

// Report Route Start
Route::get('/sale/report', [App\Http\Controllers\ReportController::class, 'index'])->name('sale.report');
Route::get('/single/product/sale_report/{product_id}', [App\Http\Controllers\ReportController::class, 'SingleSaleReport'])->name('single.sale.report');
Route::get('/sale/and/purchase/report', [App\Http\Controllers\ReportController::class, 'SaleandPurchase'])->name('saleandpurchase.report');
Route::post('/single/product/filters/date/formpost', [App\Http\Controllers\ReportController::class, 'ProductFilterFormPost'])->name('product.filter.formpost');

// HouseRange Route Start
Route::get('/house/range/form', [App\Http\Controllers\HouseRangeController::class, 'index'])->name('house.range.form');
Route::post('/house/range/data/store', [App\Http\Controllers\HouseRangeController::class, 'DataStore'])->name('range.data.store');
Route::get('/edit/house/range/{id}', [App\Http\Controllers\HouseRangeController::class, 'EditHouseRange']);
Route::get('/single/house/range/view/{id}', [App\Http\Controllers\HouseRangeController::class, 'HouseRangeView']);
Route::post('/update/house/range', [App\Http\Controllers\HouseRangeController::class, 'UpdateHouseRange'])->name('update.range.form');
Route::get('/delete/house/range/{id}', [App\Http\Controllers\HouseRangeController::class, 'DeleteHouseRange']);

// AJAX REQUEST START
Route::post('/searchh/findcustomer', [App\Http\Controllers\PosController::class, 'findCustomers'])->name('findCustomers');
Route::post('/searchpos/find/product', [App\Http\Controllers\PosController::class, 'findProductPos'])->name('findProductPos');
Route::post('view/select/product/onajax', [App\Http\Controllers\PosController::class, 'SearchProduct']);
Route::post('/select/product/onajax', [App\Http\Controllers\ProductController::class, 'SearchPro']);
Route::post('view/due/collection/onajax', [App\Http\Controllers\PosController::class, 'index']);
Route::post('view/account/number/onajax', [App\Http\Controllers\BankController::class, 'index']);
Route::post('select/due/collection/onajax', [App\Http\Controllers\DueController::class, 'Selected']);
Route::post('single_customar/due/collection/onajax', [App\Http\Controllers\DueController::class, 'CustomarDue']);
Route::post('single_supplier/due/collection/onajax', [App\Http\Controllers\DueController::class, 'SupplierDue']);
Route::post('customar/info/onajax', [App\Http\Controllers\DueController::class, 'customarinfo']);
// AJAX REQUEST END





/*----------------------------------------
| Personal Account
--------------------------------------------*/

Route::post("get-sub-head",                     [AccountHeadController::class, 'getSubHead'])->name('get-sub-head');
Route::any("search-personal-account-transaction",  [PersonalAccountTransactionController::class, 'searchPersonalAccount'])->name('search-personal-account-transaction');
Route::resource("account-head",                 AccountHeadController::class);
Route::resource("personal-account-transaction", PersonalAccountTransactionController::class);



// Route::get('/', 'HomeController@redirectAdmin')->name('index');
// Route::get('/home', 'HomeController@index')->name('home');

/**
 * Admin routes
 */
Route::post('/create/new/user', [App\Http\Controllers\UserController::class, 'index'])->name('user.store');


// Route::post('/login/submit', [App\Http\Controllers\Backend\Auth\LoginController::class, 'login'])->name('admin.login.submit');
// Route::get('/', [App\Http\Controllers\Backend\Auth\DashboardController::class, 'index'])->name('admin.dashboard');
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'App\Http\Controllers\Backend\DashboardController@index')->name('admin.dashboard');
    Route::get('/', [App\Http\Controllers\Backend\Auth\DashboardController::class, 'index'])->name('admin.dashboard');
    Route::resource('roles', 'App\Http\Controllers\Backend\RolesController', ['names' => 'admin.roles']);
    Route::resource('users', 'App\Http\Controllers\Backend\UsersController', ['names' => 'admin.users']);
    Route::resource('admins', 'App\Http\Controllers\Backend\AdminsController', ['names' => 'admin.admins']);


    // Login Routes
    Route::get('/login', 'App\Http\Controllers\Backend\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login/submit', [App\Http\Controllers\Backend\Auth\LoginController::class, 'login'])->name('admin.login.submit');

    // Logout Routes
    Route::post('/logout/submit', 'App\Http\Controllers\Backend\Auth\LoginController@logout')->name('admin.logout.submit');

    // Forget Password Routes
    Route::get('/password/reset', 'App\Http\Controllers\Backend\Auth\ForgetPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset/submit', 'App\Http\Controllers\Backend\Auth\ForgetPasswordController@reset')->name('admin.password.update');
});


Route::resource('sms-config', SMSConfigController::class);
