@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">

                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Bradd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Admins</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="col-sm-6 clearfix">
                            @include('backend.layouts.partials.logout')
                        </div>
                    </div>

                    <div class="panel-body" style="padding-bottom: 0;">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                    <div class="form-group">
                                        <input class="form-control form-control-sm shadow-none search" id="myInput" type="search"
                                            placeholder="Search for a page" aria-label="search" />
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                        <table class="table table-striped" id="myTable">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($admins as $admin)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $admin->name }}</td>
                                        <td>{{ $admin->email }}</td>
                                        <td>
                                            @foreach ($admin->roles as $role)
                                                <span class="badge badge-success mr-1">
                                                    {{ $role->name }}
                                                </span>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{-- @if (Auth::guard('admin')->user()->can('admin.edit')) --}}
                                                <a class="btn btn-sm btn-success text-white"
                                                    href="{{ route('admin.admins.edit', $admin->id) }}">Edit</a>
                                            {{-- @endif --}}

                                            {{-- @if (Auth::guard('admin')->user()->can('admin.delete')) --}}
                                                <a class="btn btn-sm btn-danger text-white"
                                                    href="{{ route('admin.admins.destroy', $admin->id) }}"
                                                    onclick="event.preventDefault(); document.getElementById('delete-form-{{ $admin->id }}').submit();">
                                                    Delete
                                                </a>
                                                <form id="delete-form-{{ $admin->id }}"
                                                    action="{{ route('admin.admins.destroy', $admin->id) }}"
                                                    method="POST" style="display: none;">
                                                    @method('DELETE')
                                                    @csrf
                                                </form>
                                            {{-- @endif --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  </script>
@endsection
