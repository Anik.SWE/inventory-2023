@extends('layouts.user_api.business_app')
@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">

                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Bradd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Admins</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="col-sm-6 clearfix">
                            @include('backend.layouts.partials.logout')
                        </div>
                    </div>

                    <div class="main-content-inner">
                        <div class="row">
                            <!-- data table start -->
                            <div class="col-12">
                                <div class="card" style="margin-top: 20px; border: none; padding-bottom: 20px">
                                    <div class="card-body">
                                        <h4 class="header-title" style="margin-left: 15px">Create New Role</h4>
                                        @include('backend.layouts.partials.messages')

                                        <form action="{{ route('admin.admins.store') }}" method="POST">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="name">Admin Name</label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="email">Admin Email</label>
                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-12">
                                                    <label for="password_confirmation">Confirm Password</label>
                                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Password">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6 col-sm-6">
                                                    <label for="password">Assign Roles</label>
                                                    <select name="roles[]" id="roles" class="form-control select2" multiple>
                                                        @foreach ($roles as $role)
                                                            <option value="{{ $role->name }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6">
                                                    <label for="username">Admin Username</label>
                                                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" required>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" style="margin-left: 15px">Save Admin</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- data table end -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    })
</script>
@endsection

@section('script')
<script>
    $(document).ready(function() {
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  </script>
@endsection

