{{-- @extends('backend.layouts.master')

@section('title')
Role Create - Admin Panel
@endsection

@section('styles')
<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection


@section('admin-content')

<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Role Create</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('admin.roles.index') }}">All Roles</a></li>
                    <li><span>Create Role</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->

<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Create New Role</h4>
                    @include('backend.layouts.partials.messages')

                    <form action="{{ route('admin.roles.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Role Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter a Role Name">
                        </div>

                        <div class="form-group">
                            <label for="name">Permissions</label>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="checkPermissionAll" value="1">
                                <label class="form-check-label" for="checkPermissionAll">All</label>
                            </div>
                            <hr>
                            @php $i = 1; @endphp
                            @foreach ($permission_groups as $group)
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="{{ $i }}Management" value="{{ $group->name }}" onclick="checkPermissionByGroup('role-{{ $i }}-management-checkbox', this)">
                                            <label class="form-check-label" for="checkPermission">{{ $group->name }}</label>
                                        </div>
                                    </div>

                                    <div class="col-9 role-{{ $i }}-management-checkbox">
                                        @php
                                            $permissions = App\User::getpermissionsByGroupName($group->name);
                                            $j = 1;
                                        @endphp
                                        @foreach ($permissions as $permission)
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="permissions[]" id="checkPermission{{ $permission->id }}" value="{{ $permission->name }}">
                                                <label class="form-check-label" for="checkPermission{{ $permission->id }}">{{ $permission->name }}</label>
                                            </div>
                                            @php  $j++; @endphp
                                        @endforeach
                                        <br>
                                    </div>

                                </div>
                                @php  $i++; @endphp
                            @endforeach


                        </div>


                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Save Role</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- data table end -->

    </div>
</div>
@endsection

@section('scripts')
     @include('backend.pages.roles.partials.scripts')
@endsection --}}




@extends('layouts.user_api.business_app')
@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <style>
        .form-check-label {
            text-transform: capitalize;
        }
    </style>
@endsection
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">

                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Bradd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Admins</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="col-sm-6 clearfix">
                            @include('backend.layouts.partials.logout')
                        </div>
                    </div>

                    <div class="main-content-inner">
                        <div class="row">
                            <!-- data table start -->
                            <div class="col-12 mt-5">
                                <div class="card"
                                    style="margin-top: 20px; border: none; margin-left:15px">
                                    <div class="card-body">
                                        <h4 class="header-title">Create New Role</h4>
                                        @include('backend.layouts.partials.messages')

                                        <form action="{{ route('admin.roles.store') }}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Role Name</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="Enter a Role Name">
                                            </div>

                                            <div class="form-group">
                                                <label for="name">Permissions</label>

                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="checkPermissionAll"
                                                        value="1">
                                                    <label class="form-check-label" for="checkPermissionAll">All</label>
                                                </div>
                                                <hr>
                                                @php $i = 1; @endphp
                                                @foreach ($permission_groups as $group)
                                                    <div class="row col-md-12">
                                                        <div class="col-md-2">
                                                            <div class="form-check">
                                                                <input type="checkbox" class="form-check-input"
                                                                    id="{{ $i }}Management"
                                                                    value="{{ $group->name }}"
                                                                    onclick="checkPermissionByGroup('role-{{ $i }}-management-checkbox', this)">
                                                                <label class="form-check-label"
                                                                    for="checkPermission">{{ $group->name }}</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4 role-{{ $i }}-management-checkbox">
                                                            @php
                                                                $permissions = App\User::getpermissionsByGroupName($group->name);
                                                                $j = 1;
                                                            @endphp
                                                            @foreach ($permissions as $permission)
                                                                <div class="form-check">
                                                                    <input type="checkbox" class="form-check-input"
                                                                        name="permissions[]"
                                                                        id="checkPermission{{ $permission->id }}"
                                                                        value="{{ $permission->name }}">
                                                                    <label class="form-check-label"
                                                                        for="checkPermission{{ $permission->id }}">{{ $permission->name }}</label>
                                                                </div>
                                                                @php  $j++; @endphp
                                                            @endforeach
                                                            <br>
                                                        </div>

                                                    </div>
                                                    @php  $i++; @endphp
                                                @endforeach


                                            </div>


                                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" style=" margin-bottom: 60px;">Save Role</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- data table end -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('backend.pages.roles.partials.scripts')
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        })
    </script>
@endsection


@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
