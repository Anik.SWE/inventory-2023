@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        function clock() {

            var hour = document.getElementById("hour");
            var min = document.getElementById("min");
            var sec = document.getElementById("sec");
            var am_pm = document.getElementById("am_pm");

            var day = document.getElementById("day");
            var date = document.getElementById("date");
            var month = document.getElementById("month");
            var year = document.getElementById("year");

            var d = new Date();

            var h = d.getHours();
            var m = d.getMinutes();
            var s = d.getSeconds();
            var ampm = h >= 12 ? 'PM' : 'AM';

            // for 12 hours clock

            if (h > 12) {
                var hh = h - 12;
                if (hh < 10) {
                    var newh = "0" + hh;
                } else {
                    var newh = hh;
                }

            } else {
                var newh = h;
            }



            if (m < 10) {
                newm = "0" + m;
            } else {
                newm = m;
            }


            if (s < 10) {
                news = "0" + s;
            } else {
                news = s;
            }

            // install months and week day name
            const weeks = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thirsday", "Friday", "Saturday"];
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"
            ];

            var dy = weeks[d.getDay()]; // for week day
            var dd = d.getDate(); // for date
            var mm = months[d.getMonth()]; // for month
            var yy = d.getFullYear();

            hour.innerHTML = newh + " : ";
            min.innerHTML = newm + " : ";
            sec.innerHTML = news;
            am_pm.innerHTML = ampm;


            day.innerHTML = dy + ", ";
            date.innerHTML = dd + " ";
            month.innerHTML = mm;
        }

        setInterval(clock, 1000);


        // function cart_total() {
        //     var Quantity = document.getElementById("quantity").value;
        //     var Price = document.getElementById("price").value;
        //     var total_amount = (parseFloat(Quantity) * parseFloat(Price));
        //     document.getElementById("total_amount").value = total_amount;

        //     var payment = document.getElementById("payment").value;
        //     var change = (parseFloat(total_amount) - parseFloat(payment));
        //     document.getElementById("change").value = change;
        // }



        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable div").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
        $(document).ready(function() {
            $("#input").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#table div").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });



        function create_tr(table_id) {
            let table_body = document.getElementById(table_id),
                first_tr = table_body.firstElementChild
            tr_clone = first_tr.cloneNode(true);
            table_body.append(tr_clone);
            clean_first_tr(table_body.firstElementChild);
        }

        function clean_first_tr(firstTr) {
            let children = firstTr.children;

            children = Array.isArray(children) ? children : Object.values(children);
            children.forEach(x => {
                if (x !== firstTr.lastElementChild) {
                    x.firstElementChild.value = '';
                }
            });
        }

        function remove_tr(This) {
            if (This.closest('tbody').childElementCount > 1) {
                This.closest('tr').remove();
            }
        }


        function on_discount() {
            var discount = 0;
            var ship_cost = 0;
            var tax = 0;
            var payment = 0;

            // Product Quantity
            var sub_price = document.querySelector('.cart_item_price').innerText;
            document.getElementsByClassName('sub_price').value = sub_price;

            // SubTotal
            var qty = document.querySelector('.product_quantity').innerText;
            document.getElementById('quantity').value = qty;


            // SubTotal
            var sub = document.querySelector('.sub_pay').innerText;
            document.getElementById('sub_total').value = sub;

            // Discount and Tex
            discount = document.getElementById("discount").value;
            ship_cost = document.getElementById("ship_cost").value;

            tax = document.getElementById("tax").value;
            var total_tax = (sub * tax) / 100;

            // Total

            var ship_costn = parseInt(ship_cost);


            var resuslt = ((sub - discount) + total_tax) + ship_costn;
            document.getElementById('total').value = resuslt;
            document.getElementById('total_pay').innerHTML = resuslt;


            // Payment
            payment = document.getElementById("payment").value;
            var change = resuslt - parseFloat(payment);
            document.getElementById("change").value = change;
            var due = document.getElementById("cus_due").value;
            due = parseFloat(due);
            document.getElementById('sub_dues').innerHTML = resuslt + due;

            // total
            total_amount = document.getElementById("total_pay").innerHTML;
            console.log(total_amount);
            document.getElementById('total_amount').value = total_amount;

        }



        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var due_id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/due/collection/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        due_id: due_id
                    },
                    success: function(result) {
                        $('#sub_id').html(result);
                    }
                });
            });
        });

        $(document).ready(function() {
            $('#radio_box').change(function() {
                var cat_id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    dataType: 'html',
                    url: '{{ url('view/select/product/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        cat_id: cat_id
                    },
                    success: function(show) {
                        $('#selectProduct').html(show);
                        // alert(show);
                    }
                });
            });
        });
    </script>
@endsection
