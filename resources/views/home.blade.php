@extends('layouts.user_api.app')

@section('content')

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">

      <header class="pageheader">
          <h3><i class="fa fa-building"></i>Your Business </h3>
          <div class="breadcrumb-wrapper">
              <span class="label">Back to:</span>
              <ol class="breadcrumb">
                  <li> <a href="#"> Home </a> </li>

              </ol>
          </div>
      </header>
        <div id="page-content">
            <div class="panel">
                <div class="panel-heading">
                 <h3 class="panel-title"><a href="{{ route('add') }}"><button class="btn btn-primary btn-labeled fa fa-plus-square">New Business</button></a></h3>
                </div>
                <!--Data Table-->
                <!--===================================================-->
                <div class="panel-body">
                                @forelse ($Total_Business as $item)
                                <div class="col-md-3">
                                    <div class="panel text-center">
                                        <div class="panel-body bg-purple pad-ver" style=" background-color: #7266BA; color: #FFFFFF;">
                                          <div class="text-center">
                                            <img src="{{ asset('user_assets/img/shop.png') }}" style=" height: 50px; width:50px; border-radius: 7px!important;">
                                            <h4>{{ $item->business_name }}</h4>
                                          </div>
                                        </div>
                                        <div class="panel-footer">
                                        <ul class="nav nav-section nav-justified">
                                            <li>
                                                <div class="section">
                                                    <div class="h4 mar-ver-5"> <a href="{{ url('edit/business') }}/{{ $item->id }}"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-edit"></i> </button></a> </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="section">
                                                    <div class="h4 mar-ver-5"> <a href="{{ url('/verify/business') }}/{{ $item->id }}"><button class="btn btn-sm btn-success" type="button"><i class="fa fa-eye"></i> View </button></a> </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="section">
                                                    <div class="h4 mar-ver-5"> <a href="{{ url('delete/business') }}/{{ $item->id }}"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-trash"></i>  </button></a> </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                        <!--===================================================-->
                                    </div>
                                </div>


                                @empty
                                    <tr>
                                        <p class="text-danger"> <strong>{{ Auth::user()->name }}
                                        </strong> You Not Create Any Business....Please Create a Business!</p>
                                    </tr>
                                @endforelse
                </div>
                <!--===================================================-->
                <!--End Data Table-->
            </div>
        </div>
    </div>

@endsection
@section('script')
<script>
    $(document).ready(function() {
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  </script>
@endsection
