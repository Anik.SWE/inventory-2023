<script>
    "use strict";
let cart = [];
let cartTotal = 0;
const cartDom = document.querySelector(".cart");
const addtocartbtnDom = document.querySelectorAll('[data-action="add-to-cart"]');

addtocartbtnDom.forEach(addtocartbtnDom => {
    addtocartbtnDom.addEventListener("click", () => {

      var sell_type = $("#sell_type").val();

        const productDom = addtocartbtnDom.parentNode.parentNode;
        const product = {
            name: productDom.querySelector(".product-name").innerText,
            price: productDom.querySelector(".product-price").innerText,
            prices: productDom.querySelector(".products-prices").innerText,
            product_code: productDom.querySelector(".product-code").innerText,
            quantity: 1
        };


        if(sell_type == "Retail"){
          product.price = product.price;
        }
        if(sell_type == "Whole"){
          product.price = product.prices;
        }


        const IsinCart = cart.filter(cartItem => cartItem.name === product.name).length > 0;
        if (IsinCart === false) {


            cartDom.insertAdjacentHTML("beforeend", `
            <div class="inner_row flex-row shadow-sm card cart-items mt-2 mb-3 animated flipInX" style="padding: 10px">
            <div class="p-2 mt-3">
                <p class="text-info cart_item_name" name="product_name[]" style="margin-bottom: 0px; width: 94px; height: 20px; overflow: hidden">${product.name}</p>
                <input type="hidden" name="product_code[]" value="${product.product_code}"/>
                <input type="hidden" name="product_name[]" value="${product.name}"/>
            </div>
            <div class="p-2 mt-3 ml-auto">
                <button type="button" data-action="increase-item" name="pro">&plus;
            </div>
            <div class="p-2 mt-3" style="padding: 0px 5px">
              <input class="text-success cart_item_quantity text-center product_quantity input" name="product_quantity[]" style="width: 30px; border-radius: 0" value="${product.quantity}">
            </div>

            <div class="p-2 mt-3" style="margin-right: 15px">
              <button type="button" data-action="decrease-item">&minus;
            </div>
            <div class="p-2 mt-3" style="margin: 0px">
                <input id="price" class="text-success cart_item_price sub_price input" onchange="on_sub();" name="sub_price[]" style="width: 50px; margin: 0px; padding-left:5px; border-radius: 0" value="${product.price}">
                <input type="hidden" class="text-success cart_item_price" name="product_price[]" value="${product.price}"/>
            </div>
            <div class="p-2 mt-3" style="margin-left: 20px">
              <button type="button" data-action="remove-item">&times;
            </div>
          </div> `);

            if (document.querySelector('.cart-footer') === null) {
                cartDom.insertAdjacentHTML("afterend", `

            <div class="col-md-12 cart-footer" style="padding: 0; display:flex; align-items: center; justify-content: space-between">
            <div class="p-2 mt-3"  style="display: flex; align-items: center">
                <p style="width: 40px; padding: 10px 0; padding-right: 5px; margin: 0" >Tax:</p>
                <input type="number" value="1" name="tax" id="tax" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px"/>
            </div>
            <div class="p-2 mt-3" style="display: flex; align-items: center">
                <p style="width: 70px; padding: 10px 0; padding-right: 20px; margin: 0" >Discount:</p>
                <input type="number" value="0" name="discount" id="discount" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px"/>
            </div>
            <div class="p-2 mt-3" style="display: flex; align-items: center">
                <p style="width: 70px; padding: 10px 0; padding-right: 20px; margin: 0" >Shipping Cost:</p>
                <input type="number" value="0" name="ship_cost" id="ship_cost" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px"/>
            </div>
            <div class="p-2 ml-auto">
                <button type="button" data-action="sub-out">Sub <span class="pay sub_pay" name="sub_total" id="sub_pay" onchange="on_discount();"></span>
                &#10137;
            </div>
            </div>

            <div class="inner_cart_row flex-row shadow-sm card cart-footer mt-2 mb-3 animated flipInX" style="padding: 10px">
      <div class="p-2">
        <button type="button" data-action="clear-cart">Clear Cart
      </div>
      <div class="p-2 ml-auto">
                <button type="button">Total <span class="pay" id="total_pay" value="0" name="total" onchange="on_discount();"></span>
                &#10137;
            </div>


            </div>




      `);
            }

            addtocartbtnDom.innerText = "In cart";
            addtocartbtnDom.disabled = true;
            cart.push(product);

            const cartItemsDom = cartDom.querySelectorAll(".cart-items");
            cartItemsDom.forEach(cartItemDom => {

                if (cartItemDom.querySelector(".cart_item_name").innerText === product
                    .name) {

                    cartTotal += parseInt(cartItemDom.querySelector(".cart_item_quantity")
                            .value) *
                        parseInt(cartItemDom.querySelector(".cart_item_price").value);
                    document.querySelector('.pay').innerText = cartTotal;
                    // "$ " +


                    // increase item in cart
                    cartItemDom.querySelector('[data-action="increase-item"]')
                        .addEventListener("click", () => {
                            cart.forEach(cartItem => {
                                if (cartItem.name === product.name) {
                                    console.log(cartItem.quantity);
                                    console.log(cartItem.price);
                                    cartItemDom.querySelector(
                                            ".cart_item_quantity")
                                        .value = ++cartItem.quantity;
                                    cartItemDom.querySelector(
                                            ".cart_item_price").value =
                                        parseInt(cartItem.quantity) *
                                        parseInt(cartItem.price);
                                    cartTotal += parseInt(cartItem.price)
                                    console.log(cartTotal);
                                    document.querySelector('.pay').innerText =
                                        cartTotal;
                                    // "$ " +
                                }
                            });
                        });

                    // decrease item in cart
                    cartItemDom.querySelector('[data-action="decrease-item"]')
                        .addEventListener("click", () => {
                            cart.forEach(cartItem => {
                                if (cartItem.name === product.name) {
                                    if (cartItem.quantity > 1) {
                                        console.log(cartItem.quantity);
                                        cartItemDom.querySelector(
                                                ".cart_item_quantity")
                                            .value = --cartItem.quantity;
                                        cartItemDom.querySelector(
                                                ".cart_item_price").value =
                                            parseInt(cartItem.quantity) *
                                            parseInt(cartItem.price);
                                        cartTotal -= parseInt(cartItem.price)
                                        document.querySelector('.pay')
                                            .innerText = "$ " + cartTotal;
                                        document.querySelector('.pay')
                                            .innerText = cartTotal;
                                        // "$ " +
                                    }
                                }
                            });
                        });

                    //remove item from cart
                    cartItemDom.querySelector('[data-action="remove-item"]')
                        .addEventListener("click", () => {
                            cart.forEach(cartItem => {
                                if (cartItem.name === product.name) {
                                    cartTotal -= parseInt(cartItemDom
                                        .querySelector(".cart_item_price")
                                        .value);
                                    document.querySelector('.pay').value =
                                        cartTotal;
                                    // "$ " +
                                    cartItemDom.remove();
                                    cart = cart.filter(cartItem => cartItem
                                        .name !== product.name);
                                    addtocartbtnDom.innerText = "Add to cart";
                                    addtocartbtnDom.disabled = false;
                                }
                                if (cart.length < 1) {
                                    document.querySelector('.cart-footer')
                                        .remove();
                                }
                            });
                        });



                    //clear cart
                    document.querySelector('[data-action="clear-cart"]').addEventListener(
                        "click", () => {
                            cartItemDom.remove();
                            cart = [];
                            cartTotal = 0;
                            if (document.querySelector('.cart-footer') !== null) {
                                document.querySelector('.cart-footer').remove();
                            }
                            addtocartbtnDom.innerText = "Add to cart";
                            addtocartbtnDom.disabled = false;
                        });
                }
            });
        }
    });
});

function animateImg(img) {
    img.classList.add("animated", "shake");
}

function normalImg(img) {
    img.classList.remove("animated", "shake");
}

</script>
