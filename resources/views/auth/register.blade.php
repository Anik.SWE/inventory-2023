@extends('layouts.auth_app')

@section('content')
    <div id="container" class="cls-container">
        <!-- REGISTRATION FORM -->
        <!--===================================================-->
        <div class="lock-wrapper">
            <div class="panel lock-box">
                <div class="center"> <img alt="" src="{{ asset('user_assets/img/user.png') }}" class="img-circle" />
                </div>
                <h4> নতুন অ্যাকাউন্ট তৈরি করুন</h4>
                <p class="text-center">স্টোলাস ব্যবহার করতে আপনার একটি একাউন্ট প্রয়োজন</p>
                <div class="row">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <div class="text-left">
                                <label for="signupInputName" class="control-label">আপনার নাম:</label>
                                <input id="name" type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="সম্পূর্ণ নাম লিখুন">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-left">
                                <label for="signupInputEmail" class="control-label">ইমেইল এড্রেস</label>
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" placeholder="আপনার ইমেইল এড্রেস বসান">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-left">
                                <label for="signupInputPassword" class="control-label">পাসওয়ার্ড</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="new-password" placeholder="স্ট্রং পাসওয়ার্ড সেট করুন">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-left">
                                <label for="signupInputRepassword" class="control-label">পুনরায় পাসওয়ার্ড</label>
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password" placeholder="পাসওয়ার্ড নিশ্চিত করুন">
                            </div>
                            <div class="form-group col-md-12">
                                <div class="text-left pad-btm">
                                    <label for="checkboxtickmark" class="form-checkbox form-icon control-label">
                                        <input id="checkboxtickmark" type="checkbox" name="agree" value="agree" checked>
                                        আমি শর্তাবলী মেনে নিচ্ছি
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary">
                                রেজিস্ট্রেশন করুন
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="registration"> অ্যাকাউন্ট করা থাকলে <a href="{{ route('login') }}"> <span class="text-primary"> লগ ইন করুন</span> </a> </div>
        </div>
        <!--===================================================-->
        <!-- REGISTRATION FORM -->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
@endsection
