@extends('layouts.auth_app')

@section('content')
    <div id="container" class="cls-container">
        <!-- LOGIN FORM -->
        <!--===================================================-->
        <div class="lock-wrapper">
            <div class="panel lock-box">
                <div class="center"> <img alt="" src="{{ asset('user_assets/img/user.png') }}" class="img-circle" />
                </div>
                <h4> লগ ইন করুন</h4>
                <p class="text-center">লগইন করতে সঠিক ইমেইল এবং পাসওয়ার্ড ব্যবহার করুন</p>
                <div class="row">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <div class="text-left">
                                <label class="text-muted">ইমেইল এড্রেস</label>
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="আপনার ইমেইল এড্রেস বসান">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-left">
                                <label for="signupInputPassword" class="text-muted">পাসওয়ার্ড</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="current-password" placeholder="সঠিক পাসওয়ার্ড নিশ্চিত করুন">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="pull-left pad-btm">
                                <label class="form-checkbox form-icon form-text">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    Remember Me
                                </label>
                            </div>
                            {{-- @if (Route::has('password.request'))
                            <div class="pull-left pad-btm">
                                <a class="btn btn-links" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                            @endif --}}
                            <button type="submit" class="btn btn-block btn-primary" style="margin-bottom: 15px">
                                লগ ইন করুন
                            </button>

                          {{--  @if (Route::has('password.request'))
                                <a class="btn btn-primary" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif --}}
                        </div>
                    </form>
                </div>
            </div>
            <div class="registration"> অ্যাকাউন্ট নেই? <a href="{{ route('register') }}"> <span class="text-primary"> রেজিস্ট্রেশন করুন</span> </a> </div>
        </div>
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
@endsection
