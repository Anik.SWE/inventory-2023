@extends('admin.app')

@section('content')
    <div class="pageheader">
        <h3><i class="fa fa-comment"></i>SMS Configuration</h3>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li> <a href="#">SMS</a> </li>
                <li class="active">SMS Configuration</li>
            </ol>
        </div>
    </div>
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="row">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="sms_config_list.html"><button
                                class="btn btn-primary btn-labeled fa fa-list">Manage Config</button></a></h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('sms-config.store') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $sMSConfig->id }}" name="id" class="form-control">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Select User</label>
                                    <select name="user_id" class="form-control selectpicker">
                                        <option value="">Select one...</option>
                                        @if (!empty($users))
                                            @foreach ($users as $in)
                                                <option value="{{ $in->id }}"
                                                    {{ $sMSConfig->user_id == $in->id ? 'selected' : '' }}>
                                                    {{ $in->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Select Business</label>
                                    <select name="business_id" class="form-control selectpicker">
                                        <option value="">Select one...</option>
                                        @if (!empty($business))
                                            @foreach ($business as $in)
                                                <option value="{{ $in->id }}"
                                                    {{ $sMSConfig->business_id == $in->id ? 'selected' : '' }}>
                                                    {{ $in->business_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">API Key</label>
                                    <input type="text" name="api_key" id="demo-vs-definput"
                                        value="{{ $sMSConfig->api_key }}" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Secret Key</label>
                                    <input type="text" id="demo-vs-definput" value="{{ $sMSConfig->secretkey }}"
                                        name="secretkey" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Caller Id</label>
                                    <input type="text" id="demo-vs-definput" value="{{ $sMSConfig->caller_id }}"
                                        name="caller_id" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">URL</label>
                                    <input type="text" id="demo-vs-definput" value="{{ $sMSConfig->url }}"
                                        name="url" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">SMS Rate</label>
                                    <input type="text" id="demo-vs-definput" value="{{ $sMSConfig->sms_rate }}"
                                        name="sms_rate" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            Configuration
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Button trigger modal-->
    </div>
@endsection
