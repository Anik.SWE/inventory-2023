<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> Stolas - Business Management Solution.</title>
	<link rel="shortcut icon" href="img/favicon.ico">
	<!--STYLESHEET-->
	<!--=================================================-->
	<!--Roboto Slab Font [ OPTIONAL ] -->
	<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400" rel="stylesheet">
	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!--Jasmine Stylesheet [ REQUIRED ]-->
	<link href="css/style.css" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!--Switchery [ OPTIONAL ]-->
	<link href="plugins/switchery/switchery.min.css" rel="stylesheet">
	<!--Bootstrap Select [ OPTIONAL ]-->
	<link href="plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<link href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
	<!--Jquery Tag It [ OPTIONAL ]-->
	<link href="plugins/tag-it/jquery.tagit.css" rel="stylesheet">
	<!--Ion.RangeSlider [ OPTIONAL ]-->
	<link href="plugins/ion-rangeslider/ion.rangeSlider.css" rel="stylesheet">
	<link href="plugins/ion-rangeslider/ion.rangeSlider.skinNice.css" rel="stylesheet">
	<!--Chosen [ OPTIONAL ]-->
	<link href="plugins/chosen/chosen.min.css" rel="stylesheet">
	<!--noUiSlider [ OPTIONAL ]-->
	<link href="plugins/noUiSlider/jquery.nouislider.min.css" rel="stylesheet">
	<link href="plugins/noUiSlider/jquery.nouislider.pips.min.css" rel="stylesheet">
	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
	<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<link href="plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
	<link href="plugins/fooTable/css/footable.core.css" rel="stylesheet">
	<!--Dropzone [ OPTIONAL ]-->
	<link href="plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">
	<link href="plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet">
	<link href="plugins/dropzone/dropzone.css" rel="stylesheet">
	<!--Summernote [ OPTIONAL ]-->
	<link href="plugins/summernote/summernote.min.css" rel="stylesheet">
	<!--Demo [ DEMONSTRATION ]-->
	<link href="css/demo/jasmine.css" rel="stylesheet">
	<!--SCRIPT-->
	<!--=================================================-->
	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="plugins/pace/pace.min.css" rel="stylesheet">
	<script src="plugins/pace/pace.min.js"></script>
</head>

<body>
	<div id="container" class="effect mainnav-lg navbar-fixed mainnav-fixed">
		<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
				<div id="navbar-container" class="boxed">
						<!--Brand logo & name-->
						<!--================================-->
						<div class="navbar-header">
								<a href="index.html" class="navbar-brand">
										<i class="fa fa-cube brand-icon"></i>
										<div class="brand-title">
												<span class="brand-text">Amar Union</span>
										</div>
								</a>
						</div>
						<!--================================-->
						<!--End brand logo & name-->
						<!--Navbar Dropdown-->
						<!--================================-->
						<div class="navbar-content clearfix">
								<ul class="nav navbar-top-links pull-left">
										<!--Navigation toogle button-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<li class="tgl-menu-btn">
												<a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
										</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End Navigation toogle button-->

										<!--Profile toogle button-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
											 <li id="profilebtn" class="hidden-xs">
													<a href="JavaScript:void(0);"> <i class="fa fa-user fa-lg"></i> </a>
											 </li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End Profile toogle button-->

										<!--Messages Dropdown-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<li class="dropdown">
												<a href="#" data-toggle="dropdown" class="dropdown-toggle"> <i class="fa fa-envelope fa-lg"></i> <span class="badge badge-header badge-warning">9</span>
												</a>
												<!--Message dropdown menu-->
												<div class="dropdown-menu dropdown-menu-md with-arrow">
														<div class="pad-all bord-btm">
															 <div class="h4 text-muted text-thin mar-no">You have 3 messages.</div>
														</div>
														<div class="nano scrollable">
																<div class="nano-content">
																		<ul class="head-list">
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av2.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Andy sent you a message</div>
																										<small class="text-muted">15 minutes ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av4.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Lucy sent you a message</div>
																										<small class="text-muted">30 minutes ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av3.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Jackson sent you a message</div>
																										<small class="text-muted">40 minutes ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av6.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Donna sent you a message</div>
																										<small class="text-muted">5 hours ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av4.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Lucy sent you a message</div>
																										<small class="text-muted">Yesterday</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <img src="img/av3.png" alt="Profile Picture" class="img-circle img-sm"> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Jackson sent you a message</div>
																										<small class="text-muted">Yesterday</small>
																								</div>
																						</a>
																				</li>
																		</ul>
																</div>
														</div>
														<!--Dropdown footer-->
														<div class="pad-all bord-top">
																<a href="#" class="btn-link text-dark box-block"> <i class="fa fa-angle-right fa-lg pull-right"></i>Show All Messages </a>
														</div>
												</div>
										</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End message dropdown-->
										<!--Notification dropdown-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<li class="dropdown">
												<a href="#" data-toggle="dropdown" class="dropdown-toggle"> <i class="fa fa-bell fa-lg"></i> <span class="badge badge-header badge-danger">5</span> </a>
												<!--Notification dropdown menu-->
												<div class="dropdown-menu dropdown-menu-md with-arrow">
														<div class="pad-all bord-btm">
															<div class="h4 text-muted text-thin mar-no"> Notification </div>
														</div>
														<div class="nano scrollable">
																<div class="nano-content">
																		<ul class="head-list">
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <span class="icon-wrap icon-circle bg-primary"> <i class="fa fa-comment fa-lg"></i> </span> </div>
																								<div class="media-body">
																										<div class="text-nowrap">New comments waiting approval</div>
																										<small class="text-muted">15 minutes ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<span class="badge badge-success pull-right">90%</span>
																								<div class="media-left"> <span class="icon-wrap icon-circle bg-danger"> <i class="fa fa-hdd-o fa-lg"></i> </span> </div>
																								<div class="media-body">
																										<div class="text-nowrap">HDD is full</div>
																										<small class="text-muted">50 minutes ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <span class="icon-wrap icon-circle bg-info"> <i class="fa fa-file-word-o fa-lg"></i> </span> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Write a news article</div>
																										<small class="text-muted">Last Update 8 hours ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<span class="label label-danger pull-right">New</span>
																								<div class="media-left"> <span class="icon-wrap icon-circle bg-purple"> <i class="fa fa-comment fa-lg"></i> </span> </div>
																								<div class="media-body">
																										<div class="text-nowrap">Comment Sorting</div>
																										<small class="text-muted">Last Update 8 hours ago</small>
																								</div>
																						</a>
																				</li>
																				<!-- Dropdown list-->
																				<li>
																						<a href="#" class="media">
																								<div class="media-left"> <span class="icon-wrap icon-circle bg-success"> <i class="fa fa-user fa-lg"></i> </span> </div>
																								<div class="media-body">
																										<div class="text-nowrap">New User Registered</div>
																										<small class="text-muted">4 minutes ago</small>
																								</div>
																						</a>
																				</li>
																		</ul>
																</div>
														</div>
														<!--Dropdown footer-->
														<div class="pad-all bord-top">
																<a href="#" class="btn-link text-dark box-block"> <i class="fa fa-angle-right fa-lg pull-right"></i>Show All Notifications </a>
														</div>
												</div>
										</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End notifications dropdown-->
								</ul>
								<ul class="nav navbar-top-links pull-right">

										<!--Fullscreen toogle button-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
											<li class="hidden-xs" id="toggleFullscreen">
												<a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
													 <span class="sr-only">Toggle fullscreen</span>
												</a>
											</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End language selector-->
										<!--User dropdown-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<li id="dropdown-user" class="dropdown">
												<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
														<span class="pull-right"> <img class="img-circle img-user media-object" src="img/av1.png" alt="Profile Picture"> </span>
														<div class="username hidden-xs">John Doe</div>
												</a>
												<div class="dropdown-menu dropdown-menu-right with-arrow">
														<!-- User dropdown menu -->
														<ul class="head-list">
																<li>
																		<a href="#"> <i class="fa fa-user fa-fw fa-lg"></i> Profile </a>
																</li>
																<li>
																		<a href="#">  <i class="fa fa-envelope fa-fw fa-lg"></i> Messages </a>
																</li>
																<li>
																		<a href="#">  <i class="fa fa-gear fa-fw fa-lg"></i> Settings </a>
																</li>
																<li>
																		<a href="#"> <i class="fa fa-sign-out fa-fw"></i> Logout </a>
																</li>
														</ul>
												</div>
										</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End user dropdown-->
										<!--Navigation toogle button-->
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<li class="hidden-xs">
												<a id="demo-toggle-aside" href="#">
												<i class="fa fa-navicon fa-lg"></i>
												</a>
										</li>
										<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
										<!--End Navigation toogle button-->
								</ul>
						</div>
						<!--================================-->
						<!--End Navbar Dropdown-->
				</div>
		</header>
		<!--===================================================-->
		<!--END NAVBAR-->
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				<div class="pageheader">
					<h3><i class="fa fa-money"></i>Collect Payment</h3>
					<div class="breadcrumb-wrapper">
							<span class="label">You are here:</span>
							<ol class="breadcrumb">
									<li> <a href="#"> Payment </a> </li>
									<li class="active"> Collect Payment</li>
							</ol>
					</div>
				</div>
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-money"> </i> Payment Info</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="demo-vs-definput" class="control-label">Select Client</label>
											<select data-placeholder="Choose a Country..." class="demo-chosen-select">
												<option value="">Select one...</option>
												<option value="">Al-Amin Shekh</option>
												<option value="A">Sazzad Hossain</option>
												<option value="B">Saifullah</option>
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="demo-vs-definput" class="control-label">Service Month</label>
											<input type="month" id="demo-vs-definput" class="form-control" required>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="demo-vs-definput" class="control-label">Payment Date</label>
											<input type="date" id="demo-vs-definput" class="form-control" required>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="demo-vs-definput" class="control-label">Amount</label>
											<input type="text" id="demo-vs-definput" class="form-control" required>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<button class="btn btn-block btn-primary">
												<i class="fa fa-money"></i> Okay
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="panel">
							<div class="panel-body">
								<div class="pad-btm form-inline">
									<div class="row">
										<div class="col-sm-6 text-xs-center">
											<div class="form-group">
												<label class="control-label">Status</label>
												<select id="demo-foo-filter-status" class="form-control">
													<option value="">Show all</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6 text-xs-center text-right">
											<div class="form-group">
												<input id="demo-foo-search" type="text" placeholder="Search" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- Foo Table - Filtering -->
								<!--===================================================-->
								<table id="demo-foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive" data-page-size="7">
									<thead>
										<tr>
											<th class="text-center">Sl</th>
											<th>Clients_Name</th>
											<th>Service Month</th>
											<th>Payment Date</th>
											<th>Amount</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">1</td>
											<td>Al-Amin Shekh</td>
											<td>April, 2023</td>
											<td>12/05/2022</td>
											<td>1500</td>
											<td class="text-center"> <button type="button" name="button" class="btn btn-danger"><i class="fa fa-trash"></i></button> </td>
										</tr>
										<tr>
											<td class="text-center">2</td>
											<td>Al-Amin Shekh</td>
											<td>April, 2023</td>
											<td>12/05/2022</td>
											<td>1500</td>
											<td class="text-center"> <button type="button" name="button" class="btn btn-danger"><i class="fa fa-trash"></i></button> </td>
										</tr>
										<tr>
											<td class="text-center">3</td>
											<td>Al-Amin Shekh</td>
											<td>April, 2023</td>
											<td>12/05/2022</td>
											<td>1500</td>
											<td class="text-center"> <button type="button" name="button" class="btn btn-danger"><i class="fa fa-trash"></i></button> </td>
										</tr>
										<tr>
											<td class="text-center">4</td>
											<td>Al-Amin Shekh</td>
											<td>April, 2023</td>
											<td>12/05/2022</td>
											<td>1500</td>
											<td class="text-center"> <button type="button" name="button" class="btn btn-danger"><i class="fa fa-trash"></i></button> </td>
										</tr>
									</tbody>
								</table>
								<!--===================================================-->
								<!-- End Foo Table - Filtering -->
							</div>
						</div>
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->
			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
			<!--MAIN NAVIGATION-->
			<!--===================================================-->
			<nav id="mainnav-container">
				<div id="mainnav">
					<!--Menu-->
					<!--================================-->
					<div id="mainnav-menu-wrap">
						<div class="nano">
							<div class="nano-content">
								<ul id="mainnav-menu" class="list-group">
									<!--Category name-->
									<!-- <li class="list-header">
										<img src="img/mise.png" alt="">
									</li> -->
									<li class="list-header">Navigation</li>
									<!--Menu list item-->
									<li><a href="index.html"><i class="fa fa-home"></i> Dashboard</a></li>
									<!--Menu list item-->
									<li class="list-divider"></li>
									<!--Category name-->
									<li class="list-header">Stolas</li>
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-users"></i>
											<span class="menu-title">
												<strong>Stolas Clients</strong>
											</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">

											<li><a href="user_list.html"><i class="fa fa-caret-right"></i> Manage Client </a></li>
										</ul>
									</li>
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-comment"></i>
											<span class="menu-title">
												<strong>SMS</strong>
											</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="sms_config.html"><i class="fa fa-caret-right"></i> Config. SMS</a></li>
											<li><a href="manage_config.html"><i class="fa fa-caret-right"></i> Manage Config.</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-money"></i>
											<span class="menu-title">Payment</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="payment_info.html"><i class="fa fa-caret-right"></i> Collect Payment </a></li>
										</ul>
									</li>
									<li class="list-divider"></li>
								</ul>
							</div>
						</div>
					</div>
					<!--================================-->
					<!--End menu-->
				</div>
			</nav>
			<!--===================================================-->
			<!--END MAIN NAVIGATION-->
			<!--ASIDE-->
			<!--===================================================-->
			<aside id="aside-container">
				<div id="aside">
					<div class="nano">
						<div class="nano-content">
							<!--Nav tabs-->
							<!--================================-->
							<ul class="nav nav-tabs nav-justified">
								<li class="active">
									<a href="#demo-asd-tab-1" data-toggle="tab"> <i class="fa fa-comments"></i> </a>
								</li>
								<li>
									<a href="#demo-asd-tab-3" data-toggle="tab"> <i class="fa fa-wrench"></i> </a>
								</li>
							</ul>
							<!--================================-->
							<!--End nav tabs-->
							<!-- Tabs Content Start-->
							<!--================================-->
							<div class="tab-content">
								<!--First tab (Contact list)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<div class="tab-pane fade in active" id="demo-asd-tab-1">
									<h4 class="pad-hor text-thin"> Online Members (7) </h4>
									<div class="list-group bg-trans">
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av1.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">John Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av2.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block pad-ver-5">
												<div class="text-small">Jose Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av3.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Roy Banks</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av7.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Steven Jordan</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av4.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Scott Owens</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av5.png" alt="" class="img-sm">
												<i class="on bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Melissa Hunt</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
									</div>
									<hr>
									<h4 class="pad-hor text-thin"> Busy Members (4) </h4>
									<div class="list-group bg-trans">
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av1.png" alt="" class="img-sm">
												<i class="busy bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">John Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av2.png" alt="" class="img-sm">
												<i class="busy bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Jose Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av3.png" alt="" class="img-sm">
												<i class="busy bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Roy Banks</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av7.png" alt="" class="img-sm">
												<i class="busy bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Steven Jordan</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
									</div>
									<hr>
									<h4 class="pad-hor text-thin"> Offline Members (4) </h4>
									<div class="list-group bg-trans">
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av1.png" alt="" class="img-sm">
												<i class="off bottom text-light"></i>
											</div>
											<div class="inline-block pad-ver-5">
												<div class="text-small">John Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av2.png" alt="" class="img-sm">
												<i class="off bottom text-light"></i>
											</div>
											<div class="inline-block pad-ver-5">
												<div class="text-small">Jose Knight</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av3.png" alt="" class="img-sm">
												<i class="off bottom text-light"></i>
											</div>
											<div class="inline-block pad-ver-5">
												<div class="text-small">Roy Banks</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
										<div class="list-group-item">
											<div class="pull-left avatar mar-rgt">
												<img src="img/av7.png" alt="" class="img-sm">
												<i class="off bottom text-light"></i>
											</div>
											<div class="inline-block">
												<div class="text-small">Steven Jordan</div>
												<small class="text-mute">Available</small>
											</div>
										</div>
									</div>
								</div>
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<!--End first tab (Contact list)-->
								<!--Second tab (Settings)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<div class="tab-pane fade" id="demo-asd-tab-3">
									<ul class="list-group bg-trans">
										<li class="list-header">
											<h4 class="text-thin">Account Settings</h4>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											<p>Show my personal status</p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											<p>Show offline contact</p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox">
											</div>
											<p>Invisible mode </p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
									</ul>
									<hr>
									<ul class="list-group bg-trans">
										<li class="list-header">
											<h4 class="text-thin">Public Settings</h4>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											Online status
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox">
											</div>
											Show offline contact
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											Show my device icon
										</li>
									</ul>
									<hr>
									<h4 class="pad-hor text-thin">Task Progress</h4>
									<div class="pad-all">
										<p>Upgrade Progress</p>
										<div class="progress progress-sm">
											<div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>
										</div>
										<small class="text-muted">15% Completed</small>
									</div>
									<div class="pad-hor">
										<p>Database</p>
										<div class="progress progress-sm">
											<div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>
										</div>
										<small class="text-muted">17/23 Database</small>
									</div>
								</div>
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<!--Second tab (Settings)-->
							</div>
							<!-- Tabs Content End -->
							<!--================================-->
						</div>
					</div>
				</div>
			</aside>
			<!--===================================================-->
			<!--END ASIDE-->
		</div>
		<!-- FOOTER -->
		<!--===================================================-->
		<footer id="footer">
			<!-- Visible when footer positions are fixed -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="show-fixed pull-right">
				<ul class="footer-list list-inline">
					<li>
						<p class="text-sm">SEO Proggres</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-danger"></div>
						</div>
					</li>
					<li>
						<p class="text-sm">Online Tutorial</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-primary"></div>
						</div>
					</li>
					<li>
						<button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
					</li>
				</ul>
			</div>
			<!-- Visible when footer positions are static -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="hide-fixed pull-right pad-rgt">Currently v2.2</div>
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<p class="pad-lft">&#0169; 2015 Your Company</p>
		</footer>
		<!--===================================================-->
		<!-- END FOOTER -->
		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->
	<!--JAVASCRIPT-->
	<!--=================================================-->
	<!--jQuery [ REQUIRED ]-->
	<script src="js/jquery-2.1.1.min.js"></script>
	<!--jQuery UI [ REQUIRED ]-->
	<script src="js/jquery-ui.min.js"></script>
	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="js/bootstrap.min.js"></script>
	<!--Fast Click [ OPTIONAL ]-->
	<script src="plugins/fast-click/fastclick.min.js"></script>
	<!--Jasmine Admin [ RECOMMENDED ]-->
	<script src="js/scripts.js"></script>
	<!--Switchery [ OPTIONAL ]-->
	<script src="plugins/switchery/switchery.min.js"></script>
	<!--Bootstrap Select [ OPTIONAL ]-->
	<script src="plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<script src="plugins/tag-it/tag-it.min.js"></script>
	<!--Chosen [ OPTIONAL ]-->
	<script src="plugins/chosen/chosen.jquery.min.js"></script>
	<!--noUiSlider [ OPTIONAL ]-->
	<script src="plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
	<!--Bootstrap Timepicker [ OPTIONAL ]-->
	<script src="plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
	<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<script src="plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<!--Dropzone [ OPTIONAL ]-->
	<script src="plugins/dropzone/dropzone.min.js"></script>
	<!--Dropzone [ OPTIONAL ]-->
	<script src="plugins/ion-rangeslider/ion.rangeSlider.min.js"></script>
	<!--Masked Input [ OPTIONAL ]-->
	<script src="plugins/masked-input/jquery.maskedinput.min.js"></script>
	<script src="plugins/fooTable/dist/footable.all.min.js"></script>
	<!--Summernote [ OPTIONAL ]-->
	<script src="plugins/summernote/summernote.min.js"></script>
	<!--Fullscreen jQuery [ OPTIONAL ]-->
	<script src="plugins/screenfull/screenfull.js"></script>
	<!--Demo script [ DEMONSTRATION ]-->
	<script src="js/demo/jasmine.js"></script>
	<script src="js/demo/tables-footable.js"></script>
	<!--Form Component [ SAMPLE ]-->
	<script src="js/demo/form-component.js"></script>
	<script src="plugins/datatables/media/js/jquery.dataTables.js"></script>
	<script src="plugins/datatables/media/js/dataTables.bootstrap.js"></script>
	<script src="plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<!--DataTables Sample [ SAMPLE ]-->
	<!--Bootstrap Tags Input [ OPTIONAL ]-->
	<script src="plugins/tag-it/tag-it.min.js"></script>
	<script src="js/demo/tables-datatables.js"></script>
</body>

</html>
