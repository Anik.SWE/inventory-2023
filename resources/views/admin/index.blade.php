@extends('admin.app')

@section('content')
    <div class="pageheader">
        <h3><i class="fa fa-home"></i> Dashboard </h3>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li> <a href="#"> Home </a> </li>
                <li class="active"> Dashboard </li>
            </ol>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-content">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-default widgetbox-item-icon">
                    <div class="widgetbox-item-left widgetbox-icon-success"> <span class="fa fa-building-o"></span> </div>
                    <div class="widgetbox-data">
                        <div class="widgetbox-int"> 5,500 </div>
                        <div class="widgetbox-title"> Stolas User </div>
                        <div class="widgetbox-subtitle">Under this service</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-default widgetbox-item-icon">
                    <div class="widgetbox-item-left widgetbox-icon-success"> <span class="fa fa-building-o"></span> </div>
                    <div class="widgetbox-data">
                        <div class="widgetbox-int"> 12,500 </div>
                        <div class="widgetbox-title"> Business Profile</div>
                        <div class="widgetbox-subtitle">Under this service</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-default widgetbox-item-icon">
                    <div class="widgetbox-item-right widgetbox-icon-warning"> <span class="fa fa-female"></span> </div>
                    <div class="widgetbox-data-left">
                        <div class="widgetbox-int"> 6,500 </div>
                        <div class="widgetbox-title"> User </div>
                        <div class="widgetbox-subtitle"> Under this service </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-default widgetbox-item-icon">
                    <div class="widgetbox-item-left widgetbox-icon-danger"> <span class="fa fa-male"></span>
                    </div>
                    <div class="widgetbox-data">
                        <div class="widgetbox-int"> 6,000 </div>
                        <div class="widgetbox-title"> User </div>
                        <div class="widgetbox-subtitle"> Under this service </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
