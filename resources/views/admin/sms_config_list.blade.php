@extends('admin.app')

@section('content')
    <div class="pageheader">
        <h3><i class="fa fa-home"></i>SMS Config Details</h3>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li> <a href="#"> SMS </a> </li>
                <li class="active"> SMS Config List</li>
            </ol>
        </div>
    </div>
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">SMS Configuration List <div class="pull-right">
                        <a href="sms_config.html"><button class="btn btn-primary btn-labeled fa fa-plus-square">SMS
                                Config</button></a>
                    </div>
                </h3>
            </div>
            <div class="panel-body">
                <div class="pad-btm form-inline">
                    <div class="row">
                        <div class="col-sm-6 text-xs-center">
                            <div class="form-group">
                                <label class="control-label">Status</label>
                                <select id="demo-foo-filter-status" class="form-control">
                                    <option value="">Show all</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 text-xs-center text-right">
                            <div class="form-group">
                                <input id="demo-foo-search" type="text" placeholder="Search" class="form-control"
                                    autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Foo Table - Filtering -->
                <!--===================================================-->
                <table id="demo-foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive"
                    data-page-size="7">
                    <thead>
                        <tr>
                            <th data-toggle="true">User Name</th>
                            <th>Business Name</th>
                            <th>SMS Rate</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($sms_config))
                        @foreach ($sms_config as $row)
                            <tr>
                                <td>{{ @$row->user_rel->name }}</td>
                                <td>{{ @$row->business_rel->business_name }}</td>

                                <td>{{ $row->sms_rate }}</td>
                                <td class="text-center">
                                    <a href="{{ route('sms-config.edit', $row->id) }}"><i class="fa fa-list"></i></a>
                                </td>

                            </tr>
                        @endforeach
                    @endif

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"></ul>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <!--===================================================-->
                <!-- End Foo Table - Filtering -->
            </div>
        </div>
    </div>
@endsection
