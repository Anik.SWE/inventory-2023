<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> Stolas - Business Management Solution.</title>
	<link rel="shortcut icon" href="img/favicon.ico">
	<!--STYLESHEET-->
	<!--=================================================-->
	<!--Roboto Slab Font [ OPTIONAL ] -->
	<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400" rel="stylesheet">
	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!--Jasmine Stylesheet [ REQUIRED ]-->
	<link href="css/style.css" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!--Switchery [ OPTIONAL ]-->
	<link href="plugins/switchery/switchery.min.css" rel="stylesheet">
	<!--Bootstrap Select [ OPTIONAL ]-->
	<link href="plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
	<!--Bootstrap Validator [ OPTIONAL ]-->
	<link href="plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">
	<!--jVector Map [ OPTIONAL ]-->
	<link href="plugins/jvectormap/jquery-jvectormap.css" rel="stylesheet">
	<!--Demo [ DEMONSTRATION ]-->
	<link href="css/demo/jquery-steps.min.css" rel="stylesheet">
	<!--Demo [ DEMONSTRATION ]-->
	<link href="css/demo/jasmine.css" rel="stylesheet">
	<!--SCRIPT-->
	<!--=================================================-->
	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="plugins/pace/pace.min.css" rel="stylesheet">
	<script src="plugins/pace/pace.min.js"></script>
</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="effect mainnav-lg navbar-fixed mainnav-fixed">
		<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">
				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">
					<a href="index.html" class="navbar-brand">
						<i class="fa fa-graduation-cap brand-icon"></i>
						<div class="brand-title">
							<span class="brand-text">EduERP</span>
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->
				<!--Navbar Dropdown-->
				<!--================================-->
				<div class="navbar-content clearfix">
					<ul class="nav navbar-top-links pull-left">
						<li class="tgl-menu-btn">
							<a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
						</li>
					</ul>
					<ul class="nav navbar-top-links pull-right">
						<li id="dropdown-user" class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right"> <img class="img-circle img-user media-object" src="img/av1.png" alt="Profile Picture"> </span>
								<div class="username hidden-xs">John Doe</div>
							</a>
							<div class="dropdown-menu dropdown-menu-right with-arrow">
								<!-- User dropdown menu -->
								<ul class="head-list">
									<li>
										<a href="profile_settings.html"> <i class="fa fa-user fa-fw fa-lg"></i> প্রোফাইল </a>
									</li>
									<li>
										<a href="profile_settings.html"> <i class="fa fa-gear fa-fw fa-lg"></i> সেটিংস </a>
									</li>
									<li>
										<a href="#"> <i class="fa fa-sign-out fa-fw"></i> লগ-আউট </a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
			</div>
		</header>
		<!--===================================================-->
		<!--END NAVBAR-->
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				<div class="pageheader">
					<h3><i class="fa fa-building-o"></i> Client Profile </h3>
					<div class="breadcrumb-wrapper">
						<span class="label">You are here:</span>
						<ol class="breadcrumb">
							<li> <a href="#"> Client </a> </li>
							<li class="active"> Client Profile </li>
						</ol>
					</div>
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<div class="panel">
								<div class="panel-body">
									<button class="btn btn-block btn-danger">
										<i class="fa fa-lock"></i> System Lock
									</button>
									<button class="btn btn-block btn-primary">
										<i class="fa fa-unlock"></i> System Unlock
									</button>
								</div>
								<div class="panel-footer">
									<h3>This System In <span style="color:red;">Look</span> Now</h3>
									<h3>This System In <span style="color:green;">Active</span> Now</h3>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<div class="panel">
								<div class="panel-body pad-no">

									<!--Default Tabs (Left Aligned)-->
									<!--===================================================-->
									<div class="tab-base">

										<!--Nav Tabs-->
										<ul class="nav nav-tabs">
											<li class="active"> <a data-toggle="tab" href="#demo-lft-tab-1">Profile </a> </li>
											<li> <a data-toggle="tab" href="#demo-lft-tab-2"> Payment Info</a> </li>
										</ul>

										<!--Tabs Content-->
										<div class="tab-content">
											<div id="demo-lft-tab-1" class="tab-pane fade active in">

												<!-- Timeline -->
												<!--===================================================-->
												<div class="timeline">

													<!-- Timeline header -->
													<div class="timeline-header">
														<div class="timeline-header-title bg-info">Business Permission</div>
													</div>
													<div class="timeline-entry">
														<div class="timeline-stat">
															<div class="timeline-icon bg-purple"><i class="fa fa-check fa-lg"></i> </div>
														</div>
														<div class="timeline-label">
															<form action="index.html" method="post">
																<div class="row">
																	<div class="col-md-8 col-sm-12">
																		<div class="form-group">
																			<label for="demo-vs-definput" class="control-label">Number Of Business Creation Permission</label>
																			<input type="text" value="1" class="form-control" required>
																		</div>
																	</div>
																	<div class="col-md-4 col-sm-12">
																		<div class="form-group pull-right">
																			<button class="btn btn-sm btn-primary">
																				Update
																			</button>
																		</div>
																	</div>
																</div>
															</form>
														</div>
														<div class="timeline-header">
															<div class="timeline-header-title bg-info">Basic Info</div>
														</div>
														<div class="timeline-stat">
															<div class="timeline-icon bg-purple"><i class="fa fa-check fa-lg"></i> </div>
														</div>
														<div class="timeline-label">
															<table class="table">
																<tbody>
																	<tr>
																		<td> <strong>Client Name:</strong> </td>
																		<td> Al-Amin Shekh</td>
																	</tr>
																	<tr>
																		<td> <strong>Phone:</strong> </td>
																		<td>+88 0147845785</td>
																	</tr>
																	<tr>
																		<td> <strong>Email:</strong> </td>
																		<td> bhaibhaiseeds@gmail.com </td>
																	</tr>
																	<tr>
																		<td> <strong>Password:</strong> </td>
																		<td> Admin3210 </td>
																	</tr>
																</tbody>
															</table>
														</div>
														<div class="timeline-header">
															<div class="timeline-header-title bg-info">Businesses</div>
														</div>
														<div class="timeline-stat">
															<div class="timeline-icon bg-purple"><i class="fa fa-check fa-lg"></i> </div>
														</div>
														<div class="timeline-label">
															<div class="row">
																<div class="panel">
																	<div class="panel-body">
																		<div class="col-md-6">
																			<div class="panel text-center">
																				<div class="panel-body bg-purple pad-ver" style=" background-color: #7266BA; color: #FFFFFF;">
																					<div class="text-center">
																						<img src="{{ asset('user_assets/img/shop.png') }}" style=" height: 50px; width:50px; border-radius: 7px!important;">
																						<h4>Bhai Bhai Seeds</h4>
																					</div>
																				</div>
																				<div class="panel-footer">
																					<button class="btn btn-block btn-danger">
																						<i class="fa fa-lock"></i> Business Lock
																					</button>
																					<button class="btn btn-block btn-primary">
																						<i class="fa fa-unlock"></i> Business Active
																					</button>
																				</div>
																				<!--===================================================-->
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="panel text-center">
																				<div class="panel-body bg-purple pad-ver" style=" background-color: #7266BA; color: #FFFFFF;">
																					<div class="text-center">
																						<img src="{{ asset('user_assets/img/shop.png') }}" style=" height: 50px; width:50px; border-radius: 7px!important;">
																						<h4>SOBUJ KRISHI VANDER</h4>
																					</div>
																				</div>
																				<div class="panel-footer">
																					<button class="btn btn-block btn-danger">
																						<i class="fa fa-lock"></i> System Lock
																					</button>
																					<button class="btn btn-block btn-primary">
																						<i class="fa fa-unlock"></i> Business Active
																					</button>
																				</div>
																				<!--===================================================-->
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--===================================================-->
												<!-- End Timeline -->
											</div>
											<div id="demo-lft-tab-2" class="tab-pane fade">
												<!--Hover Rows-->
												<!--===================================================-->
												<table class="table table-hover table-vcenter">
													<thead>
														<tr>
															<th>Service Month</th>
															<th>Payable</th>
															<th>Payment Date</th>
															<th>Amount</th>
															<th>Due</th>
															<th>Service Month For</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>April, 2022</td>
															<td>৳ 50,000.00 /-</td>
															<td>12 April, 2022</td>
															<td>৳ 40,000.00 /-</td>
															<td>৳ 10,000.00 /-</td>
															<td>April, 2022</td>
															<td>
																<button type="button" class="btn btn-danger btn-sm btn-small"><i class="fa fa-trash"></i></button>
															</td>
														</tr>
														<tr>
															<td>April, 2022</td>
															<td>৳ 50,000.00 /-</td>
															<td>12 April, 2022</td>
															<td>৳ 40,000.00 /-</td>
															<td>৳ 10,000.00 /-</td>
															<td>April, 2022</td>
															<td>
																<button type="button" class="btn btn-danger btn-sm btn-small"><i class="fa fa-trash"></i></button>
															</td>
														</tr>
														<tr>
															<td>April, 2022</td>
															<td>৳ 50,000.00 /-</td>
															<td>12 April, 2022</td>
															<td>৳ 40,000.00 /-</td>
															<td>৳ 10,000.00 /-</td>
															<td>April, 2022</td>
															<td>
																<button type="button" class="btn btn-danger btn-sm btn-small"><i class="fa fa-trash"></i></button>
															</td>
														</tr>
														<tr>
															<td>April, 2022</td>
															<td>৳ 50,000.00 /-</td>
															<td>12 April, 2022</td>
															<td>৳ 40,000.00 /-</td>
															<td>৳ 10,000.00 /-</td>
															<td>April, 2022</td>
															<td>
																<button type="button" class="btn btn-danger btn-sm btn-small"><i class="fa fa-trash"></i></button>
															</td>
														</tr>
														<tr>
															<td>April, 2022</td>
															<td>৳ 50,000.00 /-</td>
															<td>12 April, 2022</td>
															<td>৳ 40,000.00 /-</td>
															<td>৳ 10,000.00 /-</td>
															<td>April, 2022</td>
															<td>
																<button type="button" class="btn btn-danger btn-sm btn-small"><i class="fa fa-trash"></i></button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<!--===================================================-->
									<!--End Default Tabs (Left Aligned)-->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->
			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
			<div class="modal fade" id="modaladd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm modal-notify" role="document">
					<div class="panel panel-bordered panel-primary">
						<div class="panel-heading">
							<div class="panel-control">
								<button class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i></button>
							</div>
							<h3 class="panel-title">Add a Attachments</h3>
						</div>
						<div class="panel-body">
							<form action="index.html" method="post">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="demo-vs-definput" class="control-label">Select Image</label>
											<input type="file" id="demo-vs-definput" class="form-control">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-offset-4 col-md-4">
												<button class="btn btn-block btn-primary">
													Insert
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!--MAIN NAVIGATION-->
			<!--===================================================-->
			<nav id="mainnav-container">
				<div id="mainnav">
					<!--Menu-->
					<!--================================-->
					<div id="mainnav-menu-wrap">
						<div class="nano">
							<div class="nano-content">
								<ul id="mainnav-menu" class="list-group">
									<!--Category name-->
									<!-- <li class="list-header">
										<img src="img/mise.png" alt="">
									</li> -->
									<li class="list-header">Navigation</li>
									<!--Menu list item-->
									<li><a href="index.html"><i class="fa fa-home"></i> Dashboard</a></li>
									<!--Menu list item-->
									<li class="list-divider"></li>
									<!--Category name-->
									<li class="list-header">Stolas</li>
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-users"></i>
											<span class="menu-title">
												<strong>Stolas Clients</strong>
											</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">

											<li><a href="user_list.html"><i class="fa fa-caret-right"></i> Manage Client </a></li>
										</ul>
									</li>
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-comment"></i>
											<span class="menu-title">
												<strong>SMS</strong>
											</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="sms_config.html"><i class="fa fa-caret-right"></i> Config. SMS</a></li>
											<li><a href="manage_config.html"><i class="fa fa-caret-right"></i> Manage Config.</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-money"></i>
											<span class="menu-title">Payment</span>
											<i class="arrow"></i>
										</a>
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="payment_info.html"><i class="fa fa-caret-right"></i> Collect Payment </a></li>
										</ul>
									</li>
									<li class="list-divider"></li>
								</ul>
							</div>
						</div>
					</div>
					<!--================================-->
					<!--End menu-->
				</div>
			</nav>
			<!--===================================================-->
			<!--END MAIN NAVIGATION-->
			<!--END ASIDE-->
		</div>
		<!-- FOOTER -->
		<!--===================================================-->
		<footer id="footer">
			<!-- Visible when footer positions are fixed -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="show-fixed pull-right">
				<ul class="footer-list list-inline">
					<li>
						<p class="text-sm">SEO Proggres</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-danger"></div>
						</div>
					</li>
					<li>
						<p class="text-sm">Online Tutorial</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-primary"></div>
						</div>
					</li>
					<li>
						<button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
					</li>
				</ul>
			</div>
			<!-- Visible when footer positions are static -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="hide-fixed pull-right pad-rgt">Currently v1.0</div>
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<p class="pad-lft">&#0169; 2022 Technical support by <a href="https://www.techlozi.com/" target="_blank">techlozi</a></p>
		</footer>
		<!--===================================================-->
		<!-- END FOOTER -->
		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->
	<!--JAVASCRIPT-->
	<!--=================================================-->
	<script src="js/jquery-2.1.1.min.js"></script>
	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="js/bootstrap.min.js"></script>
	<!--Fast Click [ OPTIONAL ]-->
	<script src="plugins/fast-click/fastclick.min.js"></script>
	<!--Jasmine Admin [ RECOMMENDED ]-->
	<script src="js/scripts.js"></script>
	<!--Switchery [ OPTIONAL ]-->
	<script src="plugins/switchery/switchery.min.js"></script>
	<!--Jquery Steps [ OPTIONAL ]-->
	<script src="plugins/parsley/parsley.min.js"></script>
	<!--Jquery Steps [ OPTIONAL ]-->
	<script src="plugins/jquery-steps/jquery-steps.min.js"></script>
	<!--Bootstrap Select [ OPTIONAL ]-->
	<script src="plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<!--Bootstrap Wizard [ OPTIONAL ]-->
	<script src="plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
	<!--Masked Input [ OPTIONAL ]-->
	<script src="plugins/masked-input/bootstrap-inputmask.min.js"></script>
	<!--Bootstrap Validator [ OPTIONAL ]-->
	<script src="plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
	<!--Flot Chart [ OPTIONAL ]-->
	<script src="plugins/flot-charts/jquery.flot.min.js"></script>
	<script src="plugins/flot-charts/jquery.flot.resize.min.js"></script>
	<!--Flot Order Bars Chart [ OPTIONAL ]-->
	<script src="plugins/flot-charts/jquery.flot.categories.js"></script>
	<!--jvectormap [ OPTIONAL ]-->
	<script src="plugins/jvectormap/jquery-jvectormap.min.js"></script>
	<script src="plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
	<!--Easy Pie Chart [ OPTIONAL ]-->
	<script src="plugins/easy-pie-chart/jquery.easypiechart.min.js"></script>
	<!--Fullscreen jQuery [ OPTIONAL ]-->
	<script src="plugins/screenfull/screenfull.js"></script>
	<!--Form Wizard [ SAMPLE ]-->
	<script src="js/demo/index.js"></script>
	<!--Form Wizard [ SAMPLE ]-->
	<script src="js/demo/wizard.js"></script>
	<!--Demo script [ DEMONSTRATION ]-->
	<script src="js/demo/jasmine.js"></script>
	<!--Form Wizard [ SAMPLE ]-->
	<script src="js/demo/form-wizard.js"></script>
</body>

</html>
