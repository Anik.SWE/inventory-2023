<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->

                        <li class="list-header">Navigation</li>
                        <!--Menu list item-->
                        <li><a href="{{route("user")}}"><i class="fa fa-home"></i> Dashboard</a></li>
                        <!--Menu list item-->
                        <li class="list-divider"></li>
                        <!--Category name-->
                        <li class="list-header">Stolas</li>
                        <!--Menu list item-->
                        <li>
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span class="menu-title">
                                    <strong>Stolas Clients</strong>
                                </span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li><a href="user_list.html"><i class="fa fa-caret-right"></i> Manage
                                        Client </a></li>
                            </ul>
                        </li>
                        <!--Menu list item-->
                        <li>
                            <a href="#">
                                <i class="fa fa-comment"></i>
                                <span class="menu-title">
                                    <strong>SMS</strong>
                                </span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li><a href="{{route("sms-config.create")}}"><i class="fa fa-caret-right"></i> Config.
                                        SMS</a></li>
                                <li><a href="{{route("sms-config.index")}}"><i class="fa fa-caret-right"></i>
                                        Manage Config.</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">Payment</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li><a href="payment_info.html"><i class="fa fa-caret-right"></i> Collect
                                        Payment </a></li>
                            </ul>
                        </li>
                        <li class="list-divider"></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->
    </div>
</nav>
