<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Stolas - Business Management Solution.</title>

    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400"
        rel="stylesheet">
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/style.css') }}" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('common_style.css') }}" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!--Switchery [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/bootstrap-validator/bootstrapValidator.min.css') }}" rel="stylesheet">
    <!--jVector Map [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('user_assets/css/demo/jquery-steps.min.css') }}" rel="stylesheet">
    <!--Bootstrap Table [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('user_assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"
        rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('user_assets/css/demo/jasmine.css') }}" rel="stylesheet">

    @yield('styles')

    <!--Bootstrap Table [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('user_assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"
        rel="stylesheet">

    <!--SCRIPT-->
    <!--=================================================-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('user_assets/plugins/pace/pace.min.js') }}"></script>
    <!--FooTable [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/fooTable/css/footable.core.css') }}" rel="stylesheet">
    <!--SCRIPT-->
    @yield('styles')
</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
    <div id="container" class="effect mainnav-lg navbar-fixed mainnav-fixed">
        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
                <!--Brand logo & name-->
                <!--================================-->
                <div class="navbar-header">
                    <a href="index.html" class="navbar-brand">
                        <i class="fa fa-cube brand-icon"></i>
                        <div class="brand-title">
                            <span class="brand-text">Stolas</span>
                        </div>
                    </a>
                </div>
                <!--================================-->
                <!--End brand logo & name-->
                <!--Navbar Dropdown-->
                <!--================================-->
                <div class="navbar-content clearfix">
                    <ul class="nav navbar-top-links pull-left">
                        <!--Navigation toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    </ul>
                    <ul class="nav navbar-top-links pull-right">
                        <!--Fullscreen toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="hidden-xs" id="toggleFullscreen">
                            <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                                <span class="sr-only">Toggle fullscreen</span>
                            </a>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li id="dropdown-user" class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="pull-right"> <img class="img-circle img-user media-object"
                                        src="img/av1.png" alt="Profile Picture"> </span>
                                <div class="username hidden-xs">John Doe</div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right with-arrow">
                                <!-- User dropdown menu -->
                                <ul class="head-list">
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out fa-fw"></i>{{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End user dropdown-->
                    </ul>
                </div>
                <!--================================-->
                <!--End Navbar Dropdown-->
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->
        <div class="boxed">
            <div id="content-container">
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                @yield('content')

                <!--===================================================-->
                <!--End page content-->
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            @include('admin.nav')

            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
            <!--END ASIDE-->
        </div>
        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">Currently v1.0</div>
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <p class="pad-lft">&#0169; 2022 Technical support by <a href="https://www.techlozi.com/"
                    target="_blank">techlozi</a></p>
        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->
        <!-- SCROLL TOP BUTTON -->
        <!--===================================================-->
        <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
    <!--JAVASCRIPT-->

    <script src="{{ asset('user_assets/js/jquery-2.1.1.min.js') }}"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/bootstrap.min.js') }}"></script>
    <!--Fast Click [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/fast-click/fastclick.min.js') }}"></script>
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/scripts.js') }}"></script>

    <!--Switchery [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/switchery/switchery.min.js') }}"></script>
    <!--Jquery Steps [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/parsley/parsley.min.js') }}"></script>
    <!--Jquery Steps [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/jquery-steps/jquery-steps.min.js') }}"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <!--Bootstrap Wizard [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <!--Masked Input [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/masked-input/bootstrap-inputmask.min.js') }}"></script>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-validator/bootstrapValidator.min.js') }}"></script>
    <!--FooTable [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/fooTable/dist/footable.all.min.js') }}"></script>
    <!--Flot Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.resize.min.js') }}"></script>
    <!--Flot Order Bars Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <!--jvectormap [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <!--Easy Pie Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <!--Fullscreen jQuery [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/screenfull/screenfull.js') }}"></script>

    <script src="{{ asset('user_assets/plugins/jquery-print/jQuery.print.js') }}"></script>
    <!--Form Wizard [ SAMPLE ]-->
    <script src="{{ asset('user_assets/js/demo/form-wizard.js') }}"></script>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}

    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script> --}}


    <script src="{{ asset('user_assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('user_assets/js/demo/tables-datatables.js') }}"></script>
    @yield('script')
    @yield('scripts')
</body>

</html>
