<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> Inventory Management</title>
    <link rel="shortcut icon" href="{{ asset('assets/images/stock.png') }}">
    <!--STYLESHEET-->
    <!--=================================================-->
    <!--Roboto Slab Font [ OPTIONAL ] -->



    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400"
        rel="stylesheet">
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/style.css') }}" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('common_style.css') }}" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!--Switchery [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/bootstrap-validator/bootstrapValidator.min.css') }}" rel="stylesheet">
    <!--jVector Map [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('user_assets/css/demo/jquery-steps.min.css') }}" rel="stylesheet">
    <!--Bootstrap Table [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('user_assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"
        rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('user_assets/css/demo/jasmine.css') }}" rel="stylesheet">

    @yield('styles')

    <!--Bootstrap Table [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('user_assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"
        rel="stylesheet">

    <!--SCRIPT-->
    <!--=================================================-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('user_assets/plugins/pace/pace.min.js') }}"></script>
    <!--FooTable [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/fooTable/css/footable.core.css') }}" rel="stylesheet">
    <!--SCRIPT-->
    <!--=================================================-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="plugins/pace/pace.min.css" rel="stylesheet">

    @yield('topscript')


    <script src="plugins/pace/pace.min.js"></script>

    <style>
    .footer {
       position: fixed;
       left: 0;
       bottom: 0;
       width: 100%;
       background-color: #25a79f;
       color: white;
       text-align: center;
    }
    </style>

</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body onload="auto_total(); on_discount();">

  @if(!Session::get('business_id'))

      <?php
      // index.php
      header("Location: http://127.0.0.1:8000/home");
      exit();
      ?>

  @endif

    <div id="container" class="effect mainnav-sm navbar-fixed mainnav-fixed">
        <!--NAVBAR-->
        <!--===================================================-->
        <header id="navbar">
            <div id="navbar-container" class="boxed">
                <!--Brand logo & name-->
                <!--================================-->
                <div class="navbar-header">
                    <a href="{{ route('dashboard') }}" class="navbar-brand">
                        <i class="fa fa-cube brand-icon"></i>
                        <div class="brand-title">
                            <span class="brand-text">Inventory</span>
                        </div>
                    </a>
                </div>
                <!--================================-->
                <!--End brand logo & name-->
                <!--Navbar Dropdown-->
                <!--================================-->
                <div class="navbar-content clearfix">
                    <ul class="nav navbar-top-links pull-left">
                        <!--Navigation toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="tgl-menu-btn">
                            <a class="mainnav-toggle" href="#"> <i class="fa fa-navicon fa-lg"></i> </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links pull-right">

                        <!--Fullscreen toogle button-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li class="hidden-xs" id="toggleFullscreen">
                            <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                                <span class="sr-only">Toggle fullscreen</span>
                            </a>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End Fullscreen toogle button-->


                        <!--User dropdown-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <li id="dropdown-user" class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="pull-right"> <img class="img-circle img-user media-object"
                                        src="{{ asset('user_assets/img/av1.png') }}" alt="Profile Picture"> </span>
                                <div class="username hidden-xs">
                                    {{-- @if (Auth::guard('admin'))
                                        {{ Auth::guard('admin')->user()->name }}
                                    @else
                                        {{ Auth::user()->name }}
                                    @endif --}}
                                    {{ Auth::user()->name }}
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right with-arrow">
                                <!-- User dropdown menu -->
                                <ul class="head-list">
                                    <li>
                                        <a href="{{ route('profile.form') }}"> <i class="fa fa-user fa-fw fa-lg"></i>
                                            Profile </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out fa-fw"></i>{{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End user dropdown-->
                    </ul>
                </div>
                <!--================================-->
                <!--End Navbar Dropdown-->
            </div>
        </header>
        <!--===================================================-->
        <!--END NAVBAR-->
        <div class="boxed">

            @yield('content')

            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">
                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">
                                <ul id="mainnav-menu" class="list-group">
                                    <!--Category name-->
                                    <li class="list-header">Navigation</li>
                                    <!--Menu list item-->
                                    <li>
                                        <a href="{{ route('dashboard') }}">
                                            <i class="fa fa-dashboard"></i>
                                            <span class="menu-title">Dashboard</span>
                                        </a>
                                    </li>
                                    {{-- <li>
                                        <a href="">
                                            <i class="fa fa-lock"></i>
                                            <span class="menu-title">
                                                <strong>Permission</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('role.add') }}"><i class="fa fa-caret-right"></i>
                                                    Add
                                                    Role
                                                </a></li>
                                            <li><a href="{{ route('AllS') }}"><i class="fa fa-caret-right"></i> All
                                                    Supplier
                                                </a></li>
                                        </ul>
                                    </li> --}}
                                    @php
                                        $usr = Auth::guard('admin')->user();
                                    @endphp

                                    {{-- @if ($usr->can('dashboard.view'))
                                        <li class="active">
                                            <a href="javascript:void(0)" aria-expanded="true"><i
                                                    class="ti-dashboard"></i><span>dashboard</span></a>
                                            <ul class="collapse">
                                                <li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}"><a
                                                        href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                            </ul>
                                        </li>
                                    @endif --}}

                                    {{-- @if ($usr->can('role.create') || $usr->can('role.view') || $usr->can('role.edit') || $usr->can('role.delete')) --}}
                                    <!--<li>
                                        <a href="">
                                            <i class="fa fa-user"></i>
                                            <span class="menu-title">
                                                <strong>Roles & Permissions</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <ul
                                            class="collapse {{ Route::is('admin.roles.create') || Route::is('admin.roles.index') || Route::is('admin.roles.edit') || Route::is('admin.roles.show') ? 'in' : '' }}">
                                            {{-- @if ($usr->can('role.view')) --}}
                                            <li
                                                class="{{ Route::is('admin.roles.index') || Route::is('admin.roles.edit') ? 'active' : '' }}">
                                                <a href="{{ route('admin.roles.index') }}"><i
                                                        class="fa fa-caret-right"></i>All Roles</a>
                                            </li>
                                            {{-- @endif --}}
                                            {{-- @if ($usr->can('role.create')) --}}
                                            <li class="{{ Route::is('admin.roles.create') ? 'active' : '' }}">
                                                <a href="{{ route('admin.roles.create') }}"><i
                                                        class="fa fa-caret-right"></i>Create Role</a>
                                            </li>
                                            {{-- @endif --}}
                                        </ul>
                                    </li>-->
                                    {{-- @endif --}}


                                    {{-- @if ($usr->can('admin.create') || $usr->can('admin.view') || $usr->can('admin.edit') || $usr->can('admin.delete')) --}}
                                    <!--<li>
                                        <a href="">
                                            <i class="fa fa-user"></i>
                                            <span class="menu-title">
                                                <strong>Admins</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <ul
                                            class="collapse {{ Route::is('admin.admins.create') || Route::is('admin.admins.index') || Route::is('admin.admins.edit') || Route::is('admin.admins.show') ? 'in' : '' }}">

                                            {{-- @if ($usr->can('admin.view')) --}}
                                            <li
                                                class="{{ Route::is('admin.admins.index') || Route::is('admin.admins.edit') ? 'active' : '' }}">
                                                <a href="{{ route('admin.admins.index') }}"><i
                                                        class="fa fa-caret-right"></i>All Admins</a>
                                            </li>
                                            {{-- @endif --}}

                                            {{-- @if ($usr->can('admin.create')) --}}
                                            <li class="{{ Route::is('admin.admins.create') ? 'active' : '' }}">
                                                <a href="{{ route('admin.admins.create') }}"><i
                                                        class="fa fa-caret-right"></i>Create Admin</a>
                                            </li>
                                            {{-- @endif --}}
                                        </ul>
                                    </li>-->
                                    {{-- @endif --}}








                                    <li class="list-divider"></li>
                                    <!--Category name-->
                                    <li class="list-header">Components</li>
                                    <!--Menu list item-->
                                    <li>
                                        <a href="{{ route('view.pos') }}">
                                            <i class="fa fa-table"></i>
                                            <span class="menu-title">
                                                <strong>POS </strong>
                                            </span>
                                        </a>
                                    </li>


                            {{--        <li>
                                        <a href="">
                                            <i class="fa fa-file-text-o"></i>
                                            <span class="menu-title">
                                                <strong>Invoice List</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li>
                                                <a href="{{ route('customar.invoice') }}"><i class="fa fa-caret-right"></i>
                                                Customar Invoice
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('supplier.invoice') }}"><i class="fa fa-caret-right"></i>
                                                Supplier Invoice
                                                </a>
                                            </li>
                                        </ul>
                                    </li>


                                    <li>
                                        <a href="">
                                            <i class="fa fa-dollar"></i>
                                            <span class="menu-title">
                                                <strong>Manage Due</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li>
                                                <a href="{{ route('due.collection') }}">
                                                    <i class="fa fa-caret-right"></i>
                                                    <span class="menu-title">Due Collection</span>
                                                </a>
                                            </li>
                                            <!--<li><a href="{{ route('customar.due.list') }}"><i class="fa fa-caret-right"></i>
                                                    Customar Due List
                                                </a></li>

                                            <li>
                                                <a href="{{ route('supplier.due.list') }}">
                                                    <i class="fa fa-caret-right"></i>
                                                    <span class="menu-title">
                                                        <strong>Supplier Due List </strong>
                                                    </span>
                                                </a>
                                            </li>-->
                                        </ul>
                                    </li>


                                    <!--<li>
                                        <a href="{{ route('view.stock') }}">
                                            <i class="fa fa-cube"></i>
                                            <span class="menu-title">
                                                <strong>Product Stock</strong>
                                            </span>
                                        </a>
                                    </li>-->

                                    <li>
                                        <a href="{{ route('customar.addform') }}">
                                            <i class="fa fa-user"></i>
                                            <span class="menu-title">
                                                <strong>Customar</strong>
                                            </span>
                                        </a>
                                    </li>
                                    <!--<li>
                                        <a href="">
                                            <i class="fa fa-user"></i>
                                            <span class="menu-title">
                                                <strong>Employees</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>

                                        <ul class="collapse">
                                            <li><a href="{{ route('Eadd') }}"><i class="fa fa-caret-right"></i> Add
                                                    Employee
                                                </a></li>
                                            <li><a href="{{ route('AllE') }}"><i class="fa fa-caret-right"></i> All
                                                    Employee
                                                </a></li>

                                            <li>
                                                <a href="{{ route('emp.attendance') }}">
                                                    <i class="fa fa-caret-right"></i>
                                                    <span class="menu-title">
                                                        <strong>Emp Attendance </strong>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>-->

                                    <li>
                                        <a href="">
                                            <i class="fa fa-user-plus"></i>
                                            <span class="menu-title">
                                                <strong>Suppliers</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('Sadd') }}"><i class="fa fa-caret-right"></i> Add
                                                    Supplier
                                                </a></li>
                                            <li><a href="{{ route('AllS') }}"><i class="fa fa-caret-right"></i> All
                                                    Supplier
                                                </a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{ route('Cadd') }}">
                                            <i class="fa fa-th-large"></i>
                                            <span class="menu-title">Add Category</span>
                                        </a>
                                    </li>
                                    <li class="{{ Route::is('Brdelete') ? 'active' : '' }}">
                                        <a href="{{ route('Bradd') }}">
                                            <i class="fa fa-bookmark-o"></i>
                                            <span class="menu-title">Add Brands</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="">
                                            <i class="fa fa-cube"></i>
                                            <span class="menu-title">
                                                <strong>Products</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('Padd') }}"><i class="fa fa-caret-right"></i> Add
                                                    Product
                                                </a></li>
                                            <li><a href="{{ route('AllP') }}"><i class="fa fa-caret-right"></i> All
                                                    Product
                                                </a></li>
                                            <li><a href="{{ route('AllPurchase') }}"><i class="fa fa-caret-right"></i> Purchase
                                                        Product
                                                    </a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ route('AdjustProduct.List') }}">
                                            <i class="fa fa-adjust"></i>
                                            <span class="menu-title">
                                                <strong>Product Adjustment</strong>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i class="fa fa-random"></i>
                                            <span class="menu-title">
                                                <strong>Products Return</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <!--<li><a href="{{ route('sale.return') }}"><i class="fa fa-caret-right"></i> Sell Return
                                            </a></li>-->
                                            <li><a href="{{ route('purchess.return') }}"><i class="fa fa-caret-right"></i> Purchess Return
                                                </a></li>
                                            <li><a href="{{ route('sale.return') }}"><i class="fa fa-caret-right"></i> Sale Return
                                                    </a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="">
                                            <i class="fa fa-bank"></i>
                                            <span class="menu-title">
                                                <strong>Create Bank</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>
                                        <!--Submenu-->
                                        <ul class="collapse">
                                            <li><a href="{{ route('bank.add') }}"><i class="fa fa-caret-right"></i>
                                                    Add
                                                    Account
                                                </a></li>
                                            <li><a href="{{ route('transaction.add') }}"><i
                                                        class="fa fa-caret-right"></i> See Transaction
                                                </a></li>
                                        </ul>
                                    </li>

                                    <!--<li>
                                        <a href="{{ route('personal.form') }}">
                                            <i class="fa fa-file-text"></i>
                                            <span class="menu-title">Personal Document</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('house.range.form') }}">
                                            <i class="fa fa-home"></i>
                                            <span class="menu-title">House Range</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('Exadd') }}">
                                            <i class="fa fa-book"></i>
                                            <span class="menu-title">See Expense</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="">
                                            <i class="fa fa-pie-chart"></i>
                                            <span class="menu-title">
                                                <strong>Manage Report</strong>
                                            </span>
                                            <i class="arrow"></i>
                                        </a>

                                        <ul class="collapse">
                                            <li>
                                                <a href="{{ route('sale.report') }}"><i class="fa fa-caret-right"></i>
                                                Sale Report
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('saleandpurchase.report') }}">
                                                    <i class="fa fa-caret-right"></i>
                                                    <span class="menu-title">Sells & Purchase</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>--> --}}
                                </ul>
                                <!--Widget-->
                                <!--================================-->
                            {{--    <div class="mainnav-widget">
                                    <!-- Show the button on collapsed navigation -->
                                    <div class="show-small">
                                        <a href="#" data-toggle="menu-widget" data-target="#demo-wg-server">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div>
                                    <!-- Hide the content on collapsed navigation -->
                                    <div id="demo-wg-server" class="hide-small mainnav-widget-content">
                                        <ul class="list-group">
                                            <li class="list-header pad-no pad-ver">Server Status</li>
                                            <li class="mar-btm">
                                                <span class="label label-primary pull-right">15%</span>
                                                <p>CPU Usage</p>
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar progress-bar-primary"
                                                        style="width: 15%;">
                                                        <span class="sr-only">15%</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="mar-btm">
                                                <span class="label label-purple pull-right">75%</span>
                                                <p>Bandwidth</p>
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar progress-bar-purple" style="width: 75%;">
                                                        <span class="sr-only">75%</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div> --}}
                                <!--================================-->
                                <!--End widget-->
                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->
                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
        </div>
        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer" style=" background-color: #25A79F; color: white; ">
            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">Currently v2.2</div>
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <p class="pad-lft">Inventory © <script>document.write(new Date().getFullYear());</script> All rights reserved. powered by <a href="https://techlozi.com/"> <strong> techlozi.com </strong></p>
        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->
        <!-- SCROLL TOP BUTTON -->
        <!--===================================================-->
        <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--jQuery [ REQUIRED ]-->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> --}}

    {{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> --}}
    <script src="{{ asset('user_assets/js/jquery-2.1.1.min.js') }}"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/bootstrap.min.js') }}"></script>
    <!--Fast Click [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/fast-click/fastclick.min.js') }}"></script>
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/scripts.js') }}"></script>

    <!--Switchery [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/switchery/switchery.min.js') }}"></script>
    <!--Jquery Steps [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/parsley/parsley.min.js') }}"></script>
    <!--Jquery Steps [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/jquery-steps/jquery-steps.min.js') }}"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <!--Bootstrap Wizard [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <!--Masked Input [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/masked-input/bootstrap-inputmask.min.js') }}"></script>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-validator/bootstrapValidator.min.js') }}"></script>
    <!--FooTable [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/fooTable/dist/footable.all.min.js') }}"></script>
    <!--Flot Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.resize.min.js') }}"></script>
    <!--Flot Order Bars Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <!--jvectormap [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap.min.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <!--Easy Pie Chart [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <!--Fullscreen jQuery [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/screenfull/screenfull.js') }}"></script>

    <script src="{{ asset('user_assets/plugins/jquery-print/jQuery.print.js') }}"></script>
    <!--Form Wizard [ SAMPLE ]-->
    <script src="{{ asset('user_assets/js/demo/form-wizard.js') }}"></script>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}

    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script> --}}


    <script src="{{ asset('user_assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('user_assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ asset('user_assets/js/demo/tables-datatables.js') }}"></script>
    @yield('script')
    @yield('scripts')



</body>

</html>
