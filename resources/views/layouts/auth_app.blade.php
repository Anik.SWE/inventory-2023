<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Stolas Login | Business Managment Software</title>
    <link rel="shortcut icon" href="{{ asset('assets/images/stock.png') }}">
    <!--STYLESHEET-->
    <!--=================================================-->
    <!--Roboto Slab Font [ OPTIONAL ] -->
    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400"
        rel="stylesheet">
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('user_assets/css/style.css') }}" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!--Switchery [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('user_assets/css/demo/jasmine.css') }}" rel="stylesheet">
    <!--SCRIPT-->
    <!--=================================================-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="{{ asset('user_assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('user_assets/plugins/pace/pace.min.js') }}"></script>
</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>



    @yield('content')


    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('user_assets/js/jquery-2.1.1.min.js') }}"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/bootstrap.min.js') }}"></script>
    <!--Fast Click [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/fast-click/fastclick.min.js') }}"></script>
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="{{ asset('user_assets/js/scripts.js') }}"></script>
    <!--Switchery [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/switchery/switchery.min.js') }}"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="{{ asset('user_assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <!--Demo script [ DEMONSTRATION ]-->
    <script src="{{ asset('user_assets/js/demo/jasmine.js') }}"></script>
</body>

</html>
