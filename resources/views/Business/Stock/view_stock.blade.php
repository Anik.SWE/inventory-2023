@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"
                            style="display: flex; justify-content:space-between; align-items:center; padding-right: 20px">
                            <h3 class="panel-title">Total Product: {{ $Total_Stock_Item }} </h3>
                            <div class="wrapper" style="float: right">
                                <div class="time">
                                    <span>Time: </span>
                                    <span id="hour"></span>
                                    <span id="min"></span>
                                    <span id="sec"></span>
                                    <span id="am_pm"></span>
                                </div>
                                <div class="date">
                                    <span>Date: </span>
                                    <span id="day"></span>
                                    <span id="date"></span>
                                    <span id="month"></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel" style="margin-top: 5px">
                                <div class="panel-body" style="padding-bottom: 0;">
                                    <div class="pad-btm form-inline">
                                        <div class="row">
                                            <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                                <div class="btn-group">
                                                    <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                                class="fa fa-plus"></i></a></button>
                                                    <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                                    <button class="btn btn-default"><i
                                                            class="fa fa-exclamation-circle"></i></button>
                                                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                                <div class="form-group">
                                                    <input id="myInput"type="text" placeholder="Search"
                                                        class="form-control" autocomplete="off">
                                                </div>
                                                <div class="btn-group">
                                                    <button class="btn btn-default"><i
                                                            class="fa fa fa-cloud-download"></i></button>
                                                    <div class="btn-group">
                                                        <button data-toggle="dropdown"
                                                            class="btn btn-default dropdown-toggle">
                                                            <i class="fa fa-cog"></i>
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="#">Action</a></li>
                                                            <li><a href="#">Another action</a></li>
                                                            <li><a href="#">Something else here</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#">Separated link</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive"
                                    style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                                    <table class="table table-bordered table-hover" id="myTable">
                                        <thead>
                                            <tr>
                                                <th data-toggle="true">Product Name</th>
                                                <th data-hide="phone, tablet">Quentity</th>
                                                <th class="">Buying Date</th>
                                                <th data-hide="phone, tablet" class="hidden-xs">Expire Date</th>
                                                <th data-hide="phone, tablet">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($Total_Stock as $item)
                                                <tr>
                                                    <td>{{ $item->product_name }}</td>
                                                    <td>{{ $item->product_quantity }}</td>
                                                    <td class="hidden-xs">
                                                        <span class="text-muted"><i class="fa fa-clock-o"></i>
                                                            {{ Carbon\Carbon::parse($item->product_buying_date)->format('d-m-Y') }}</span>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        <span class="text-muted"><i class="fa fa-clock-o"></i>
                                                            {{ Carbon\Carbon::parse($item->product_expire_date)->format('d-m-Y') }}</span>
                                                    </td>
                                                    <td>
                                                        {{-- <span class="label label-table label-success">Active</span> --}}
                                                        @php
                                                        if ($item->product_quantity > 0) {
                                                            // echo '<span style="color: red">' . abs($profit) . '</span>';
                                                            echo '<span class="label label-table label-success">' .'Active'. '</span>';
                                                        } else {
                                                            echo '<span class="label label-table label-danger">' .'Dactive'. '</span>';
                                                        }
                                                    @endphp
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="50" class="text-danger"> <strong>

                                                            {{-- @if (Auth::guard('admin'))
                                                                {{ Auth::guard('admin')->user()->name }}
                                                            @else
                                                                {{ Auth::user()->name }}
                                                            @endif --}}
                                                        </strong> You Not Create Any Product....Please Create a Product!
                                                    </td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function clock() {

            var hour = document.getElementById("hour");
            var min = document.getElementById("min");
            var sec = document.getElementById("sec");
            var am_pm = document.getElementById("am_pm");

            var day = document.getElementById("day");
            var date = document.getElementById("date");
            var month = document.getElementById("month");
            var year = document.getElementById("year");

            var d = new Date();

            var h = d.getHours();
            var m = d.getMinutes();
            var s = d.getSeconds();
            var ampm = h >= 12 ? 'PM' : 'AM';

            // for 12 hours clock

            if (h > 12) {
                var hh = h - 12;
                if (hh < 10) {
                    var newh = "0" + hh;
                } else {
                    var newh = hh;
                }

            } else {
                var newh = h;
            }



            if (m < 10) {
                newm = "0" + m;
            } else {
                newm = m;
            }


            if (s < 10) {
                news = "0" + s;
            } else {
                news = s;
            }

            // install months and week day name
            const weeks = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thirsday", "Friday", "Saturday"];
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"
            ];

            var dy = weeks[d.getDay()]; // for week day
            var dd = d.getDate(); // for date
            var mm = months[d.getMonth()]; // for month
            var yy = d.getFullYear();

            hour.innerHTML = newh + " : ";
            min.innerHTML = newm + " : ";
            sec.innerHTML = news;
            am_pm.innerHTML = ampm;


            day.innerHTML = dy + ", ";
            date.innerHTML = dd + " ";
            month.innerHTML = mm;
        }

        setInterval(clock, 1000);

        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
