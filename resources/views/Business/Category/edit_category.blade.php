@extends('layouts.user_api.business_app')
@section('content')

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-th-large"></i> Product Category </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="{{ route('AllS') }}">All Category</a> </li>
              <li class="active">Edit Category </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Category List</h4>
                    </div>

                    <div class="panel-body">
                      <div class="table-responsive">
                          <table id="demo-dt-basic" class="table table-striped table-bordered">
                              <thead>
                                  <tr>
                                      <th>SL.No</th>
                                      <th>Category Name</th>
                                      <th>Description</th>
                                      <th>Created Date</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @forelse ($Total_Category as $item)
                                  @if(Session::get('business_id') == $item->business)
                                  <tr>
                                      <td>{{ $loop->index + 1 }}</td>
                                      <td>{{ $item->category_name }}</td>
                                      <td>{{ $item->category_description }}</td>
                                      <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                          {{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</span></td>
                                      <td style="align-items: center">
                                          <a href="{{ url('edit/category') }}/{{ $item->id }}"><i class="fa fa-edit text-primary" style="margin-right: 15px"> Edit </i></a>
                                          <a href="{{ url('delete/category') }}/{{ $item->id }}"><i class="fa fa-trash text-danger"> Del </i></a>
                                      </td>
                                  </tr>
                                  @endif
                                  @empty
                                      <tr>
                                          <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong> You Not Create Any Category....Please Create a Category!</td>
                                      </tr>
                                @endforelse
                              </tbody>
                          </table>
                      </div>
                    </div>
                </div>
            </div>
              <div class="col-md-4">
                  <div class="panel" style="margin-top: 5px">
                      <div class="panel-heading" style="border: 1px solid #25A79F">
                          <div class="panel-control">
                              <a href="{{ route('Cadd') }}" class="btn"
                                  style="background-color: #25A79F; color: #fff; margin-right: 10px">All Category</a>
                              <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                          </div>
                          <h4 class="panel-title">Edit Category</h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <div class="panel-body">
                        <form class="form-horizontal" style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                            action="{{ route('Ceditformpost') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                    for="demo-text-input">Category Name:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="category_name" class="form-control"
                                        placeholder="Enter Your Category Name" value="{{ $Category_info->category_name }}">
                                    @error('category_name')
                                        <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <input type="hidden" id="demo-text-input" name="id" class="form-control"
                                        placeholder="Enter Your Category Name" value="{{ $Category_info->id }}">
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Description:</label>
                                <div class="col-md-8">
                                    <textarea placeholder="Message" rows="2" class="form-control" name="category_description">{{ $Category_info->category_description }}</textarea>
                                    @error('category_description')
                                        <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 text-center" style="padding: 5px">
                                <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit Category</button>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>

@endsection
