@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        $transaction_id = uniqid();
    @endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-bank"></i> Make a Transaction</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Bank</a> </li>
              <li class="active">Bank Transaction </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title"> Make a Transaction</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <div class="panel-body">
                      <form class="form-horizontal" action="{{ route('transactionform.add') }}" method="POST">
                          @csrf
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                  for="demo-text-input">Select Date:</label>
                              <div class="col-md-8">
                                  <input type="date" class="form-control" name="transaction_date" required>
                                  @error('transaction_date')
                                      <small class="help-block text-danger">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                  for="demo-text-input">Select Bank:</label>
                              <div class="col-md-8">
                                <select class="form-control selectpicker" data-live-search="true" name="bank_name"
                                    id="my_radio_box" required>
                                    <option value="">-- Select Option --</option>
                                    @foreach ($bank_info as $bank)
                                        <option value="{{ $bank->id }}">{{ $bank->bank_name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('bank_name')
                                    <small class="help-block text-danger">{{ $message }}</small>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                  for="demo-text-input">A/C Number:</label>
                              <div class="col-md-8">
                                <input type="number" id="sub_id" name="account_number" class="form-control" readonly>
                                @error('account_number')
                                    <small class="help-block text-danger">{{ $message }}</small>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                  for="demo-text-input">Operation Type:</label>
                              <div class="col-md-8">
                                <select class="form-control selectpicker" data-live-search="true" name="operation_type" required>
                                    <option value="">-- Select Option --</option>
                                    <option value="Withdraw">Withdraw</option>
                                    <option value="Deposit">Deposit</option>
                                </select>
                                @error('operation_type')
                                    <small class="help-block text-danger">{{ $message }}</small>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                  for="demo-text-input">Amount:</label>
                              <div class="col-md-8">
                                <input type="number" id="demo-text-input" name="balance" class="form-control"
                                    placeholder="Enter Your Balance" step="any" required>
                                @error('payment')
                                    <small class="help-block text-danger">{{ $message }}</small>
                                @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                  for="demo-text-input">Take a Note:</label>
                              <div class="col-md-8">
                                <input type="text" id="demo-text-input" name="media" class="form-control" required>
                                @error('payment')
                                    <small class="help-block text-danger">{{ $message }}</small>
                                @enderror
                              </div>
                          </div>
                          <input type="hidden" name="transaction_id" class="form-control" value="{{ $transaction_id }}">

                          <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-xs" type="submit">Add
                                    Transaction</button>
                            </div>
                          </div>
                      </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Transaction History</h4>
                    </div>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                              <table id="demo-dt-basic" class="table table-condensed">
                                  <thead>
                                      <tr>
                                          <th>Sl</th>
                                          <th>Bank Name</th>
                                          <th class="min-tablet">Operation</th>
                                          <th class="min-tablet">A/C Number</th>
                                          <th class="min-tablet">Amount</th>
                                          <th class="min-desktop">Date</th>
                                          <th>Media</th>
                                          {{-- <th>Status</th> --}}
                                          {{-- <th>Action</th> --}}
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @forelse ($transaction_info as $item)
                                      @if(Session::get('business_id') == $item->business)
                                          <tr>
                                              <td>{{ $loop->iteration }} </td>
                                              <td>
                                                  @php
                                                      $bank_name = DB::table('banks')
                                                          ->where('user_id', Auth::id())
                                                          ->where('id', $item->bank)
                                                          ->get();
                                                  @endphp
                                                  @foreach ($bank_name as $name)
                                                      {{ $name->bank_name }}
                                                  @endforeach
                                              </td>
                                              <td>
                                                  {{ $item->operation_type }}
                                              </td>
                                              <td>
                                                  {{ $item->account_number }}
                                              </td>
                                              <td>
                                                  {{ $item->balance }}
                                              </td>
                                              <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                      {{ Carbon\Carbon::parse($item->transaction_date)->format('d-m-Y') }}</span>
                                              </td>
                                              <td>{{ $item->media }}</td>

                                              {{-- <td>
                                                  <div class="label label-table label-success"><a
                                                          href="{{ url('/single/bank/transaction/view') }}/{{ $item->bank_name }}"
                                                          style="color: #fff">View</a>
                                                  </div>
                                              </td> --}}
                                              {{-- <td style="align-items: center">
                                                  <a href="{{ url('delete/transaction') }}/{{ $item->id }}"><i
                                                          class="fa fa-trash text-danger"> Del </i></a>
                                              </td> --}}
                                              @if($item->operation_type == "Withdraw" || $item->operation_type == "Deposit")
                                              <td style="align-items: center">
                                                  <a href="{{ url('delete/transaction') }}/{{ $item->id }}"><i
                                                          class="fa fa-trash text-danger"> Del </i></a>
                                              </td>
                                              @endif
                                          </tr>
                                      @endif
                                      @empty
                                      @endforelse


                                  </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
