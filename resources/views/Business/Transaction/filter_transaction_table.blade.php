@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-bank"></i> Bank Transaction</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Transaction</a> </li>
              <li class="active">Fillter </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="row">
                <!-- Basic Data Tables -->
                <!--===================================================-->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Filtered data</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Media</th>
                                    <th class="min-tablet">Deposite</th>
                                    <th class="min-tablet">Withdraw</th>
                                    <th class="min-desktop">Date</th>
                                    <th class="min-desktop">Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($find_data as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }} </td>
                                        <td>{{ $item->media }}</td>
                                        <td>
                                            @php
                                                if ($item->operation_type == 'Withdraw') {
                                                    echo 'NUll';
                                                } elseif ($item->operation_type == 'Deposit') {
                                                    echo $item->balance;
                                                }
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                if ($item->operation_type == 'Deposit') {
                                                    echo 'NUll';
                                                } elseif ($item->operation_type == 'Withdraw') {
                                                    echo $item->balance;
                                                }
                                            @endphp
                                        </td>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                {{ Carbon\Carbon::parse($item->transaction_date)->format('d-m-Y') }}</span>
                                        </td>
                                        <td>
                                            @php
                                                $balance = DB::table('banks')
                                                    ->where('user_id', Auth::id())
                                                    ->where('id', $item->bank_name)
                                                    ->get();
                                            @endphp
                                            @php
                                            $balance = 0;
                                            if($item->operation_type == "Deposit"){
                                                $balance += $item->balance;
                                            }else if($item->operation_type == "Withdraw"){
                                                $balance -= $item->balance;
                                            }
                                            @endphp
                                            {{ $balance}}
                                        </td>
                                    </tr>
                                @empty
                                @endforelse


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    @endsection
    @section('script')
        {{-- <script>
            $(document).ready(function() {
                $('#demo-dt-basic').DataTable({
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, 'All'],
                    ],
                });
            });
        </script> --}}
    @endsection
