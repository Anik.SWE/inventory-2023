@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-bank"></i> Bank Transaction</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Bank</a> </li>
              <li class="active">Bank Transaction </li>
            </ol>
          </div>
				</div>
      <div id="page-content">
        <div class="row">
          <div class="col-md-12">
              <div class="panel">
                  <div class="panel-heading">
                      <h4 class="panel-title">Bank Transaction</h4>
                  </div>
                  <!-- BASIC FORM ELEMENTS -->
                  <!--===================================================-->
                  <div class="panel-body">
                    <form class="form-horizontal"
                        action="{{ route('filter.formpost') }}" method="POST">
                        @csrf

                        @foreach ($bank_transaction_view as $item)
                            <input type="hidden" value="{{ $item->bank_name }}" name="bank_id">
                        @endforeach
                        <div class="col-md-12">
                            <div id="demo-dp-component col-md-10">
                                <div class="col-md-10">
                                    <div id="demo-dp-range">
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input type="date" class="form-control" name="start_date" />
                                            <span class="input-group-addon">to</span>
                                            <input type="date" class="form-control" name="end_date" />
                                        </div>
                                    </div>
                                    @error('start_date')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                    @error('end_date')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button class="btn" style="background-color: #25A79F; color: #fff; float: right;"
                                    type="submit">Check</button>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
          <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Bank Name:

                        @php
                            $bank_name = DB::table('banks')
                                ->where('id', $bank_tid)
                                ->where('business', Session::get('business_id'))
                                ->get();
                        @endphp
                        @foreach ($bank_name as $name)
                            {{ $name->bank_name }}
                        @endforeach
                    </h3>
                </div>
                <div class="panel-body">
                    <table id="demo-dt-basic" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Media</th>
                                <th class="min-tablet">Deposite</th>
                                <th class="min-tablet">Withdraw</th>
                                <th class="min-desktop">Date</th>
                                <th class="min-desktop">Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $balance = 0;
                            @endphp
                            @forelse ($bank_transaction_view->sortBy('id') as $item)
                            @if(Session::get('business_id') == $item->business)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->media }}</td>
                                    <td>
                                        @php
                                            if ($item->operation_type == 'Withdraw') {
                                                echo '0';
                                            } elseif ($item->operation_type == 'Deposit' && !$item->sell) {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell) {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->purchase) {
                                                echo '0';
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->due && $item->media == 'Due Collection') {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->due && $item->media == 'Due Payment') {
                                                echo '0';
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->purchase_return && $item->media == 'Purchase Return') {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell_return && $item->media == 'Sell Return') {
                                                echo '0';
                                            }

                                        @endphp
                                    </td>
                                    <td>
                                        @php
                                            if ($item->operation_type == 'Deposit') {
                                                echo '0';
                                            } elseif ($item->operation_type == 'Withdraw' && !$item->purchase) {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->purchase) {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->media == 'Due Payment') {
                                                echo $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell) {
                                                echo '0';
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->due && $item->media == 'Due Collection') {
                                                echo '0';
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->purchase_return && $item->media == 'Purchase Return') {
                                                echo '0';
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell_return && $item->media == 'Sell Return') {
                                                echo $item->balance;
                                            }
                                        @endphp
                                    </td>
                                    <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                            {{ Carbon\Carbon::parse($item->transaction_date)->format('d-m-Y') }}</span>
                                    </td>
                                    <td>
                                        @php
                                            // $balance = DB::table('transactions')->where('user_id', Auth::id())->where('bank_name',$item->bank_name)->get();
                                            // $total_balance = App\Models\TransactionDetail::fine()
                                            $amount = App\Models\Bank::find($item->bank)->opening_amount;
                                        @endphp
                                        @php
                                            if ($item->operation_type == 'Deposit' && !$item->sell) {
                                                $balance += $item->balance;
                                            } elseif ($item->operation_type == 'Withdraw' && !$item->purchase) {
                                                $balance -= $item->balance;
                                            } elseif ($item->operation_type == 'Auto' && $item->purchase) {
                                                $balance -= $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell) {
                                                $balance += $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->due && $item->media == 'Due Collection') {
                                                $balance += $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->due && $item->media == 'Due Payment') {
                                                $balance -= $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->purchase_return && $item->media == 'Purchase Return') {
                                                $balance += $item->balance;
                                            }
                                            elseif ($item->operation_type == 'Auto' && $item->sell_return && $item->media == 'Sell Return') {
                                                $balance -= $item->balance;
                                            }
                                        @endphp
                                        {{ $balance }}
                                    </td>
                                </tr>
                            @endif
                            @empty
                            @endforelse


                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection
