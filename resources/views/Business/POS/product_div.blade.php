

                        @foreach ($Total_Stock as $item)
                            <div class="col-sm-3"
                                style="margin-bottom: 10px; height: 140px">
                                <div class="shadow-sm card mb-3 product"
                                    style="margin-left: 10px">
                                    <div class="card-body" style="height: 90px">

                                        <h5 class="card-title text-info product-code"
                                            style="padding-left: 10px; margin-bottom: 0; font-weight: normal">
                                            {{ $item->product_code }}
                                        </h5>
                                        <h5 class="card-title text-info bold product-name"
                                            style="padding-left: 10px; margin-bottom: 0; margin-top: 3px; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif">
                                            {{ $item->product_name }}
                                        </h5>
                                        <div class="stock"
                                            style="display: flex; align-items: center">
                                            <p class="card-text text-success product-price cart_item_quantity"
                                                style="margin-bottom: 0; margin-right: 5px">
                                                {{ $item->retail_selling_price }}</p>

                                            <p class="card-text text-warning products-prices cart_item_quantity"
                                                    style="margin-bottom: 0; margin-right: 5px">
                                                    {{ $item->whole_selling_price }}</p>


                                        </div>
                                        <!-- <p class="card-text text-success product-price cart_item_quantity"
                                            style="margin-bottom: 0; margin-right: 5px">
                                            Quantity: {{ $item->product_quantity }}</p> -->

                                            @php
                                                if ($item->product_quantity <= 10) {
                                                  echo '<p class="card-text text-danger product-price cart_item_quantity"
                                                      style="margin-bottom: 0; margin-right: 5px">
                                                      Warning Qnt:'. $item->product_quantity . '</p>';

                                                }
                                                else{
                                                  echo '<p class="card-text text-success product-price cart_item_quantity"
                                                      style="margin-bottom: 0; margin-right: 5px">
                                                      Quantity:'. $item->product_quantity. '</p>';
                                                }

                                            @endphp


                                        @php
                                            if ($item->product_quantity <= 0) {
                                                echo "<button style='margin-left: 10px; margin-bottom: 15px'
                                            class='btn badge badge-pill badge-danger mt-2 float-right'
                                            type='button'> Stock Out </button>";
                                            } else {
                                                echo "<button style='margin-left: 10px; margin-bottom: 15px'
                                            class='btn badge badge-pill badge-secondary mt-2 float-right'
                                            type='button'
                                            data-action='add-to-carts'>Add to
                                            cart</button>";
                                            }
                                        @endphp
                                    </div>
                                </div>
                            </div>
                        @endforeach


  <script src="{{ asset('default.js') }}"></script>
