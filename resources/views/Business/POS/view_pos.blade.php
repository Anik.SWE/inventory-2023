@extends('layouts.user_api.pos_app')
@section('content')




<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        $transaction_id = uniqid();
    @endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-shopping-cart"></i> POS</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="{{ route('dashboard') }}" style="color: #25A79F;"> <b>Dashboard</b> </a> </li>
              <li class="active">POS </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="well">
                            <div class="row">
                                <div class="col-sm-9">
                                    <input type="search" id="searchPro"
                                     placeholder="Search any products" aria-describedby="button-addon1" style="color: #25a79f;"
                                     class="form-control form-control-sm shadow-none search rounded rounded-pill border-0 bg-light" onfocus="showSearchPro()"  >
                                </div>
                                <div class="col-sm-3">
                                  <div class="wrapper">
                                      <div class="time">
                                          <span>Time: </span>
                                          <span id="hour"></span>
                                          <span id="min"></span>
                                          <span id="sec"></span>
                                          <span id="am_pm"></span>
                                      </div>
                                      <div class="date">
                                          <span>Date: </span>
                                          <span id="day"></span>
                                          <span id="date"></span>
                                          <span id="month"></span>
                                      </div>
                                  </div>
                                </div>
                            </div>
                        </div>
            <div class="panel">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-8">
                        <div class="container" style="max-height: 600px">
                            <div class="row" id="page">
                                <div class="col-md-12 col-md-12">
                                    <div class="row" id="myTable">
                                        <div id="selectProduct">

                                          @foreach ($Total_Stock as $item)
                                              <div class="col-sm-3"
                                                  style="margin-bottom: 10px; height: 140px">
                                                  <div class="shadow-sm card mb-3 product"
                                                      style="margin-left: 10px">
                                                      <div class="card-body" style="height: 90px">

                                                          <h5 class="card-title text-info product-code"
                                                              style="padding-left: 10px; margin-bottom: 0; font-weight: normal">
                                                              {{ $item->product_code }}
                                                          </h5>
                                                          <h5 class="card-title text-info bold product-name"
                                                              style="padding-left: 10px; margin-bottom: 0; margin-top: 3px; font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif">
                                                              {{ $item->product_name }}
                                                          </h5>
                                                          <div class="stock"
                                                              style="display: flex; align-items: center">
                                                              <p class="card-text text-success product-price cart_item_quantity"
                                                                  style="margin-bottom: 0; margin-right: 5px">
                                                                  {{ $item->retail_selling_price }}</p>

                                                              <p class="card-text text-warning products-prices cart_item_quantity"
                                                                      style="margin-bottom: 0; margin-right: 5px">
                                                                      {{ $item->whole_selling_price }}</p>


                                                          </div>
                                                          <!-- <p class="card-text text-success product-price cart_item_quantity"
                                                              style="margin-bottom: 0; margin-right: 5px">
                                                              Quantity: {{ $item->product_quantity }}</p> -->

                                                              @php
                                                                  if ($item->product_quantity <= 10) {
                                                                    echo '<p class="card-text text-danger product-price cart_item_quantity"
                                                                        style="margin-bottom: 0; margin-right: 5px">
                                                                        Warning Qnt:'. $item->product_quantity . '</p>';

                                                                  }
                                                                  else{
                                                                    echo '<p class="card-text text-success product-price cart_item_quantity"
                                                                        style="margin-bottom: 0; margin-right: 5px">
                                                                        Quantity:'. $item->product_quantity. '</p>';
                                                                  }

                                                              @endphp

                                                          @php
                                                              if ($item->product_quantity <= 0) {
                                                                  echo "<button style='margin-left: 10px; margin-bottom: 15px' class='btn badge badge-pill badge-danger mt-2 float-right' type='button'> Stock Out </button>";
                                                              } else {
                                                                  echo "<button style='margin-left: 10px; margin-bottom: 15px' class='btn badge badge-pill badge-secondary mt-2 float-right' type='button' data-action='add-to-carts'>Add to cart</button>";
                                                              }
                                                          @endphp
                                                      </div>
                                                  </div>
                                              </div>
                                          @endforeach

                                        </div>

                                    </div>
                                    <div class="row" style="margin-left: 14px">


                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="tab-base col-md-4"
                        style="padding: 0; border: 1px solid #25A79F; margin-bottom: 0">
                        <!--Nav Tabs-->
                        <ul class="nav nav-tabs">
                            <li class="active"> <a data-toggle="tab" href="#demo-lft-tab-1"> POS </a> </li>
                            <!--<li> <a data-toggle="tab" href="#demo-lft-tab-3">Add Customar</a> </li>-->
                            <li> <a data-toggle="tab" href="#demo-lft-tab-2">Invoice</a> </li>
                        </ul>

                        <!--Tabs Content-->
                        <div class="tab-content" id="page-content">
                            <div id="demo-lft-tab-1" class="tab-pane fade active in">
                                <form action="{{ route('order.store') }}" method="POST" id="row_add">
                                    @csrf
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="input-group mar-btm">
                                            <span class="input-group-addon"><i class="fa fa-dollar fa-lg"></i></span>
                                            <select class="form-control rounded rounded-pill border-0 bg-light" data-live-search="true" name="sell_type" id="sell_type">
                                                  <option value="Retail">Retail sell</option>
                                                  <option value="Whole">Whole sell</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="input-group mar-btm">
                                            <input type="date" id="demo-text-input" name="order_date"
                                                class="form-control" value="{{ old('order_date') }}" required>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="input-group mar-btm">
                                            <span class="input-group-addon"><i class="fa fa-users fa-lg"></i></span>
                                            <select class="form-control selectpicker" data-live-search="true" name="select_customer" onchange="get_custo_id();" id="custo_id" required>
                                                <option value="">-- Select Customer --</option>
                                                      @foreach ($Customar_info as $item)
                                                            <option value="{{ $item->id }}">{{ $item->customar_name }} - {{ $item->address }}</option>
                                                      @endforeach
                                            </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">


                                            <div class="col-md-12">

                                                    <div id="suggestCustomer">

                                                    </div>


                                                    <div class="col-md-12" style="margin-top: 4px;">

                                                     </div>



                                                @error('category_id')
                                                    <small class="help-block text-danger"
                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                @enderror
                                                <input type="hidden" name="transaction_id" class="form-control" value="{{ $transaction_id }}">
                                                <input type="hidden" name="operation_type" class="form-control" value="Deposit">
                                            </div>

                                            <div class="col-sm-12" style="padding: 0">
                                                <table class="table-responsive table table-striped cart-wrap"
                                                    style="margin-bottom: 5px">
                                                    <thead>
                                                        <tr>
                                                            <th class="product" style="padding-left: 0">
                                                                Product</th>
                                                            <th class="quantity">Quantity</th>
                                                            <th class="ptice">Price</th>
                                                            <th class="remove">Remove</th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                                <div class="cart col-md-12" style="padding: 0">

                                                </div>
                                            </div>


                                            <table
                                                class="table-responsive table table-striped cart-wrap _table">

                                            </table>
                                            <input type="hidden" id="total" name="total"
                                                class="form-control" value="0" readonly>
                                            <input type="hidden" id="sub_total" name="sub_total"
                                                class="form-control" value="0" readonly>






                                                <hr style=" border: 2px solid black;">
                                                 <div class="col-md-12 cart-footer" style="padding: 0; display:flex; align-items: center; justify-content: space-between">
                                                 <!-- <div class="p-2 ml-auto" style="align-items: center">
                                                     <br><p  >Shipping Cost:</p>
                                                     <input type="number" value="0" name="ship_cost" id="ship_cost" onkeyup="on_discount()" onchange="on_discount();" class="form-control cart_item_discount" style="width: 111px; height: 30px" />
                                                 </div> -->
                                                 <div class="p-2 mt-3"  style="display: flex; align-items: center">
                                                     <label>Shipping: </label>
                                                     <input type="number" value="0" name="ship_cost" id="ship_cost" onkeyup="on_discount()" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px" />
                                                 </div>
                                                 <div class="p-2 mt-3"  style="display: flex; align-items: center">
                                                     <label>Tax: </label>
                                                     <input type="number" value="0" name="tax" id="tax" onkeyup="on_discount()" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px"/>
                                                 </div>
                                                 </div>
                                            <div class="col-md-12 cart-footer" style="padding: 0; display:flex; align-items: center; justify-content: space-between">

                                            <div class="p-2 mt-3" style="display: flex; align-items: center">
                                                <p style="width: 70px; padding: 10px 0; padding-right: 20px; margin-left: 10px; margin-top: 10px;" >Discount:</p>
                                                <input type="number" value="0" name="discount" id="discount" onkeyup="on_discount()" onchange="on_discount();" class="form-control cart_item_discount" style="width: 70px; height: 30px" step="any"/>
                                            </div>
                                            <div class="p-2 ml-auto">
                                                <button type="button" data-action="sub-out">Sub <span class="pay sub_pay" name="sub_total" id="sub_pay" onchange="on_discount();"></span>
                                                &#10137;
                                            </div>
                                            </div>


                                            <div class="inner_cart_row flex-row shadow-sm card cart-footer mt-2 mb-3 animated flipInX" style="padding: 10px">
                                      <div class="p-2">
                                          <button type="button" data-action="clear-cart">Clear Cart
                                      </div>
                                      <div class="p-2 ml-auto">
                                                <button type="button">Total <span class="payt" id="total_pay" value="0" name="total" onchange="on_discount();"></span>
                                                &#10137;
                                                <input type="number" id="vanco" step="any" hidden>
                                            </div>


                                            </div>






                                            <div class="inner_cart_row flex-row shadow-sm card  mt-2 mb-3 animated flipInX"
                                                style="padding: 10px; margin-bottom: 10px">
                                                <div id="stop_sell" class="p-2" style="margin: 0 auto; display: none;">
                                                    <a href="#" data-toggle="modal"
                                                        data-target="#modaladd"><button onclick="on_discount();" type="button"
                                                            style="width: 150px">Payment</button></a>
                                                </div>


                                                {{-- ----- Modal Body------------- --}}


                                                <div class="modal fade" id="modaladd" tabindex="-1"
                                                    role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-sm modal-notify"
                                                        role="document">
                                                        <div class="panel panel-bordered panel-primary"
                                                            style="width: 600px;">
                                                            <div class="panel-heading">
                                                                <div class="panel-control">
                                                                    <button class="btn btn-primary"
                                                                        data-dismiss="modal"><i
                                                                            class="fa fa-times"></i></button>
                                                                </div>
                                                                <h3 class="panel-title">Payment Method</h3>




                                                                <div class="panel-body"
                                                                    style="width: 600px; background-color: white; color: black">
                                                                    <div class="row">
                                                                        <div class="container-fluid d-flex justify-content-center"
                                                                            style="padding: 0">
                                                                            <div class="col-md-12"
                                                                                style="padding: 0">
                                                                                <div class="card">

                                                                                    <div class="card-body"
                                                                                        style="height: 350px; padding: 20px">

                                                                                        <div class="container">
                                                                                            <table
                                                                                                class="_table">

                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>
                                                                                                            <p
                                                                                                                style="margin-bottom: 5px; margin-top: 15px">
                                                                                                                Total
                                                                                                                Amount
                                                                                                            </p>
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                id="total_amount"
                                                                                                                name="total_pay"
                                                                                                                class="form-control"
                                                                                                                value="0"
                                                                                                                onchange="on_discount()"
                                                                                                                readonly>
                                                                                                            @error('total_pay')
                                                                                                                <small
                                                                                                                    class="help-block text-danger"
                                                                                                                    style="margin-bottom: 0px">{{ $message }}</small>
                                                                                                            @enderror
                                                                                                        </th>
                                                                                                        <th>
                                                                                                            <p
                                                                                                                style="margin-bottom: 5px; margin-top: 15px;">
                                                                                                                Total
                                                                                                                Change
                                                                                                            </p>
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                id="change"
                                                                                                                name="change"
                                                                                                                class="form-control"
                                                                                                                value="0"
                                                                                                                style="width: 145px;"
                                                                                                                readonly>
                                                                                                            @error('change')
                                                                                                                <small
                                                                                                                    class="help-block text-danger"
                                                                                                                    style="margin-bottom: 0px">{{ $message }}</small>
                                                                                                            @enderror
                                                                                                        </th>
                                                                                                        <th>
                                                                                                            <div class="due"
                                                                                                                style="text-align: center">
                                                                                                                <p
                                                                                                                    style="margin-bottom: 5px; margin-top: 15px;">
                                                                                                                    Previous
                                                                                                                    Due
                                                                                                                </p>
                                                                                                                <span
                                                                                                                    id="sub_id">

                                                                                                                    00

                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>
                                                                                                            <p
                                                                                                                style="margin-bottom: 5px; margin-top: 15px">
                                                                                                                Payment
                                                                                                                Methode
                                                                                                            </p>

                                                                                                        </th>
                                                                                                        <th>
                                                                                                            <p
                                                                                                                style="margin-bottom: 5px; margin-top: 15px;">
                                                                                                                Total
                                                                                                                Payment
                                                                                                            </p>
                                                                                                        </th>
                                                                                                        <th
                                                                                                            width="50px" style="text-align: center">

                                                                                                            <p
                                                                                                                style="margin-bottom: 5px; margin-top: 15px;">
                                                                                                                Total

                                                                                                            </p>
                                                                                                            <span
                                                                                                                id="sub_dues">

                                                                                                                00

                                                                                                            </span>
                                                                                                            <!--<div
                                                                                                                class="action_container">
                                                                                                                <input
                                                                                                                    class="success"
                                                                                                                    onclick="create_tr('table_body')"
                                                                                                                    type="button"
                                                                                                                    value="add more">
                                                                                                            </div>-->
                                                                                                        </th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody
                                                                                                    id="table_body">
                                                                                                    <tr>
                                                                                                        <td
                                                                                                            class="col-md-4">
                                                                                                            <div class="col-md-4"
                                                                                                                style="margin-bottom: 5px; padding: 0">

                                                                                                                <select
                                                                                                                    class="form-control"
                                                                                                                    name="payment_type"
                                                                                                                    id="havePay_id" onchange="havePay();"
                                                                                                                    style="width: 200px">
                                                                                                                    <option
                                                                                                                        value="0">
                                                                                                                        --
                                                                                                                        Due
                                                                                                                        Sell
                                                                                                                        --
                                                                                                                    </option>
                                                                                                                    @forelse ($banks as $bank)
                                                                                                                        <option
                                                                                                                            value="{{ $bank->id }}">
                                                                                                                            {{ $bank->bank_name }}
                                                                                                                        </option>
                                                                                                                    @empty
                                                                                                                        <option
                                                                                                                            value="">
                                                                                                                            No
                                                                                                                            Bank
                                                                                                                            Found!
                                                                                                                        </option>
                                                                                                                    @endforelse
                                                                                                                </select>
                                                                                                                @error('payment_type')
                                                                                                                    <small
                                                                                                                        class="help-block text-danger"
                                                                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                                                                @enderror
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td
                                                                                                            class="col-md-4">
                                                                                                            <div class="col-md-4"
                                                                                                                style="padding: 0; display: none;" id="payment_div">
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    id="payment"
                                                                                                                    name="payment"
                                                                                                                    class="form-control minus"
                                                                                                                    value="0"
                                                                                                                    style="width: 140px"
                                                                                                                    onchange="on_discount();" onkeyup="on_discount();" step="any">

                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <!--<div
                                                                                                                class="action_container">
                                                                                                                <input
                                                                                                                    class="danger"
                                                                                                                    onclick="remove_tr(this)"
                                                                                                                    type="button"
                                                                                                                    value="X">
                                                                                                            </div>-->
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>

                                                                                        <div class="col-12"
                                                                                            style="padding: 5px">
                                                                                            <div
                                                                                                class="form-check">
                                                                                                <input
                                                                                                    class="form-check-input"
                                                                                                    type="checkbox"
                                                                                                    value=""
                                                                                                    id="invalidCheck"
                                                                                                    required>
                                                                                                <label
                                                                                                    class="form-check-label"
                                                                                                    for="invalidCheck">
                                                                                                    Agree to
                                                                                                    terms and
                                                                                                    conditions
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group"
                                                                                            style="margin-top: 10px;">
                                                                                            <button
                                                                                                type="submit"
                                                                                                class="btn btn-success btn-lg form-control"
                                                                                                style="width: 533px">
                                                                                                Sell
                                                                                            </button>
                                                                                        </div>
                                </form>

                                <!--<div class="p-2 col-md-12" style="text-align:center">
                                    <a href="#" data-toggle="modal" data-target="#modalbankadd"><button
                                            type="button" style="width: 150px">Add
                                            New
                                            Bank</button></a>
                                </div>-->




                                <div class="modal fade" id="modalbankadd" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true"
                                    style="width: 820px; overflow-y:hidden; text-align: left">
                                    <div class="modal-dialog modal-sm modal-notify" role="document">
                                        <div class="panel panel-bordered panel-primary" style="width: 600px;">
                                            <div class="panel-heading">
                                                <div class="panel-control">
                                                    <button class="btn btn-primary" data-dismiss="modal"><i
                                                            class="fa fa-times"></i></button>
                                                </div>
                                                <h3 class="panel-title">
                                                    Bank Add Form
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <form class="panel-body form-horizontal"
                                                        style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                                                        action="{{ route('bankform.add') }}" method="POST">
                                                        @csrf
                                                        <!--Text Input-->
                                                        <div class="form-group" style="margin-bottom: 10px">
                                                            <label
                                                                class="col-md-3 control-label text-danger text-left"
                                                                style="padding-left: 20px"
                                                                for="demo-text-input">Bank Name:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" id="demo-text-input"
                                                                    name="bank_name" class="form-control"
                                                                    placeholder="Enter Your Bank Name">
                                                                @error('bank_name')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-bottom: 10px">
                                                            <label class="col-md-3 control-label text-left"
                                                                style="padding-left: 20px; padding-right: 0"
                                                                for="demo-text-input">Account Holder:</label>
                                                            <div class="col-md-6">
                                                                <input type="text" id="demo-text-input"
                                                                    name="account_holder" class="form-control"
                                                                    placeholder="Enter Holder Name">
                                                                @error('account_holder')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-bottom: 10px">
                                                            <label class="col-md-3 control-label text-left"
                                                                style="padding-left: 20px; padding-right: 0"
                                                                for="demo-text-input">Account Number:</label>
                                                            <div class="col-md-6">
                                                                <input type="number" id="demo-text-input"
                                                                    name="account_number"
                                                                    class="form-control">
                                                                @error('account_number')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-bottom: 5px">
                                                            <label class="col-md-3 control-label text-left"
                                                                style="padding-left: 20px"
                                                                for="demo-text-input">Account Group:</label>
                                                            <div class="col-md-6" style="margin-bottom: 5px">
                                                                <select class="form-control"
                                                                    name="account_group" id="">
                                                                    <option value="">-- Select Option --
                                                                    </option>
                                                                    <option value="Bank Account">Bank Account
                                                                    </option>
                                                                    <option value="Bkash">Mobile Banking
                                                                    </option>
                                                                    <option value="Fixed Asset">Fixed Asset
                                                                    </option>
                                                                </select>
                                                                @error('account_group')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-bottom: 5px">
                                                            <label class="col-md-3 control-label text-left"
                                                                style="padding-left: 20px"
                                                                for="demo-text-input">Account Type:</label>
                                                            <div class="col-md-6" style="margin-bottom: 5px">
                                                                <select class="form-control"
                                                                    name="account_type" id="">
                                                                    <option value="">-- Select Option --
                                                                    </option>
                                                                    <option value="Savings accounts">Savings
                                                                        accounts</option>
                                                                    <option value="Checking accounts">Checking
                                                                        accounts</option>
                                                                    <option value="Money market accounts">Money
                                                                        market accounts</option>
                                                                    <option value="Certificates of deposit">
                                                                        Certificates of deposit</option>
                                                                    <option value="Brokerage accounts">
                                                                        Brokerage accounts</option>
                                                                </select>
                                                                @error('account_type')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-bottom: 10px">
                                                            <label class="col-md-3 control-label text-left"
                                                                style="padding-left: 20px; padding-right: 0"
                                                                for="demo-text-input">Amount:</label>
                                                            <div class="col-md-6">
                                                                <input type="number" id="demo-text-input"
                                                                    name="opening_amount"
                                                                    class="form-control">
                                                                @error('opening_amount')
                                                                    <small class="help-block text-danger"
                                                                        style="margin-bottom: 0px">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="col-12 text-center" style="padding: 5px">
                                                            <button class="btn"
                                                                style="background-color: #25A79F; color: #fff"
                                                                type="submit">Add
                                                                Bank Info</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>



</div>
</div>
</div>
</div>

</div>
<div id="demo-lft-tab-2" class="tab-pane fade">
<div class="row">
<div class="table-responsive">
    <table id="demo-dt-basic" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>SL</th>
                <th>Order_ID</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($invoice_list as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->order_id }}</td>
                    <td><span class="text-muted"><i class="fa fa-clock-o"></i>{{ $item->order_date }}</span></td>
                    <td>
                        <div class="label label-table label-success"><a href="{{ url('/invoice/view') }}/{{ $item->id }}" style="color: #fff">Invoice</a>
                        </div>
                    </td>
                </tr>
            @empty
            @endforelse
        </tbody>
    </table>
</div>
</div>
</div>
<div id="demo-lft-tab-3" class="tab-pane fade">
<!--===================================================-->
<div class="panel">
<div class="panel-heading" style="border: 1px solid #25A79F">
    <div class="panel-control">
        <a href="{{ route('customar.addform') }}" class="btn"
            style="background-color: #25A79F; color: #fff; margin-right: 10px">All
            Customar</a>
        <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
    </div>
    <h4 class="panel-title">Add New</h4>
</div>
<!-- BASIC FORM ELEMENTS -->
<!--===================================================-->
<form class="panel-body form-horizontal" style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
    action="{{ route('customar.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <!--Text Input-->
    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Customar
            Name:</label>
        <div class="col-md-8">
            <input type="text" id="demo-text-input" name="customar_name" class="form-control"
                placeholder="Enter Customar Name" value="{{ old('customar_name') }}">
            @error('customar_name')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Email:</label>
        <div class="col-md-8">
            <input type="email" id="demo-text-input" name="customar_email" class="form-control"
                placeholder="Enter Customar Email" value="{{ old('customar_email') }}">
            @error('customar_email')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 5px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Phone:</label>
        <div class="col-md-8" style="margin-bottom: 5px">
            <input type="text" id="demo-text-input" name="phone_number" class="form-control"
                placeholder="Enter Customar Phone" value="{{ old('phone_number') }}">
            @error('phone_number')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 5px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Location:</label>
        <div class="col-md-8" style="margin-bottom: 5px">
            <select class="form-control" name="location" id="">
                <option value="">-- Select Option --</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Pakisten">Pakista</option>
            </select>
            @error('location')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">City:</label>
        <div class="col-md-8">
            <input type="text" id="quantity" name="city" class="form-control">
            @error('city')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" for="demo-text-input">Address:</label>
        <div class="col-md-8">
            <input type="Text" id="address" name="address" class="form-control">
            @error('address')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Zip:</label>
        <div class="col-md-3">
            <input type="text" id="quantity" name="zip" class="form-control">
            @error('zip')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Bank:</label>
        <div class="col-md-3">
            <select class="form-control" name="bank_name" id="">
                <option value="">-- Select Option --</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Pakisten">Pakista</option>
            </select>
            @error('bank_name')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Account:</label>
        <div class="col-md-8">
            <input type="text" id="demo-text-input" name="bank_account" class="form-control"
                placeholder="Enter Account Number" value="{{ old('bank_account') }}">
            @error('product_selling_price')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div>

    {{-- <div class="form-group" style="margin-bottom: 10px">
        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
            for="demo-text-input">Photo:</label>
        <div class="col-md-8">
            <input type="file" class="form-control upload" name="customar_photo" id="validationCustom03"
                placeholder="Choose Photo" accept="image/*" onchange="readURL(this);" required>
            <img id="image" src="#" alt="">
            @error('customar_photo')
                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
            @enderror
        </div>
    </div> --}}

    <div class="col-12" style="padding: 5px; margin-left: 130px">
        <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
            Customar</button>
    </div>

    <!--===================================================-->
    <!-- END BASIC FORM ELEMENTS -->
</div>
</div>
</div>
</div>



                  </div>

    </div>
    </div>
    </div>

  <script src="{{ asset('default.js') }}"></script>

    <script type="text/javascript">

    $("body").on("keyup","#searchPro", function(){

      let searchData = $("#searchPro").val();
      if (searchData.length > 0){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
          type: 'POST',
          url: "{{ url('/searchpos/find/product') }}",
          data:{search:searchData},
          success:function(result){
              $("#selectProduct").empty();
              $('#selectProduct').html(result);
          }
        });
      }

      if(searchData.length < 1) $('#selectProduct').html("");

    })

    </script>

    <script type="text/javascript">

    function havePay() {


      var drop = document.getElementById("havePay_id").value;
      var div = document.getElementById("payment_div");

        if(drop > 0){
          div.style.display = "block";
        }
        else{
          div.style.display = "none";
        }

    }

      function showSearchPro(){
        $('#selectProduct').slideDown();
      }

      function hideSearchPro(){
        $('#selectProduct').slideUp();
      }

    </script>




    <script type="text/javascript">

    function get_custo_id(){
      let searchData = $("#custo_id").val();
      if (searchData.length > 0){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
          type: 'POST',
          url: "{{ url('/searchh/findcustomer') }}",
          data:{search:searchData},
          success:function(result){
              $('#suggestCustomer').html(result);
              var cus_due = $("#cus_due").val();
              $('#sub_id').html(cus_due);
          }
        });
      }

      if(searchData.length < 1) $('#suggestCustomer').html("");

    }

    </script>

    <script type="text/javascript">

      function showSearch(){
        $('#suggestCustomer').slideDown();
      }

      function hideSearch(){
        $('#suggestCustomer').slideUp();
      }

    </script>

@endsection

@include('default')
