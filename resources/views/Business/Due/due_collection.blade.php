@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-dollar"></i> Manage Due </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Due</a> </li>
              <li class="active">Manage Due </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-4">
              <div class="panel">
                  <div class="panel-heading">
                      <h4 class="panel-title">Due Collection</h4>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" action="{{ route('payment.add') }}" method="POST">
                      @csrf
                      <div>
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                for="demo-text-input">Select Date:</label>
                            <div class="col-md-8">
                              <input type="date" class="form-control" name="collection_date" required>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                for="demo-text-input">Due For:</label>
                            <div class="col-md-8">
                              <div class="form-check">
                                <input onclick="mycusF()" class="form-check-input" type="radio" value="1" name="due_for" checked="checked">
                                <label class="form-check-label" for="flexCheckDefault">
                                  Customer
                                </label>
                              </div>
                              <div class="form-check">
                                <input onclick="mysupF()" class="form-check-input" type="radio" value="2" name="due_for">
                                <label class="form-check-label" for="flexCheckChecked">
                                  Supplier
                                </label>
                              </div>
                            </div>
                        </div>

                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                for="demo-text-input">Select one:</label>
                                <div class="col-md-8" style="margin-bottom: 5px" id="mycus">
                            <select class="form-control selectpicker" data-live-search="true" name="customer_id" >
                                <option value="">-- Select Customer --</option>
                                 @foreach ($Customar_info as $c_item)
                                    @if(!$c_item->advance && $c_item->due)
                                    <option value="{{ $c_item->id }}">
                                             {{ $c_item->customar_name }} @if($c_item->due) {{ $c_item->due  }}
                                             <span>&#2547;</span> Due - {{ $c_item->address }}
                                             @endif @if($c_item->advance)
                                             {{ $c_item->advance  }}
                                             <span>&#2547;</span> Advance
                                             @endif
                                    </option>
                                    @endif
                                 @endforeach
                            </select>
                        </div>
                        <div class="col-md-8" style="margin-bottom: 5px; display: none;" id="mysup">
                    <select class="form-control selectpicker" data-live-search="true" name="supplier_id" >
                        <option value="">-- Select Supplier --</option>
                         @foreach ($Supplier_info as $s_item)
                           @if(!$s_item->advance && $s_item->due)
                            <option value="{{ $s_item->id }}">
                                {{ $s_item->supplier_name }} @if($s_item->due) {{ $s_item->due  }}<span>&#2547;</span> Due @endif @if($s_item->advance) {{ $s_item->advance  }}<span>&#2547;</span> Advance @endif
                            </option>
                           @endif
                        @endforeach
                    </select>
                </div>
                        </div>

                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                for="demo-text-input">Payment By:</label>
                            <div class="col-md-8">
                              <select class="form-control selectpicker" data-live-search="true" name="operation_type" required>
                                  <option value="">-- Select Option --</option>
                                  @foreach ($bank_info as $b_item)
                                     <option value="{{ $b_item->id }}">
                                         {{ $b_item->bank_name }}
                                     </option>
                                 @endforeach
                              </select>
                              @if($errors->any())
                                  <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                              @endif
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                for="demo-text-input">Amount:</label>
                            <div class="col-md-8">
                              <input type="number" id="demo-text-input" name="payment" class="form-control"
                                  placeholder="Enter Your Balance" step="any" required>
                              @error('payment')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                            </div>
                        </div>

                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                for="demo-text-input">Have a Note:</label>
                            <div class="col-md-8">
                              <input type="text" name="note" class="form-control"
                                      placeholder="Enter sort note!" step="any" required>
                              @error('payment')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                            </div>
                        </div>
                        <input type="hidden" name="product_code[]">
                        <input type="hidden" name="product_quantity[]">
                      </div>
                      <div class="col-md-12" style="padding: 0px">
                          <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add Due
                              Payment</button>
                      </div>
                    </form>
                  </div>
              </div>
            </div>
              <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Collection List</h4>
                        @if($errors->any())
                            <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                        @endif
                    </div>
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table id="demo-dt-basic" class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Name</th>
                                    <th>Previous</th>
                                    <th class="min-tablet">Operation</th>
                                    <th class="min-tablet">Payment</th>
                                    <th class="min-desktop">Date</th>
                                    <th class="min-tablet">Note</th>
                                    <th>Current</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Due_info as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        @if($item->customar_id) <td>{{ App\Models\Customar::find($item->customar_id)->customar_name }}</td> @endif
                                        @if($item->supplier_id) <td>{{ App\Models\Supplier::find($item->supplier_id)->supplier_name }}</td> @endif
                                        <td>
                                            {{ number_format($item->previous_due, 2) }}
                                        </td>
                                        <td>
                                            {{ $item->operation_type }}
                                        </td>
                                        <td>
                                            {{ number_format($item->payment, 2) }}
                                        </td>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                {{ $item->collection_date }}</span>
                                        </td>
                                        <td>{{ $item->note }}</td>
                                        <td>{{ number_format($item->current_due, 2) }}</td>
                                        <td style="align-items: center">
                                            <a href="{{ url('delete/due') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
<script>


function mycusF() {
  var x = document.getElementById("mycus");
  var y = document.getElementById("mysup");

    x.style.display = "block";
    y.style.display = "none";

}

function mysupF() {
  var x = document.getElementById("mycus");
  var y = document.getElementById("mysup");

    x.style.display = "none";
    y.style.display = "block";

}




    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });


    // $(document).ready(function () {
    //     $('#demo-dt-basic').DataTable({
    //         lengthMenu: [
    //             [0, 25, 50, -1],
    //             [0, 25, 50, 'All'],
    //         ],
    //     });
    // });

    $(document).ready(function() {
        $('#my_radio_box').change(function() {
            var id = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                dataType: 'html',
                url: '{{ url('select/due/collection/onajax') }}',
                // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                data: {
                    id: id
                },
                success: function(result) {
                    $('#box').html(result);
                }
            });
        });
    });
    $(document).ready(function() {
        $('#box').change(function() {
            var id = $(this).val();
            selectElement = document.querySelector('#my_radio_box');
            select = selectElement.value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: '{{ url('single_supplier/due/collection/onajax') }}',
                // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                data: {
                    id: id,
                    select: select,
                },
                success: function(result) {
                  alert(id);
                    $('#sub_id').val(result);
                }
            });
        });
    });
</script>
@endsection
