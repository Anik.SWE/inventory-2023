@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('view.pos') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">POS</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Customar Due List</h4>
                    </div>
                    <div id="page-content" style="padding: 0">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL.No</th>
                                            <th>Customar Name</th>
                                            <th class="min-tablet">Phone</th>
                                            <th>Purchase Amount</th>
                                            <th>Payment</th>
                                            <th>Due Payment</th>
                                            <th><span style="color: red">Due</span> / <span style="color: green">Advance</span></th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($customar_invoice as $item)
                                            @php
                                                $customars = DB::table('customars')
                                                    ->where('user_id', Auth::id())
                                                    ->where('id', $item->customar_id)->get();
                                                $purchese = DB::table('orders')
                                                    ->where('user_id', Auth::id())
                                                    ->where('customar_id', $item->customar_id)->groupBy('customar_id')
                                                    ->sum('total');
                                                $pay_amount = DB::table('orders')
                                                    ->where('user_id', Auth::id())
                                                    ->where('customar_id', $item->customar_id)->groupBy('customar_id')
                                                    ->sum('payment');
                                                $due_pay_amount = DB::table('dues')
                                                    ->where('user_id', Auth::id())
                                                    ->where('customar_id', $item->customar_id)->groupBy('customar_id')
                                                    ->sum('payment');
                                                $add = $pay_amount + $due_pay_amount;
                                                $due = $purchese - $add;
                                            @endphp
                                            @if ($item->customar_id == 'walk_in_customar' || $due == 0)
                                                @continue
                                            @endif
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>

                                                <td>
                                                    @if ($item->customar_id == 'walk_in_customar')
                                                        {{ $item->customar_id }}
                                                    @else
                                                        {{ App\Models\Customar::find($item->customar_id)->customar_name }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @foreach ($customars as $customar)
                                                        {{ $customar->phone_number }}
                                                    @endforeach
                                                </td>
                                                <td>
                                                    Total : {{ $purchese }}
                                                </td>
                                                <td>
                                                    Total : {{ $pay_amount }}
                                                </td>
                                                <td>
                                                    Total : {{ $due_pay_amount }}
                                                </td>
                                                <td>
                                                    Total : @php
                                                    if($due > 0){
                                                        echo '<span style="color: red">' .$due .'</span>';
                                                    }else {
                                                        echo '<span style="color: green">' .abs($due) .'</span>';
                                                    }
                                                @endphp
                                                </td>

                                                <td>
                                                    <div class="" style="text-align: center; border: 1px solid black"><a>
                                                            @php
                                                        if($due > 0){
                                                            echo '<span style="color: red">Due</span>';
                                                        }else {
                                                            echo '<span style="color: green">Advance</span>';
                                                        }
                                                    @endphp

                                                        </a>
                                                    </div>
                                                </td>
                                                <td style="align-items: center">
                                                    <div class="label label-table label-success"><a
                                                        href="{{ route('due.collection') }}"
                                                        style="color: #fff">Pay</a>
                                                </div>
                                                </td>
                                            </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
