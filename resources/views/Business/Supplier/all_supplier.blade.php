@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-users"></i> Suppliers </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Supplier</a> </li>
              <li class="active">All Suppliers </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <img style="width: 100%; height: 200px" src="{{ asset('user_assets/img/thumbs/img3.jpg') }}" alt="">
                        </div>
                        <div class="panel-footer">
                            <ul class="nav nav-section nav-justified">
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> {{ $Count }} </div>
                                        <p class="mar-no">Total Supplier</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> 00 </div>
                                        <p class="mar-no">Paid Amount</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> 00 </div>
                                        <p class="mar-no">UnPaid Amount</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!--===================================================-->
                    </div>
                </div>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <h4 class="panel-title">All Supplier's Here</h4>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                    <table id="demo-dt-basic" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>SL.No</th>
                                <th>Supplier Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Total_Supplier as $item)
                             @if(Session::get('business_id') == $item->business)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->supplier_name }}</td>
                                    <td>{{ $item->supplier_address }}</td>
                                    <td><span class="text-muted"><i class="fa fa-phone"></i> {{ $item->phone_number }}</span></td>
                                    <td> @if($item->due)
                                      <span class="text-danger">&#x09F3; {{ number_format($item->due, 2) }}</span>
                                      @endif  @if($item->advance)
                                      <span class="text-success">&#x09F3; {{ number_format($item->advance, 2) }}</span>
                                      @endif</td>
                                    <td>
                                      <div class="btn-group btn-group-sm dropup">
                                                                <button class="btn btn-sm btn-success dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                                  Action <i class="dropdown-caret fa fa-caret-up"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                  <li><a href="{{ url('/single/supplier/view') }}/{{ $item->id }}"><i class="fa fa-user"></i> Profile </a></li>
                                                                  <li><a href="{{ url('edit/supplier') }}/{{ $item->id }}"><i class="fa fa-edit"></i> Edit </a></li>
                                                                  <li><a href="{{ url('delete/supplier') }}/{{ $item->id }}"><i class="fa fa-trash"></i> Delete</a></li>
                                                                </ul>
                                                              </div>
                                    </td>
                                </tr>
                              @endif
                            @empty
                                <tfoot>
                                  <tr>
                                      <th colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong> You Not
                                          Create Any Supplier....Please Create a Supplier!</th>
                                  </tr>
                                </tfoot>
                            @endforelse
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
