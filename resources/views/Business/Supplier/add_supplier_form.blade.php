@extends('layouts.user_api.business_app')

@section('content')
@php
    error_reporting(0);
@endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user-plus"></i> Supplier </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="{{ route('AllS') }}"> Supplier</a> </li>
              <li class="active">Add Supplier </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="panel">
                  <div class="panel-heading">
                      <h4 class="panel-title">New Supplier</h4>
                  </div>
                  <!-- BASIC FORM ELEMENTS -->
                  <!--===================================================-->
                  <div class="panel-body">
                    <form class="form-horizontal"
                        action="{{ route('s.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <!--Text Input-->
                        <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />

                        <div class="row">
                           <div class="col-md-12">
                               <div class="form-group">
                                   <label class="control-label">Supplier Name</label>
                                   <input type="text" id="demo-text-input" name="supplier_name" class="form-control"
                                       placeholder="Enter Your Supplier Name" required>
                                   @error('supplier_name')
                                       <small class="help-block text-danger" >{{ $message }}</small>
                                   @enderror
                               </div>
                           </div>
                       </div>
                       <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="control-label">Supplier Email</label>
                                  <input type="email" id="demo-email-input" name="supplier_email" class="form-control"
                                      placeholder="Enter Your Supplier Email">
                                  @error('supplier_email')
                                      <small class="help-block text-danger" >{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="control-label">Phone Number</label>
                                  <input type="text" id="demo-email-input" name="phone_number" class="form-control"
                                      placeholder="Enter Supplier Phone Number" required>
                                  @error('phone_number')
                                      <small class="help-block text-danger" >{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                      </div>
                      <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Supplier Type</label>
                                                        <select class="form-control" name="supplier_type" id="">
                                                            <option value="">-- Select Option --</option>
                                                            <option value="Manufacturers">Manufacturers</option>
                                                            <option value="Wholesalers">Wholesalers</option>
                                                            <option value="Importers">Importers</option>
                                                        </select>
                                                        @error('supplier_type')
                                                            <small class="help-block text-danger" >{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Shop Name</label>
                                                        <input type="text" class="form-control" name="supplier_shop_name" id="validationCustom03" placeholder="Enter Supplier Shop Name">
                                                        @error('supplier_shop_name')
                                                            <small class="help-block text-danger" >{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Address</label>
                                                        <input type="text" class="form-control" name="supplier_address" placeholder="Enter Supplier Address" id="validationCustom03">
                                                        @error('supplier_address')
                                                            <small class="help-block text-danger" >{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Holder</label>
                                                        <input type="text" class="form-control" name="account_holder" id="validationCustom03" placeholder="Enter Account Holder">
                                                        @error('account_holder')
                                                            <small class="help-block text-danger" >{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-sm-6">
                                                  <div class="form-group">
                                                      <label class="control-label">Account Number</label>
                                                      <input type="text" class="form-control" name="account_number" placeholder="Enter Account Number" id="validationCustom03">
                                                      @error('account_number')
                                                          <small class="help-block text-danger" >{{ $message }}</small>
                                                      @enderror
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div class="form-group">
                                                      <label class="control-label">Bank Name</label>
                                                      <input type="text" class="form-control" name="bank_name" id="validationCustom03" placeholder="Type Bank Name">
                                                      @error('bank_name')
                                                          <small class="help-block text-danger" >{{ $message }}</small>
                                                      @enderror
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Branch Name</label>
                                                <input type="text" class="form-control" name="branch_name" id="validationCustom03" placeholder="Enter Branch Name">
                                                @error('branch_name')
                                                    <small class="help-block text-danger" >{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">City</label>
                                                <input type="text" class="form-control" name="city" id="validationCustom03" placeholder="Enter City">
                                                @error('city')
                                                    <small class="help-block text-danger" >{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                        <!--State-->
                        <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                                  <label class="control-label">Amount As</label>
                                  <select class="form-control" name="amount_type" id="">
                                      <option value="">-- Select Option --</option>
                                      <option value="Due">-- Due --</option>
                                      <option value="Advance">-- Advance --</option>
                                  </select>
                                    @error('city')
                                        <small class="help-block text-danger" >{{ $message }}</small>
                                    @enderror
                              </div>
                          </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Initial Amount</label>
                                <input type="number" id="demo-text-input" name="opening_amount" class="form-control" step="any">
                                @error('branch_name')
                                    <small class="help-block text-danger" >{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                        <!--Photo-->
                        {{-- <div class="form-group" >
                            <label class="col-md-2 control-label text-left"
                                for="demo-textarea-input"><span style="color: #25A79F">Step 12:</span> Photo </label>
                            <div class="col-md-10">
                                <input type="file" class="form-control upload" name="supplier_photo" id="validationCustom03" placeholder="Choose Photo" accept="image/*" onchange="readURL(this);" required>
                                <img id="image" src="#" alt="">
                                @error('supplier_photo')
                                    <small class="help-block text-danger" >{{ $message }}</small>
                                @enderror
                            </div>
                        </div> --}} <hr>
                        <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck">
                                <label class="form-check-label" for="invalidCheck">
                                    Agree to terms and conditions
                                </label>
                              </div>
                          </div>
                        </div>

                        <div class="col-12 pull-right">
                            <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add Supplier</button>
                        </div>
                    </form>
                  </div>

                  <!--===================================================-->
                  <!-- END BASIC FORM ELEMENTS -->
              </div>
          </div>

        </div>

    </div>

    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $('#image')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(100);
                };
                reader.readAsDataURL(input.files[0])
            }
        }
    </script>
@endsection
