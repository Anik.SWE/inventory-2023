@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user"></i> Supplier </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="{{ route('AllS') }}">All Supplier</a> </li>
              <li class="active">Supplier Profile</li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="panel">
              <div class="panel-heading">
                  <h3 class="panel-title">{{ $single_supplier_view->supplier_name }}</h3>
              </div>
              <div class="panel-body">
                  <table class="table table-bordered">
                      <tbody>
                        <tr>
                            <td>Total Due </td>
                            <td><span class="label label-sm label-warning">&#x09F3;
                                {{ $single_supplier_view->due }}
                            </span></td>
                            <td>Total Advance </td>
                            <td><span class="label label-sm label-success">&#x09F3;
                                {{ $single_supplier_view->advance }}
                            </span></td>

                        </tr>

                          <tr>
                              <td> Email </td>
                              <td>{{ $single_supplier_view->supplier_email }}</td>
                              <td> Phone Number </td>
                              <td>{{ $single_supplier_view->phone_number }}</td>
                          </tr>
                          <tr>
                              <td> Supplier Type </td>
                              <td><span class="label label-sm label-success">
                                      {{ $single_supplier_view->supplier_type }}</span></td>
                              <td> Shop Name </td>
                              <td><span class="label label-sm label-danger">{{ $single_supplier_view->supplier_shop_name }}</span></td>
                          </tr>
                          <tr>
                              <td colspan="4"> </td>
                          </tr>
                          <tr>
                              <td> Address </td>
                              <td>{{ $single_supplier_view->supplier_address }}</td>
                              <td> City </td>
                              <td>{{ $single_supplier_view->city }}</td>
                          </tr>
                          <tr>
                              <td> Account Holder </td>
                              <td>{{ $single_supplier_view->account_holder }}</td>
                              <td> Account Number </td>
                              <td>{{ $single_supplier_view->account_number }}</td>
                          </tr>
                          <tr>
                              <td> Bank Name </td>
                              <td>{{ $single_supplier_view->bank_name }}</td>
                              <td> Branch Name </td>
                              <td>{{ $single_supplier_view->branch_name }} </td>
                          </tr>
                          <tr>
                              <td></td>
                              <td></td>

                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>

        <div class="text-center no-print">
            <a class="btn btn-primary btn-sm" onClick="jQuery('#page-content').print()">
                <i class="fa fa-print"></i> Print
            </a>
        </div>
    </div>
  </div>
@endsection
