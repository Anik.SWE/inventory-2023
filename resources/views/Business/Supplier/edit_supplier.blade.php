@extends('layouts.user_api.business_app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">

      <div class="pageheader">
          <h3><i class="fa fa-user-plus"></i> Supplier </h3>
          <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="{{ route('AllS') }}">All Supplier</a> </li>
              <li class="active">Edit Supplier </li>
            </ol>
          </div>
        </div>

        <div id="page-content">
            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">Edit Supplier</h4>
                </div>
                <!-- BASIC FORM ELEMENTS -->
                <!--===================================================-->
                <div class="panel-body">
                  <form class="form-horizontal" style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                      action="{{ url('/edit/supplier/formpost/'.$supplier_info->id) }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <!--Text Input-->
                      <div class="form-group" style="margin-bottom: 10px">
                          <label class="col-md-2 control-label text-danger text-left" style="padding-left: 20px"
                              for="demo-text-input"><span style="color: #25A79F">Step 1:</span>Supplier Name</label>
                          <div class="col-md-10">
                              <input type="text" id="demo-text-input" name="supplier_name" class="form-control"
                                  placeholder="Enter Your Supplier Name" value="{{ $supplier_info->supplier_name }}">
                              @error('supplier_name')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>
                      <!--Email Input-->
                      <div class="form-group text-left" style="margin-bottom: 0px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                              <span style="color: #25A79F">Step 2:</span> Supplier Email </label>
                          <div class="col-md-4" style="margin-bottom: 10px">
                              <input type="email" id="demo-email-input" name="supplier_email" class="form-control"
                                  placeholder="Enter Your Supplier Email" value="{{ $supplier_info->supplier_email }}">
                              @error('supplier_email')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>

                          <label class="col-md-2 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                              <span style="color: #25A79F">Step 3:</span> Phone Number </label>
                          <div class="col-md-4" style="margin-bottom: 10px">
                              <input type="text" id="demo-email-input" name="phone_number" class="form-control"
                                  placeholder="Enter Supplier Phone Number" value="{{ $supplier_info->phone_number }}">
                              @error('phone_number')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>
                      <!--Textarea-->
                      <div class="form-group" style="margin-bottom: 0px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 4:</span> Supplier Type </label>
                          <div class="col-md-4" style="margin-bottom: 10px">
                              <select class="form-control" name="supplier_type" id="">
                                  <option value="{{ $supplier_info->supplier_type }}">-- {{ $supplier_info->supplier_type }} --</option>
                                  <option value="Manufacturers">Manufacturers</option>
                                  <option value="Wholesalers">Wholesalers</option>
                                  <option value="Importers">Importers</option>
                              </select>
                              @error('supplier_type')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>

                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 5:</span> Shop Name </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="supplier_shop_name" id="validationCustom03" placeholder="Enter Supplier Shop Name" value="{{ $supplier_info->supplier_shop_name }}">
                              @error('supplier_shop_name')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>
                      <!--City-->
                      <div class="form-group" style="margin-bottom: 10px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 6:</span> Address </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="supplier_address" placeholder="Enter Supplier Address" id="validationCustom03" value="{{ $supplier_info->supplier_address }}">
                              @error('supplier_address')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 7:</span> Account Holder </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="account_holder" id="validationCustom03" placeholder="Enter Account Holder" value="{{ $supplier_info->account_holder }}">
                              @error('account_holder')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>
                      <!--State-->
                      <div class="form-group" style="margin-bottom: 10px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 8:</span> Account Number </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="account_number" placeholder="Enter Account Number" id="validationCustom03" value="{{ $supplier_info->account_number }}">
                              @error('account_number')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>

                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 9:</span> Bank Name </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="bank_name" id="validationCustom03" placeholder="Type Bank Name" value="{{ $supplier_info->bank_name }}">
                              @error('bank_name')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>

                      <!--State-->
                      <div class="form-group" style="margin-bottom: 10px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 10:</span> Branch Name </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="branch_name" id="validationCustom03" placeholder="Enter Branch Name" value="{{ $supplier_info->branch_name }}">
                              @error('branch_name')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>

                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 11:</span> City </label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" name="city" id="validationCustom03" placeholder="Enter City" value="{{ $supplier_info->city }}">
                              @error('city')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div>

                      <!--Photo-->
                      {{-- <div class="form-group" style="margin-bottom: 10px">
                          <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                              for="demo-textarea-input"><span style="color: #25A79F">Step 12:</span> Photo </label>
                          <div class="col-md-10">
                              <input type="file" class="form-control upload" name="supplier_photo" id="validationCustom03"
                                  placeholder="Choose Photo" value="{{ $supplier_info->supplier_photo }}" accept="image/*" onchange="readURL(this);">
                              <img id="image"
                                  src="{{ asset('uploads/supplier_photos') }}/{{ $supplier_info->supplier_photo }}"
                                  alt="" style="width: 120px; height:100px">
                              @error('supplier_photo')
                                  <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                              @enderror
                          </div>
                      </div> --}}

                      <div class="col-12" style="padding: 5px">
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="invalidCheck">
                              <label class="form-check-label" for="invalidCheck">
                                  Agree to terms and conditions
                              </label>
                          </div>
                      </div>
                      <div class="col-12" style="padding: 5px">
                          <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit Supplier</button>
                      </div>
                  </form>
                </div>

                <!--===================================================-->
                <!-- END BASIC FORM ELEMENTS -->
            </div>
        </div>
        {{-- {{ $business_id }} --}}

    </div>

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image')
                        .attr('src', e.target.result)
                        .width(120)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0])
            }
        }
    </script>
@endsection
