@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-bank"></i> Bank & Summary</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Bank</a> </li>
              <li class="active">Bank & Summary </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Bank List</h4>
                      </div>

                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL.No</th>
                                        <th>Bank Name</th>
                                        {{-- <th>Account Holder</th> --}}
                                        <th>A/C Number</th>
                                        <th>Transaction</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Bank as $item)
                                        <tr id="charactersList">
                                            <td>{{ $loop->iteration }} </td>
                                            <td>{{ $item->bank_name }}</td>
                                            {{-- <td>{{ $item->account_holder }}</td> --}}
                                            <td>{{ $item->account_number }}</td>
                                            <td>
                                                @php
                                                    $find = DB::table('transactions')
                                                        ->select('id')
                                                        ->where('bank', $item->id)
                                                        ->count();
                                                @endphp
                                                @if ($find > 0)
                                                    <div class="label label-table label-success"><a
                                                            href="{{ url('/single/bank/transaction/view') }}/{{ $item->id }}" style="color: #fff;">View</a>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>&#x09F3; {{ $item->opening_amount }}</td>
                                            <td>
                                              <div class="btn-group btn-group-sm dropup">
                                              													<button class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                              														Action <i class="dropdown-caret fa fa-caret-up"></i>
                                              													</button>
                                              													<ul class="dropdown-menu">
                                              														<li><a href="{{ url('edit/bank') }}/{{ $item->id }}"><i class="fa fa-edit"></i> Edit </a></li>
                                              														<li><a href="{{ url('delete/bank') }}/{{ $item->id }}"><i class="fa fa-trash"></i> Delete</a></li>
                                              													</ul>
                                              												</div>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Bank....Please Create a Bank Account!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Add Bank</h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('bankform.add') }}" method="POST">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                    for="demo-text-input">Bank Name:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="bank_name" class="form-control"
                                        placeholder="Enter Your Bank Name">
                                    @error('bank_name')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                    for="demo-text-input">Account Holder:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="account_holder" class="form-control"
                                        placeholder="Enter Holder Name">
                                    @error('account_holder')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                    for="demo-text-input">Account Number:</label>
                                <div class="col-md-8">
                                    <input type="number" id="demo-text-input" name="account_number" class="form-control">
                                    @error('account_number')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 5px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Account Group:</label>
                                <div class="col-md-8" style="margin-bottom: 5px">
                                    <select class="form-control" name="account_group" id="">
                                        <option value="">-- Select Option --</option>
                                        <option value="Bank Account">Bank Account</option>
                                        <option value="Bkash">Mobile Banking</option>
                                        <option value="Fixed Asset">Fixed Asset</option>
                                    </select>
                                    @error('account_group')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 5px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Account Type:</label>
                                <div class="col-md-8" style="margin-bottom: 5px">
                                    <select class="form-control" name="account_type" id="">
                                        <option value="">-- Select Option --</option>
                                        <option value="Savings accounts">Savings accounts</option>
                                        <option value="Checking accounts">Checking accounts</option>
                                        <option value="Money market accounts">Money market accounts</option>
                                        <option value="Certificates of deposit">Certificates of deposit</option>
                                        <option value="Brokerage accounts">Brokerage accounts</option>
                                    </select>
                                    @error('account_type')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px; padding-right: 0"
                                    for="demo-text-input">Amount:</label>
                                <div class="col-md-8">
                                    <input type="number" id="demo-text-input" name="opening_amount" class="form-control" step="any">
                                    @error('opening_amount')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12 text-center" style="padding: 5px">
                                <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                    Bank Info</button>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
