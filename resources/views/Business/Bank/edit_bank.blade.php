@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-edit"></i>Edit Bank & Summary</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Bank</a> </li>
              <li class="active">Edit Bank & Summary </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Bank List</h4>
                      </div>

                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>SL.No</th>
                                        <th>Bank Name</th>
                                        <th>Account Holder</th>
                                        <th>A/C Number</th>
                                        <th>Account Type</th>
                                        <th>Account</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Bank as $item)
                                        <tr id="charactersList">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->bank_name }}</td>
                                            <td>{{ $item->account_holder }}</td>
                                            <td>{{ $item->account_number }}</td>
                                            <td>{{ $item->account_type }}</td>
                                            <td>{{ $item->opening_amount }}</td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/bank') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Bank....Please Create a Bank Account!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Add Bank</h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('bank.editformpost') }}" method="POST">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px;">
                                <label class="col-md-4 control-label text-danger text-left"
                                    for="demo-text-input">Bank Name:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="bank_name" class="form-control"
                                        placeholder="Enter Your Bank Name" value="{{ $Bank_info->bank_name }}">

                                    <input type="hidden" id="demo-text-input" name="id" class="form-control"
                                        placeholder="Enter Your Category Name" value="{{ $Bank_info->id }}">
                                    @error('bank_name')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Account Holder:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="account_holder" class="form-control"
                                        placeholder="Enter Holder Name" value="{{ $Bank_info->account_holder }}">
                                    @error('account_holder')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Account Number:</label>
                                <div class="col-md-8">
                                    <input type="number" id="demo-text-input" name="account_number" class="form-control" value="{{ $Bank_info->account_number }}">
                                    @error('account_number')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Account Group:</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="account_group">
                                        <option value="{{ $Bank_info->account_group }}">{{ $Bank_info->account_group }}</option>
                                        <option value="Bank Accounts">Bank Accounts</option>
                                        <option value="Fixed Asset">Fixed Asset</option>
                                    </select>
                                    @error('account_group')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Account Type:</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="account_type">
                                        <option value="{{ $Bank_info->account_type }}">{{ $Bank_info->account_type }}</option>
                                        <option value="Savings Accounts">Savings accounts</option>
                                        <option value="Loan Account">Loan Account</option>
                                        <option value="Current Account">Current Account</option>
                                    </select>
                                    @error('account_type')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Opening Amount:</label>
                                <div class="col-md-8">
                                    <input type="number" id="demo-text-input" name="opening_amount" class="form-control" value="{{ $Bank_info->opening_amount }}" readonly>
                                    @error('opening_amount')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                <button class="btn btn-xs btn-warning" type="submit">Update
                                 Info</button>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
