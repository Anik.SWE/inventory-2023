@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">

            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('emp.attendance') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">Take Attendance</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Employee List</h4>
                    </div>

                    <div class="panel-body" style="padding-bottom: 0;">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                    <div class="form-group">
                                        <input class="form-control form-control-sm shadow-none search" id="myInput"
                                            type="search" placeholder="Search for a page" aria-label="search" />
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                        <table class="table table-striped" id="myTable">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Employee Name</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <form>
                                @csrf
                                <tbody>
                                    @forelse ($Attendance_Date_info as $item)
                                        <tr id="charactersList">
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>
                                                {{ App\Models\Employee::find($item->emp_id)->employee_name }}
                                            </td>
                                            <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                {{ $item->att_date }}
                                                </span>
                                            </td>
                                            <input type="hidden" name="id[]" value="{{ $item->id }}">
                                            <input type="hidden" name="att_date" value="{{ $item->att_date }}">
                                            <input type="hidden" name="att_year" value="{{ $item->att_year }}">
                                            <td style="align-items: center">
                                                <input type="radio" name="attendance[{{ $item->id }}]"
                                                    value="Present"
                                                    @php
                                                        if($item->attendance == 'Present'){
                                                            echo 'checked';
                                                        }
                                                    @endphp
                                                    ><span class="text-success">Present</span>
                                                <input type="radio" name="attendance[{{ $item->id }}]"
                                                    value="Absent"
                                                    @php
                                                        if($item->attendance == 'Absent'){
                                                            echo 'checked';
                                                        }
                                                    @endphp
                                                    ><span class="text-danger">Absent</span>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }}
                                                </strong>
                                                You Not Create Any Employee....Please Create a Employee!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                <input type="button" class="form-control text-left" value="Date: {{ $item->att_date }}">
                        </table>
                    </div>

                </form>
                </div>
            </div>

        </div>
    </div>
@endsection
