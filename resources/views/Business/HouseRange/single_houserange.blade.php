@extends('layouts.user_api.business_app')
@section('content')
@php
    error_reporting(0);
@endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-8">
                <div class="panel" style="margin-top: 5px;">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Padd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Product</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Product List</h4>
                    </div>


                    <div class="panel-body">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right">
                                    <div class="form-group">
                                        <input id="demo-input-search2" type="text" placeholder="Search"
                                            class="form-control" autocomplete="off">
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F;">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Advance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($house_range as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item->shop_name }}</td>
                                        <td>{{ $item->owner_name }}</td>
                                        <td>
                                            {{ $item->advance }}
                                        </td>
                                        <td>
                                            <div class="label label-table label-success"><a
                                                    href=""
                                                    style="color: #fff">Active</a>
                                            </div>
                                        </td>
                                        <td style="align-items: center">
                                            <a href="{{ url('edit/house/range') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>
                                            <a href="{{ url('delete/house/range') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any House Range....Please Create a Range!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('AllP') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">See Product</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Add Product</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal"
                        style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px" action="{{ route('PFadd') }}"
                        method="POST">
                        @csrf
                        <!--Text Input-->

                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                for="demo-text-input">Selected Month:</label>
                            <div class="col-md-8">
                                <select class="form-select form-control" aria-label="Default select example" name="month">
                                    <option value="1" {{ \Carbon\Carbon::now()->month ==  '1'? 'selected' : '' }} >January</option>
                                    <option value="2" {{ \Carbon\Carbon::now()->month ==  '2'? 'selected' : '' }} >February</option>
                                    <option value="3" {{ \Carbon\Carbon::now()->month ==  '3'? 'selected' : '' }} >March</option>
                                    <option value="4" {{ \Carbon\Carbon::now()->month ==  '4'? 'selected' : '' }} >April</option>
                                    <option value="5" {{ \Carbon\Carbon::now()->month ==  '5'? 'selected' : '' }} >May</option>
                                    <option value="6" {{ \Carbon\Carbon::now()->month ==  '6'? 'selected' : '' }} >June</option>
                                    <option value="7" {{ \Carbon\Carbon::now()->month ==  '7'? 'selected' : '' }} >July</option>
                                    <option value="8" {{ \Carbon\Carbon::now()->month ==  '8'? 'selected' : '' }} >August</option>
                                    <option value="9" {{ \Carbon\Carbon::now()->month ==  '9'? 'selected' : '' }} >September</option>
                                    <option value="10"{{ \Carbon\Carbon::now()->month ==  '10'? 'selected' : '' }} >October</option>
                                    <option value="11"{{ \Carbon\Carbon::now()->month ==  '11'? 'selected' : '' }} >November</option>
                                    <option value="12"{{ \Carbon\Carbon::now()->month ==  '12'? 'selected' : '' }} >December</option>
                                </select>

                                @error('month')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="payment">
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Total Amount:</label>
                                <div class="col-md-8">
                                    <input type="text" id="total_amount" name="total_amount" class="form-control"
                                        readonly value="{{ $range->salary_range }}" onchange="auto_total();">
                                    @error('total_amount')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Payment Type:</label>
                                <div class="col-md-8" style="display: flex">
                                    <div class="form" style="margin-right: 10px">
                                        <input class="form-check-input" type="radio" name="payment_type"
                                            id="exampleRadios1" value="Cash" {{ old('payment_type') == 'Cash' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Cash
                                        </label>
                                    </div>
                                    <div class="form">
                                        <input class="form-check-input" type="radio" name="payment_type"
                                            id="exampleRadios1" value="Bank Transfer" {{ old('payment_type') == 'Bank Transfer' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Bank Transfer
                                        </label>
                                    </div>
                                    @error('payment_type')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Payment:</label>
                                <div class="col-md-3">
                                    <input type="number" id="payment" name="payment" class="form-control"
                                        value="0" onchange="auto_total();">
                                    @error('payment')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>

                                <label class="col-md-2 control-label text-left" for="demo-text-input">Change:</label>
                                <div class="col-md-3">
                                    <input type="text" id="change" name="change" class="form-control"
                                        value="0" onchange="auto_total();" readonly>
                                    @error('change')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>



                            <div class="col-12" style="padding: 5px; margin-left: 130px">
                                <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                    Product</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function auto_total() {
            var total_amount = document.getElementById("total_amount").value;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
    </script>
@endsection
