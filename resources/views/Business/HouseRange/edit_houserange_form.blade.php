@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-8">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('house.range.form') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Range</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">House Range List</h4>
                    </div>

                    <div class="panel-body" style="padding-bottom: 0;">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                    <div class="form-group">
                                        <input id="myInput" type="text" placeholder="Search" class="form-control"
                                            autocomplete="off">
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                        <table class="table table-striped" id="myTable">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Shop Name</th>
                                    <th>Owner Name</th>
                                    <th>Advance</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Range_info as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item->shop_name }}</td>
                                        <td>{{ $item->owner_name }}</td>
                                        <td>
                                            {{ $item->advance }}
                                        </td>
                                        <td>
                                            <div class="label label-table label-success">
                                                <a style="color: #fff">
                                                    @php
                                                        if($item->status == "active"){
                                                            echo "Active";
                                                        }else {
                                                            echo "Panding";
                                                        }
                                                    @endphp

                                                </a>
                                            </div>
                                        </td>
                                        <td style="align-items: center">
                                            <a href="{{ url('edit/house/range') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any House Range....Please Create a Range!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('update.range.form') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All House Range</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Add Document</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal"
                        style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                        action="{{ route('update.range.form') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <!--Text Input-->
                        <div class="form-group" style="margin-bottom: 10px">
                            <div class="col-md-6">
                                <label for="validationCustom01" class="form-label" style="color: red">Shop Name</label>
                                <input type="text" class="form-control" placeholder="Enter Shop Name"
                                    id="validationCustom01" name="shop_name" value="{{ $House_info->shop_name }}" required>
                                <input type="hidden" class="form-control" placeholder="Enter Shop Name"
                                    id="validationCustom01" name="id" value="{{ $House_info->id }}" required>
                                @error('shop_name')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom02" class="form-label">Owner Name</label>
                                <input type="text" class="form-control" placeholder="Owner Name" name="owner_name"
                                    id="validationCustom02" value="{{ $House_info->owner_name }}" required>
                                @error('owner_name')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px">
                            <div class="col-md-6">
                                <label for="validationCustom01" class="form-label">Start Month</label>
                                <input type="date" class="form-control" id="validationCustom01" name="start_month" value="{{ $House_info->start_month }}"
                                    required>
                                @error('start_month')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="validationCustom02" class="form-label">Advance</label>
                                <input type="number" class="form-control" name="advance" id="validationCustom02"
                                    value="{{ $House_info->advance }}" required>
                                @error('advance')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group" style="padding-left:12px; padding-right:12px">
                            <input type="number" class="form-control" name="salary_range" placeholder="Month Wise Salary Range"
                                    required value="{{ $House_info->salary_range }}">
                            @error('salary_range')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group" style="padding-left:12px; padding-right:12px">
                            <select class="form-select form-control" aria-label="Default select example" name="status">
                                <option value="">Select Option</option>
                                <option value="active" {{ $House_info->status ==  'active'? 'selected' : '' }}>Active</option>
                                <option value="inactive" {{ $House_info->status ==  'inactive'? 'selected' : '' }}>Panding</option>
                            </select>
                            @error('status')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="col-12 text-center" style="padding: 5px">
                            <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Update
                                Range</button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // Search
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection

{{-- $image = array();
        if ($files = $request->hasFile('image')) {
            if($files = $request->file('image')){
                foreach ($files as $file) {
                    $fileName = time() . '.' . $file->getClientOriginalExtension();
                    $location = 'public/multiple_imager/' . $fileName;
                    Image::make($file)->save(base_path($location));
                    $image_url = $location.$fileName;
                    $image[] = $image_url;
                }
            }
        } --}}
