@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        error_reporting(0);
    @endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('view.pos') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">POS</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Sale And Purchase Report List</h4>
                    </div>
                    <div id="page-content" style="padding: 0">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL.No</th>
                                            <th>Item Name</th>
                                            <th>Unit Price</th>
                                            <th>Quantity</th>
                                            <th>Sale Price</th>
                                            <th><span style="color: green">Profit</span> / <span
                                                    style="color: red">Loss</span></th>
                                            <th>Total Sale</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sale_report as $item)
                                            @php
                                                $qty = $item->old_quantity - $item->product_quantity;
                                                $buy_price = $item->product_buying_price * $qty;
                                                $sale_price = $item->product_selling_price * $qty;
                                                $profit = $sale_price - $buy_price;
                                            @endphp
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>

                                                <td>
                                                    {{ $item->product_name }}
                                                </td>
                                                <td>
                                                    {{ $item->product_buying_price }} Taka
                                                </td>
                                                <td>
                                                    {{ $qty }} pice
                                                </td>
                                                <td style="align-items: center">
                                                    {{ $item->product_selling_price }} Taka
                                                </td>
                                                <td>
                                                    @php
                                                        if ($profit > $buy_price) {
                                                            echo '<span style="color: red">' . abs($profit) . '</span>';
                                                        } else {
                                                            echo '<span style="color: green">' . $profit . '</span>';
                                                        }
                                                    @endphp
                                                </td>
                                                <td>
                                                    <div class="label label-table label-success"><a href="#"
                                                            data-toggle="modal" data-target="#modaladd"
                                                            style="color: white">View</a>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                                {{-- ----- Modal Body Start------------- --}}


                                <div class="modal fade" id="modaladd" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm modal-notify" role="document" style="width: 700px"
                                        id="content">
                                        <div class="panel panel-bordered panel-primary">
                                            <div class="panel-heading">
                                                <div class="panel-control">
                                                    <button class="btn btn-primary" data-dismiss="modal"><i
                                                            class="fa fa-times"></i></button>
                                                </div>
                                                <h3 class="panel-title">Total Sale Product</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="panel">
                                                        <div class="panel-body">
                                                            <address>
                                                                <div class="product_details"
                                                                    style="display: flex; justify-content: space-between">
                                                                    <div class="product_name">
                                                                        <strong>Product Info:-- </strong>
                                                                        <br>
                                                                        <span>Product Name: </span>
                                                                        @foreach ($sale_report as $product_name)
                                                                            <span>{{ $product_name->product_name }}</span>
                                                                        @break
                                                                    @endforeach

                                                                    <br>
                                                                    <span title="Phone">P-code:</span>
                                                                    @foreach ($sale_report as $product_name)
                                                                        <span>{{ $product_name->product_code }}</span>
                                                                    @break
                                                                @endforeach
                                                                <br>
                                                                <span title="Phone">Price:</span>
                                                                @foreach ($sale_report as $product_name)
                                                                    <span>{{ $product_name->product_selling_price }}
                                                                        Taka</span>
                                                                @break
                                                            @endforeach
                                                        </div>
                                                        <div class="line"
                                                            style="width: 1px; background: gray; opacity: .3">

                                                        </div>
                                                        <div class="product_quantity">
                                                            <strong>Sale Details:-- </strong>
                                                            <br>
                                                            <span>Product Quantity: </span>
                                                            @foreach ($sale_report as $product_quantity)
                                                                <span>x
                                                                    {{ $product_quantity->old_quantity }}</span>
                                                            @break
                                                        @endforeach

                                                        <br>
                                                        <span title="Phone">Total sale:</span>
                                                        @foreach ($sale_report as $product_quantity)
                                                            <span>
                                                                @php
                                                                    $qty = $product_quantity->old_quantity - $product_quantity->product_quantity;
                                                                    $amount = $product_quantity->product_selling_price * $qty;
                                                                @endphp
                                                                {{ $qty }} pice
                                                            </span>
                                                        @break
                                                    @endforeach
                                                    <br>
                                                    <span title="Phone">Sale Amount:</span>
                                                    @foreach ($sale_report as $product_quantity)
                                                        <span>
                                                            {{ $amount }} Taka
                                                        </span>
                                                    @break
                                                @endforeach
                                            </div>
                                        </div>
                                    </address>
                                    <hr>
                                    <address>
                                        <div class="cal"
                                            style="display: flex; justify-content: space-between">
                                            <div class="contact">
                                                <strong>Producer Contact</strong><br>
                                                <a>
                                                    first.last@example.com
                                                </a>
                                            </div>

                                            <div class="calculation">
                                                <strong>Calculation:--</strong><br>
                                                <span>Sale Amount:</span>
                                                    <span>
                                                        {{ $buy_price }} Taka
                                                    </span>

                                                <br>
                                                <span>Payment:</span>
                                                @foreach ($sale_report as $product_quantity)
                                                    <span>
                                                        {{ $buy_price }} Taka
                                                    </span>
                                                    @break
                                                @endforeach
                                                <br>
                                                <span>Due:</span>
                                                @foreach ($sale_report as $product_quantity)
                                                    <span>
                                                        {{ $amount }} Taka
                                                    </span>
                                                    @break
                                                @endforeach
                                            </div>
                                        </div>
                                    </address>
                                    <hr>
                                    <button class="btn btn-default pull-right"
                                        onClick="jQuery('#content').print()"><span
                                            class="fa fa-print"></span> Print </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- ----- Modal Body End------------- --}}

    </div>
</div>
</div>

</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });


    // $(document).ready(function () {
    //     $('#demo-dt-basic').DataTable({
    //         lengthMenu: [
    //             [0, 25, 50, -1],
    //             [0, 25, 50, 'All'],
    //         ],
    //     });
    // });





    $(document).ready(function() {
        $('#my_radio_box').change(function() {
            var id = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: '{{ url('view/account/number/onajax') }}',
                // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                data: {
                    id: id
                },
                success: function(result) {
                    $('#sub_id').val(result);
                }
            });
        });
    });
</script>
@endsection
