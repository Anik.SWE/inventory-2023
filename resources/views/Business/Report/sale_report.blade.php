@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('view.pos') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">POS</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Sale Report List</h4>
                    </div>
                    <div id="page-content" style="padding: 0">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL.No</th>
                                            <th>Item Name</th>
                                            <th>Old Quantity</th>
                                            <th>Ouantity</th>
                                            <th>Price</th>
                                            <th>Total Sale</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sale_report as $item)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>

                                                <td>
                                                    {{ $item->product_name }}
                                                </td>
                                                <td>
                                                    {{ $item->old_quantity }}
                                                </td>
                                                <td>
                                                    {{ $item->product_quantity }}
                                                </td>
                                                <td style="align-items: center">
                                                    {{ $item->product_selling_price }} Taka
                                                </td>
                                                <td>
                                                    <div class="label label-table label-success"><a href="{{ url('/single/product/sale_report') }}/{{ $item->product_code }}"
                                                            style="color: white">View</a>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
