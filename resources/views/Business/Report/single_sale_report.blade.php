@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">

            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('bank.add') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">Bank Account</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Total Sale Product</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <div class="row">
                                <div class="panel">
                                    <div class="panel-body" id="content">
                                        <address>
                                            <div class="product_details"
                                                style="display: flex; justify-content: space-between">
                                                <div class="product_name">
                                                    <strong>Product Info:-- </strong>
                                                    <br>
                                                    <span>Product Name: </span>
                                                    @foreach ($single_sale_report as $product_name)
                                                        <span>{{ $product_name->product_name }}</span>

                                                        <br>
                                                        <span title="Phone">P-code:</span>
                                                        <span>{{ $product_name->product_code }}</span>
                                                        <br>
                                                        <span title="Phone">Price:</span>
                                                        <span>{{ $product_name->product_selling_price }}
                                                            Taka</span>
                                                    @endforeach
                                                </div>
                                                <div class="line" style="width: 1px; background: gray; opacity: .3">

                                                </div>
                                                <div class="product_quantity">
                                                    <strong>Sale Details:-- </strong>
                                                    <br>
                                                    <span>Product Quantity: </span>
                                                    @foreach ($single_sale_report as $product_quantity)
                                                        <span>x
                                                            {{ $product_quantity->old_quantity }}</span>

                                                        <br>
                                                        <span title="Phone">Total sale:</span>
                                                        <span>
                                                            @php
                                                                $qty = $product_quantity->old_quantity - $product_quantity->product_quantity;
                                                                $amount = $product_quantity->product_selling_price * $qty;
                                                            @endphp
                                                            {{ $qty }} pice
                                                        </span>
                                                        <br>
                                                        <span title="Phone">Sale Amount:</span>
                                                        <span>
                                                            {{ $amount }} Taka
                                                        </span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </address>
                                        <hr>
                                        <address>
                                            <div class="cal" style="display: flex; justify-content: space-between">
                                                <div class="contact">
                                                    <strong>Producer Contact</strong><br>
                                                    <a>
                                                        first.last@example.com
                                                    </a>
                                                </div>

                                                <div class="calculation">
                                                    <strong>Calculation:--</strong><br>
                                                    <span>Sale Amount:</span>

                                                    @foreach ($single_sale_report as $product_quantity)
                                                    @php
                                                        $payment = DB::table('search_products')->select('product_code')->where('product_code',  $product_name->product_code)->groupBy('product_code')->where('user_id', Auth::id())->sum('payment');
                                                        $due = DB::table('search_products')->select('product_code')->where('product_code',  $product_name->product_code)->groupBy('product_code')->where('user_id', Auth::id())->sum('change');
                                                        
                                                    @endphp
                                                        <span>
                                                            {{ $amount }} Taka
                                                        </span>
                                                        <br>
                                                        <span>Payment:</span>
                                                        <span>
                                                            {{ $payment }} Taka
                                                        </span>
                                                        <br>
                                                        <span>Due:</span>
                                                        <span>
                                                            {{ $due }} Taka
                                                        </span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </address>
                                        <hr>
                                        <button class="btn btn-default pull-left" onClick="jQuery('#content').print()"><span
                                                class="fa fa-print"></span> Print
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">
                            <form class="panel-body form-horizontal"
                                style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                                action="{{ route('product.filter.formpost') }}" method="POST">
                                @csrf

                                @foreach ($single_sale_report as $item)
                                    <input type="hidden" value="{{ $item->product_code }}" name="product_code">
                                @endforeach
                                <div class="col-md-12" style="padding: 0; display:flex; justify-content:space-between">
                                    <div id="demo-dp-component col-md-10">
                                        <div class="col-md-10" style="padding: 0;">
                                            <div id="demo-dp-range">
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="date" class="form-control" name="start_date" />
                                                    <span class="input-group-addon">to</span>
                                                    <input type="date" class="form-control" name="end_date" />
                                                </div>
                                            </div>
                                            @error('start_date')
                                                <small class="help-block text-danger"
                                                    style="margin-bottom: 0px">{{ $message }}</small>
                                            @enderror
                                            @error('end_date')
                                                <small class="help-block text-danger"
                                                    style="margin-bottom: 0px">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-1" style="padding: 0px; margin-left: 20px">
                                        <button class="btn" style="background-color: #25A79F; color: #fff; float: right;"
                                            type="submit">Check</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-md-12">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Bradd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Brands</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Sale Product List</h4>
                    </div>
                    <div id="page-content" style="padding: 0">
                        <!-- Basic Data Tables -->
                        <!--===================================================-->
                        <div class="panel">
                            <div class="panel-body">
                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL.No</th>
                                            <th>Product Name</th>
                                            <th>Product Code</th>
                                            <th class="min-tablet">Quantity</th>
                                            <th class="min-tablet">Price</th>
                                            <th class="min-tablet">Total Sale</th>
                                            <th class="min-desktop">Date</th>
                                            {{-- <th>Status</th> --}}
                                            {{-- <th>Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($saledetails_report as $item)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>
                                                    @php
                                                        $name = DB::table('products')
                                                            ->where('user_id', Auth::id())
                                                            ->where('product_code', $item->product_code)
                                                            ->get();
                                                    @endphp
                                                    @foreach ($name as $product_name)
                                                        {{ $product_name->product_name }}
                                                    @endforeach

                                                </td>
                                                <td>
                                                    {{ $item->product_code }}
                                                </td>
                                                <td>
                                                    x {{ $item->product_quantity }}
                                                </td>
                                                <td>
                                                    @foreach ($name as $product_name)
                                                        {{ $product_name->product_selling_price }} Taka
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach ($name as $product_name)
                                                    @php
                                                        $price = $product_name->product_selling_price * $item->product_quantity;
                                                    @endphp
                                                        {{ $price }} Taka
                                                    @endforeach
                                                </td>
                                                <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                        {{ Carbon\Carbon::parse($item->transaction_date)->format('d-m-Y') }}</span>
                                                </td>

                                                {{-- <td>
                                                    <div class="label label-table label-success"><a
                                                            href="{{ url('/single/product/search/view') }}/{{ $item->product_code }}"
                                                            style="color: #fff">View</a>
                                                    </div>
                                                </td> --}}
                                                {{-- <td style="align-items: center">
                                                    <a href="{{ url('delete/transaction') }}/{{ $item->id }}"><i
                                                            class="fa fa-trash text-danger"> Del </i></a>
                                                </td> --}}
                                            </tr>
                                        @empty
                                        @endforelse


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
