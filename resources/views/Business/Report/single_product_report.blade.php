@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        {{-- <div class="col-md-12">
            <div class="panel" style="margin-top: 5px">
                <div class="panel-heading" style="border: 1px solid #25A79F">
                    <div class="panel-control">
                        <a href="{{ route('transaction.add') }}" class="btn"
                            style="background-color: #25A79F; color: #fff; margin-right: 10px">Transaction</a>
                        <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                    </div>
                    <h4 class="panel-title">Transaction Filters</h4>
                </div>
                <!-- BASIC FORM ELEMENTS -->
                <!--===================================================-->
                <form class="panel-body form-horizontal" style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                    action="{{ route('filter.formpost') }}" method="POST">
                    @csrf

                    @foreach ($bank_transaction_view as $item)
                        <input type="hidden" value="{{ $item->id }}" name="id">
                    @endforeach
                    <div class="col-md-12" style="padding: 0">
                        <div id="demo-dp-component col-md-10">
                            <div class="col-md-10" style="padding: 0">
                                <div id="demo-dp-range">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="date" class="form-control" name="start_date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="date" class="form-control" name="end_date" />
                                    </div>
                                </div>
                                @error('start_date')
                                    <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                                @error('end_date')
                                    <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-1" style="padding: 0px; margin-left: 20px">
                            <button class="btn" style="background-color: #25A79F; color: #fff; float: right;"
                                type="submit">Check</button>
                        </div>
                    </div>
                </form>
            </div>
        </div> --}}
        <div class="col-lg-12">
            <div id="page-content" style="padding: 0">
                <!-- Basic Data Tables -->
                <!--===================================================-->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Product Name:
                            @forelse ($search_details as $item)
                                @php
                                    $name = DB::table('products')
                                        ->where('user_id', Auth::id())
                                        ->where('product_code', $item->product_code)
                                        ->get();
                                @endphp
                                @foreach ($name as $product_name)
                                    {{ $product_name->product_name }}
                                @endforeach
                                @break
                            @endforeach

                        </h3>
                    </div>
                    <div class="panel-body">
                        <table id="demo-dt-basic" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Product Name</th>
                                    <th>Product Code</th>
                                    <th class="min-tablet">Quantity</th>
                                    <th class="min-tablet">Price</th>
                                    <th class="min-tablet">Total Sale</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $balance = 0;
                                @endphp
                                @forelse ($search_details as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>
                                            @php
                                                $name = DB::table('products')
                                                    ->where('user_id', Auth::id())
                                                    ->where('product_code', $item->product_code)
                                                    ->get();
                                            @endphp
                                            @foreach ($name as $product_name)
                                                {{ $product_name->product_name }}
                                            @endforeach

                                        </td>
                                        <td>
                                            {{ $item->product_code }}
                                        </td>
                                        <td>
                                            x {{ $item->product_quantity }}
                                        </td>
                                        <td>
                                            @foreach ($name as $product_name)
                                                {{ $product_name->product_selling_price }} Taka
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($name as $product_name)
                                                @php
                                                    $price = $product_name->product_selling_price * $item->product_quantity;
                                                @endphp
                                                {{ $price }} Taka
                                            @endforeach
                                        </td>
                                    </tr>
                                @empty
                                @endforelse


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endsection
