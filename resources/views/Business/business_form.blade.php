@extends('layouts.user_api.app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div class="pageheader">
  					<h3><i class="fa fa-home"></i> Add Business </h3>
  					<div class="breadcrumb-wrapper">
              <span class="label">You are here:</span>
              <ol class="breadcrumb">
                <li> <a href="{{ route('user') }}"> Home</a> </li>
                <li class="active">Add Business </li>
              </ol>
            </div>
  				</div>
        @if (session('add_status'))
            <a href="" class="btn" style="margin: 10px 00px 0px 20px; width:1338px; text-align: left; font-weight:300; background-color: #25A79F; color: #fff">
                <strong>Congratulations!</strong> Now You'r {{ session('add_status') }} Business Ready To Sell...</a>
        @endif
        <div id="page-content">
          <div class="panel">
              <div class="panel-heading">
                  <h4 class="panel-title">Add Business Name: 'Company Name'</h4>
              </div>
              <div class="panel-body">
                <form class="form-horizontal" action="{{ url('/business/formpost') }}" method="POST">
                    @csrf
                    <!--Text Input-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                            for="demo-text-input">Step 1: Business Name [ Required ]</label>
                        <div class="col-md-8">
                            <input type="text" id="demo-text-input" name="business_name" class="form-control"
                                placeholder="Enter Your Business Name">
                            @error('business_name')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--Email Input-->
                    <div class="form-group text-left" style="margin-bottom: 0px">
                        <label class="col-md-4 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                            <span style="color: #25A79F">Step 2:</span> Business Email </label>
                        <div class="col-md-8" style="margin-bottom: 10px">
                            <input type="email" id="demo-email-input" name="business_email" class="form-control"
                                placeholder="Enter Your Business Email">
                            @error('business_email')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--Textarea-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 3:</span> Phone: </label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="city" id="validationCustom03" required>
                            @error('city')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                        <label class="col-md-1 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F"></span> Zip: </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="zip" id="validationCustom03" required>
                            @error('zip')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group text-left" style="margin-bottom: 0px">
                        <label class="col-md-4 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                            <span style="color: #25A79F">Step 4:</span> Business Address: </label>
                        <div class="col-md-8" style="margin-bottom: 10px">
                            <input type="text" name="business_location" id="" class="form-control"
                                placeholder="Enter Your Business Address">
                                @error('business_location')
                                    <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                        </div>
                    </div>
                    <!--City-->
                    <!--State-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 5:</span> State: </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="state" id="validationCustom03" required>
                            @error('state')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="col-12" style="padding: 5px">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                            <label class="form-check-label" for="invalidCheck">
                                Agree to terms and conditions
                            </label>
                        </div>
                    </div>
                    <div class="col-12" style="padding: 5px">
                        You have {{ 5 - DB::table('businesses')->where('user_id' , Auth::user()->id)->count() }} business left! <br>

                        @if( DB::table('businesses')->where('user_id' , Auth::user()->id)->count() < 5 )
                        <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                            Business</button>
                        @endif
                    </div>
                </form>
              </div>
          </div>
        </div>

    </div>
    {{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
