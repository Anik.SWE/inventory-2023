@extends('layouts.user_api.business_app')
@section('content')
    @php
        $transaction_id = uniqid();
    @endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
          <h3><i class="fa fa-reply-all"></i>Product</h3>
          <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Product</a> </li>
              <li class="active">All Product's </li>
            </ol>
          </div>
        </div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Purchase Return List</h4>
                      </div>
                      <div class="panel-body table-responsive">
                        <table id="demo-dt-basic" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Product Name</th>
                                    {{-- <th>Brands</th> --}}
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Return_Product as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ App\Models\Product::find( App\Models\ProductPurchase::find($item->purchase)->product )->product_name }}</td>
                                        <td>&#x09F3; {{ $item->product_price }}</td>
                                        <td>{{ $item->product_quantity }}</td>
                                        <td>
                                        {{-- <a href="{{ url('edit/purchess-return/product') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a> --}}
                                            <a href="{{ url('delete/purchess-return/product') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                    @endforelse
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
              @if($purchase)
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Add Return Product</h4>
                      </div>
                      <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('purchess.return.add') }}" method="POST">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">

                                <input type="number" name="pur_id" value="{{$purchase->id}}" hidden>
                                <input type="number" name="pro_id"  value="{{$purchase->product}}" hidden>
                                <input type="number" name="supplier_id"  value="{{ $purchase->supplier_id }}" hidden>
                                <input type="hidden" name="operation_type" class="form-control" value="Deposit">


                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                              @if($errors->any())
                                  <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                              @endif
                                <label class="col-md-4 control-label text-left"
                                    for="demo-text-input">Quantity:</label>
                                <div class="col-md-8">
                                    <input type="number" id="quantityp" name="product_quantityp" class="form-control"
                                        value="{{$purchase->quantity}}" onchange="auto_total();" readonly>
                                    @error('product_quantity')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" for="demo-text-input">Price:</label>
                              <div class="col-md-8">
                                  <input type="number" id="buying_price" name="product_price" class="form-control"
                                      value="{{$purchase->price}}" onkeyup="auto_total()" onchange="auto_total()" readonly>
                                  @error('product_price')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>

                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Purchase Amount:</label>
                                <div class="col-md-8">
                                    <input  type="text" id="total_amountp" name="total_amountp" class="form-control"
                                        readonly value="{{$purchase->total_amount - $purchase->shipping_cost}}">
                                    @error('total_amount')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Return Quantity:</label>
                                <div class="col-md-8">
                                    <input type="number" id="quantity" name="product_quantity" class="form-control"
                                        value="0" onkeyup="auto_total()" onchange="auto_total();">
                                    @error('product_quantity')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>

                            </div>

                            <div class="payment">
                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Return Amount:</label>
                                    <div class="col-md-8">
                                        <input type="text" id="total_amount" name="total_amount" class="form-control"
                                            readonly value="0">
                                        @error('total_amount')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Payment Type:</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="bankr" onchange="havePay();" id="havePay_id">
                                            <option value="">-- Select Option --
                                            </option>
                                            @forelse ($banks as $bank)
                                                <option value="{{ $bank->id }}">
                                                    {{ $bank->bank_name }}
                                                </option>
                                            @empty
                                                <option value="">No Bank Found!
                                                </option>
                                            @endforelse
                                        </select>
                                        @error('payment_type')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 10px; display: none;" id="payment_div">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Payment:</label>
                                    <div class="col-md-8">
                                        <input type="number" id="payment" name="payment" class="form-control"
                                            value="0" onkeyup="auto_total()" onchange="auto_total();" step="any">
                                        @error('payment')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" for="demo-text-input" style="padding-left: 20px">Change:</label>
                                    <div class="col-md-8">
                                        <input type="text" id="change" name="change" class="form-control"
                                            value="0" onchange="auto_total();" readonly>
                                        @error('change')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>




                                <div class="col-12" style="padding: 5px; margin-left: 150px">
                                    <button class="btn" style="background-color: #25A79F; color: #fff"
                                        type="submit">Return
                                        Product</button>
                                </div>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
              @endif
          </div>
        </div>
    </div>
@endsection

@section('script')
    <script>


    function havePay() {


      var drop = document.getElementById("havePay_id").value;
      var div = document.getElementById("payment_div");

        if(drop){
          div.style.display = "block";
        }
        else{
          div.style.display = "none";
        }

    }


        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price));
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
