@extends('layouts.user_api.business_app')
@section('content')
    @php
        $transaction_id = uniqid();
    @endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
          <h3><i class="fa fa-reply-all"></i>Sale Return</h3>
          <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Return</a> </li>
              <li class="active">Sale Return </li>
            </ol>
          </div>
        </div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel" >
                      <div class="panel-heading">
                          <h4 class="panel-title">Sale Return List</h4>
                      </div>

                      <div class="panel-body table-responsive">
                        <table id="demo-dt-basic" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Product Name</th>
                                    {{-- <th>Brands</th> --}}
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Return_Product as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td> {{ App\Models\Product::find( App\Models\SaleReturn::find($item->id)->product )->product_name }} </td>
                                        <td>$ {{ $item->product_price }}</td>
                                        <td>{{ $item->product_quantity }}</td>
                                        <td style="align-items: center">
                                            <!--<a href="{{ url('edit/sale-return/product') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>-->
                                            <a href="{{ url('/delete/sale-return/product') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any Product....Please Create a Product!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                      </div>

                  </div>
              </div>
              @if($sell)<div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Return sell </h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <form class="panel-body form-horizontal" action="{{ route('sale.return.add') }}" method="POST">
                          @csrf
                          <!--Text Input-->
                          <div class="form-group" style="margin-bottom: 10px">


                              <input style=" display: none;" type="number" name="sell_id" value="{{$sell->id}}" >

                              <input style=" display: none;" type="number" name="customer_id"  value="{{ $sell->customar_id }}" hidden>
                              <input type="hidden" name="operation_type" class="form-control" value="Withdraw">
                              @if($errors->any())
                                  <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                              @endif
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Product:</label>
                              <div class="col-md-8">
                                  <select class="form-control selectpicker" data-live-search="true" name="pro_id" id="">
                                      <option value="0">
                                          Select
                                      </option>
                                      @foreach ($Total_Product as $item)
                                          <option value="{{ $item->id }}">{{ $item->product_name }}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>


                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Quantity:</label>
                              <div class="col-md-3">
                                  <input type="number" id="quantity" name="product_quantity" class="form-control"
                                      value="0" onchange="auto_total();">
                                  @error('product_quantity')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="payment">



                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Payment:</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="bankr" onchange="havePay();" id="havePay_id">
                                        <option value="">-- Select --
                                        </option>

                                            <option value="Due">
                                                Due Adjust
                                            </option>

                                            @if($sell->payment)

                                            <option value="Cash">
                                                Cash
                                            </option>

                                            @endif

                                    </select>
                                    @error('payment_type')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>



                              <div class="col-12" style="padding: 5px; margin-left: 150px">
                                  <button class="btn" style="background-color: #25A79F; color: #fff"
                                      type="submit">Return
                                      Sell</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
              @endif
          </div>
        </div>
    </div>
@endsection

@section('script')
    <script>


    function havePay() {


      var drop = document.getElementById("havePay_id").value;
      var div = document.getElementById("payment_div");

        if(drop){
          div.style.display = "block";
        }
        else{
          div.style.display = "none";
        }

    }

        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price));
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
