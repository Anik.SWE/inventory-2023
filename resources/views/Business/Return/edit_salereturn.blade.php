@extends('layouts.user_api.business_app')
@section('content')
    @php
        $transaction_id = uniqid();
        // error_reporting(0);
    @endphp

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container" style="margin: 15px 0">
            <div class="col-lg-12">
                <div class="col-md-8">
                    <div class="panel" style="margin-top: 5px;">
                        <div class="panel-heading" style="border: 1px solid #25A79F">
                            <div class="panel-control">
                                <a href="{{ route('Padd') }}" class="btn"
                                    style="background-color: #25A79F; color: #fff; margin-right: 10px">All Product</a>
                                <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                            </div>
                            <h4 class="panel-title">Return Product List</h4>
                        </div>


                        <div class="panel-body">
                            <div class="pad-btm form-inline">
                                <div class="row">
                                    <div class="col-sm-6 table-toolbar-left">
                                        <div class="btn-group">
                                            <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                        class="fa fa-plus"></i></a></button>
                                            <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                            <button class="btn btn-default"><i
                                                    class="fa fa-exclamation-circle"></i></button>
                                            <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 table-toolbar-right">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search"
                                                class="form-control" autocomplete="off">
                                        </div>
                                        <div class="btn-group">
                                            <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                    <i class="fa fa-cog"></i>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive" style="border: 1px solid #25A79F;">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>SL.No</th>
                                        <th>Product Name</th>
                                        {{-- <th>Brands</th> --}}
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Return_Product as $item)
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>$ {{ $item->product_price }}</td>
                                            <td>{{ $item->product_quantity }}</td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/sale-return/product') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }}
                                                </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel" style="margin-top: 5px">
                        <div class="panel-heading" style="border: 1px solid #25A79F">
                            <div class="panel-control">
                                <a href="{{ route('AllP') }}" class="btn"
                                    style="background-color: #25A79F; color: #fff; margin-right: 10px">See Product</a>
                                <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                            </div>
                            <h4 class="panel-title">Add Return Product</h4>
                        </div>
                        <!-- BASIC FORM ELEMENTS -->
                        <!--===================================================-->
                        <form class="panel-body form-horizontal"
                            style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                            action="{{ route('edit.return.add') }}" method="POST">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Product Name:</label>
                                <input type="hidden" name="transaction_id" class="form-control"
                                    value="{{ $transaction_id }}">
                                <input type="hidden" name="operation_type" class="form-control" value="Withdraw">
                                <input type="hidden" name="id" class="form-control" value="{{ $info->id }}">

                                <div class="col-md-8">
                                    <select class="form-control" name="product_name" id="">
                                        <option value="{{ $info->product_name }}">
                                            {{ App\Models\Product::find($info->product_name)->product_name }}</option>
                                        @foreach ($Total_Product as $item)
                                            <option value="{{ $item->id }}">{{ $item->product_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 5px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Supplier Name:</label>
                                <div class="col-md-8" style="margin-bottom: 5px">
                                    <select class="form-control" name="supplier_id" id="">
                                        @php
                                            $supplier = App\Models\Product::find($info->product_name)->supplier_id;
                                            $supp = App\Models\Supplier::find($supplier)->supplier_name;
                                        @endphp
                                        <option value="{{ $supplier }}">{{ $supp }}</option>
                                        @foreach ($Supplier_info as $item)
                                            <option value="{{ $item->id }}">{{ $item->supplier_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('supplier_id')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Quantity:</label>
                                <div class="col-md-3">
                                    <input type="number" id="quantity" name="product_quantity" class="form-control"
                                        value="{{ $info->product_quantity }}" onchange="auto_total();">
                                    @error('product_quantity')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>

                                <label class="col-md-2 control-label text-left" for="demo-text-input">Price:</label>
                                <div class="col-md-3">
                                    <input type="number" id="buying_price" name="product_price" class="form-control"
                                        value="{{ $info->product_price }}" onchange="auto_total();">
                                    @error('product_price')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="payment">
                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Total Amount:</label>
                                    <div class="col-md-8">
                                        <input type="text" id="total_amount" name="total_amount" class="form-control"
                                            readonly value="{{ old('total_amount') }}">
                                        @error('total_amount')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Payment Type:</label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="payment_type">
                                            @php
                                                // $bank_id = App\Models\SaleReturn::find($info->payment_type)->payment_type;
                                                $bank = App\Models\Bank::find($info->payment_type)->bank_name;
                                            @endphp
                                            <option value="{{ $info->payment_type }}">{{ $bank }}</option>
                                            @forelse ($banks as $bank)
                                                <option value="{{ $bank->id }}">
                                                    {{ $bank->bank_name }}
                                                </option>
                                            @empty
                                                <option value="">No Bank Found!
                                                </option>
                                            @endforelse
                                        </select>
                                        @error('payment_type')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Payment:</label>
                                    <div class="col-md-3">
                                        <input type="number" id="payment" name="payment" class="form-control"
                                            value="{{ $info->payment }}" onchange="auto_total();">
                                        @error('payment')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>

                                    <label class="col-md-2 control-label text-left" for="demo-text-input">Change:</label>
                                    <div class="col-md-3">
                                        <input type="text" id="change" name="change" class="form-control"
                                            value="0" onchange="auto_total();" readonly>
                                        @error('change')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>



                                <div class="col-12" style="padding: 5px; margin-left: 150px">
                                    <button class="btn" style="background-color: #25A79F; color: #fff"
                                        type="submit"> Update Return
                                        </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('script')
    <script>
        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price));
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
