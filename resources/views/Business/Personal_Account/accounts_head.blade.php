@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
        <div class="pageheader">
            <h3><i class="fa fa-bank"></i> Accounts Category</h3>
            <div class="breadcrumb-wrapper">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li> <a href="#"> Account </a> </li>
                    <li class="active">Head Category</li>
                </ol>
            </div>
        </div>
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">
            <div class="row">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="#" data-toggle="modal" data-target="#modaladd"><button
                                    class="btn btn-primary btn-labeled fa fa-plus-square">New One</button></a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table id="demo-dt-basic" class="table table-striped table-bordered">
                            <thead style="background-color:#25A79F; color: #FFFFFF;">
                                <tr>
                                    <th>SL</th>
                                    <th>Main Head</th>
                                    <th>Sub Head</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($accountHead))
                                    @foreach ($accountHead as $acc)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $acc->head_name }}</td>
                                            <td>{{ @$acc->account_head->head_name }} </td>
                                            <td class="text-center">
                                                <div class="widgetbox-subtitle">
                                                    
                                                    <button
                                                        class="btn btn-default-sm pull-right" data-toggle="modal"
                                                        data-target="#modaledit"
                                                        onclick="editContent('{{ $acc->id }}','{{ $acc->head_name }}','{{ $acc->main_head_id }}')"><i
                                                            class="fa fa-edit"></i>
                                                    </button>


                                                    <a href="{{ route('account-head.edit', $acc->id) }}"
                                                        class="btn btn-default-sm"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Button trigger modal-->
        </div>
        <!--===================================================-->
        <!--End page content-->
    </div>
    <!--===================================================-->

    <div class="modal fade" id="modaladd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify" role="document">
            <div class="panel panel-bordered panel-primary">
                <div class="panel-heading">
                    <div class="panel-control">
                        <button class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                    <h3 class="panel-title">Create Accounts Head</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('account-head.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Category Name</label>
                                    <input type="text" id="demo-vs-definput" name="head_name" class="form-control"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Select Type: Main/Sub
                                        Head</label>
                                    <select class="form-control selectpicker" name="main_head_id">
                                        <option value="0">Main Head</option>
                                        @if (!empty($accountHead))
                                            @foreach ($accountHead as $acc)
                                                <option value="{{ $acc->id }}">{{ $acc->head_name }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            Okay
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- edit Modal Start -->
    <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm modal-notify" role="document">
            <div class="panel panel-bordered panel-primary">
                <div class="panel-heading">
                    <div class="panel-control">
                        <button class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i></button>
                    </div>
                    <h3 class="panel-title">Edit Account Head</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('account-head.store') }}" method="post">
                        @csrf
                        <input type="hidden"  id="id" name="id" class="form-control">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Category Name</label>
                                    <input type="text"  id="head_name" name="head_name"
                                        class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="demo-vs-definput" class="control-label">Select Type: Main/Sub
                                        Head</label>
                                    <select class="form-control " id="main_head_id" name="main_head_id">
                                        <option value="0">Main Head</option>
                                        @if (!empty($accountHead))
                                            @foreach ($accountHead as $acc)
                                                <option value="{{ $acc->id }}">{{ $acc->head_name }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            Okay
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function editContent(id, category_name, main_head_id) {
            console.log(category_name);
            $("#id").val(id);
            $("#head_name").val(category_name);
            $("#main_head_id").val(main_head_id);
        }
    </script>
@endsection
