@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
        <!--Page Title-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="pageheader">
            <h3><i class="fa fa-bar-chart"></i> Search Report </h3>
            <div class="breadcrumb-wrapper">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li> <a href="#"> Accounts </a> </li>
                    <li class="active"> Search Report </li>
                </ol>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <!--End page title-->
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Filtering Glance Report by Date Range</h3>
                                </div>
                                <div class="panel-body">
                                    <form method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">From
                                                        Date</label>
                                                    <input type="date" name="transaction_date" class="form-control"
                                                        value="{{ old('transaction_date') }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">To
                                                        Date</label>
                                                    <input type="date" name="to_transaction_date" class="form-control"
                                                        value="{{ old('to_transaction_date') }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">Head
                                                        Category</label>
                                                    <select name="main_head_id" data-placeholder="Choose a head..."
                                                        class="demo-chosen-select form-control"
                                                        onchange="getSubHead(this.value)">
                                                        <option value="">Select one...</option>
                                                        @if (!empty($accountHead))
                                                            @foreach ($accountHead as $acc)
                                                                <option value="{{ $acc->id }}">{{ $acc->head_name }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">Sub
                                                        Head</label>
                                                    <select data-placeholder="Choose a sub head..."
                                                        class="form-control demo-chosen-select" name="sub_head_id"
                                                        id="sub_head">
                                                        <option value="">Select one...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">Select
                                                        Account</label>
                                                    <select name="bank_id" data-placeholder="Choose a bank..."
                                                        class="form-control demo-chosen-select">
                                                        <option value="">Select one...</option>
                                                        @foreach ($banks as $bank)
                                                            <option value="{{ $bank->id }}">{{ $bank->bank_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="demo-vs-definput" class="control-label">Transaction
                                                        Type</label>
                                                    <select class="form-control selectpicker" name="transaction_type">
                                                        <option value="">Select one...</option>
                                                        <option value="Income">Income</option>
                                                        <option value="Expenses">Expenses</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <button type="submit" name="button"
                                            class="btn btn-primary pull-right">Report</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead style="background-color:#25A79F; color: #FFFFFF;">
                                                <tr>
                                                    <th>Sl</th>
                                                    <th>Date</th>
                                                    <th>Head Category</th>
                                                    <th>Sub Head</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Account</th>
                                                    <th>Note</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $income = 0;
                                                    $expense = 0;
                                                @endphp
                                                @if (!empty($personalAccountTransaction))
                                                    @foreach ($personalAccountTransaction as $transaction)
                                                        @php
                                                            if ($transaction->transaction_type == 'Income') {
                                                                $income += $transaction->amount;
                                                            } else {
                                                                $expense += $transaction->amount;
                                                            }

                                                        @endphp

                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $transaction->transaction_date }}</td>
                                                            <td>{{ @$transaction->account_head->head_name }}</td>
                                                            <td>{{ @$transaction->sub_account_head->head_name }}</td>
                                                            <td>{{ $transaction->transaction_type }}</td>
                                                            <td>{{ $transaction->amount }}</td>
                                                            <td>{{ $transaction->bank->bank_name }}</td>
                                                            <td>{{ $transaction->note }}</td>

                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="9">Total Income: {{ $income }}/-</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="9">Total Expenses: {{ $expense }}/-</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--===================================================-->
        <!--End page content-->
    </div>
@endsection



@section('script')
    <script>
        function getSubHead(head_id) {
            $.ajax({
                type: "post",
                url: "{{ route('get-sub-head') }}",
                // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                data: {
                    head_id: head_id,
                    _token: `{{ csrf_token() }}`,
                },
                success: function(result) {
                    var html = '<option value="">select one</option>';
                    for (var i = 0; i < result.length; i++) {
                        html += `<option value="${result[i].id}">${result[i].head_name}</option>`;
                    }
                    console.log(html);
                    $('#sub_head').html(html);
                }
            });
        }
    </script>
@endsection
