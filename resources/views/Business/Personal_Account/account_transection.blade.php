@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
        <div class="pageheader">
            <h3><i class="fa fa-bank"></i> Account Transaction</h3>
            <div class="breadcrumb-wrapper">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li> <a href="#"> Account </a> </li>
                    <li class="active"> Account Transaction </li>
                </ol>
            </div>
        </div>
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Create Transaction</h3>
                        </div>
                        <form action="{{ route('personal-account-transaction.store') }}" method="post">
                            @csrf
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Transaction
                                                Date</label>
                                            <input type="date" name="transaction_date" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Select Account
                                                Head</label>
                                            <select name="main_head_id" data-placeholder="Choose a head..."
                                                class="form-control" required onchange="getSubHead(this.value)">
                                                <option value="">Select one...</option>
                                                @if (!empty($accountHead))
                                                    @foreach ($accountHead as $acc)
                                                        <option value="{{ $acc->id }}">{{ $acc->head_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Sub Head
                                                (Optional)</label>
                                            <select data-placeholder="Choose a sub head..." class="form-control"
                                                name="sub_head_id" id="sub_head">
                                                <option value="">Select one...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Select
                                                Account</label>
                                            <select name="bank_id" data-placeholder="Choose a bank..."
                                                class="form-control" required>
                                                <option value="">Select one...</option>
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->bank_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Transaction
                                                Type</label>
                                            <select name="transaction_type" class="form-control selectpicker" required>
                                                <option value="">Select one...</option>
                                                <option value="Income">Income</option>
                                                <option value="Expenses">Expenses</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Amount</label>
                                            <input type="text" class="form-control" name="amount" placeholder="BDT"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="demo-vs-definput" class="control-label">Note</label>
                                            <textarea name="note" class="form-control" rows="3" cols="60"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="demo-dt-basic" class="table table-striped table-bordered dataTable">
                                    <thead style="background-color:#25A79F; color: #FFFFFF;">
                                        <tr>
                                            <th>Sl</th>
                                            <th>Date</th>
                                            <th>Head Category</th>
                                            <th>Sub Head</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Account</th>
                                            <th>Note</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $income = 0;
                                            $expense = 0;
                                        @endphp
                                        @if (!empty($personalAccountTransaction))
                                            @foreach ($personalAccountTransaction as $transaction)
                                                @php
                                                    if ($transaction->transaction_type == 'Income') {
                                                        $income += $transaction->amount;
                                                    } else {
                                                        $expense += $transaction->amount;
                                                    }

                                                @endphp

                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $transaction->transaction_date }}</td>
                                                    <td>{{ @$transaction->account_head->head_name }}</td>
                                                    <td>{{ @$transaction->sub_account_head->head_name }}</td>
                                                    <td>{{ $transaction->transaction_type }}</td>
                                                    <td>{{ $transaction->amount }}</td>
                                                    <td>{{ $transaction->bank->bank_name }}</td>
                                                    <td>{{ $transaction->note }}</td>
                                                    <td style="">
                                                        <a title="Delete Transaction"
                                                            href="{{ route('personal-account-transaction.edit', $transaction->id) }}"
                                                            class="btn btn-xs btn-danger pull-right"><i
                                                                class="icon fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="9">Total Income: {{ $income }}/-</th>
                                        </tr>
                                        <tr>
                                            <th colspan="9">Total Expenses: {{ $expense }}/-</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Button trigger modal-->
        </div>
        <!--===================================================-->
        <!--End page content-->
    </div>
@endsection

@section('script')
    <script>
        function getSubHead(head_id) {
            $.ajax({
                type: "post",
                url: "{{ route('get-sub-head') }}",
                // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                data: {
                    head_id: head_id,
                    _token: `{{ csrf_token() }}`,
                },
                success: function(result) {
                    var html = '<option value="">select one</option>';
                    for (var i = 0; i < result.length; i++) {
                        html += `<option value="${result[i].id}">${result[i].head_name}</option>`;
                    }
                    console.log(html);
                    $('#sub_head').html(html);
                }
            });
        }
    </script>
@endsection
