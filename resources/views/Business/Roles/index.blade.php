@extends('layouts.user_api.business_app')
@section('content')
@php
    error_reporting(0);
@endphp
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-8">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('transaction.add') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Role</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Role List</h4>
                    </div>

                    <div class="panel-body" style="padding-bottom: 0;">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                    <div class="form-group">
                                        <input class="form-control form-control-sm shadow-none search" id="myInput" type="search"
                                            placeholder="Search for a page" aria-label="search" />
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                        <table class="table table-striped" id="myTable">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Role Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Role as $item)
                                    <tr id="charactersList">
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td style="align-items: center">
                                            <a href="{{ url('edit/bank') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>
                                            <a href="{{ url('delete/bank') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any Bank....Please Create a Bank Account!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('view.pos') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">POS</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Add Role</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal"
                        style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px" action="{{ route('bankform.add') }}"
                        method="POST">
                        @csrf
                        <!--Text Input-->
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                for="demo-text-input">Role Name:</label>
                            <div class="col-md-8">
                                <input type="text" id="demo-text-input" name="name" class="form-control"
                                    placeholder="Enter Your Role Name">
                                @error('name')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                            <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px; padding-top:0; margin-top: 10px"
                                for="demo-text-input">Permissions: </label>
                            <div class="col-md-8" style="margin-top: 10px">
                                <div class="form-check">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="checkPermissionAll" value="1">
                                        <label class="form-check-label" for="checkPermissionAll">All</label>
                                    </div>
                                    <hr>
                                    @foreach ($Permissions as $permission)
                                    <input class="form-check-input" type="checkbox" value="" id="permission{{ $permission->id }}">
                                    <label class="form-check-label" for="permission{{ $permission->id }}" style="margin-right: 10px">
                                      {{ $permission->name }}
                                    </label>

                                    @endforeach
                                  </div>
                                @error('name')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>

                        </div>

                        <div class="col-12 text-center" style="padding: 5px">
                            <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                Role Info</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });


    // Check all the permissions
         $("#checkPermissionAll").click(function(){
             if($(this).is(':checked')){
                 // check all the checkbox
                 $('input[type=checkbox]').prop('checked', true);
             }else{
                 // un check all the checkbox
                 $('input[type=checkbox]').prop('checked', false);
             }
         });

  </script>
@endsection
