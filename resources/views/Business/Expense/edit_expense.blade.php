@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">
            <div class="col-md-8">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Exadd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Expense</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Expense List</h4>
                    </div>

                    <div class="panel-body" style="padding-bottom: 0;">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                                    <div class="btn-group">
                                        <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                                    class="fa fa-plus"></i></a></button>
                                        <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                                <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                                    <div class="form-group">
                                        <input id="demo-input-search2" type="text" placeholder="Search"
                                            class="form-control" autocomplete="off">
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                                <i class="fa fa-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="border: 1px solid #25A79F; margin-top: 5px; padding: 10px">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Expense Details</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Total_Expense as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $item->expense_details }}</td>
                                        <td>$ {{ $item->expense_amount }}</td>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i>
                                                {{ Carbon\Carbon::parse($item->expense_date)->format('d-m-Y') }}</span></td>
                                        <td style="align-items: center">
                                            <a href="{{ url('edit/expense') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any Brands....Please Create a Brand!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel" style="margin-top: 5px">
                    <div class="panel-heading" style="border: 1px solid #25A79F">
                        <div class="panel-control">
                            <a href="{{ route('Exadd') }}" class="btn"
                                style="background-color: #25A79F; color: #fff; margin-right: 10px">All Expense</a>
                            <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                        </div>
                        <h4 class="panel-title">Edit Expense</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <form class="panel-body form-horizontal"
                        style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                        action="{{ route('Exeditformpost') }}" method="POST">
                        @csrf
                        <!--Text Input-->
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                for="demo-text-input">Expense Details:</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="expense_details" id="" cols="10" rows="2">
                                    {{ $Expense_info->expense_details }}
                                </textarea>
                                @error('expense_details')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 10px">
                            <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                for="demo-text-input">Amount:</label>
                            <div class="col-md-8">
                                <input type="text" id="demo-text-input" name="expense_amount" class="form-control"
                                    placeholder="Enter Your Expense Amount" value="{{ $Expense_info->expense_amount }}">
                                @error('expense_amount')
                                    <small class="help-block text-danger"
                                        style="margin-bottom: 0px">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" id="demo-text-input" name="expense_date" class="form-control"
                                value="{{ Carbon\Carbon::now() }}">
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" id="demo-text-input" name="expense_month" class="form-control"
                                value="{{ Carbon\Carbon::now() }}">
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" id="demo-text-input" name="id" class="form-control"
                                value="{{ $Expense_info->id }}">
                        </div>
                        <div class="col-12 text-center" style="padding: 5px">
                            <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit
                                Expense</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
