@extends('layouts.user_api.business_app')

@section('content')
    @php
        $Total = App\Models\Order::orderBy('user_id')
            ->where('user_id', Auth::id())
            ->count();
        $Customar = App\Models\Customar::orderBy('user_id')
            ->where('user_id', Auth::id())
            ->count();
        $Orders = App\Models\Order::orderBy('user_id')
            ->where('user_id', Auth::id())
            ->sum('payment');
        $Employee = App\Models\Employee::orderBy('user_id')
            ->where('user_id', Auth::id())
            ->count();
    @endphp

    @if(!Session::get('business_id'))

        <?php
        // index.php
        header("Location: http://127.0.0.1:8000/home");
        exit();
        ?>

    @endif

    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-building"></i> Business Dashboard </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Home</a> </li>
              <li class="active">Business Dashboard </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-9">
              <div class="widgetbox widgetbox-primary">
                <div class="row">
                  <div class="col-md-3">
                    <div class="text-center" style=" padding-top: 20px;" >
                      <img src="{{ asset('user_assets/img/shop.png') }}" style=" height: 100px; width:100px; border-radius: 7px!important;">
                    </div>
                  </div>
                  <div class="col-md-9 pull-left">
                    <h2>{{ App\Models\Business::find(Session::get('business_id'))->business_name }}</h2>
                    <table width=100%>
                      <tbody>
                        <tr>
                          <td class="text-left text-bold">Shopkeeper Name:</td>
                          <td class="text-left text-bold">{{ Auth::user()->name }}</td>
                        </tr>
                        <tr>
                          <td class="text-left text-bold">Email:</td>
                          <td class="text-left text-bold">{{ Auth::user()->email }}</td>
                        </tr>
                        <tr>
                          <td class="text-left text-bold">Address:</td>
                          <td class="text-left text-bold">{{ App\Models\Business::find(Session::get('business_id'))->business_location }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                    <div class="widgetbox widgetbox-primary widgetbox-item-icon">
                        <div class="widgetbox-item-left"> <span class="fa fa-shopping-cart"></span></div>
                        <div class="widgetbox-data">
                            <div class="widgetbox-int">
                              <span style=" font-size: 17px;">
                              &#x09F3; {{number_format(DB::table('orders')->where('user_id' , Auth::id())
                                ->where('business' , Session::get('business_id'))
                                ->whereRaw('Date(order_date) = CURDATE()')
                                ->sum('total'), 2)}}
                              </span>
                             </div>
                            <div class="widgetbox-title"> Todays Sale </div>
                            <div class="widgetbox-subtitle"> Todays Report </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                   <div class="panel">
                       <div class="panel-body text-center">
                           <ul class="list-inline nm">
                               <li class="left-easypiechart-data"> <span class="sell-percent">
                                <strong> {{number_format(DB::table('orders')->where('user_id' , Auth::id())
                               ->where('business' , Session::get('business_id'))
                               ->whereRaw('Date(order_date) = CURDATE()')
                               ->sum('payment'), 2)}}</strong>
                             </span> <span>Received On Sale</span> </li>
                               <li class="right-easypiechart-data"> <span class="sell-percent">
                                 <strong>{{number_format(App\Models\Due::where('user_id' , Auth::id())
                                 ->where('business' , Session::get('business_id'))
                                 ->whereNotNull('customar_id')
                                 ->whereRaw('Date(collection_date) = CURDATE()')
                                 ->sum('payment'), 2)}}</strong>
                               </span> <span>Due Collection</span> </li>
                           </ul>
                       </div>
                   </div>
               </div>

              <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                  <div class="widgetbox widgetbox-success widgetbox-item-icon">
                      <div class="widgetbox-item-right"> <span class="fa fa-money"></span> </div>
                      <div class="widgetbox-data-left">
                          <div class="widgetbox-int">
                            <span style=" font-size: 17px;">
                            @php
                            $r_p = DB::table('orders')->where('user_id' , Auth::id())
                            ->where('business' , Session::get('business_id'))
                            ->whereRaw('Date(order_date) = CURDATE()')
                            ->sum('payment') + App\Models\Due::where('user_id' , Auth::id())
                            ->where('business' , Session::get('business_id'))
                            ->whereNotNull('customar_id')
                            ->whereRaw('Date(collection_date) = CURDATE()')
                            ->sum('payment');
                            @endphp
                            &#x09F3; {{ number_format($r_p, 2) }} </span></div>
                          <div class="widgetbox-title">Total Received </div>
                          <div class="widgetbox-subtitle"> Todays Report </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-warning widgetbox-item-icon">
                    <div class="widgetbox-item-left"> <span class="fa fa-cart-plus"></span> </div>
                    <div class="widgetbox-data">
                        <div class="widgetbox-int">
                          <span style=" font-size: 17px;">
                          &#x09F3; {{number_format(DB::table('product_purchases')->where('user_id' , Auth::id())
                        ->where('business' , Session::get('business_id'))
                        ->whereRaw('Date(product_buying_date) = CURDATE()')
                        ->sum('total_amount'), 2)}} </span></div>
                        <div class="widgetbox-title"> Purchase </div>
                        <div class="widgetbox-subtitle"> Todays Report </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="panel">
                   <div class="panel-body text-center">
                       <ul class="list-inline nm">
                           <li class="left-easypiechart-data"> <span class="sell-percent">
                            <strong> {{number_format(DB::table('product_purchases')->where('user_id' , Auth::id())
                            ->where('business' , Session::get('business_id'))
                            ->whereRaw('Date(product_buying_date) = CURDATE()')
                            ->sum('payment'), 2)}}</strong>
                         </span> <span> On Purchase</span> </li>
                           <li class="right-easypiechart-data"> <span class="sell-percent">
                             <strong>{{number_format(App\Models\Due::where('user_id' , Auth::id())
                             ->where('business' , Session::get('business_id'))
                             ->whereNotNull('supplier_id')
                             ->whereRaw('Date(collection_date) = CURDATE()')
                             ->sum('payment'), 2)}}</strong>
                           </span> <span>Due Payment</span> </li>
                       </ul>
                   </div>
               </div>
           </div>
            <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                <div class="widgetbox widgetbox-danger widgetbox-item-icon">
                    <div class="widgetbox-item-right"> <span class="fa fa-money"></span> </div>
                    <div class="widgetbox-data-left">
                        <div class="widgetbox-int">
                          <span style=" font-size: 17px;">
                          @php
                        $t_p = DB::table('product_purchases')->where('user_id' , Auth::id())
                        ->where('business' , Session::get('business_id'))
                        ->whereRaw('Date(product_buying_date) = CURDATE()')
                        ->sum('payment') + App\Models\Due::where('user_id' , Auth::id())
                        ->where('business' , Session::get('business_id'))
                        ->whereNotNull('supplier_id')
                        ->whereRaw('Date(collection_date) = CURDATE()')
                        ->sum('payment');
                        @endphp
                        &#x09F3; {{ number_format($t_p, 2) }} </span></div>
                        <div class="widgetbox-title"> Total Payment </div>
                        <div class="widgetbox-subtitle"> Todays Report </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body text-center" style=" background-color: #FCFDED;">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="text-info text-bold"><h3> &#x09F3;
                                {{number_format(DB::table('products')
                                ->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))
                                ->sum(DB::raw('products.product_quantity * products.whole_selling_price')), 2)}} </h3></div>
                              <div class="h4" style="color:#C92E70;"> Stock Amount In Wholesell Price </div>
                            </div>
                            <div class="col-md-6">
                              <div class="text-info text-bold"><h3> &#x09F3;
                                {{number_format(DB::table('products')
                                ->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))
                                ->sum(DB::raw('products.product_quantity * products.retail_selling_price')), 2)}}
                              </h3></div>
                              <div class="h4" style="color:#C92E70;"> Stock Amount In Retail Price </div>
                            </div>
                          </div><hr>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="text-primary text-bold"><h3> &#x09F3; {{number_format(DB::table('product_purchases')->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))->sum('total_amount'), 2)}} </h3></div>
                              <div class="h4" style="color: #7266BA;"> Purchase Amount In Total </div>
                            </div>
                            <div class="col-md-6">
                              <div class="text-primary text-bold"><h3> &#x09F3; {{number_format(DB::table('orders')->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))->sum('total'), 2)}} </h3></div>
                              <div class="h4" style="color: #7266BA;"> Sale Amount In Total </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer" style=" background-color: #FCFDED;">
                            <ul class="nav nav-section nav-justified">
                                <li>
                                    <div class="section pad-no">
                                        <div class="h4 text-pink text-bold"> {{DB::table('customars')->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))->count()}} </div>
                                        <p> <span class="text-pink fa fa-users"></span> Customar</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 text-pink text-bold"> {{DB::table('suppliers')->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))->count()}} </div>
                                        <p> <span class="text-pink fa fa-truck"></span> Suppliers</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 text-pink text-bold"> {{DB::table('orders')->where('user_id' , Auth::id())->where('business' , Session::get('business_id'))->count()}} </div>
                                        <p> <span class="text-pink fa fa-shopping-cart"></span> Sale Invoice </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-xs-12">
            <div class="widgetbox widgetbox-purple">
              <div class="widgetbox-title"> Customer Support </div>
              <div class="widgetbox-subtitle text-center"> <img src="{{ asset('user_assets/img/cs.png') }}" style="height: 75px; width: 75px;" > </div>
              <div class="widgetbox-title"> <i class="fa fa-whatsapp"></i> +88 01306-535342 </div>
            </div>
            <!-- <div class="widgetbox" style=" background-color: #044BA5; color: #FFFFFF;">
              <div class="widgetbox-title"> Payment Status </div>
              <div class="widgetbox-subtitle text-center"> <img src="{{ asset('user_assets/img/payment.png') }}" style="height: 75px; width: 75px;" > </div>
              <div class="widgetbox-title"></i> {{ \Carbon\Carbon::now()->translatedFormat('F') }}-{{ now()->year }} - DUE</div>
              <div class="widgetbox-subtitle"> <button type="button" class="btn btn-xs btn-success" name="button">Pay Now</button> </div>
            </div> -->
            <div class="widgetbox widgetbox-default widgetbox-item-icon">
                <div class="widgetbox-item-right widgetbox-icon-warning"> <span class="fa fa-envelope"></span>
                </div>
                @php
              $response = Http::get("https://smpp.ajuratech.com/portal/sms/smsConfiguration/smsClientBalance.jsp?client=bhaibhaiseeds");
              $response_decode = json_decode($response->body());
              $balance = isset($response_decode->Balance) ? $response_decode->Balance : 0;
              @endphp
                <div class="widgetbox-data-left">
                    <div class="widgetbox-int"> {{ $balance }}
                    </div>
                    <div class="widgetbox-title"> SMS Balance</div>
                    <div class="widgetbox-subtitle">  </div>
                </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-12 col-md-6 col-xs-12">
            <div class="panel widget" style=" font-size: 20px;">
                <div class="widget-body text-center">
                    <h4 class="mar-no">Customar Balance</h4>
                </div><hr>
                <ul class="menu-items">
                    <li>
                        <a href="#" class="clearfix">
                          Due
                        <span class="text-pink text-bold pull-right">&#x09F3; {{ number_format(DB::table('customars')->where('business' , Session::get('business_id'))->sum('due'), 2) }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="clearfix">
                          Advance
                        <span class="text-primary text-bold pull-right">&#x09F3; {{number_format(DB::table('customars')->where('business' , Session::get('business_id'))->sum('advance'), 2)}}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-sm-12 col-md-6 col-xs-12">
          <div class="panel widget" style=" font-size: 20px;">
              <div class="widget-body text-center">
                  <h4 class="mar-no">Supplier Balance</h4>
              </div><hr>
              <ul class="menu-items">
                  <li>
                      <a href="#" class="clearfix">
                        Due
                      <span class="text-pink text-bold pull-right">&#x09F3; {{number_format(DB::table('suppliers')->where('business' , Session::get('business_id'))->sum('due'), 2)}}</span>
                      </a>
                  </li>
                  <li>
                      <a href="#" class="clearfix">
                        Advance
                      <span class="text-primary text-bold pull-right">&#x09F3; {{number_format(DB::table('suppliers')->where('business' , Session::get('business_id'))->sum('advance'), 2)}}</span>
                      </a>
                  </li>
              </ul>
          </div>
      </div>
          </div>
        </div>
    </div>
@endsection
