@extends('layouts.user_api.app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <header class="pageheader">
            <h3><a href="{{ route('user') }}"><i class="fa fa-home"></i></a> Edit Business </h3>
            <div class="breadcrumb-wrapper">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li> <a href="{{ route('user') }}"> Home </a> </li>
                    <li class=""> Edit Business </li>
                    <li class="active"> Main Business </li>
                </ol>
            </div>
        </header>
        @if (session('add_status'))
            <a href="" class="btn"
                style="margin: 10px 00px 0px 20px; width:1338px; text-align: left; font-weight:300; background-color: #25A79F; color: #fff">
                <strong>Congratulations!</strong> Now You'r {{ session('add_status') }} Business Ready To Sell...</a>
        @endif


        <div id="page-content">
          <div class="panel">
              <div class="panel-heading">
                  <h4 class="panel-title">Edit Business Name: 'Company Name'</h4>
              </div>
              <!-- BASIC FORM ELEMENTS -->
              <!--===================================================-->
              <form class="panel-body form-horizontal" action="{{ url('/edit/business/formpost') }}" method="POST">
                  @csrf
                  <!--Text Input-->
                  <div class="form-group">
                      <label class="col-md-4 control-label text-danger text-left" for="demo-text-input">Step 1: Business Name [ Required ]</label>
                      <div class="col-md-8">
                          <input type="text" id="demo-text-input" name="business_name" class="form-control"
                              placeholder="Enter Your Business Name" value="{{ $business_info->business_name }}">
                          @error('business_name')
                              <small class="help-block text-danger" >Please Enter Your Business
                                  Name.</small>
                          @enderror
                      </div>
                  </div>
                  <input type="hidden" name="id" class="form-control"
                              value="{{ $business_info->id }}">
                  <!--Email Input-->
                  <div class="form-group text-left" >
                      <label class="col-md-4 control-label text-left"  for="demo-email-input">
                          <span style="color: #25A79F">Step 2:</span> Business Email </label>
                      <div class="col-md-8" >
                          <input type="email" id="demo-email-input" name="business_email" class="form-control"
                              placeholder="Enter Your Business Email" value="{{ $business_info->business_email }}">
                      </div>
                  </div>
                  <div class="form-group" >
                      <label class="col-md-4 control-label text-left"
                          for="demo-textarea-input"><span style="color: #25A79F">Step 3:</span> Phone: </label>
                      <div class="col-md-3">
                          <input type="text" class="form-control" name="city" value="{{ $business_info->city }}" id="validationCustom03" required>
                      </div>
                      <label class="col-md-1 control-label text-left"
                          for="demo-textarea-input"><span style="color: #25A79F"></span> Zip: </label>
                      <div class="col-md-4">
                          <input type="text" class="form-control" name="zip" value="{{ $business_info->zip }}" id="validationCustom03" required>
                      </div>
                  </div>
                  <!--Textarea-->
                  <div class="form-group text-left">
                      <label class="col-md-4 control-label text-left" for="demo-email-input">
                          <span style="color: #25A79F">Step 4:</span> Business Address: </label>
                      <div class="col-md-8" >
                          <input type="text" name="business_location" id="" class="form-control" value="{{ $business_info->business_location }}">
                      </div>
                  </div>
                  <!--State-->
                  <div class="form-group">
                      <label class="col-md-4 control-label text-left"
                          for="demo-textarea-input"><span style="color: #25A79F">Step 6:</span> State </label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="state" value="{{ $business_info->state }}" id="validationCustom03" required>
                      </div>
                  </div>

                  <div class="col-12">
                      <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                          <label class="form-check-label" for="invalidCheck">
                              Agree to terms and conditions
                          </label>
                      </div>
                  </div>
                  <div class="col-12">
                      <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit Business</button>
                  </div>
              </form>

              <!--===================================================-->
              <!-- END BASIC FORM ELEMENTS -->
          </div>
        </div>
    </div>
@endsection
