@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-edit"></i>Edit Document</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Edit Document</a> </li>
              <li class="active">Edit Document </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Document List</h4>
                      </div>

                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>SL.No</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Document as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }} </td>
                                            <td>{{ $item->document_title }}</td>
                                            <td>{{ $item->document_des }}</td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/document') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Brands....Please Create a Brand!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Update Document</h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <div class="panel-body">
                        <form class="form-horizontal"
                            style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                            action="{{ url('/edit/document/formpost/' . $document_info->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                    for="demo-text-input">Doc Title:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="document_title" class="form-control"
                                        placeholder="Enter Your Document Name" value="{{ $document_info->document_title }}">
                                    @error('document_title')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Description:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="document_des" id="" cols="10" rows="2">
                                        {{ $document_info->document_des }}
                                    </textarea>
                                    @error('document_des')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-textarea-input">Doc Photo </label>
                                <div class="col-md-8">
                                    <input type="file" class="form-control upload" name="image"
                                        id="validationCustom03" placeholder="Choose Photo" accept="image/*"
                                        onchange="readURL(this);">
                                    <img id="image"
                                        src="{{ asset('uploads/document_photos') }}/{{ $document_info->image }}"
                                        alt="" style="width: 120px; height:100px">
                                    @error('image')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 text-center" style="padding: 5px">
                                <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Update
                                    Document</button>
                            </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // Image Show
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image')
                        .attr('src', e.target.result)
                        .width(120)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0])
            }
        }

        // Search
    </script>
@endsection
