@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-file"></i>Store You Doc</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#">Personal Document</a> </li>
              <li class="active">Document </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Add Document</h4>
                    </div>
                    <!-- BASIC FORM ELEMENTS -->
                    <!--===================================================-->
                    <div class="panel-body">
                      <form class="form-horizontal" action="{{ route('personal.data.store') }}"
                          method="POST" enctype="multipart/form-data">
                          @csrf
                          <!--Text Input-->
                          <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                  for="demo-text-input">Doc Title:</label>
                              <div class="col-md-8">
                                  <input type="text" id="demo-text-input" name="document_title" class="form-control"
                                      placeholder="Enter Your Document Name" value="{{ old('document_name') }}">
                                  @error('document_title')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Description:</label>
                              <div class="col-md-8">
                                  <textarea class="form-control" name="document_des" id="" cols="10" rows="2">
                                      {{ old('expense_details') }}
                                  </textarea>
                                  @error('document_des')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-textarea-input">Doc Photo </label>
                              <div class="col-md-8">
                                  <input type="file" class="form-control upload" name="image" id="validationCustom03" multiple placeholder="Choose Photo" accept="image/*" onchange="readURL(this);" required>
                                  <img id="image" src="#" alt="">
                                  @error('image')
                                      <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>
                          <div class="col-12 text-center" style="padding: 5px">
                              <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                  Document</button>
                          </div>
                      </form>
                    </div>
                </div>
            </div>
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Document List</h4>
                      </div>
                      <div class="panel-body table-responsive">
                        <table id="demo-dt-basic" class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>SL.No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Photo</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($Total_Document as $item)
                                @if(Session::get('business_id') == $item->business)
                                    <tr>
                                        <td>{{ $loop->iteration }} </td>
                                        <td>{{ $item->document_title }}</td>
                                        <td>{{ $item->document_des }}</td>
                                        <td>
                                            {{-- @php
                                            $image = DB::table('personals')->where('id', $item->id)->get();
                                            $images = explode('|', $image->image);
                                            @endphp
                                            @foreach ($images as $img)
                                            <img src="{{ asset('multiple_imager') }}/{{ $img}}" styLe-"height:60px;" aLt="">
                                            @break
                                            @endforeach
                                            {{ $image }} --}}



                                            <img id="image"
                                                src="{{ asset('uploads/document_photos') }}/{{ $item->image }}"
                                                alt="" style="width: 50px; height:50px">
                                        </td>
                                        <td>
                                            <div class="label label-table label-success"><a
                                                    href="{{ url('/view/document') }}/{{ $item->id }}" style="color: #fff">View</a>
                                            </div>
                                        </td>
                                        <td style="align-items: center">
                                            <a href="{{ url('edit/document') }}/{{ $item->id }}"><i
                                                    class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                </i></a>
                                            <a href="{{ url('delete/document') }}/{{ $item->id }}"><i
                                                    class="fa fa-trash text-danger"> Del </i></a>
                                        </td>
                                    </tr>
                                    @endif
                                    @empty
                                    <tr>
                                        <td colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                            You Not Create Any Document....Please Create a Document!</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>

          </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
