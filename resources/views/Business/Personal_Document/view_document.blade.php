@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-eye"></i>You Doc</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#">Personal Document</a> </li>
              <li class="active">Document </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Your Document's</h4>
                </div>
                <div class="panel-body">
                  <div class="timeline-entry">
                      <div class="timeline-stat">
                          <div class="timeline-icon bg-primary"><i class="fa fa-image fa-lg"></i> </div>
                      </div>
                      <div class="timeline-label">
                          <img class="img-xs img-circle" src="{{ asset('user_assets/img/av1.png') }}" alt="Profile picture">
                          <a href="#" class="btn-link text-semibold"> {{ Auth::user()->name }} </a> Uploaded 1 images
                          <p style="margin-left: 35px">{{ $document_info->document_des }}</p>
                          <div>
                              <a href="{{ asset('uploads/document_photos') }}/{{ $document_info->image }}" download>
                                  <img src="{{ asset('uploads/document_photos') }}/{{ $document_info->image }}"
                                      style="width: 150px; height: 120px" alt="Image"></a>
                              {{-- <img src="img/photos/profile-pic-1.png"
                                  class="profile-img" alt="">
                                  <img src="img/photos/profile-pic-1.png"
                                  class="profile-img" alt="">
                                  <img src="img/photos/profile-pic-1.png"
                                  class="profile-img" alt="">  --}}
                          </div>
                      </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // Search
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection
