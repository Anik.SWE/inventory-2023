@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
        <div class="row"style="padding: 20px 20px" id="page-content">
            <div class="col-lg-12 col-md-3 col-sm-4 col-xs-12">
                <div class="panel">
                    <div class="panel-body np" style="padding: 10px 10px">
                        <div class="text-left col-md-10">
                            <!-- panel body -->
                            <h4 class="text-lg text-overflow mar-top">{{ $single_employee_view->employee_name }}</h4>
                            <p class="text-sm">Digital Marketing Director</p>
                            <!--/ panel body -->
                            <div class="pad-ver">
                                <a title="" href="javascript:void(0)"
                                    class="btn btn-primary btn-icon fa fa-facebook icon-sm add-tooltip"
                                    data-original-title="Facebook" data-container="body"></a>
                                <a title="" href="javascript:void(0)"
                                    class="btn btn-info btn-icon fa fa-twitter icon-sm add-tooltip"
                                    data-original-title="Twitter" data-container="body"></a>
                                <a title="" href="javascript:void(0)"
                                    class="btn btn-danger btn-icon fa fa-google-plus icon-sm add-tooltip"
                                    data-original-title="Google+" data-container="body"></a>
                                <a title="" href="javascript:void(0)"
                                    class="btn btn-warning btn-icon fa fa-envelope icon-sm add-tooltip"
                                    data-original-title="Email" data-container="body"></a>
                            </div>
                        </div>
                        <div class="employee_photo col-md-2" style="padding-top: 10px">
                            <img src="{{ asset('uploads/employee_photos') }}/{{ $single_employee_view->employee_photo }}"
                                alt="Cover" style="width: 160px; height: 120px" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-user"> </i> Employee Information</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td> Name </td>
                                    <td>{{ $single_employee_view->employee_name }}</td>
                                    <td> Position </td>
                                    <td>Digital Marketing Director</td>
                                </tr>
                                <tr>
                                    <td> Employee Email </td>
                                    <td>{{ $single_employee_view->employee_email }}</td>
                                    <td> Phone Number </td>
                                    <td>{{ $single_employee_view->phone_number }}</td>
                                </tr>
                                <tr>
                                    <td> Salary </td>
                                    <td><span class="label label-sm label-success">$
                                            {{ $single_employee_view->salary }}</span></td>
                                    <td> Status </td>
                                    <td><span class="label label-sm label-danger">Unpaid</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4"> </td>
                                </tr>
                                <tr>
                                    <td> Location </td>
                                    <td>{{ $single_employee_view->location }}</td>
                                    <td> City </td>
                                    <td>{{ $single_employee_view->city }}</td>
                                </tr>
                                <tr>
                                    <td> Address </td>
                                    <td>{{ $single_employee_view->address }}</td>
                                    <td> Zip Code </td>
                                    <td>{{ $single_employee_view->zip }}</td>
                                </tr>
                                <tr>
                                    <td> Experience </td>
                                    <td>{{ $single_employee_view->experience }}</td>
                                    <td> Vacation </td>
                                    <td>{{ $single_employee_view->vacation }} Days</td>
                                </tr>
                                <tr>
                                    <td> NID No </td>
                                    <td>{{ $single_employee_view->nid }}</td>
                                    <td> Print </td>
                                    <td>Exported</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center no-print">
            <a class="btn btn-primary btn-sm" onClick="jQuery('#page-content').print()">
                <i class="fa fa-print"></i> Print
            </a>
        </div>
    </div>
@endsection
