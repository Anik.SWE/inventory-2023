@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
        <div id="page-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel text-center">
                        <!--Profile Widget-->
                        <!--===================================================-->
                        <div class="panel-body">
                            {{-- <i class="fa fa-facebook fa-5x"></i> --}}
                            <img style="width: 100%; height: 200px" src="{{ asset('user_assets/img/thumbs/img3.jpg') }}" alt="">
                        </div>
                        <div class="panel-footer">
                            <ul class="nav nav-section nav-justified">
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> {{ $Count }} </div>
                                        <p class="mar-no">Total Employee</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> 1853 </div>
                                        <p class="mar-no">Paid Salary</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="section">
                                        <div class="h4 mar-ver-5"> 3451 </div>
                                        <p class="mar-no">UnPaid Salary</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!--===================================================-->
                    </div>
                </div>
            </div>

            <div class="panel-body" style="padding-bottom: 0;">
                <div class="pad-btm form-inline">
                    <div class="row">
                        <div class="col-sm-6 table-toolbar-left" style="padding-left: 3px">
                            <div class="btn-group">
                                <button class="btn btn-default"><a href="{{ route('add') }}"><i
                                            class="fa fa-plus"></i></a></button>
                                <button class="btn btn-default"><i class="fa fa-print"></i></button>
                                <button class="btn btn-default"><i class="fa fa-exclamation-circle"></i></button>
                                <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                        <div class="col-sm-6 table-toolbar-right" style="padding-right: 3px">
                            <div class="form-group">
                                <input id="demo-input-search2" type="text" placeholder="Search"
                                    class="form-control" autocomplete="off">
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-default"><i class="fa fa fa-cloud-download"></i></button>
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                        <i class="fa fa-cog"></i>
                                        <span class="caret"></span>
                                    </button>
                                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive" style="margin-top: 10px; border: 1px solid #25A79F">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SL.No</th>
                            <th>Employee Name</th>
                            <th>Country</th>
                            <th>Vacation</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($Total_Employee as $item)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $item->employee_name }}</td>
                                <td>{{ $item->location }}</td>
                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $item->vacation }}</span></td>
                                <td>{{ $item->salary }}</td>
                                <td>
                                    <div class="label label-table label-success"><a
                                            href="{{ url('/single/employee/view') }}/{{ $item->id }}" style="color: #fff">View</a>
                                    </div>
                                </td>
                                <td style="align-items: center">
                                    <a href="{{ url('edit/employee') }}/{{ $item->id }}"><i class="fa fa-edit text-primary"
                                            style="margin-right: 15px"> Edit </i></a>
                                    <a href="{{ url('delete/employee') }}/{{ $item->id }}"><i
                                            class="fa fa-trash text-danger">
                                            Del </i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong> You Not
                                    Create Any Business....Please Create a Business!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
