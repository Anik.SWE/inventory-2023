@extends('layouts.user_api.business_app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">

        <header class="pageheader">
            <h3><a href="{{ route('user') }}"><i class="fa fa-home"></i></a> .Add Employee </h3>
            <div class="breadcrumb-wrapper">
                <span class="label">You are here:</span>
                <ol class="breadcrumb">
                    <li> <a href="{{ route('dashboard') }}"> Home </a> </li>
                    <li class=""> Employees </li>
                    <li class="active"> Add Employee </li>
                </ol>
            </div>
        </header>
        @if (session('add_status'))
            <a href="" class="btn"
                style="margin: 10px 00px 0px 20px; width:1338px; text-align: left; font-weight:300; background-color: #25A79F; color: #fff">
                <strong>Congratulations!</strong> Now You'r {{ session('add_status') }} Business Ready To Sell...</a>
        @endif


        <div class="col-lg-12" style="padding: 15px 20px">
            <div class="panel">
                <div class="panel-heading" style="border: 1px solid #25A79F">
                    <div class="panel-control">
                        <a href="{{ route('AllE') }}" class="btn"
                            style="background-color: #25A79F; color: #fff; margin-right: 10px">All Employee</a>
                        <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                    </div>
                    <h4 class="panel-title">Add New Employee</h4>
                </div>
                <!-- BASIC FORM ELEMENTS -->
                <!--===================================================-->
                <form class="panel-body form-horizontal" style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                    action="/employee/data/store" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!--Text Input-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-2 control-label text-danger text-left" style="padding-left: 20px"
                            for="demo-text-input">Step 1: Employee Name</label>
                        <div class="col-md-10">
                            <input type="text" id="demo-text-input" name="employee_name" class="form-control"
                                placeholder="Enter Your Employee Name">
                            @error('employee_name')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--Email Input-->
                    <div class="form-group text-left" style="margin-bottom: 0px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                            <span style="color: #25A79F">Step 2:</span> Employee Email </label>
                        <div class="col-md-4" style="margin-bottom: 10px">
                            <input type="email" id="demo-email-input" name="employee_email" class="form-control"
                                placeholder="Enter Your Employee Email">
                            @error('employee_email')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>

                        <label class="col-md-2 control-label text-left" style="padding-left: 20px" for="demo-email-input">
                            <span style="color: #25A79F">Step 3:</span> Phone Number </label>
                        <div class="col-md-4" style="margin-bottom: 10px">
                            <input type="text" id="demo-email-input" name="phone_number" class="form-control"
                                placeholder="Enter Phone Number">
                            @error('phone_number')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--Textarea-->
                    <div class="form-group" style="margin-bottom: 0px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 4:</span> Location </label>
                        <div class="col-md-4" style="margin-bottom: 10px">
                            <select class="form-control" name="location" id="">
                                <option value="">-- Select Option --</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="India">India</option>
                                <option value="Pakistan">Pakistan</option>
                            </select>
                            @error('location')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>

                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 5:</span> City </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="city" id="validationCustom03" placeholder="Enter Your City" required>
                            @error('city')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--City-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 6:</span> Address </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="address" placeholder="Enter Your Address" id="validationCustom03" required>
                            @error('address')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 7:</span> Zip Code</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="zip" id="validationCustom03" placeholder="Enter Zip Code" required>
                            @error('zip')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <!--State-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 8:</span> Experience </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="experience" placeholder="Enter Your Experience" id="validationCustom03" required>
                            @error('experience')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>

                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 9:</span> NID No </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="nid" id="validationCustom03" placeholder="Type NID Number" required>
                            @error('nid')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <!--State-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 10:</span> Salary </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="salary" id="validationCustom03" placeholder="Select Salary" required>
                            @error('salary')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>

                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 11:</span> Vacation </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="vacation" id="validationCustom03" placeholder="Enter Vacation" required>
                            @error('vacation')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <!--State-->
                    <div class="form-group" style="margin-bottom: 10px">
                        <label class="col-md-2 control-label text-left" style="padding-left: 20px"
                            for="demo-textarea-input"><span style="color: #25A79F">Step 12:</span> Photo </label>
                        <div class="col-md-10">
                            <input type="file" class="form-control upload" name="employee_photo" id="validationCustom03" placeholder="Choose Photo" accept="image/*" onchange="readURL(this);" required>
                            <img id="image" src="#" alt="">
                            @error('employee_photo')
                                <small class="help-block text-danger" style="margin-bottom: 0px">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="col-12" style="padding: 5px">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                            <label class="form-check-label" for="invalidCheck">
                                Agree to terms and conditions
                            </label>
                        </div>
                    </div>
                    <div class="col-12" style="padding: 5px">
                        <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add Employee</button>
                    </div>
                </form>

                <!--===================================================-->
                <!-- END BASIC FORM ELEMENTS -->
            </div>
        </div>
        {{-- {{ $business_id }} --}}

    </div>

    <script type="text/javascript">
        function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $('#image')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(100);
                };
                reader.readAsDataURL(input.files[0])
            }
        }
    </script>
@endsection
