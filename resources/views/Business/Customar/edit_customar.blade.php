@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user"></i> Customar Edit</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Customar</a> </li>
              <li class="active"> Customar Edit </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
            <div class="col-md-8">
              <div class="panel">
                <div class="panel-heading">
                  <h4 class="panel-title">Customer List</h4>
                </div>
                  <div class="panel-body">
                      <table id="demo-dt-basic" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Customar</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Balances</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Customar as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->customar_name }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->phone_number }}</td>
                                    <td> @if($item->due) {{ $item->due }} &#x09F3; Due @endif  @if($item->advance) {{ $item->advance }} &#x09F3; Advance @endif</td>
                                    <td>
                                      <div class="btn-group btn-group-sm dropup">
                                                                <button class="btn btn-sm btn-success dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                                                  Action <i class="dropdown-caret fa fa-caret-up"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                  <li><a href="{{ url('/single/customar/view') }}/{{ $item->id }}"><i class="fa fa-user"></i> Profile </a></li>
                                                                  <li><a href="{{ url('edit/customar') }}/{{ $item->id }}"><i class="fa fa-edit"></i> Edit </a></li>
                                                                  <li><a href="{{ url('delete/customar') }}/{{ $item->id }}"><i class="fa fa-trash"></i> Delete</a></li>
                                                                </ul>
                                                              </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                        You Not Create Any Customar....Please Create a Customar!</td>
                                </tr>
                            @endforelse
                        </tbody>
                      </table>
                  </div>
              </div>
            </div>
              <div class="col-md-4">
                  <div class="panel" style="margin-top: 5px">
                      <div class="panel-heading" style="border: 1px solid #25A79F">
                          <div class="panel-control">
                              <a href="{{ route('AllP') }}" class="btn"
                                  style="background-color: #25A79F; color: #fff; margin-right: 10px">See Product</a>
                              <button class="btn" data-dismiss="panel"><i class="fa fa-times"></i></button>
                          </div>
                          <h4 class="panel-title">Add Customar</h4>
                      </div>
                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <form class="panel-body form-horizontal"
                          style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px"
                          action="{{ url('/edit/customar/formpost/'.$customar_info->id) }}" method="POST" enctype="multipart/form-data">
                          @csrf
                          <!--Text Input-->
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Customar Name:</label>
                              <div class="col-md-8">
                                  <input type="text" id="demo-text-input" name="customar_name" class="form-control"
                                      placeholder="Enter Customar Name" value="{{ $customar_info->customar_name }}">
                                  @error('customar_name')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Email:</label>
                              <div class="col-md-8">
                                  <input type="email" id="demo-text-input" name="customar_email" class="form-control"
                                      placeholder="Enter Customar Email" value="{{ $customar_info->customar_email }}">
                                  @error('customar_email')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 5px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Phone:</label>
                              <div class="col-md-8" style="margin-bottom: 5px">
                                  <input type="text" id="demo-text-input" name="phone_number" class="form-control"
                                      placeholder="Enter Customar Phone" value="{{ $customar_info->phone_number }}">
                                  @error('phone_number')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <!--<div class="form-group" style="margin-bottom: 5px">-->
                          <!--    <label class="col-md-4 control-label text-left" style="padding-left: 20px"-->
                          <!--        for="demo-text-input">Location:</label>-->
                          <!--    <div class="col-md-8" style="margin-bottom: 5px">-->
                          <!--        <select class="form-control" name="location" id="">-->
                          <!--          <option value="Bangladesh" {{$customar_info->location == "Bangladesh"  ? 'selected' : ''}}>Bangladesh</option>-->
                          <!--          <option value="Pakisten" {{$customar_info->location == "Pakisten"  ? 'selected' : ''}}>Pakista</option>-->
                          <!--        </select>-->
                          <!--        @error('location')-->
                          <!--            <small class="help-block text-danger"-->
                          <!--                style="margin-bottom: 0px">{{ $message }}</small>-->
                          <!--        @enderror-->
                          <!--    </div>-->
                          <!--</div>-->

                          <!--<div class="form-group" style="margin-bottom: 10px">-->
                          <!--    <label class="col-md-4 control-label text-left" style="padding-left: 20px"-->
                          <!--        for="demo-text-input">City:</label>-->
                          <!--    <div class="col-md-8">-->
                          <!--        <input type="text" id="quantity" name="city" class="form-control""-->
                          <!--            value="{{ $customar_info->city }}">-->
                          <!--        @error('city')-->
                          <!--            <small class="help-block text-danger"-->
                          <!--                style="margin-bottom: 0px">{{ $message }}</small>-->
                          <!--        @enderror-->
                          <!--    </div>-->
                          <!--</div>-->

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" for="demo-text-input">Address:</label>
                              <div class="col-md-8">
                                  <input type="Text" id="address" name="address" class="form-control"
                                      value="{{ $customar_info->address }}">
                                  @error('address')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <!--<div class="form-group" style="margin-bottom: 10px">-->
                          <!--    <label class="col-md-4 control-label text-left" style="padding-left: 20px"-->
                          <!--        for="demo-text-input">Zip:</label>-->
                          <!--    <div class="col-md-3">-->
                          <!--        <input type="text" id="quantity" name="zip" class="form-control"-->
                          <!--            value="{{ $customar_info->zip }}">-->
                          <!--        @error('zip')-->
                          <!--            <small class="help-block text-danger"-->
                          <!--                style="margin-bottom: 0px">{{ $message }}</small>-->
                          <!--        @enderror-->
                          <!--    </div>-->
                          <!--</div>-->

                          <!--<div class="form-group" style="margin-bottom: 10px">-->
                          <!--    <label class="col-md-4 control-label text-left" for="demo-text-input">Address:</label>-->
                          <!--    <div class="col-md-8">-->
                          <!--      <select class="form-control" name="bank_name" id="">-->
                          <!--          <option value="Bank Account" {{$customar_info->bank_name == "Bank Account"  ? 'selected' : ''}}>Bank Account</option>-->
                          <!--          <option value="Bkash" {{$customar_info->bank_name == "Bkash"  ? 'selected' : ''}}>Bkash</option>-->
                          <!--      </select>-->
                          <!--    </div>-->
                          <!--</div>-->

                          <!--<div class="form-group" style="margin-bottom: 10px">-->
                          <!--    <label class="col-md-4 control-label text-left" style="padding-left: 20px"-->
                          <!--        for="demo-text-input">Account:</label>-->
                          <!--    <div class="col-md-8">-->
                          <!--        <input type="text" id="demo-text-input" name="bank_account" class="form-control"-->
                          <!--            placeholder="Enter Account Number" value="{{ $customar_info->bank_account }}">-->
                          <!--        @error('product_selling_price')-->
                          <!--            <small class="help-block text-danger"-->
                          <!--                style="margin-bottom: 0px">{{ $message }}</small>-->
                          <!--        @enderror-->
                          <!--    </div>-->
                          <!--</div>-->

                          {{-- <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Photo:</label>
                              <div class="col-md-8">
                                  <input type="file" class="form-control upload" name="customar_photo"
                                      id="validationCustom03" placeholder="Choose Photo"
                                      value="{{ $customar_info->customar_photo }}" accept="image/*"
                                      onchange="readURL(this);">
                                  <img id="image"
                                      src="{{ asset('uploads/customar_photos') }}/{{ $customar_info->customar_photo }}"
                                      alt="" style="width: 120px; height:100px">
                                  @error('customar_photo')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div> --}}

                          <div class="col-12" style="padding: 5px; margin-left: 130px">
                              <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                  Customar</button>
                          </div>
                  </div>
                  </form>
              </div>
          </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price));
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }
    </script>
@endsection
