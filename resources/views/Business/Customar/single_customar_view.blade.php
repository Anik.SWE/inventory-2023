@extends('layouts.user_api.business_app')
@section('content')
    @php
    $balance = DB::table('banks')->where('user_id', Auth::id())->where('account_number',$customar_info->bank_account)->get();
    @endphp
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user"></i> Customar</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Customar</a> </li>
              <li class="active">Profile </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="row">
              <div class="col-lg-12 col-md-3 col-sm-4 col-xs-12">
                  <!--  <div class="panel">
                      <div class="panel-body np" style="padding: 10px 10px">
                        <div class="text-left col-md-10">

                              <h4 class="text-lg text-overflow mar-top"></h4>
                              <p class="text-sm">Email: {{ $customar_info->customar_email }}</p>

                              <div class="pad-ver">
                                  <a title="" href="javascript:void(0)"
                                      class="btn btn-primary btn-icon fa fa-facebook icon-sm add-tooltip"
                                      data-original-title="Facebook" data-container="body"></a>
                                  <a title="" href="javascript:void(0)"
                                      class="btn btn-info btn-icon fa fa-twitter icon-sm add-tooltip"
                                      data-original-title="Twitter" data-container="body"></a>
                                  <a title="" href="javascript:void(0)"
                                      class="btn btn-danger btn-icon fa fa-google-plus icon-sm add-tooltip"
                                      data-original-title="Google+" data-container="body"></a>
                                  <a title="" href="javascript:void(0)"
                                      class="btn btn-warning btn-icon fa fa-envelope icon-sm add-tooltip"
                                      data-original-title="Email" data-container="body"></a>
                              </div>
                          </div>
                          <div class="employee_photo col-md-2" style="padding-top: 10px">
                              <img src="{{ asset('uploads/customar_photos') }}/{{ $customar_info->customar_photo }}"
                                  alt="Cover" style="width: 160px; height: 120px" class="img-responsive">
                          </div>
                      </div>
                  </div>-->
                  <div class="panel">

                      <div class="panel-body">
                          <table class="table table-bordered">
                              <tbody>
                                  <tr>
                                      <td> Customer </td>
                                      <td>{{ $customar_info->customar_name }}</td>
                                      <td> Position </td>
                                      <td>Fixed Customar</td>
                                  </tr>
                                  <tr>
                                      <td> Employee Email </td>
                                      <td>{{ $customar_info->customar_email }}</td>
                                      <td> Phone Number </td>
                                      <td>{{ $customar_info->phone_number }}</td>
                                  </tr>
                                  <tr>
                                      <td>Total Due </td>
                                      <td><span class="label label-sm label-warning">&#x09F3;
                                          {{ $customar_info->due }}
                                      </span></td>
                                      <td>Total Advance </td>
                                      <td><span class="label label-sm label-success">&#x09F3;
                                          {{ $customar_info->advance }}
                                      </span></td>

                                  </tr>


                                  <tr>
                                      <td> Address </td>
                                      <td>{{ $customar_info->address }}</td>

                                  </tr>

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>
        </div>

        <div class="text-center no-print">
            <a class="btn btn-primary btn-sm" onClick="jQuery('#page-content').print()">
                <i class="fa fa-print"></i> Print
            </a>
        </div>
    </div>
@endsection
