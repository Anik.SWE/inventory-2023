@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container" style="margin: 15px 0">
        <div class="col-lg-12">

            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="panel">
                            <div class="panel-body np">
                                <img src="{{ asset('user_assets/img/av1.png') }}"
                                    style="margin: 0 auto; width: 300px; height: 250px" alt="Cover"
                                    class="img-responsive">
                                <div class="text-center">
                                    <!-- panel body -->
                                    <h4 class="text-lg text-overflow mar-top">
                                        Name: {{ Auth::user()->name }}
                                    </h4>
                                    <p class="text-sm">Digital Marketing Director</p>
                                    <!--/ panel body -->
                                    <div class="pad-ver">
                                        <b><a href="">Social Links: </a></b><br>
                                        <a title="" href="" target="blank"
                                            class="btn btn-primary btn-icon btn-sm fa fa-facebook icon-sm add-tooltip"
                                            data-original-title="Facebook" data-container="body"></a>
                                        <a title="" href="javascript:void(0)"
                                            class="btn btn-info btn-icon btn-sm fa fa-twitter icon-sm add-tooltip"
                                            data-original-title="Twitter" data-container="body"></a>
                                        <a title="" href="javascript:void(0)"
                                            class="btn btn-danger btn-icon btn-sm fa fa-google-plus icon-sm add-tooltip"
                                            data-original-title="Google+" data-container="body"></a>
                                        <a title="" href="javascript:void(0)"
                                            class="btn btn-warning btn-sm btn-icon fa fa-envelope icon-sm add-tooltip"
                                            data-original-title="Email" data-container="body"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user"> </i> User Information</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td> Position </td>
                                            <td>UI Designer</td>
                                        </tr>
                                        <tr>
                                            <td> Last Logged In </td>
                                            <td>56 min </td>
                                        </tr>
                                        <tr>
                                            <td> Status </td>
                                            <td><span class="label label-sm label-success">Administrator</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="panel">
                            <div class="panel-body pad-no">

                                <!--Default Tabs (Left Aligned)-->
                                <!--===================================================-->
                                <div class="tab-base">

                                    <!--Nav Tabs-->
                                    <ul class="nav nav-tabs">
                                        <li class="active"> <a data-toggle="tab" href="#demo-lft-tab-1"> Edit Profile </a>
                                        </li>
                                    </ul>

                                    <!--Tabs Content-->
                                    <div class="tab-content">
                                        <div id="demo-lft-tab-1" class="tab-pane fade active in">

                                            <!-- Timeline -->
                                            <!--===================================================-->
                                            <div class="timeline">

                                                <!-- Timeline header -->
                                                <div class="timeline-header" style="margin-bottom: 15px">
                                                    <div class="timeline-header-title bg-info">Now Update</div>
                                                </div>
                                                <div class="timeline-entry" style="margin: 0">
                                                    <div class="timeline-stat">
                                                        <div class="timeline-icon bg-success"><i
                                                                class="fa fa-check fa-lg"></i> </div>
                                                    </div>
                                                    <div class="timeline-label">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-control"><button
                                                                                class="btn btn-default"
                                                                                data-dismiss="panel"><i
                                                                                    class="fa fa-times"></i></button>
                                                                        </div>
                                                                        <h3 class="panel-title">Update Prodfile</h3>
                                                                    </div>
                                                                    <!--No Label Form-->
                                                                    <!--===================================================-->
                                                                    <form class="form-horizontal"
                                                                        action="{{ route('profile.edit.formpost') }}"
                                                                        method="POST">
                                                                        @csrf
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-12 mar-btm">
                                                                                    <input type="text"
                                                                                        class="form-control"
                                                                                        placeholder="Enter Your Name"
                                                                                        name="name">
                                                                                    @if (session('name_change_status'))
                                                                                        <small
                                                                                            class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ session('password_error') }}</small>
                                                                                    @endif
                                                                                    @error('name')
                                                                                        <small class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ $message }}</small>
                                                                                    @enderror
                                                                                    {{-- @error('name')
                                                                                        <small class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ $message }}</small>
                                                                                    @enderror --}}
                                                                                    @foreach ($errors->all(); as $error)
                                                                                    <small class="help-block text-danger"
                                                                                    style="margin-bottom: 0px">{{ $error }}</small>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="panel-footer text-right">
                                                                            <button class="btn btn-success"
                                                                                type="submit">Update</button>
                                                                        </div>
                                                                    </form>
                                                                    <!--===================================================-->
                                                                    <!--End No Label Form-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-control"><button
                                                                                class="btn btn-default"
                                                                                data-dismiss="panel"><i
                                                                                    class="fa fa-times"></i></button>
                                                                        </div>
                                                                        <h3 class="panel-title">Update Password</h3>
                                                                    </div>
                                                                    <!--No Label Form-->
                                                                    <!--===================================================-->
                                                                    <form class="form-horizontal"
                                                                        action="{{ route('password.edit.formpost') }}"
                                                                        method="POST">
                                                                        @csrf
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-4 mar-btm"
                                                                                    style="display: flex">
                                                                                    <input type="password"
                                                                                        class="form-control"
                                                                                        placeholder="Enter Old Password"
                                                                                        name="old_password"
                                                                                        id="show1">
                                                                                    <a class="btn btn-default"
                                                                                        type="button"
                                                                                        onclick="show_Function()"><i
                                                                                            class="fa fa-eye"></i></a>
                                                                                    @if (session('password_error'))
                                                                                        <small
                                                                                            class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ session('password_error') }}</small>
                                                                                    @endif
                                                                                    @error('old_password')
                                                                                        <small class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ $message }}</small>
                                                                                    @enderror
                                                                                </div>
                                                                                <div class="col-md-4 mar-btm"
                                                                                    style="display: flex">
                                                                                    <input type="password"
                                                                                        class="form-control"
                                                                                        placeholder="Enter New Password"
                                                                                        name="password" id="show2">
                                                                                    <a class="btn btn-default"
                                                                                        type="button"
                                                                                        onclick="show__Function()"><i
                                                                                            class="fa fa-eye"></i></a>
                                                                                    @error('password')
                                                                                        <small class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ $message }}</small>
                                                                                    @enderror
                                                                                </div>
                                                                                <div class="col-md-4 mar-btm"
                                                                                    style="display: flex">
                                                                                    <input type="password"
                                                                                        class="form-control"
                                                                                        placeholder="Enter Re-Type Password"
                                                                                        name="password_confirmation"
                                                                                        id="show3">
                                                                                    <a class="btn btn-default"
                                                                                        type="button"
                                                                                        onclick="show___Function()"><i
                                                                                            class="fa fa-eye"></i></a>
                                                                                    @error('password_confirmation')
                                                                                        <small class="help-block text-danger"
                                                                                            style="margin-bottom: 0px">{{ $message }}</small>
                                                                                    @enderror

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="panel-footer text-right">
                                                                            <button class="btn btn-success"
                                                                                type="submit">Update</button>
                                                                        </div>
                                                                    </form>
                                                                    <!--===================================================-->
                                                                    <!--End No Label Form-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--===================================================-->
                                            <!-- End Timeline -->
                                        </div>
                                    </div>
                                </div>
                                <!--===================================================-->
                                <!--End Default Tabs (Left Aligned)-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--===================================================-->
            <!--End page content-->
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        function show_Function() {
            var x = document.getElementById("show1");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function show__Function() {
            var x = document.getElementById("show2");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function show___Function() {
            var x = document.getElementById("show3");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
@endsection
