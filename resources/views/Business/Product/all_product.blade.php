@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
      <div class="pageheader">
          <h3><i class="fa fa-shopping-cart"></i>All Product</h3>
          <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Product</a> </li>
              <li class="active">All Product's </li>
            </ol>
          </div>
        </div>
        <div id="page-content">
            <div class="panel">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-md-6 pull-left">
                    <h4 class="panel-title">All Product's</h4>
                  </div>
                  <div class="col-md-6 pull-right">
                      <div class="form-group panel-title">
                        <select class="form-control" name="customar_id" id="radio_box">
                            <option value="">-- Select Category --</option>
                            @foreach ($Category_info as $item)
                                <option value="{{ $item->id }}" id="cat{{ $item->id }}">
                                    {{ $item->category_name }}
                                </option>
                            @endforeach
                        </select>
                        @error('category_id')
                            <small class="help-block text-danger"
                                style="margin-bottom: 0px">{{ $message }}</small>
                        @enderror
                      </div>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-md-12">
                        <div class="row" id="myTable">
                            <div id="selectProduct">
                                @foreach ($Total_Product as $item)
                                   @if(Session::get('business_id') == $item->business)
                                    <div class="col-sm-2" style="margin-bottom: 10px; height: 150px; margin-top: 10px">
                                        <div class="shadow-sm card mb-3 product">
                                            <div class="card-body" style="height: 90px">
                                                {{-- @if ($item->product_quantity <= 5)
                                                this product is not available
                                            @endif --}}

                                                <h5 class="card-title text-info product-code"
                                                    style="padding-left: 10px; margin-bottom: 0; font-weight: normal">
                                                    {{ $item->product_code }}
                                                </h5>
                                                <h5 class="card-title text-info bold product-name"
                                                    style="padding-left: 10px; margin-bottom: 0; margin-top: 3px; font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">
                                                    {{ $item->product_name }}
                                                </h5>
                                                <div class="stock" style="display: flex; align-items: center">
                                                    <p class="card-text text-success product-price cart_item_quantity"
                                                        style="margin-bottom: 0; margin-right: 5px">
                                                        {{ $item->whole_selling_price }} - {{ $item->retail_selling_price }} &#x09F3;</p>
                                                </div>
                                                <!-- <p class="card-text text-success product-price cart_item_quantity"
                                                    style="margin-bottom: 0; margin-right: 5px">
                                                    Quantity: {{ $item->product_quantity }}</p> -->

                                                    @php
                                                        if ($item->product_quantity <= 10) {
                                                          echo '<p class="card-text text-danger product-price cart_item_quantity"
                                                              style="margin-bottom: 0; margin-right: 5px">
                                                              Warning Qnt:'. $item->product_quantity . '</p>';

                                                        }
                                                        else{
                                                          echo '<p class="card-text text-success product-price cart_item_quantity"
                                                              style="margin-bottom: 0; margin-right: 5px">
                                                              Quantity:'. $item->product_quantity. '</p>';
                                                        }

                                                    @endphp
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable div").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        $(document).ready(function() {
            $('#radio_box').change(function() {
                var cat_id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    dataType: 'html',
                    url: '{{ url('/select/product/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        cat_id: cat_id
                    },
                    success: function(show) {
                        $('#selectProduct').html(show);
                        // alert(show);
                    }
                });
            });
        });
    </script>
@endsection
