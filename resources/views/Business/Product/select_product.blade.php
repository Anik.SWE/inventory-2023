@foreach ($Total_Product as $item)
    <div class="col-sm-2" style="margin-bottom: 10px; height: 150px; margin-top: 10px">
        <div class="shadow-sm card mb-3 product"">
            <div class="card-body" style="height: 90px">
                {{-- @if ($item->product_quantity <= 5)
                                                this product is not available
                                            @endif --}}

                <h5 class="card-title text-info product-code"
                    style="padding-left: 10px; margin-bottom: 0; font-weight: normal">
                    {{ $item->product_code }}
                </h5>
                <h5 class="card-title text-info bold product-name"
                    style="padding-left: 10px; margin-bottom: 0; margin-top: 3px">
                    {{ $item->product_name }}
                </h5>
                <div class="stock" style="display: flex; align-items: center">
                    <p class="card-text text-success product-price cart_item_quantity"
                        style="margin-bottom: 0; margin-right: 5px">
                        {{ $item->product_selling_price }}</p>

                    @php
                        if ($item->product_quantity < 5) {
                            echo "<span style='width:40px; color: orange; text-align:center; margin-left: 10px; '> Warning
                                                              </span>";
                        }

                    @endphp
                </div>
                <p class="card-text text-success product-price cart_item_quantity"
                    style="margin-bottom: 0; margin-right: 5px">
                    Quantity: {{ $item->product_quantity }}</p>
            </div>
        </div>
    </div>
@endforeach
