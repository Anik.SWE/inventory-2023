@extends('layouts.user_api.business_app')
@section('content')

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
          <h3><i class="fa fa-shopping-cart"></i>All Product</h3>
          <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Product</a> </li>
              <li class="active">All Product's </li>
            </ol>
          </div>
        </div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Product List</h4>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Expire Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($Purchase_info as $item)
                                      @if(Session::get('business_id') == $item->business)
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ App\Models\Product::find($item->product)->product_name }}</td>
                                            <td>&#x09F3; {{ $item->price }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->product_expire_date)->format('d-m-Y') }}</td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/purchase/product') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                            </td>
                                        </tr>
                                      @endif

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Edit Product</h4>
                      </div>
                      <div class="panel-body">
                        <form class="panel-body form-horizontal" action="{{ url('/edit/purchase/formpost') }}" method="POST">
                            @csrf
                            <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />
                            <input type="number" name="purchase_ids" value="{{$Purchase_infoS->id}}" hidden />
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px;">
                                <label class="col-md-4 control-label text-left" for="demo-text-input">Quantity:</label>
                                <div class="col-md-8">
                                    <input type="number" id="quantity" name="product_quantity" class="form-control"
                                        value="{{$Purchase_infoS->quantity}}" onchange="auto_total();" onKeyUp="auto_total();" readonly>
                                    @error('product_quantity')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 10px;">
                              <label class="col-md-4 control-label text-left" for="demo-text-input">Price:</label>
                              <div class="col-md-8">
                                  <input type="number" id="buying_price" name="product_buying_price" class="form-control"
                                      value="{{$Purchase_infoS->price}}" onchange="auto_total();" onKeyUp="auto_total();" readonly>
                                  @error('product_buying_price')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                            </div>


                            <div class="form-group" style="margin-bottom: 10px;">
                                <label class="col-md-4 control-label text-left" for="demo-text-input">Buying Date:</label>
                                <div class="col-md-8">
                                    <input type="date" id="demo-text-input" name="product_buying_date" class="form-control"value="{{$Purchase_infoS->product_buying_date}}">
                                    @error('product_buying_date')
                                        <small class="help-block text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 10px;">
                                <label class="col-md-4 control-label text-left" for="demo-text-input">Expire Date:</label>
                                <div class="col-md-8">
                                    <input type="date" id="demo-text-input" name="product_expire_date"
                                        class="form-control"value="{{$Purchase_infoS->product_expire_date}}">
                                    @error('product_expire_date')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="payment">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <label class="col-md-4 control-label text-left" for="demo-text-input">Total Amount:</label>
                                    <div class="col-md-8">
                                        <input type="text" id="total_amount" name="total_amount" class="form-control"
                                            readonly value="{{$Purchase_infoS->total_amount}}">
                                        @error('total_amount')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group" style="margin-bottom: 10px">
                                    <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                        for="demo-text-input">Payment:</label>
                                    <div class="col-md-8">
                                        <input type="number" id="payment" name="payment" class="form-control"
                                            value="{{$Purchase_infoS->payment}}" onchange="auto_total();" onKeyUp="auto_total();" readonly>
                                        @error('payment')
                                            <small class="help-block text-danger"
                                                style="margin-bottom: 0px">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 10px">
                                  <label class="col-md-4 control-label text-left" for="demo-text-input">Change:</label>
                                  <div class="col-md-8">
                                      <input type="text" id="change" name="change" class="form-control"
                                          value="{{$Purchase_infoS->change}}" onchange="auto_total();" readonly>
                                      @error('change')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                                </div>

                                <div class="col-12 text-center" style="padding: 5px">
                                    <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit
                                        Information</button>
                                </div>
                            </div>
                        </form>
                      </div>

                  </div>
              </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price));
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
    </script>
@endsection
