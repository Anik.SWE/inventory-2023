@extends('layouts.user_api.business_app')
@section('content')



<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Adjustment List</h4>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL.No</th>
                                        <th>Product Name</th>
                                        <th>Reason</th>
                                        <th>type</th>
                                        <th>Qty</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Adjust as $item)
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>


                                               <td>{{ App\Models\Product::find($item->product)->product_name }}</td>


                                            <td>$ {{ $item->reason }}</td>
                                            <td>$ {{ $item->type }}</td>
                                            <td>{{ $item->quantity }}</td>

                                            <td style="align-items: center">


                                              <a href="{{ url('product/adjust/delete') }}/{{ $item->id }}"><i
                                                      class="fa fa-trash text-danger"> Del </i></a>


                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                    @endforelse
                                </tbody>

                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                    <div class="panel-heading">
                      <h4 class="panel-title">Adjust One</h4>
                    </div>
                    <div class="panel-body">
                      @if($product)
                      <form class="form-horizontal"  action="{{ route('product.adjust') }}"
                          method="POST">
                          @csrf

                          @if($errors->any())
                              <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                          @endif

                          <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />
                          <!--Text Input-->
                          <input type="number" name="pro_id" value="{{$product->id}}" hidden >

                          <div class="form-group">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Operation Type:</label>
                              <div class="col-md-8" style="margin-bottom: 5px" id="">
                                <select class="form-control" name="operation_type" id="">
                                    <option value="">-- Select Option --</option>
                                    <option value="Increase">-- Increase --</option>
                                    <option value="Decrease">-- Decrease --</option>
                                </select>
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Reason:</label>
                              <div class="col-md-8">
                                  <input type="text" id="reason" name="reason" class="form-control"
                                      placeholder="Enter adjustment reason">
                                  @error('quantity')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>


                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Quantity:</label>
                              <div class="col-md-8">
                                  <input type="number" id="quantity" name="quantity" class="form-control"
                                      value="0">
                                  @error('quantity')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>


                              <div class="col-12" style="padding: 5px; margin-left: 130px">
                                  <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">
                                      Adjust</button>
                              </div>

                      </form>
                      @endif
                    </div>
                  </div>
              </div>
          </div>
        </div>
    </div>







@endsection

@section('script')
    <script>
        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price)) + parseInt(document.getElementById("shipp_costid").value);
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
    </script>
@endsection
