@extends('layouts.user_api.business_app')
@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-shopping-cart"></i> Product List </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Home</a> </li>
              <li class="active">Business Dashboard </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                    <div class="panel-heading">
                      <h4 class="panel-title">Product's List</h4>
                    </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        {{-- <th>Brands</th> --}}
                                        <th>Whole</th>
                                        <th>Retail</th>
                                        <th>Qty</th>
                                        <th>Stock_Ajt.</th>
                                        <th>Adjustment</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Product as $item)
                                      @if(Session::get('business_id') == $item->business)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->product_name }}</td>
                                            {{-- <td>{{ App\Models\Brand::find($item->brand_id)->brand_name }}</td> --}}
                                            <td>&#x09F3; {{ $item->whole_selling_price }}</td>
                                            <td>&#x09F3; {{ $item->retail_selling_price }}</td>
                                            <td>{{ $item->product_quantity }}</td>
                                            <td>
                                              Up
                                              {{DB::table('product_adjustments')->where('product' , $item->id)->where('type' , "Increase")->sum('quantity')}}
                                              Down
                                              {{DB::table('product_adjustments')->where('product' , $item->id)->where('type' , "Decrease")->sum('quantity')}}
                                            </td>
                                              <td>
                                                      <a href="{{ url('product/adjust/form') }}/{{ $item->id }}"><i class="fa fa-adjust text-info" style="margin-right: 15px"> Adjust </i></a>
                                              </td>
                                            <td>
                                                <div class="label label-table label-success"><a
                                                        href="{{ url('/single/product/view') }}/{{ $item->id }}"
                                                        style="color: #fff">View</a>
                                                </div>
                                            </td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/product') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                              @if(!DB::table('product_adjustments')->where('product' , $item->id)->sum('quantity') > 0 )
                                                <a href="{{ url('delete/product') }}/{{ $item->id }}"><i
                                                        class="fa fa-trash text-danger"> Del </i></a>
                                              @endif
                                            </td>
                                        </tr>

                                        @endif
                                    @empty
                                        
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                            <td colspan="9" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                        @endforelse
                                </tfoot>

                            </table>

                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">New Product</h4>
                      </div>

                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->
                      <form class="panel-body form-horizontal" action="{{ route('PFadd') }}" method="POST">
                          @csrf

                          <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />
                          <!--Text Input-->
                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Product Name:</label>
                              <div class="col-md-8" >
                                  <input type="text" id="demo-text-input" name="product_name" class="form-control"
                                      placeholder="Product Name..." value="{{ old('product_name') }}">
                                  @error('product_name')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>

                              <!--<div class="col-md-4">
                                  <select class="form-control selectpicker" data-live-search="true" name="select_product_name" id="">
                                      <option value="0">
                                          Select
                                      </option>
                                      @foreach ($Total_Product as $item)
                                          <option value="{{ $item->id }}">{{ $item->product_name }}</option>
                                      @endforeach
                                  </select>
                              </div>-->
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Description:</label>
                              <div class="col-md-8">
                                  <textarea class="form-control" name="product_description" id="" cols="30" rows="1">
                                      {{ old('product_description') }}
                                  </textarea>
                                  @error('product_description')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" >
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Category:</label>
                              <div class="col-md-8" >
                                  <select class="form-control selectpicker" data-live-search="true" name="category_id" id="">
                                      <option value="">-- Select Option --</option>
                                      @foreach ($Category_info as $item)
                                          <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                                      @endforeach
                                  </select>
                                  @error('category_id')
                                      <small class="help-block text-danger">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-md-4 control-label text-left"
                                  for="demo-text-input">Brands:</label>
                              <div class="col-md-8" >
                                  <select class="form-control selectpicker" data-live-search="true" name="brand_id" id="">
                                      <option value="">-- Select Option --</option>
                                      @foreach ($Brand_info as $item)
                                          <option value="{{ $item->id }}">{{ $item->brand_name }}</option>
                                      @endforeach
                                  </select>
                                  @error('brand_id')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>



                          <div class="form-group">

                              <label class="col-md-4 control-label text-left" style="padding-left: 20px" for="demo-text-input">Whole Sell:</label>
                              <div class="col-md-8">
                                  <input type="number" id="buying_price" name="whole_selling_price" class="form-control"
                                     placeholder="Whole Sell Price" step="any">
                                  @error('product_buying_price')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left"
                                style="padding-left: 20px"  for="demo-text-input">Retail Sell:</label>
                              <div class="col-md-8">
                                  <input type="number" id="demo-text-input" name="retail_selling_price"
                                      class="form-control" placeholder="Retail Sell Price" step="any">
                                  @error('product_selling_price')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>





                              <div class="col-12" style="padding: 5px; margin-left: 100px">
                                  <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                      Product</button>
                              </div>

                      </form><br><br>
                  </div>
                  </div>
              </div>
        </div>
        </div>

@endsection
