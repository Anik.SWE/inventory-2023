@extends('layouts.user_api.business_app')
@section('content')

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-edit"></i>Edit Product</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Product</a> </li>
              <li class="active">Product Edit </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Product List</h4>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        {{-- <th>Brands</th> --}}
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Expired</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_Product as $item)
                                      @if(Session::get('business_id') == $item->business)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->product_name }}</td>
                                            {{-- <td>{{ App\Models\Brand::find($item->brand_id)->brand_name }}</td> --}}
                                            <td>&#x09F3; {{ $item->whole_selling_price }}-{{ $item->retail_selling_price }}</td>
                                            <td>{{ $item->product_quantity }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->product_expire_date)->format('d-m-Y') }}</td>
                                            <td>
                                                <div class="label label-table label-success"><a
                                                        href="{{ url('/single/product/view') }}/{{ $item->id }}"
                                                        style="color: #fff">View</a>
                                                </div>
                                            </td>
                                            <td style="align-items: center">
                                                <a href="{{ url('edit/product') }}/{{ $item->id }}"><i
                                                        class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                    </i></a>
                                            </td>
                                        </tr>
                                      @endif
                                    @empty
                                        <tr>
                                            <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Edit Product</h4>
                      </div>
                      <div class="panel-body">
                        <form class="panel-body form-horizontal" action="{{ url('/edit/product/formpost') }}" method="POST">
                            @csrf
                            <!--Text Input-->
                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-danger text-left" style="padding-left: 20px"
                                    for="demo-text-input">Product Name:</label>
                                <div class="col-md-8">
                                    <input type="text" id="demo-text-input" name="product_name" class="form-control"
                                        placeholder="Enter Your Product Name" value="{{ $Product_info->product_name }}">
                                    @error('product_name')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <input type="hidden" id="demo-text-input" name="id" class="form-control"
                                        placeholder="Enter Your Category Name" value="{{ $Product_info->id }}">

                            <div class="form-group" style="margin-bottom: 10px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Description:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" style="" name="product_description"  rows="2">
                                        {{$Product_info->product_description}}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 5px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Category:</label>
                                <div class="col-md-8" style="margin-bottom: 5px">
                                    <select class="form-control" name="category_id" id="">
                                        <option value="{{ $Product_info->category_id }}">{{ App\Models\Category::find($Product_info->category_id)->category_name }}</option>
                                        @foreach ($Category_info as $item)
                                            <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group" style="margin-bottom: 5px">
                                <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                    for="demo-text-input">Brands:</label>
                                <div class="col-md-8" style="margin-bottom: 5px">
                                    <select class="form-control" name="brand_id" id="">
                                        <option value="{{ $Product_info->brand_id }}">{{ App\Models\Brand::find($Product_info->brand_id)->brand_name }}</option>
                                        @foreach ($Brand_info as $item)
                                            <option value="{{ $item->id }}">{{ $item->brand_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('brand_id')
                                        <small class="help-block text-danger"
                                            style="margin-bottom: 0px">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                              <div class="form-group" style="margin-bottom: 10px">

                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px" for="demo-text-input">Whole Sell:</label>
                                  <div class="col-md-8">
                                      <input type="number" id="buying_price" name="whole_selling_price" class="form-control"
                                         placeholder="Whole Sell Price" value="{{ $Product_info->whole_selling_price }}" step="any">
                                      @error('product_buying_price')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 10px;">
                                  <label class="col-md-4 control-label text-left"
                                    style="padding-left: 20px"  for="demo-text-input">Retail Sell:</label>
                                  <div class="col-md-8">
                                      <input type="number" id="demo-text-input" name="retail_selling_price"
                                          class="form-control" placeholder="Retail Sell Price"
                                          value="{{ $Product_info->retail_selling_price }}" step="any">
                                      @error('product_selling_price')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                              </div>


                                <div class="col-12 text-center" style="padding: 5px">
                                    <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Edit
                                        Information</button>
                                </div>
                        </form>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
@endsection
