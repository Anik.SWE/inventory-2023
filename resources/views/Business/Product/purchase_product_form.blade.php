@extends('layouts.user_api.business_app')
@section('content')



<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-shopping-cart"></i> Product List </h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Home</a> </li>
              <li class="active">Business Dashboard </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
          <div class="row">
              <div class="col-md-8">
                  <div class="panel">
                      <div class="panel-heading">
                          <h4 class="panel-title">Purchase History</h4>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Expire Date</th>
                                        <th>Return</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($Total_ProductPurchase as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ App\Models\Product::find($item->product)->product_name }}</td>
                                            <td>&#x09F3; {{ $item->price }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->product_expire_date)->format('d-m-Y') }}</td>
                                            <td style="align-items: center">

                                                    {{DB::table('purchess_returns')->where('purchase' , $item->id)->sum('product_quantity')}}
                                                    @if(DB::table('purchess_returns')->where('purchase' , $item->id)->sum('product_quantity') < $item->quantity )
                                                    <a href="{{ url('purchess/return/form') }}/{{ $item->id }}"><i
                                                                    class="fa fa-undo text-info" style="margin-right: 15px"> Return </i></a>
                                                    @endif
                                            </td>
                                            <td style="align-items: center">
                                              <a href="{{ url('edit/purchase/product') }}/{{ $item->id }}"><i
                                                      class="fa fa-edit text-primary" style="margin-right: 15px"> Edit
                                                  </i></a>
                                            @if(!DB::table('purchess_returns')->where('purchase' , $item->id)->sum('product_quantity'))
                                              <a href="{{ url('delete/product/purchase') }}/{{ $item->id }}"><i
                                                      class="fa fa-trash text-danger"> Del </i></a>
                                            @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                                You Not Create Any Product....Please Create a Product!</td>
                                        </tr>
                                    @endforelse
                                </tbody>

                            </table>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="panel" style="margin-top: 5px">

                      <!-- BASIC FORM ELEMENTS -->
                      <!--===================================================-->





                      <form class="panel-body form-horizontal"
                          style="border: 1px solid #25A79F; margin-top: 5px; padding: 20px" action="{{ route('PHadd') }}"
                          method="POST">
                          @csrf

                          <input type="number" name="business_ids" value="{{ Session::get('business_id') }}" hidden />
                          <!--Text Input-->


                          <div class="form-group">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Product:</label>
                              <div class="col-md-8" style="margin-bottom: 5px" id="supplier_div_id">
                                <select class="form-control selectpicker" data-live-search="true" name="select_product_name" id="" required>
                                    <option value="">-- Select Product --</option>
                                          @foreach ($Total_Product as $item)
                                            @if(Session::get('business_id') == $item->business)
                                                <option value="{{ $item->id }}">{{ $item->product_name }}</option>
                                            @endif
                                          @endforeach
                                </select>
                              </div>
                          </div>


                          <div class="form-group">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Supplier Name:</label>
                              <div class="col-md-8" style="margin-bottom: 5px" id="supplier_div_id">
                                <select class="form-control selectpicker" data-live-search="true" name="supplier_id" id="" required>
                                    <option value="">-- Select Option --</option>
                                          @foreach ($Supplier_info as $item)
                                            @if(Session::get('business_id') == $item->business)
                                                <option value="{{ $item->id }}">{{ $item->supplier_name }} {{ $item->phone_number }}</option>
                                            @endif
                                          @endforeach
                                </select>
                              </div>
                          </div>




                          <div class="form-group" style="margin-bottom: 10px">



                                      <div id="suggestProduct">

                                      </div>


                          </div>


                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Quantity:</label>
                              <div class="col-md-8">
                                  <input type="number" id="quantity" name="product_quantity" class="form-control"
                                      value="0" onchange="auto_total();" onKeyUp="auto_total();">
                                  @error('product_quantity')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" for="demo-text-input" style="padding-left: 20px">Price:</label>
                              <div class="col-md-8">
                                  <input type="number" id="buying_price" name="product_buying_price" class="form-control"
                                      value="0" onchange="auto_total();" onKeyUp="auto_total();" step="any">
                                  @error('product_buying_price')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>



                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Buying Date:</label>
                              <div class="col-md-8">
                                  <input type="date" id="demo-text-input" name="product_buying_date"
                                      class="form-control"value="{{ old('product_buying_date') }}" required>
                                  @error('product_buying_date')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="form-group" style="margin-bottom: 10px">
                              <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                  for="demo-text-input">Expire Date:</label>
                              <div class="col-md-8">
                                  <input type="date" id="demo-text-input" name="product_expire_date"
                                      class="form-control"value="{{ old('product_expire_date') }}" required>
                                  @error('product_expire_date')
                                      <small class="help-block text-danger"
                                          style="margin-bottom: 0px">{{ $message }}</small>
                                  @enderror
                              </div>
                          </div>

                          <div class="payment">

                              <div class="form-group" style="margin-bottom: 10px">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                      for="demo-text-input">Shipping Cost:</label>
                                  <div class="col-md-8">
                                      <input type="number" id="shipp_costid" name="shipp_cost" class="form-control"
                                           value="0" onchange="auto_total();" onKeyUp="auto_total();">
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 10px">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                      for="demo-text-input">Discount:</label>
                                  <div class="col-md-8">
                                      <input type="number" id="dis_costid" name="discount" class="form-control"
                                           value="0" onchange="auto_total();" onKeyUp="auto_total();" step="any">
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 10px">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                      for="demo-text-input">Total Amount:</label>
                                  <div class="col-md-8">
                                      <input type="text" id="total_amount" name="total_amount" class="form-control"
                                          readonly value="{{ old('total_amount') }}">
                                      @error('total_amount')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 5px">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                      for="demo-text-input">Bank:</label>
                                  <div class="col-md-8" style="margin-bottom: 5px">
                                    @if($errors->any())
                                        <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                                    @endif
                                      <select class="form-control selectpicker" data-live-search="true" onchange="havePay();" name="bankB_id" id="havePay_id">
                                          <option value="">-- Select Option --</option>
                                          @foreach ($bank_info as $item)
                                              <option value="{{ $item->id }}">{{ $item->bank_name }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 10px; display: none;" id="payment_div">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px"
                                      for="demo-text-input">Payment:</label>
                                  <div class="col-md-8">
                                      <input type="number" id="payment" name="payment" class="form-control"
                                          value="0" onchange="auto_total();" onKeyUp="auto_total();" step="any">
                                      @error('payment')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                              </div>

                              <div class="form-group" style="margin-bottom: 10px">
                                  <label class="col-md-4 control-label text-left" style="padding-left: 20px" for="demo-text-input">Change:</label>
                                  <div class="col-md-8">
                                      <input type="text" id="change" name="change" class="form-control"
                                          value="0" onchange="auto_total();" readonly>
                                      @error('change')
                                          <small class="help-block text-danger"
                                              style="margin-bottom: 0px">{{ $message }}</small>
                                      @enderror
                                  </div>
                              </div>



                              <div class="col-12" style="padding: 5px; margin-left: 130px">
                                  <button class="btn" style="background-color: #25A79F; color: #fff" type="submit">Add
                                      Product</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
        </div>
    </div>




    <script type="text/javascript">


    function havePay() {


      var drop = document.getElementById("havePay_id").value;
      var div = document.getElementById("payment_div");

        if(drop){
          div.style.display = "block";
        }
        else{
          div.style.display = "none";
        }

    }

      function showSearch(){
        $('#suggestProduct').slideDown();
      }

      function hideSearch(){
        $('#suggestProduct').slideUp();
      }

    </script>


@endsection

@section('script')
    <script>
        function auto_total() {
            var Quantity = document.getElementById("quantity").value;
            var Buying_price = document.getElementById("buying_price").value;
            var total_amount = (parseFloat(Quantity) * parseFloat(Buying_price)) + parseInt(document.getElementById("shipp_costid").value);

            total_amount = total_amount - document.getElementById("dis_costid").value;
            document.getElementById("total_amount").value = total_amount;

            var payment = document.getElementById("payment").value;
            var change = (parseFloat(total_amount) - parseFloat(payment));
            document.getElementById("change").value = change;
        }

        $(document).ready(function() {
          $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
          });
        });
    </script>
@endsection
