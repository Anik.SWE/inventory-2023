@extends('layouts.user_api.business_app')
@section('content')
    @php
    $barcode = DNS1D::getBarcodeSVG($single_product_view->product_code, 'C128', 1, 50);
    @endphp
    <div id="content-container">
                    <!--Page Title-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <div class="pageheader">
                        <h3><i class="fa fa-cube"></i> Product </h3>
                        <div class="breadcrumb-wrapper">
                            <span class="label">You are here:</span>
                            <ol class="breadcrumb">
                                <li> <a href="#"> Product </a> </li>
                                <li class="active"> Products Details </li>
                            </ol>
                        </div>
                    </div>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End page title-->
                    <!--Page content-->
                    <!--===================================================-->
                    <script>
            function printPageArea(areaID) {
                var printContent = document.getElementById(areaID).innerHTML;
                var originalContent = document.body.innerHTML;
                document.body.innerHTML = printContent;
                window.print();
                document.body.innerHTML = originalContent;
            }
        </script>
                    <div id="page-content">
                        <div class="row">
                            <div class="col-md-12">
                                <!--Default Tabs (Left Aligned)-->
                                <!--===================================================-->
                                <div class="tab-base">
                                    <!--Nav Tabs-->
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#demo-lft-tab-1"> Product </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#demo-lft-tab-2">Barcode</a>
                                        </li>
                                    </ul>
                                    <!--Tabs Content-->
                                    <div class="tab-content">
                                        <div id="demo-lft-tab-1" class="tab-pane fade active in">
                                            <h4 class="text-thin">Product Information</h4>
                                            <p>Lorem ipsum do<table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td> Product Name </td>
                                    <td>{{ $single_product_view->product_name }}</td>
                                    <td> Procduct Code </td>
                                    <td>{{ $single_product_view->product_code }}</td>
                                </tr>
                                <tr>
                                    <td> Product Category </td>
                                    <td>{{ App\Models\Category::find($single_product_view->category_id)->category_name }}
                                    </td>
                                    <td> Product Brands </td>
                                    <td>{{ App\Models\Brand::find($single_product_view->brand_id)->brand_name }}</td>
                                </tr>
                                <tr>
                                    <td> Whole Sell </td>
                                    <td><span class="label label-sm label-success">&#x09F3;
                                            {{ $single_product_view->whole_selling_price }}</span></td>
                                    <td> Retail Sell </td>
                                    <td><span class="label label-sm label-success">&#x09F3;
                                            {{ $single_product_view->retail_selling_price }}</span></td>
                                </tr>
                                <tr>
                                    <td colspan="4"> </td>
                                </tr>
                                <tr>
                                    <td> Buying Date </td>
                                    <td>{{ Carbon\Carbon::parse($single_product_view->product_buying_date)->format('d-m-Y') }}
                                    </td>
                                    <td> Expire Date </td>
                                    <td>{{ Carbon\Carbon::parse($single_product_view->product_expire_date)->format('d-m-Y') }}
                                    </td>
                                </tr>
                                <tr>
                                  @php
                                     $produt_stk = DB::table('product_adjustments')->where('product' , $single_product_view->id)->where('type' , "Increase")->where('business' , Session::get('business_id'))->sum('quantity') + DB::table('product_purchases')->where('product' , $single_product_view->id)->where('business' , Session::get('business_id'))->sum('quantity') - DB::table('product_adjustments')->where('product' , $single_product_view->id)->where('type' , "Decrease")->where('business' , Session::get('business_id'))->sum('quantity') - $total_sell_pr;
                                     DB::table('products')->where('id', $single_product_view->id)->update(['product_quantity' => $produt_stk]);
                                  @endphp
                                    <td> Total Stock </td>
                                    <td>{{ $single_product_view->product_quantity }}</td>
                                    <td> Bar Code </td>
                                    <td>
                                      <div id="content2">
                                        @php
                                            echo $barcode;
                                        @endphp
                                        <div>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Total Sell </td>
                                    <td>{{ $total_sell_pr }}</td>
                                    <td> Total Purchase </td>
                                    <td>
                                          {{ DB::table('product_purchases')->where('product' , $single_product_view->id)->where('business' , Session::get('business_id'))->sum('quantity') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td> Adjustment Up </td>
                                    <td>{{ DB::table('product_adjustments')->where('product' , $single_product_view->id)->where('type' , "Increase")->where('business' , Session::get('business_id'))->sum('quantity') }}</td>
                                    <td> Adjustment Down </td>
                                    <td>
                                          {{ DB::table('product_adjustments')->where('product' , $single_product_view->id)->where('type' , "Decrease")->where('business' , Session::get('business_id'))->sum('quantity') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td> Product Description </td>
                                    <td colspan="3">{{ $single_product_view->product_description }}</td>
                                </tr>
                            </tbody>
                        </table>
                                            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SL.No</th>
                            <th>Product Name</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Expire Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($Total_ProductPurchase as $item)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>

                                   <td>{{ App\Models\Product::find($item->product)->product_name }}</td>

                                <td>&#x09F3; {{ $item->price }}</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ Carbon\Carbon::parse($item->product_expire_date)->format('d-m-Y') }}</td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="50" class="text-danger"> <strong>{{ Auth::user()->name }} </strong>
                                    You Not Create Any Product....Please Create a Product!</td>
                            </tr>
                        @endforelse
                    </tbody>

                </table>
            </div>
                                        </div>
                                        <div id="demo-lft-tab-2" class="tab-pane fade">
                                            <h4 class="text-thin">Product's Barcode</h4>
                                            <div id="printableArea">
                                            <table width="100%">
                                                                <tbody>
                                                                    @for ($i = 0; $i < 10; $i++)
                                                                    <tr>
                                                                        
                                                                        <td style="text-align: center; padding: 2px; padding-top: 4px; padding-bottom:4px;">
                                                                            <span style="vertical-align: bottom;"><b style="font-size: 14px;">MRP: Tk. {{ $single_product_view->retail_selling_price }}</b></span><br>
                                                                            @php
                                                                              echo $barcode;
                                                                             @endphp
                                                                        </td>
                                                                        <td style="text-align: center; padding: 2px; padding-top: 4px; padding-bottom:4px;">
                                                                            <span style="vertical-align: bottom;"><b style="font-size: 14px;">MRP: Tk. {{ $single_product_view->retail_selling_price }}</b></span><br>
                                                                            @php
                                                                              echo $barcode;
                                                                             @endphp
                                                                        </td>
                                                                        <td style="text-align: center; padding: 2px; padding-top: 4px; padding-bottom:4px;">
                                                                            <span style="vertical-align: bottom;"><b style="font-size: 14px;">MRP: Tk. {{ $single_product_view->retail_selling_price }}</b></span><br>
                                                                            @php
                                                                              echo $barcode;
                                                                             @endphp
                                                                        </td>
                                                                        <td style="text-align: center; padding: 2px; padding-top: 4px; padding-bottom:4px;">
                                                                            <span style="vertical-align: bottom;"><b style="font-size: 14px;">MRP: Tk. {{ $single_product_view->retail_selling_price }}</b></span><br>
                                                                            @php
                                                                              echo $barcode;
                                                                             @endphp
                                                                        </td>
                                                                    </tr>
                                                                    @endfor
                                                                </tbody>
                                                            </table>
                                                            </div>
                                                            <div class="text-center no-print">
                            <a class="btn btn-primary btn-xs" onclick="printPageArea('printableArea')">
                                <i class="fa fa-print"></i> Print
                            </a>
                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!--===================================================-->
                                <!--End Default Tabs (Left Aligned)-->
                            </div>
                        </div>
                        
                    </div>
                    <!--===================================================-->
                    <!--End page content-->
                </div>
    
@endsection
