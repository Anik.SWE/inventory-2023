@extends('layouts.user_api.business_app')
@section('content')
    @php
        error_reporting(0);
    @endphp
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-file"></i> Supplier Invoice</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Invoice</a> </li>
              <li class="active">Supplier Invoice </li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                      <table id="demo-dt-basic" class="table table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th>SL.No</th>
                                  <th>Supplier Name</th>
                                  <th>Purchase Amount</th>
                                  <th>Payment</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach ($supplier_invoice as $item)
                                  @php
                                      $supplier = DB::table('product_purchases')
                                          ->where('business', Session::get('business_id'))
                                          ->where('supplier_id', $item->supplier_id)
                                          ->get();
                                      $sale_amount = DB::table('product_purchases')
                                          ->where('business', Session::get('business_id'))
                                          ->where('supplier_id', $item->supplier_id)
                                          ->groupBy('supplier_id')
                                          ->sum('total_amount');
                                      $pay_amount = DB::table('product_purchases')
                                          ->where('business', Session::get('business_id'))
                                          ->where('supplier_id', $item->supplier_id)
                                          ->groupBy('supplier_id')
                                          ->sum('payment');
                                      $due_amount = DB::table('product_purchases')
                                          ->where('business', Session::get('business_id'))
                                          ->where('supplier_id', $item->supplier_id)
                                          ->groupBy('supplier_id')
                                          ->sum('change');
                                      $due_pay_amount = DB::table('product_purchases')
                                          ->where('business', Session::get('business_id'))
                                          ->where('supplier_id', $item->supplier_id)
                                          ->sum('payment');
                                      $add = $pay_amount + $due_pay_amount;
                                      $due = $sale_amount - $add;
                                  @endphp
                                  <tr>
                                      <td>{{ $loop->index + 1 }}</td>

                                      <td>
                                          {{ App\Models\Supplier::find($item->supplier_id)->supplier_name }}
                                      </td>
                                      <td>
                                          Total :
                                          {{ $sale_amount }}
                                      </td>
                                      <td style="align-items: center">
                                          Total :
                                          {{ $pay_amount }}
                                      </td>
                                      <td>
                                          <div class="label label-table label-success"><a
                                                  href="{{ url('/single/supplier/invoice/view') }}/{{ $item->supplier_id }}"
                                                  style="color: #fff">View</a>
                                          </div>
                                      </td>

                                  </tr>
                              @endforeach


                          </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
