@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user"></i> Customar Details</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Invoice</a> </li>
              <li class="active">Invoice Details</li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body pad-no">

                            <!--Default Tabs (Left Aligned)-->
                            <!--===================================================-->
                            <div class="tab-base">

                                <!--Nav Tabs-->
                                <ul class="nav nav-tabs">
                                    <li class="active"> <a data-toggle="tab" href="#demo-lft-tab-1"> Invoice </a>
                                    </li>
                                    <li> <a data-toggle="tab" href="#demo-lft-tab-2">Ledger</a> </li>
                                </ul>

                                <!--Tabs Content-->
                                <div class="tab-content">
                                    <div id="demo-lft-tab-1" class="tab-pane fade active in">

                                        <!-- Timeline -->
                                        <!--===================================================-->
                                        <div class="timeline">

                                            <!-- Timeline header -->
                                             <div class="timeline-header" style="margin-bottom: 15px">

                                                @if($errors->any())
                                                    <h6 class="text-center text-danger">{{$errors->first()}}</h6>
                                                @endif

                                            </div>
                                            <div class="timeline-entry" style="margin: 0">
                                                <div class="timeline-stat">
                                                    <div class="timeline-icon bg-success"><i
                                                            class="fa fa-check fa-lg"></i> </div>
                                                </div>
                                                <div class="timeline-label">
                                                    <div class="row">
                                                        <div class="panel">
                                                          <div class="panel-heading">
                                                            <h4 class="panel-title">Invoice For {{ $customar_info->customar_name }}</h4>
                                                          </div>
                                                          <div class="panel-body">
                                                            <div class="table-responsive">
                                                              <table id="demo-dt-basic" class="table table-striped table-bordered">
                                                                  <thead>
                                                                      <tr>
                                                                          <th>Sl</th>
                                                                          <th>Invoice</th>
                                                                          <th>Bill_amount</th>
                                                                          <th>Billing_Date</th>
                                                                          <th>Pro_Return</th>
                                                                          <th>Status</th>
                                                                      </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                      @forelse ($fixed_customar_order as $item)
                                                                          <tr>
                                                                              <td>{{ $loop->iteration }}</td>
                                                                              <td>{{ $item->order_id }}</td>
                                                                              <td class="text-center">&#x9F3; {{ number_format($item->total,2) }}
                                                                              </td>
                                                                              <td><span class="text-muted"><i
                                                                                          class="fa fa-clock-o"></i>
                                                                                      {{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</span>
                                                                              </td>

                                                                              <td style="align-items: center">

                                                                                      {{DB::table('sale_returns')->where('sell' , $item->id)->sum('product_quantity')}}

                                                                                      <a href="{{ url('sale/return/form') }}/{{ $item->id }}"><i
                                                                                                      class="fa fa-undo text-info" style="margin-right: 15px"> Return </i></a>

                                                                              </td>

                                                                              <td>
                                                                                  <div
                                                                                      class="label label-table label-success">
                                                                                      <a href="{{ url('/invoice/view') }}/{{ $item->id }}"
                                                                                          style="color:white">view</a>
                                                                                  </div>
                                                                                  <div
                                                                                      class="label label-table bg-light">
                                                                                          @if(!DB::table('sale_returns')->where('sell' , $item->id)->sum('product_quantity'))
                                                                                          <a href="{{ url('delete/product/sell') }}/{{ $item->id }}"><i
                                                                                                  class="fa fa-trash text-danger"> Del </i></a>
                                                                                          @endif
                                                                                  </div>
                                                                              </td>
                                                                          </tr>
                                                                      @empty
                                                                      @endforelse

                                                                  </tbody>
                                                              </table>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--===================================================-->
                                        <!-- End Timeline -->
                                    </div>
                                    <div id="demo-lft-tab-2" class="tab-pane fade">
                                        <div class="row" id="content">
                                          <div class="panel">
                                            <div class="panel-heading">
                                              <h4>Sell Ledger</h4>
                                            </div>
                                            <div class="panel-body">
                                              <div class="table-responsive">
                                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>SL</th>
                                                            <th>Item</th>
                                                            <th>Price</th>
                                                            <th>Qnt</th>
                                                            <th>Total</th>
                                                            <th>Payment</th>
                                                            <th>Shipping</th>
                                                            <th>Discount</th>
                                                            <th>Previous</th>
                                                            <th>Current</th>
                                                            <th>Delivery_Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $balance = 0;
                                                            $total_shipping = 0;
                                                            $total_discount = 0;
                                                            $sum_total=0;
                                                        @endphp
                                                        @forelse ($fixed_customar_ledger as $item)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>
                                                                    @php
                                                                        $pro_code = json_decode($item->product_code);
                                                                    @endphp
                                                                    @foreach ($pro_code as $code)
                                                                      @php
                                                                         $produt_nm = DB::table('products')->where('product_code' , $code)->first();
                                                                      @endphp
                                                                         {{$produt_nm->product_name}} <br><br>
                                                                    @endforeach
                                                                </td>
                                                                     <td>
                                                                      @php
                                                                          $pric = json_decode($item->product_price);
                                                                      @endphp
                                                                      @foreach ($pric as $pr)
                                                                          {{ $pr }} <br><br><br>
                                                                      @endforeach
                                                                      </td>
                                                                <td>
                                                                    @php
                                                                        $quantity = json_decode($item->product_quantity);
                                                                    @endphp
                                                                    @foreach ($quantity as $qty)
                                                                        x {{ $qty }} <br><br><br>
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                  @php
                                                                  $sum_total += $item->total;
                                                                  @endphp
                                                                  {{ $item->total }}</td>
                                                                <td>
                                                                  @php

                                                                          $balance += $item->payment;

                                                                  @endphp
                                                                  {{ $item->payment }}
                                                                </td>
                                                                <td>
                                                                  @php
                                                                  $total_shipping += $item->shipping_cost;
                                                                  @endphp
                                                                  {{ $item->shipping_cost }}
                                                                </td>
                                                                <td>
                                                                  @php
                                                                  $total_discount += $item->discount;
                                                                  @endphp
                                                                  {{ $item->discount }}
                                                                </td>
                                                                <td>
                                                                      @if($item->previous_due)
                                                                        <span style="color: red;">&#x9F3; {{ number_format($item->previous_due, 2) }}</span>
                                                                      @endif
                                                                      @if($item->previous_advance)
                                                                       <span style="color: green;">&#x9F3;  {{ number_format($item->previous_advance, 2) }}</span>
                                                                      @endif
                                                                </td>
                                                                <td>

                                                                        @if($item->current_due)
                                                                         <span style="color: red;">&#x9F3;  {{ number_format($item->current_due, 2) }}</span>
                                                                        @endif
                                                                        @if($item->current_advance)
                                                                         <span style="color:green;">&#x9F3;  {{ number_format($item->current_advance, 2) }}</span>
                                                                        @endif
                                                                </td>
                                                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $item->order_date }}</span>
                                                                </td>




                                                            </tr>


                                                              @php

                                                                      $sell_r = DB::table('sale_returns')->where('sell' , $item->id)->get();

                                                              @endphp

                                                              @if($sell_r)


                                                                    @forelse ( $sell_r as $sr_item)
                                                                    <tr>
                                                                        <td>R:</td>
                                                                        <td>{{ App\Models\Product::find( App\Models\SaleReturn::find($sr_item->id)->product)->product_name }}</td>
                                                                        <td>{{ $sr_item->product_price }}</td>
                                                                         <td colspan="4">x {{ $sr_item->product_quantity }}</td>
                                                                         <td><span class="text-muted"><i class="fa fa-clock-o"></i>{{ Carbon\Carbon::parse($sr_item->created_at)->format('Y-m-d') }}</span></td>

                                                                    </tr>
                                                                    @empty
                                                                    @endforelse



                                                              @endif

                                                        @empty
                                                        @endforelse

                                                    </tbody>
                                                    <tfoot>
                                                      <th>#</th>
                                                      <th colspan="3">Total:</th>
                                                      <th>{{$sum_total}}</th>
                                                      <th>{{$balance}}</th>
                                                      <th>{{$total_shipping}}</th>
                                                      <th>{{$total_discount}}</th>
                                                      <th colspan="2" class="text-center">
                                                        @if($item->current_due)
                                                         <span style="color: red;">&#x9F3;  {{ number_format($item->current_due, 2) }}</span>
                                                        @endif
                                                        @if($item->current_advance)
                                                         <span style="color:green;">&#x9F3;  {{ number_format($item->current_advance, 2) }}</span>
                                                        @endif
                                                      </th>
                                                      <th></th>
                                                    </tfoot>
                                                </table>
                                              </div>
                                            </div>
                                          </div>

                                            <b>Main Balance: @if(App\Models\Customar::find($customar_info->id)->due) {{App\Models\Customar::find($customar_info->id)->due}}৳ Due @endif
                                                             @if(App\Models\Customar::find($customar_info->id)->advance) {{App\Models\Customar::find($customar_info->id)->advance}}৳ Advance @endif
                                            </b><br>

                                            <button class="btn btn-default pull-right"
                                                onClick="jQuery('#content').print()"><span class="fa fa-print"></span>
                                                Print
                                            </button>
                                            <!--===================================================-->
                                            <!--End Hover Rows-->
                                        </div>





                                        <div class="row" id="content2">
                                            <div class="panel">
                                              <h3 class="panel-title">Due Ledger: </h3>
                                              <div class="panel-body">
                                                <table id="demo-dt-basic" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>SL</th>
                                                            <th >Payment</th>
                                                            <th >Previous due</th>
                                                            <th >Current due</th>

                                                            <th >Note</th>
                                                            <th >Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $balance = 0;
                                                            $total = 0;
                                                        @endphp
                                                        @forelse ($due_ledgers as $item)
                                                            <tr>
                                                                <td>{{ $loop->index + 1 }}</td>

                                                                <td>{{ $item->payment }}</td>
                                                                <td>{{ $item->previous_due }}</td>
                                                                <td>{{ $item->current_due }}</td>

                                                                <td>{{ $item->note }}</td>
                                                                <td>{{ $item->collection_date }}<span class="text-muted"><i class="fa fa-clock-o"></i></span></td>

                                                            </tr>
                                                        @empty
                                                        @endforelse

                                                    </tbody>
                                                </table>
                                              </div>
                                            </div>
                                            <button class="btn btn-default pull-right"
                                                onClick="jQuery('#content2').print()"><span class="fa fa-print"></span>
                                                Print
                                            </button>
                                            <!--===================================================-->
                                            <!--End Hover Rows-->
                                        </div>



                                    </div>
                                </div>
                            </div>
                            <!--===================================================-->
                            <!--End Default Tabs (Left Aligned)-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        // $(document).ready(function() {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [10, 25, 50, -1],
        //             [10, 25, 50, 'All'],
        //         ],
        //     });
        // });
    </script>
@endsection
