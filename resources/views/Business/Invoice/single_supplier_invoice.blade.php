@extends('layouts.user_api.business_app')
@section('content')
@php
    error_reporting(0);
@endphp
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-user"></i> Supplier Details</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Invoice</a> </li>
              <li class="active">Invoice Details</li>
            </ol>
          </div>
				</div>
        <div id="page-content">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="panel">
                        <div class="panel-body np">

                            {{-- <img src="
                            @if ($customar_info->customar_name == '') {{ asset('user_assets/img/av1.png') }}
                                    @else
                                    {{ asset('uploads/customar_photos') }}/{{ $customar_info->customar_photo }} @endif
                            "
                                style="margin: 0 auto; width: 300px; height: 250px" alt="Cover"
                                class="img-responsive"> --}}
                            <img src="{{ asset('user.png') }}" style="margin: 0 auto; width: 300px; height: 250px"
                                alt="Cover" class="img-responsive">
                            <div class="text-center">
                                <!-- panel body -->
                                <h4 class="text-lg text-overflow mar-top">
                                    Name:
                                    @if ($supplier_info->supplier_name == '')
                                        {{ 'Walk In Customar' }}
                                    @else
                                        {{ $supplier_info->supplier_name }}
                                    @endif
                                </h4>
                                <p class="text-sm">
                                    @if ($supplier_info->supplier_name == '')
                                        {{ 'Walk In Customar' }}
                                    @else
                                        {{ 'Fixed Supplier' }}
                                    @endif
                                </p>
                                <!--/ panel body -->
                                <div class="pad-ver">
                                    <b><a href="">Social Links: </a></b><br>
                                    <a title="" href="javascript:void(0)"
                                        class="btn btn-primary btn-icon btn-sm fa fa-facebook icon-sm add-tooltip"
                                        data-original-title="Facebook" data-container="body"></a>
                                    <a title="" href="javascript:void(0)"
                                        class="btn btn-info btn-icon btn-sm fa fa-twitter icon-sm add-tooltip"
                                        data-original-title="Twitter" data-container="body"></a>
                                    <a title="" href="javascript:void(0)"
                                        class="btn btn-danger btn-icon btn-sm fa fa-google-plus icon-sm add-tooltip"
                                        data-original-title="Google+" data-container="body"></a>
                                    <a title="" href="javascript:void(0)"
                                        class="btn btn-warning btn-sm btn-icon fa fa-envelope icon-sm add-tooltip"
                                        data-original-title="Email" data-container="body"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-user"> </i> User Information</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td> Position </td>
                                        <td>
                                            {{ 'Fixed Customar' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Last Logged In </td>
                                        <td>56 min </td>
                                    </tr>
                                    <tr>
                                        <td> Status </td>
                                        <td><span class="label label-sm label-success">Customar</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>-->
                </div>
                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body pad-no">

                            <!--Default Tabs (Left Aligned)-->
                            <!--===================================================-->
                            <div class="tab-base">

                                <!--Nav Tabs-->
                                <ul class="nav nav-tabs">
                                    <li class="active"> <a data-toggle="tab" href="#demo-lft-tab-1"> Invoice </a>
                                    </li>
                                    <li> <a data-toggle="tab" href="#demo-lft-tab-2">Ledger</a> </li>
                                </ul>

                                <!--Tabs Content-->
                                <div class="tab-content">
                                    <div id="demo-lft-tab-1" class="tab-pane fade active in">

                                        <!-- Timeline -->
                                        <!--===================================================-->
                                        <div class="timeline">

                                            <!-- Timeline header -->
                                            {{-- <div class="timeline-header" style="margin-bottom: 15px">
                                                <div class="timeline-header-title bg-info">Total Invoice</div>
                                            </div> --}}
                                            <div class="timeline-entry" style="margin: 0">
                                                <div class="timeline-stat">
                                                    <div class="timeline-icon bg-success"><i
                                                            class="fa fa-check fa-lg"></i> </div>
                                                </div>
                                                <div class="timeline-label">
                                                    <div class="row">
                                                        <div class="panel-body" style="padding-bottom: 0;">

                                                        </div>
                                                        <!--Hover Rows-->
                                                        <!--===================================================-->
                                                        <table id="demo-dt-basic"
                                                            class="table table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>SL.No</th>
                                                                    <th>Invoice</th>
                                                                    <th class="text-center">Value</th>
                                                                    <th>Delivery date</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($fixed_supplier_order as $item)
                                                                    <tr>
                                                                        <td>{{ $loop->index + 1 }}</td>
                                                                        <td>ORD-00{{ $item->id }}</td>
                                                                        {{-- <td>
                                                                        <span class="text-semibold">Desktop</span>
                                                                        <br>
                                                                        <small class="text-muted">Last 7 days :
                                                                            4,234k</small>
                                                                    </td> --}}
                                                                        <td class="text-center">$ {{ $item->total_amount }}
                                                                        </td>
                                                                        <td><span class="text-muted"><i
                                                                                    class="fa fa-clock-o"></i>
                                                                                {{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}</span>
                                                                        </td>
                                                                        <td>
                                                                            <div
                                                                                class="label label-table label-success">
                                                                                <a href="{{ url('/supplier/invoice/view') }}/{{ $item->id }}"
                                                                                    style="color:white">view</a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                @endforelse

                                                            </tbody>
                                                        </table>
                                                        <!--===================================================-->
                                                        <!--End Hover Rows-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--===================================================-->
                                        <!-- End Timeline -->
                                    </div>
                                    <div id="demo-lft-tab-2" class="tab-pane fade">
                                        <div class="row" id="content" style="padding: 0 20px">
                                          <h3>Purchase Ledger: </h3>
                                            <div class="panel-body" style="padding-bottom: 0;">

                                            </div>
                                            <!--Hover Rows-->
                                            <!--===================================================-->
                                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>SL.No</th>
                                                        <th>Item</th>
                                                        <th class="text-center">Amount</th>
                                                        <th class="text-center">Price</th>
                                                        <th class="text-center">Total</th>
                                                        <th class="text-center">Payment</th>
                                                        <th class="text-center">Shipping</th>
                                                        <th class="text-center">Discount</th>
                                                        <th class="text-center">Due_Advance</th>
                                                        <th>Purchase_Date</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($fixed_supplier_order as $item)
                                                        <tr>
                                                            <td>{{ $loop->index + 1 }}</td>
                                                            <td>
                                                                {{ App\Models\Product::find($item->product)->product_name }}
                                                            </td>
                                                            <td>
                                                                x {{ $item->quantity }}
                                                            </td>
                                                            <td>{{ $item->price }}</td>
                                                            <td>{{ $item->total_amount }}</td>
                                                            <td>{{ $item->payment }}</td>
                                                            <td class="text-center"> {{ $item->shipping_cost }}
                                                            </td>
                                                            <td class="text-center"> {{ $item->discount_amount }}
                                                            </td>
                                                            <td>
                                                              @if($item->previous_due)
                                                                Pr.Due {{ $item->previous_due }}  <br>
                                                              @endif
                                                              @if($item->current_due)
                                                               Cr.Due  {{ $item->current_due }}
                                                              @endif
                                                              @if($item->previous_advance)
                                                               Pr.Advance  {{ $item->previous_advance }}  <br>
                                                              @endif
                                                              @if($item->current_advance)
                                                               Cr.Advance  {{ $item->current_advance }}
                                                              @endif
                                                            </td>
                                                            <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-m-Y') }}<span class="text-muted"><i class="fa fa-clock-o"></i>
                                                                    </span>
                                                            </td>

                                                        </tr>



                                                        @php

                                                                $pur_r = DB::table('purchess_returns')->where('purchase' , $item->id)->get();

                                                        @endphp

                                                        @if($pur_r)


                                                              @forelse ( $pur_r as $pr_item)
                                                              <tr>
                                                                  <td></td>
                                                                  <td>{{ App\Models\Product::find( App\Models\ProductPurchase::find($pr_item->purchase)->product)->product_name }}</td>
                                                                   <td><br>Quantity: {{ $pr_item->product_quantity }}</td>
                                                                   <td><br>Price: {{ $pr_item->product_price }}</td>
                                                                   <td><br>Return__Date:<br>{{ Carbon\Carbon::parse($pr_item->created_at)->format('Y-m-d') }}<span class="text-muted"><i class="fa fa-clock-o"></i></span></td>

                                                              </tr>
                                                              @empty
                                                              @endforelse



                                                        @endif


                                                    @empty
                                                    @endforelse

                                                </tbody>
                                            </table>
                                            <b>Main Balance: @if(App\Models\Supplier::find($supplier_info->id)->due) {{App\Models\Supplier::find($supplier_info->id)->due}}৳ Due @endif
                                                             @if(App\Models\Supplier::find($supplier_info->id)->advance) {{App\Models\Supplier::find($supplier_info->id)->advance}}৳ Advance @endif
                                            </b><br>
                                            <b>Total Balances: </b> <span>
                                                {{ $balance }}
                                            <button class="btn btn-default pull-right"
                                                onClick="jQuery('#content').print()"><span class="fa fa-print"></span>
                                                Print
                                            </button>
                                            <!--===================================================-->
                                            <!--End Hover Rows-->
                                        </div>

                                        <div class="row" id="content2" style="padding: 0 20px">
                                            <h3>Due Ledger: </h3>
                                            <div class="panel-body" style="padding-bottom: 0;">

                                            </div>
                                            <!--Hover Rows-->
                                            <!--===================================================-->
                                            <table id="demo-dt-basic" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>SL</th>
                                                        <th >Payment</th>
                                                        <th >Previous due</th>
                                                        <th >Current due</th>

                                                        <th >Note</th>
                                                        <th >Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $balance = 0;
                                                        $total = 0;
                                                    @endphp
                                                    @forelse ($due_ledgers as $item)
                                                        <tr>
                                                            <td>{{ $loop->index + 1 }}</td>

                                                            <td>{{ $item->payment }}</td>
                                                            <td>{{ $item->previous_due }}</td>
                                                            <td>{{ $item->current_due }}</td>

                                                            <td>{{ $item->note }}</td>
                                                            <td>{{ $item->collection_date }}<span class="text-muted"><i class="fa fa-clock-o"></i></span></td>

                                                        </tr>


                                                    @empty
                                                    @endforelse

                                                </tbody>
                                            </table>


                                            <button class="btn btn-default pull-right"
                                                onClick="jQuery('#content2').print()"><span class="fa fa-print"></span>
                                                Print
                                            </button>
                                            <!--===================================================-->
                                            <!--End Hover Rows-->
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!--===================================================-->
                            <!--End Default Tabs (Left Aligned)-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        // $(document).ready(function() {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [10, 25, 50, -1],
        //             [10, 25, 50, 'All'],
        //         ],
        //     });
        // });
    </script>
@endsection
