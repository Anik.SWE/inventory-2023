@extends('layouts.user_api.business_app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">
      <div class="pageheader">
					<h3><i class="fa fa-file"></i> Customer Invoice</h3>
					<div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
              <li> <a href="#"> Invoice</a> </li>
              <li class="active">Customer Invoice </li>
            </ol>
          </div>
				</div>
      <div id="page-content">
          <!-- Basic Data Tables -->
          <!--===================================================-->
          <div class="panel">
              <div class="panel-body">
                  <div class="table-responsive">
                    <table id="demo-dt-basic" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>SL.No</th>
                                <th>Customar Name</th>
                                <th>Purchase Amount</th>
                                <th>Payment</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customar_invoice as $item)
                                @if ($item->customar_id == 'walk_in_customar')
                                    @continue
                                @endif
                                @php
                                    $purchese = DB::table('orders')
                                        ->where('user_id', Auth::id())
                                        ->where('customar_id', $item->customar_id)
                                        ->sum('total');
                                    $pay_amount = DB::table('orders')
                                        ->where('user_id', Auth::id())
                                        ->where('customar_id', $item->customar_id)
                                        ->sum('payment');
                                @endphp
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>

                                    <td>
                                        @if ($item->customar_id == 'walk_in_customar')
                                            {{ $item->customar_id }}
                                        @else
                                            {{ App\Models\Customar::find($item->customar_id)->customar_name }}
                                        @endif
                                    </td>
                                    <td>
                                        Total :
                                        {{ $purchese }}
                                    </td>
                                    <td style="align-items: center">
                                        Total :
                                        {{ $pay_amount }}
                                    </td>
                                    <td>
                                        <div class="label label-table label-success"><a
                                                href="{{ url('/single/customar/invoice/view') }}/{{ $item->customar_id }}"
                                                style="color: #fff">View</a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                  </div>
              </div>
          </div>
      </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });


        // $(document).ready(function () {
        //     $('#demo-dt-basic').DataTable({
        //         lengthMenu: [
        //             [0, 25, 50, -1],
        //             [0, 25, 50, 'All'],
        //         ],
        //     });
        // });





        $(document).ready(function() {
            $('#my_radio_box').change(function() {
                var id = $(this).val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '{{ url('view/account/number/onajax') }}',
                    // data: 'due_id='+due_id+'&_token={{ csrf_token() }}',
                    data: {
                        id: id
                    },
                    success: function(result) {
                        $('#sub_id').val(result);
                    }
                });
            });
        });
    </script>
@endsection
