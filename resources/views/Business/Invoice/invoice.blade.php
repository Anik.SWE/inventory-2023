@extends('layouts.user_api.business_app')
@section('content')
    <div id="content-container">
      <div class="pageheader">
        <h3><i class="fa fa-credit-card"></i> Invoice </h3>
        <div class="breadcrumb-wrapper">
          <span class="label">You are here:</span>
          <ol class="breadcrumb">
            <li> <a href="#"> Invoice List </a> </li>
            <li class="active">Invoice </li>
          </ol>
        </div>
      </div>
      <script>
                  function printPageArea(areaID) {
                      var printContent = document.getElementById(areaID).innerHTML;
                      var originalContent = document.body.innerHTML;
                      document.body.innerHTML = printContent;
                      window.print();
                      document.body.innerHTML = originalContent;
                  }
              </script>
        <div id="page-content">
            <div class="invoice-wrapper">
                <section class="invoice-container">
                  <div class="invoice-inner">
                    <div class="panel">
                            <div class="panel-body" id="printableArea">
                              <div class="row">
                                <div class="col-md-12">
                                  <div style=" text-align: center;">
                                    <h2>{{ App\Models\Business::find(Session::get('business_id'))->business_name }}</h2>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="pull-left">
                                    <h5 style=" padding-left: 10px;" > Date: {{ $order_details->order_date }}</h5>
                                  </div>
                                  <div class="pull-right">
                                    <h5 style="padding-right: 10px;" >{{ $order_details->order_id }}</h5>
                                  </div>
                                </div>
                              </div>
                              <table width="100%" style=" border: 1px solid #1F4E79;">
                                <thead>
                                  <tr style="background-color: #44AFBB !important; -webkit-print-color-adjust: exact;">
                                    <th width="50%" style="padding: 5px; background-color: #44AFBB; border: 1px solid #1F4E79; font-size: 14px;">Biller Information</th>
                                    <th width="50%" style="padding: 5px; background-color: #44AFBB; border: 1px solid #1F4E79; font-size: 14px;">Bill For</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td style=" border: 1px solid #1F4E79;">
                                      <table width="100%">
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Address :</b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;">  {{ App\Models\Business::find(Session::get('business_id'))->business_location }} </td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Zip Code :</b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;">{{ App\Models\Business::find(Session::get('business_id'))->zip }}</td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Email: </b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;">{{ App\Models\Business::find(Session::get('business_id'))->business_email }}</td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Phone: </b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;">{{ App\Models\Business::find(Session::get('business_id'))->city }}</td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td style=" border: 1px solid #1F4E79;">
                                      <table width="100%">
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Customer : </b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;">{{ App\Models\Customar::find($order_details->customar_id)->customar_name }} </td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Address :</b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"> {{ App\Models\Customar::find($order_details->customar_id)->address }}</td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Phone :</b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"> {{ App\Models\Customar::find($order_details->customar_id)->phone_number }}</td>
                                        </tr>
                                        <tr>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"><b>Email :</b></td>
                                          <td style="text-align:left; padding: 5px; vertical-align: text-top; font-size: 14px;"> {{ App\Models\Customar::find($order_details->customar_id)->customar_email }}</td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <br>
                              <table width="100%" style=" border: 1px solid #1F4E79;">
                                <thead>
                                  <tr style="background-color: #44AFBB !important; -webkit-print-color-adjust: exact;">
                                    <th style=" font-size: 14px; padding: 5px; background-color: #44AFBB !important; border: 1px solid #1F4E79; text-align: center;">Sl</th>
                                    <th style=" font-size: 14px; padding: 5px; background-color: #44AFBB !important; border: 1px solid #1F4E79; text-align: left;">Item</th>
                                    <th style=" font-size: 14px; padding: 5px; background-color: #44AFBB !important; border: 1px solid #1F4E79; text-align: center;">Unit Price</th>
                                    <th style=" font-size: 14px; padding: 5px; background-color: #44AFBB !important; border: 1px solid #1F4E79; text-align: center;">Qty.</th>
                                    <th style=" font-size: 14px; padding: 5px; background-color: #44AFBB !important; border: 1px solid #1F4E79; text-align: right;">Line Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @php

                                        $product_code = json_decode($order_details->product_code);
                                        @endphp
                                      @foreach ($product_code as $item)
                                  <tr>
                                      <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: center;">{{ $loop->iteration }}</td>
                                      <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: left;">{{ DB::table('products')->where('product_code' , $item)->value('product_name') }}</td>
                                      <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: center;">
                                        @php
                                                                        $Product_Price = json_decode($order_details->product_price);
                                                                    @endphp
                                                                     {{ number_format($Product_Price[$loop->index ], 2) }}</td>
                                      <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: center;">
                                        @php
                                                                        $Product_Qty = json_decode($order_details->product_quantity);
                                                                    @endphp
                                                                x {{  $Product_Qty[$loop->index ]   }}</td>
                                      <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;">
                                        @php
                                                                      $Sub_Price = json_decode($order_details->sub_price);
                                                                  @endphp
                                                                 {{ number_format($Sub_Price[$loop->index ], 2) }}</td>
                                  </tr>
                            @endforeach
                              <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Subtotal &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->sub_total, 2) }}</strong> </td>
                              </tr>
                              <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Tax({{ $order_details->tax }} %)&nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->tax, 2) }}</strong> </td>
                              </tr>
                              <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Shipping &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>
                                  @if($order_details->shipping_cost)
                                  {{ number_format($order_details->shipping_cost, 2) }}
                                  @endif
                                  @if(!$order_details->shipping_cost)
                                  0.00
                                  @endif</strong> </td>
                              </tr>
                              <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Discount &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->discount, 2) }}</strong> </td>
                              </tr>
                              @if($order_details->previous_due)
                              <tr>
                                  <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Previous Due &nbsp;</strong> </td>
                                  <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->previous_due, 2) }}</strong> </td>
                              </tr>
                              @endif
                              @if($order_details->previous_advance)
                            <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Previous Advance &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;">
                                    <strong>{{ number_format($order_details->previous_advance, 2) }}</strong>
                            </td>
                            </tr>
                            @endif
                              <tr style="background-color: #44AFBB !important; -webkit-print-color-adjust: exact;">
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Total &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->total, 2) }}</strong> </td>
                              </tr>
                              <tr>
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Payment &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>{{ number_format($order_details->payment, 2) }}</strong> </td>
                              </tr>
                              @if($order_details->current_due)
                            <tr style=" background-color: #44AFBB !important; -webkit-print-color-adjust: exact;">
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Current Dues &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;">

                                      <strong>{{ number_format($order_details->current_due, 2) }}</strong>

                                </td>
                            </tr>
                            @endif
                            @if($order_details->current_advance)
                            <tr style=" background-color: #44AFBB !important; -webkit-print-color-adjust: exact;">
                                <td colspan="4" style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;"> <strong>Current Advance &nbsp;</strong> </td>
                                <td style=" font-size: 14px; padding: 5px; border: 1px solid #1F4E79; text-align: right;">
                                      <strong>{{ number_format($order_details->current_advance, 2) }}</strong>
                                </td>
                            </tr>
                            @endif
                                </tbody>
                              </table>
                              <div class="row">
                                <div class="col-md-12">
                                  <h4 style="color: #5B9BD5;">Thank you for believing in us!</h4>
                                </div>

                              </div>
                            </div>

                            <div class="text-center no-print">
                        <a class="btn btn-primary btn-xs" onclick="printPageArea('printableArea')">
                            <i class="fa fa-print"></i> Print
                        </a>
                    </div>
                    </div>
              </div>
            </section>
        </div>
    </div>
    <!--===================================================-->
    <!--End page content-->
</div>
@endsection
