@extends('layouts.user_api.business_app')
@section('content')
@foreach ($supplier_invoice as $item )
<div id="content-container">
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="invoice-wrapper">
            <section class="invoice-container">
                <div class="invoice-inner">
                    <div class="row">
                        @php
                            $supplier = App\Models\Supplier::where('id',$item->supplier_id)->get();
                        @endphp
                        @foreach ($supplier as $sup)
                        <div class="col-xs-3">
                            <h3>Invoice</h3>
                        </div>
                        <div class="col-xs-6 text-center">
                            <h3>
                                {{ App\Models\Business::find(Session::get('business_id'))->business_name }}
                            </h3>
                        </div>
                        <div class="col-xs-3 text-right">
                            <h3>
                                ORD-00{{ $item->id }}
                            </h3>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Billed To:</strong><br />
                                {{-- {{ App\Models\Supplier::find($order_details->customar_id) }} <br> --}}
                                Supplier Name: {{ $sup->supplier_name }}
                                    <br>
                                    City: {{ $sup->city }} <br>
                                    Address: {{ $sup->supplier_address }}
                            </address>
                        </div>
                        <div class="col-xs-6 text-right">
                            <address>
                                <strong>{{ Auth::user()->name }}</strong><br />

                                1234 Main<br />
                                Apt. 4B<br />
                                Springfield, ST 54321
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>Payment Method:</strong>
                            <br /> Payment Type:  {{ $item->payment_type }}
                            <br /> <strong>Payment:</strong> {{ $item->payment }}
                        </div>
                        <div class="col-xs-6 text-right">
                            <strong>Order Date:</strong>
                            <br />  Date: {{ $item->created_at }}
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 pad-top">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Order summary</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><strong>Item Name</strong></td>
                                                    <td><strong>Price</strong></td>
                                                    <td><strong>Quantity</strong></td>
                                                    <td><strong>Total</strong></td>
                                                    <td><strong>Shipping Cost</strong></td>
                                                    <td><strong>Subtotal</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        {{ App\Models\Product::find($item->product)->product_name }}
                                                    </td>
                                                    <td>
                                                        {{ $item->price }}
                                                    </td>
                                                    <td>
                                                        {{ $item->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ $item->price * $item->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ $item->shipping_cost }}
                                                    </td>
                                                    <td>
                                                        {{ $item->total_amount }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"></td>
                                                    <td class="thick-line"><strong>Payment</strong></td>
                                                    <td class="thick-line">
                                                        $ {{ $item->payment }}
                                                    </td>
                                                </tr>

                                                @if($item->previous_due)
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"><strong>Previous Due</strong></td>
                                                    <td class="no-line">

                                                       {{ $item->previous_due }}

                                                </td>
                                                </tr>
                                                @endif

                                                @if($item->previous_advance)
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"><strong>Previous Advance</strong></td>
                                                    <td class="no-line">

                                                       {{ $item->previous_advance }}

                                                </td>
                                                </tr>
                                                @endif

                                                @if($item->current_due)
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"><strong>Current Dues</strong></td>
                                                    <td class="no-line">

                                                         {{ $item->current_due }}

                                                    </td>
                                                </tr>
                                                @endif
                                                @if($item->current_advance)
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"></td>
                                                    <td class="no-line"><strong>Current Advance</strong></td>
                                                    <td class="no-line">

                                                         {{ $item->current_advance }}

                                                    </td>
                                                </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center no-print">
                      <a class="btn btn-primary btn-lg" onClick="jQuery('#page-content').print()">
                        <i class="fa fa-print"></i> Print
                      </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--===================================================-->
    <!--End page content-->
</div>
@endforeach
@endforeach

@endsection
