<!DOCTYPE html>
<html lang="en">
<head>
    <title>Stolas - Business Managment Software</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Business Managment Software">
    <meta name="author" content="techlozi">
    <link rel="shortcut icon" href="assets/images/stock.png">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Global CSS -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">

</head>

<body>
    <!-- ******HEADER****** -->
    <header id="header" class="header">
        <div class="container">
            <h1 class="logo">
                <a class="scrollto" href="#hero">
                    <span class="logo-icon-wrapper"><img class="logo-icon" src="assets/images/logo-icon.svg" alt="icon"></span>
                    <span class="text"><span class="highlight">Stolas</a>
            </h1><!--//logo-->
            <nav class="main-nav navbar-expand-md float-right navbar-inverse" role="navigation">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><!--//nav-toggle-->

                <div id="navbar-collapse" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a class="active nav-link scrollto" href="#about">কেন স্টোলাস ?</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="#testimonials">গ্রাহক মন্তব্য</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="#features">ফিচার্স</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="#clients">ক্লায়েন্ট</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="#pricing">প্যাকেজ মূল্য</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="#contact">যোগাযোগ</a></li>
                    </ul><!--//nav-->
                </div><!--//navabr-collapse-->
            </nav><!--//main-nav-->
        </div><!--//container-->
    </header><!--//header-->

    <div id="hero" class="hero-section">

        <div id="hero-carousel" class="hero-carousel carousel carousel-fade slide" data-ride="carousel" data-interval="10000">

            <div class="figure-holder-wrapper">
        		<div class="container">
            		<div class="row justify-content-end">
                		<div class="figure-holder">
                	        <img class="figure-image img-fluid" src="assets/images/imac.png" alt="image" />
                        </div><!--//figure-holder-->
            		</div><!--//row-->
        		</div><!--//container-->
    		</div><!--//figure-holder-wrapper-->

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li class="active" data-slide-to="0" data-target="#hero-carousel"></li>
				<li data-slide-to="1" data-target="#hero-carousel"></li>
				<li data-slide-to="2" data-target="#hero-carousel"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">

				<div class="carousel-item item-1 active">
					<div class="item-content container">
    					<div class="item-content-inner">

				            <h2 class="heading">ব্যবসা পরিচালনার সেরা এবং নির্ভরযোগ্য সফটওয়্যার মানেই <br class="d-none d-md-block">স্টোলাস</h2>
				            <p class="intro">বেচা-কেনা আয়-ব্যয় এর হিসাব চোখের সামনে</p>
                    @php
                        $usr = Auth::guard('admin')->user();
                    @endphp
                    @if (Route::has('login'))
                    <div class="col-md-8">
                      @auth
                      <a class="btn btn-success" href="{{ url('/home') }}">Go To Home</a>
                      @else
                      <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                        @if (Route::has('register'))
                      <a class="btn btn-danger" href="{{ route('register') }}">Register</a>
                      @endif
                  @endauth
                    </div>
                  @endif
    					</div>
					</div>
				</div>

				<div class="carousel-item item-2">
					<div class="item-content container">
						<div class="item-content-inner">

				            <h2 class="heading">সহজে ব্যবসার হিসাব</h2>
				            <p class="intro">বাকি বিক্রি লিখে না রাখলে বা যোগে ভুল হলে ব্যবসার লস হয়। স্টোলাস সফটওয়্যারে হিসাব রাখা খুব সহজ। অটো যোগবিয়োগ হওয়ায়, হিসাব থাকে নির্ভুল। </p>
                    @php
                        $usr = Auth::guard('admin')->user();
                    @endphp
                    @if (Route::has('login'))
                    <div class="col-md-8">
                      @auth
                      <a class="btn btn-success" href="{{ url('/home') }}">Go To Home</a>
                      @else
                      <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                        @if (Route::has('register'))
                      <a class="btn btn-danger" href="{{ route('register') }}">Register</a>
                      @endif
                  @endauth
                    </div>
                  @endif

    					</div>
					</div>
				</div>

				<div class="carousel-item item-3">
					<div class="item-content container">
						<div class="item-content-inner">
				            <h2 class="heading">ব্যবসায়ীর পণ্যের স্টক এবং দেনা পাওনার হিসাব রাখা ও পেমেন্ট এর সহজ সমাধান</h2>
				            <p class="intro">স্টোলাস - বিজনেস ইনভেন্টরি ম্যানেজমেন্ট সফটওয়্যার</p>
                    @php
                        $usr = Auth::guard('admin')->user();
                    @endphp
                    @if (Route::has('login'))
                    <div class="col-md-8">
                      @auth
                      <a class="btn btn-success" href="{{ url('/home') }}">Go To Home</a>
                      @else
                      <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                        @if (Route::has('register'))
                      <a class="btn btn-danger" href="{{ route('register') }}">Register</a>
                      @endif
                  @endauth
                    </div>
                  @endif

    					</div>
					</div>
				</div>
			</div>

		</div>
    </div>

    <div id="about" class="about-section">
        <div class="container text-center">
            <h2 class="section-title">ব্যবসার জন্য ইনভেন্টরি সফটওয়্যার কেন দরকার ...?</h2>
            <p class="intro" style="text-align: justify;text-justify: inter-word;">ব্যবসার হিসাব-নিকাশ রক্ষার জন্য রেজিস্টার বুক, ওয়ার্ক অর্ডার বুক, ইনভয়েস বুক, বা অন্যান্য হিসাবের খাতা নিয়ে আর মূল্যবান সময় নষ্ট করার দরকার নেই।
আপনার সব হিসাব-নিকাশ রক্ষা করবে সফটওয়্যার। সুতরাং হাতে-কলমে কাজ করতে গিয়ে আপনার যে ভুল হওয়ার আশংকা ছিল, সফটওয়্যার ব্যবহারে তা আর থাকছে না।
সামান্য ক্লিক এর মাধ্যমে মুহূর্তেই যে কোন তথ্য বা রিপোর্ট (সেলস রিপোর্ট, স্টক রিপোর্ট, ইত্যাদি) আপনার সামনে চলে আসবে।
আপনি যে কোন ক্যাটাগরি অনুসারে আপনার ব্যবসার রিপোর্ট গুলো জানতে পারবেন। কোন প্রোডাক্ট গুলোর এক্সপায়ারি ডেট দ্রুত শেষ হবে তাও আপনি জানতে পারবেন। কাজেই আপনি সিদ্ধান্ত নিতে পারবেন, আপনার কোন প্রোডাক্ট এর স্টক বাড়ানো উচিৎ আর কোন প্রোডাক্ট দ্রুত বিক্রি করে দেয়া উচিৎ।
আপনার ব্যবসার গুরুত্বপূর্ণ ডকুমেন্ট গুলো হারিয়ে যাওয়ার কোন ভয় থাকছে না। সবকিছু জমা থাকবে সফটওয়্যারে। চাহিদামত সার্চ দিয়ে মুহূর্তেই বের করে নিন প্রয়োজনীয় ডকুমেন্ট।
কত টাকা খরচ করতেছেন।
টাকা কি কি খাতে খরচ হচ্ছে।
ব্যবসায়ীক সফটওয়্যার ব্যবহার এর ফলে আপনি যেমন এই ব্যাপারগুলো পর্যবেক্ষণ করতে পারবেন তেমনি পাশাপাশি আপনি আপনার কর্মপরিকল্পনা ঠিক করতে পারবেন।</p>

            <div class="items-wrapper row">
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/wholesale.png" alt="image"><br><br>
                            <h3 class="item-title">পাইকারি হিসাব</h3>
                            <div class="item-desc">
                                পাইকারি বাজার থেকে বিভিন্ন পণ্য নিয়ে তাও অনলাইন স্টোরের মাধ্যমে বিক্রি করতে পারবেন কাস্টমারদের কাছে।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/bechabikri.png" alt="image"><br><br>
                            <h3 class="item-title">বেচা–বিক্রি</h3>
                            <div class="item-desc">
                                আপনার ব্যবসাকে ডিজিটালাইজ করা এবং ব্যবসায়ের বৃদ্ধি ও অন্য সকল পরিস্থিতি সঠিকভাবে বুঝতে ব্যবসায়ের সার্বক্ষনিক নিয়ন্ত্রণ রাখার জন্য ব্যবহার করুন বেচা-বিক্রি ফিচার।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/bakir-khata.png" alt="image"><br><br>
                            <h3 class="item-title">বাকীর খাতা</h3>
                            <div class="item-desc">
                                ব্যবসায়ের সকল বকেয়া হিসাব এক জায়গায় লিপিবদ্ধ রাখতে ও দেখতে ব্যবহার করুন বাকীর খাতা ফিচার।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/stock.png" alt="image"><br><br>
                            <h3 class="item-title">পণ্যের স্টক</h3>
                            <div class="item-desc">
                                আপনার ব্যবসায়ের সকল পণ্যের হিসাব এক জায়গায় লিপিবদ্ধ রাখতে ব্যবহার করুন স্টক ফিচার। স্টকে পণ্যের নাম, পণ্যের বর্ণনা, কয়টি পণ্য আছে, এসবের এন্ট্রি দিন। বিক্রির পর স্টক থেকেই পেয়ে যাবেন পণ্যের লাইভ আপডেট
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="items-wrapper row">
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/sales.png" alt="image"><br><br>
                            <h3 class="item-title">বিক্রির খাতা</h3>
                            <div class="item-desc">
                                আপনার ব্যবসায়ের সকল বিক্রয়ের এন্ট্রির তালকা দেখতে ব্যবহার করুন বিক্রির খাতা ফিচার, যেখান থেকে আপনার কাস্টমারের কাছে বিক্রয়কৃত পণ্য বা সেলসের হিস্টোরি দেখতে পারবেন।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/customer.png" alt="image"><br><br>
                            <h3 class="item-title">গ্রাহক তালিকা</h3>
                            <div class="item-desc">
                                ব্যবসা পরিচালনার জন্য আপনার সবচেয়ে ভাল কাস্টমার কারা, আপনার মোট কতজন কাস্টমার আছেন, এদের মধ্যে কারা নিয়মিত আপনার থেকে পণ্য ক্রয় করেন, এসকল বিষয় জানা খুব জরুরি।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/report.png" alt="image"><br><br>
                            <h3 class="item-title">রিপোর্ট</h3>
                            <div class="item-desc">
                                ব্যবসায়ের যে কোন ছোট বড় সিদ্ধান্ত নিতে হলে ব্যবসায়ের সার্বিক অবস্থার ধারনা থাকা খুব প্রয়োজন। তাই ব্যবসায়ের যে কোন রিপোর্ট এক জায়গায় দেখার উদ্দ্যেশ্যে রিপোর্ট ফিচারটি সংযোজন করা হয়েছে।
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/images/bar-graph.png" alt="image"><br><br>
                            <h3 class="item-title">ড্যাশবোর্ড</h3>
                            <div class="item-desc">
                                আপনার ব্যবসায়ের নির্ভুল ও কুইক ডিসিশান নেওয়ার জন্য আমরা সংযোজন করেছি ড্যাশবোর্ড ফিচারটির, যেখান থেকে আপনি খুব সহজেই আপনার ব্যবসায়ের ভাল-মন্দের সিদ্ধান্ত নিতে পারবেন।
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="testimonials" class="testimonials-section">
        <div class="container">
            <h2 class="section-title text-center">স্টোলাস ব্যবহারকারীদের অভিজ্ঞতা এবং মন্তব্য</h2>
            <div class="item mx-auto">
                <div class="profile-holder">
                    <img class="profile-image" src="assets/images/profile-1.png" alt="profile">
                </div>
                <div class="quote-holder">
                    <blockquote class="quote">
                        <p>আগে দোকানে প্রোডাক্টের স্টক মিলাতে অনেক ঝামেলা হতো, এখন স্টোলাস এর মাদ্ধমে
                          সেল করে অটোমেটেড ইনভয়েস পাচ্ছি, এবং স্টক মিলানোর কোনো ঝামেলা থাকছে না</p>
                        <div class="quote-source">
                            <!-- <span class="name">@JohnK,</span>
                            <span class="meta">San Francisco</span> -->
                        </div><!--//quote-source-->
                    </blockquote>
                </div><!--//quote-holder-->
            </div><!--//item-->
            <div class="item item-reversed mx-auto">
                <div class="profile-holder">
                    <img class="profile-image" src="assets/images/profile-2.png" alt="profile">
                </div>
                <div class="quote-holder">
                    <blockquote class="quote">
                        <p>আমার আলাদা আলাদা বিসনেস এর জন্য আলাদা সফটওয়্যার ব্যবহার করতে হতো, এখন  আলাদা আলাদা বিসনেস এর হিসাব রাখি এক সফটওয়্যারে, এতে সময় এবং অর্থ দুইই বাঁচলো </p>
                        <div class="quote-source">
                            <!-- <span class="name">@LisaWhite,</span>
                            <span class="meta">London</span> -->
                        </div>
                    </blockquote>
                </div>
            </div>
            <div class="item mx-auto">
                <div class="profile-holder">
                    <img class="profile-image" src="assets/images/profile-3.png" alt="profile">
                </div>
                <div class="quote-holder">
                    <blockquote class="quote">
                        <p>একজন কাস্টমার এর বাকির হিসাব মিলাতে সময় লাগলো কয়েকদিন, এখন সকল কাস্টমারের বাকির হিসাব কয়েক মিনিটেই মিলিয়ে নিতে পারি</p>
                        <div class="quote-source">
                            <!-- <span class="name">@MattH,</span>
                            <span class="meta">Berlin</span> -->
                        </div>
                    </blockquote>
                </div>
            </div>
            <div class="item item-reversed mx-auto">
                <div class="profile-holder">
                    <img class="profile-image" src="assets/images/profile-4.png" alt="profile">
                </div>
                <div class="quote-holder">
                    <blockquote class="quote">
                        <p>সারাদিন এর বেচা - বিক্রি শেষে ব্যাংক এবং ক্যাশ মিলাতে হিমশিম খেয়ে যেতাম, এখন বেচা কিংবা কেনা পোস্টিং এর সাথে সাথে ব্যাংক কিংবা ক্যাশ সবই মিলে যাচ্ছে অটোমেটিক ভাবে</p>
                         <div class="quote-source">
                            <!-- <span class="name">@RyanW,</span>
                            <span class="meta">Paris</span> -->
                        </div>
                    </blockquote>

                </div>
            </div>
        </div>
      </div>

    <div id="features" class="features-section">
        <div class="container text-center">
            <h2 class="section-title">স্টোলাস এর ফিচার্স সমূহ</h2>
            <p class="intro">You can use this section to list your product features. The screenshots used here were taken from <a href="#" target="_blank">Bootstrap 4 admin theme Appify</a></p>

            <div class="tabbed-area row">


                <div class="feature-nav nav nav-pill flex-column col-lg-4 col-md-6 col-12 order-0 order-md-1" role="tablist" aria-orientation="vertical">
                     <a class="nav-link active mb-lg-3" href="#feature-1" aria-controls="feature-1" data-toggle="pill" role="tab" aria-selected="true"><i class="fas fa-magic mr-2"></i>20+ Use Case-based App Pages</a>
                     <a class="nav-link mb-lg-3" href="#feature-2" aria-controls="feature-2" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-cubes mr-2"></i>100+ Components and Widgets</a>
                     <a class="nav-link mb-lg-3" href="#feature-3" aria-controls="feature-3" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-chart-bar mr-2"></i>Useful Chart Libraries</a>
                     <a class="nav-link mb-lg-3" href="#feature-4" aria-controls="feature-4" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-code mr-2"></i>Valid HTML5 + CSS3</a>
                     <a class="nav-link mb-lg-3" href="#feature-5" aria-controls="feature-5" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-rocket mr-2"></i>Built on Bootstrap 4 &amp; SCSS</a>
                     <a class="nav-link mb-lg-3" href="#feature-6" aria-controls="feature-6" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-mobile-alt mr-2"></i>Fully Responsive</a>
                     <a class="nav-link mb-lg-3" href="#feature-7" aria-controls="feature-7" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-star mr-2"></i>Beautiful UI</a>
                     <a class="nav-link mb-lg-3" href="#feature-8" aria-controls="feature-8" data-toggle="pill" role="tab" aria-selected="false"><i class="fas fa-heart mr-2"></i>Free Updates &amp; Support</a>
                </div>


                <div class="feature-content tab-content col-lg-8 col-md-6 col-12 order-1 order-md-0">
                    <div role="tabpanel" class="tab-pane fade show active" id="feature-1">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-1.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-2">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-2.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-3">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-3.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-4">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-4.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-5">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-5.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-6">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-6.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-7">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-7.png" alt="screenshot" ></a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="feature-8">
                        <a href="#" target="_blank"><img class="img-fluid" src="assets/images/feature-8.png" alt="screenshot" ></a>
                    </div>

                </div>


            </div>

        </div>
    </div>
    <hr>
    <div class="team-section" id="clients">
        <div class="container text-center">
            <h2 class="section-title">আমাদের সম্মানিত গ্রাহকগণ</h2>
            <div class="story">
                <p>আমাদের উপর আস্থা রেখে স্টলস এর সাথে যারা দীর্ঘদিন যাবৎ পথ চলছেন</p>
            </div>
            <div class="items-wrapper row">
              <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                  <div class="item-inner">
                      <div class="figure-holder">
                          <img class="figure-image" src="assets/clients/download.png" alt="image" width="100px" height="100px"><br><br>
                          <h3 class="item-title">ভাই ভাই সীডস্</h3>
                      </div>
                  </div>
              </div>
              <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                  <div class="item-inner">
                      <div class="figure-holder">
                          <img class="figure-image" src="assets/clients/download.jpg" alt="image" width="100px" height="100px"><br><br>
                          <h3 class="item-title">এম আর ট্রেডার্স</h3>
                      </div>
                  </div>
              </div>
              <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                  <div class="item-inner">
                      <div class="figure-holder">
                          <img class="figure-image" src="assets/clients/huzurishah.jpg" alt="image" width="100px" height="100px"><br><br>
                          <h3 class="item-title">হুজুরী শাহ আই কেয়ার</h3>
                      </div>
                  </div>
              </div>
                <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/clients/ex_logo.png" alt="image" width="100px" height="100px"><br><br>
                            <h3 class="item-title">এক্সেকিউটিভ ফুটওয়ার</h3>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/clients/3543.jpeg" alt="image" width="100px" height="100px"><br><br>
                            <h3 class="item-title">অপরূপ ফ্যাশন</h3>
                        </div>
                    </div>
                </div>
                <div class="item col-md-3 col-12 col-xs-6" style="padding: 5px;">
                    <div class="item-inner">
                        <div class="figure-holder">
                            <img class="figure-image" src="assets/clients/sazzad_motors.png" alt="image" width="100px" height="100px"><br><br>
                            <h3 class="item-title">সাজ্জাদ মটরস</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div id="pricing" class="pricing-section">
        <div class="container text-center">
            <h2 class="section-title">স্টোলাস প্যাকেজ মূল্য</h2>
            <div class="intro">স্টোলাস এর ভবিষ্যত আপডেট সমূহ বিদ্যমান গ্রাহকদের জন্য 100% বিনামূল্যে</div>
            <div class="pricing-wrapper row">
                <div class="item item-1 col-md-4 col-12">
                    <div class="item-inner">
                        <h3 class="item-heading">Basic<br><span class="item-heading-desc">(1 Business)</span></h3>
                        <div class="price-figure">
                            <span class="currency">৳</span><span class="number">15,000/-</span>
                        </div><!--//price-figure-->
                        <ul class="list-unstyled mb-3">
	                        <li class="mb-2"><i class="fas fa-check"></i> Single installation</li>
	                        <li class="mb-2"><i class="fas fa-check"></i> Multiple installations</li>
                            <li class="mb-2"><i class="fas fa-times"></i> Use without attribution link</li>
                        </ul>
                        <div class="mb-3"><a href="#" target="_blank">License Details</a></div>
                        <a class="btn btn-cta" href="#contact">Get it now</a>

                    </div><!--//item-inner-->
                </div><!--//item-->
                <div class="item item-2 col-md-4 col-12">
                    <div class="item-inner">
                        <h3 class="item-heading">Profesional<br><span class="item-heading-desc">(2 Business)</span></h3>

                        <div class="price-figure">
                            <span class="currency">৳</span><span class="number">20,000/-</span>
                        </div><!--//price-figure-->
                        <ul class="list-unstyled mb-3">
	                        <li class="mb-2"><i class="fas fa-check"></i> Single installation</li>
	                        <li class="mb-2"><i class="fas fa-times"></i> Multiple installations</li>
	                        <li class="mb-2"><i class="fas fa-check"></i> Use without attribution link</li>
                        </ul>
                        <div class="mb-3"><a href="#" target="_blank">License Details</a></div>
                        <a class="btn btn-cta" href="#contact">Get it now</a>

                    </div><!--//item-inner-->
                </div><!--//item-->

                <div class="item item-3 col-md-4 col-12">
                    <div class="item-inner">
                        <h3 class="item-heading">Premium<br><span class="item-heading-desc">(3 Business)</span></h3>
                        <div class="price-figure">
                            <span class="currency">৳</span><span class="number">35,000/-</span>
                        </div><!--//price-figure-->
                        <ul class="list-unstyled mb-3">
	                        <li class="mb-2"><i class="fas fa-check"></i> Single installation</li>
	                        <li class="mb-2"><i class="fas fa-check"></i> Multiple installations</li>
	                        <li class="mb-2"><i class="fas fa-check"></i> Use without attribution link</li>
                        </ul>
                        <div class="mb-3"><a href="#" target="_blank">License Details</a></div>
                        <a class="btn btn-cta" href="#contact">Get it now</a>

                    </div>
                </div>
            </div><!--//pricing-wrapper-->

        </div><!--//container-->
    </div><!--//pricing-section-->
    <div id="contact" class="contact-section">
        <div class="container text-center">
            <h2 class="section-title">যোগাযোগ করুন (২৪/৭)</h2> <hr>
            <div class="contact-content">
              <address>
                ঠিকানা: হাউজ নং: ২০৯, আশকোনা মেডিকেল রোড, ঢাকা – ১২৩০
              </address>
              মোবাইল: +৮৮ ০১৩০৬-৫৩৫৩৪২ <br> ই-মেইল: techlozibd@gmail.com <br> ওয়েবসাইট: www.techlozi.com <br>
              ফেইসবুক পেইজ: www.facebook.com/techlozi

            </div>
            <!-- <a class="btn btn-cta btn-primary" href="#">Get in Touch</a> -->

        </div><!--//container-->
    </div><!--//contact-section-->

    <footer class="footer text-center">
        <div class="container">
            <h6 class="copyright">Powered by </i> by <a href="https://techlozi.com/" target="_blank">techlozi</a></h6>


        </div><!--//container-->
    </footer>

    <!-- Javascript -->
    <script type="text/javascript" src="assets/plugins/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>
