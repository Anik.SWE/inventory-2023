<?php
namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'mohammad.naeemislam.info@gmail.com')->first();
        if (is_null($user)) {
            $user = new User();
            $user->name = "Naeem Islam";
            $user->email = "mohammad.naeemislam.info@gmail.com";
            $user->password = Hash::make('naeem12345');
            $user->save();
        }
    }
}
