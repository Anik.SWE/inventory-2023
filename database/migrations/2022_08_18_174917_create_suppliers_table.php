<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('supplier_name');
            $table->string('supplier_email')->unique();
            $table->string('phone_number')->unique();
            $table->string('supplier_type');
            $table->string('supplier_shop_name');
            $table->string('supplier_address');
            $table->string('account_holder');
            $table->string('account_number');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->string('city');
            // $table->string('supplier_photo')->default('supplier_photo.jpg');

            $table->double('due')->nullable();
            $table->double('advance')->nullable();
            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
