<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
         

            $table->boolean('suparadmin')->default(0);


            $table->boolean('businessadmin')->default(0);
            $table->integer('max_business')->nullable();
            $table->string('profileimage')->nullable();
            $table->string('city')->nullable();
            $table->string('gender')->nullable();
            $table->string('experience')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('nid')->nullable();
            
            $table->string('business')->nullable();
            $table->boolean('businessmanager')->default(0);
            $table->string('managertype')->nullable();
            $table->boolean('businessemployee')->default(0);
            $table->string('employeetype')->nullable();

            $table->boolean('account')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
