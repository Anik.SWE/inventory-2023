<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_id');
            $table->string('order_date');


            $table->bigInteger('customar_id')->nullable()->unsigned();
            $table->foreign('customar_id')->references('id')->on('customars');

           
            $table->string('product_code');
            $table->string('product_quantity');
            $table->string('product_price');
            $table->string('sub_price');
            $table->float('tax');
            $table->float('shipping_cost')->nullable();
            $table->float('previous_due')->nullable();
            $table->float('current_due')->nullable();
            $table->float('previous_advance')->nullable();
            $table->float('current_advance')->nullable();
            $table->float('discount');
            $table->double('total');
            $table->string('payment_type');
            $table->double('payment');
            $table->double('sub_total');
            $table->double('change');
            $table->string('status')->default(1);
            $table->string('currency')->default("BDT");
            // $table->string('transaction_id');

            $table->bigInteger('product')->nullable()->unsigned();
            $table->foreign('product')->references('id')->on('products');
            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
