<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalAccountTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_account_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_date')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('note')->nullable();
            $table->double('amount')->nullable();
            $table->foreignId('main_head_id')->nullable();
            $table->foreignId('sub_head_id')->nullable();
            $table->foreignId('bank_id')->nullable();
            $table->bigInteger('business_id')->nullable()->unsigned();
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_account_transactions');
    }
}
