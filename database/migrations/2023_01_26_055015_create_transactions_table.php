<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            
            $table->string('bank_name');
            $table->string('operation_type');
            $table->string('account_number');
            $table->double('balance');
            $table->string('media');
            $table->string('transaction_id')->unique();
            $table->string('transaction_date');

            $table->bigInteger('bank')->nullable()->unsigned();
            $table->foreign('bank')->references('id')->on('banks');

            $table->bigInteger('purchase')->nullable()->unsigned();
            $table->foreign('purchase')->references('id')->on('product_purchases');
            $table->bigInteger('purchase_return')->nullable()->unsigned();
            $table->foreign('purchase_return')->references('id')->on('purchess_returns');

            $table->bigInteger('sell')->nullable()->unsigned();
            $table->foreign('sell')->references('id')->on('orders');
            $table->bigInteger('sell_return')->nullable()->unsigned();
            $table->foreign('sell_return')->references('id')->on('sale_returns');

            $table->bigInteger('due')->nullable()->unsigned();
            $table->foreign('due')->references('id')->on('dues');

            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
