<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomarLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customar_ledgers', function (Blueprint $table) {
            $table->id();
            $table->string('product_code');
            $table->string('product_quantity');
            $table->string('product_price');

            $table->bigInteger('customar_id')->nullable()->unsigned();
            $table->foreign('customar_id')->references('id')->on('customars'); 

            $table->float('discount');
            $table->float('payment');
            $table->float('change');
            $table->float('total');

            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customar_ledgers');
    }
}
