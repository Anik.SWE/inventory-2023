<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSMSConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_m_s_configs', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();
            $table->string('business_id')->nullable();
            $table->string('api_key')->nullable();
            $table->string('secretkey')->nullable();
            $table->string('caller_id')->nullable();
            $table->string('url')->nullable();
            $table->string('sms_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_m_s_configs');
    }
}
