<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dues', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customar_id')->nullable()->unsigned();
            $table->foreign('customar_id')->references('id')->on('customars');
            $table->bigInteger('supplier_id')->nullable()->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            
            $table->float('payment');
            $table->float('previous_due')->nullable();
            $table->float('current_due')->nullable();
            $table->string('operation_type');
            $table->string('note');
            $table->string('collection_date');
           


            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dues');
    }
}
