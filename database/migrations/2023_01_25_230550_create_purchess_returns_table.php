<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchessReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchess_returns', function (Blueprint $table) {
            $table->id();


            $table->integer('product_quantity');
            $table->float('product_price');


            $table->bigInteger('purchase')->nullable()->unsigned();
            $table->foreign('purchase')->references('id')->on('product_purchases'); 

            $table->double('total_amount');
            $table->bigInteger('bank')->nullable()->unsigned();
            $table->foreign('bank')->references('id')->on('banks');
            $table->double('payment');
            $table->double('change');
            
            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchess_returns');
    }
}
