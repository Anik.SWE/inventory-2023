<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_purchases', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('product')->nullable()->unsigned();
            $table->foreign('product')->references('id')->on('products');
            $table->integer('quantity');
            $table->double('price');
            $table->float('shipping_cost')->nullable();
            $table->float('discount_amount')->nullable();
            $table->float('discount_parsent')->nullable();
            $table->double('total_amount');
            

            $table->float('previous_due')->nullable();
            $table->float('current_due')->nullable();
            $table->float('previous_advance')->nullable();
            $table->float('current_advance')->nullable();

            $table->string('product_buying_date');
            $table->string('product_expire_date');           
            $table->string('payment_type');
            $table->double('payment');
            $table->double('change');
            
            $table->bigInteger('supplier_id')->nullable()->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers');

            $table->bigInteger('business')->nullable()->unsigned();
            $table->foreign('business')->references('id')->on('businesses'); 
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_purchases');
    }
}
